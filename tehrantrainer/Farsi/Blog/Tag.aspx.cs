﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Farsi_Blog_Tag : System.Web.UI.Page
{
    protected void Page_Init(object sender, EventArgs e)
    {
        string TagString = "1";
        if (Page.RouteData.Values["tag"] != null)
        {
            TagString = Page.RouteData.Values["tag"].ToString().Trim();
        }
        else if (Request.QueryString["tag"] != null)
        {
            TagString = Request.QueryString["tag"].ToString().Trim();
        }
        TagLabel.Text = TagString.Replace("_", " ").Replace("-", " ");
        H1Label.Text = TagString.Replace("-","_");
    }
    
}