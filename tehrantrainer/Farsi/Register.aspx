﻿<%@ Page Title="" Language="C#" MasterPageFile="~/farsi/FaMasterPage.master" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Farsi_Register" %>

<%@ Register Assembly="PersianDateControls 2.0" Namespace="PersianDateControls" TagPrefix="pdc" %>
<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContentPlaceHolder" runat="Server">
    <link href="<%=SSClass.WebDomainFile %>/css/progress-wizard.min.css" rel="stylesheet" />
    <style>
        h2 {
            font-size: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="BodyContentPlaceHolder" runat="Server">
    <div class="container-fluid ">
        <div class="container -white">
            <div style="margin-top: 40px">
                <div class="row" runat="server" id="StepBarDiv" visible="false">
                    <div class="col-md-12 BYekan" style="font-size: large; font-weight: 700!important">
                        <ul class="progress-indicator custom-complex">
                            <li class="completed">
                                <span class="bubble "></span>
                                بررسی سبد
                            </li>
                            <li class="active">
                                <span class="bubble"></span>
                                <h1 style="font-size: 15px; margin-top: 0px">ورود به تهران آموز</h1>
                            </li>
                            <li class="">
                                <span class="bubble"></span>
                                آدرس و ارسال
                            </li>
                            <li class="">
                                <span class="bubble"></span>
                                پرداخت
                            </li>
                            <%--<li>
                        <span class="bubble"></span>
                        پایان
                        </li>--%>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 BYekan">
                    <div runat="server" id="InfoDiv" class="alert alert-info alert-dismissable text-right" visible="false">
                        <span runat="server" id="InfoSpan" class="glyphicon glyphicon-remove"></span>
                        <asp:Literal ID="InfoLiteral" runat="server" Text=""></asp:Literal>
                    </div>
                </div>
            </div>
            <div class="row" runat="server" id="ContactUsDiv">
                <div class="col-md-12 BYekan text-center CallOrder">
                    <h5><span class="glyphicon glyphicon-phone-alt" style="margin-left: 5px; font-size: large"></span>در صورت نیاز می توانید به صورت تلفنی سفارش خود را ثبت کنید. تلفن: <%=SSClass.PhoneNo %></h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="row">

                        <div class="col-md-12 text-center">
                            <br />
                            <div style="font-size: small;padding:15px" class="nice-shadow">
                                <asp:Panel ID="Panel2" runat="server" DefaultButton="RegisterButton">
                                    <div style="" class="form-group RegFrame">
                                        <div class="text-right BYekan">
                                            <h2>کاربر جدید هستم:</h2>
                                            <div style="display: none">
                                                <asp:TextBox Style="margin-top: 10px" placeholder="نام" CssClass="form-control regform  text-right " ID="NameTextBox" runat="server" Text="کاربر"></asp:TextBox>
                                                <asp:RequiredFieldValidator Font-Size="Small" ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" Text="نام الزامیست" ControlToValidate="NameTextBox" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>

                                                <asp:TextBox Style="margin-top: 10px" placeholder="نام خانوادگی" CssClass="form-control regform  text-right " ID="FamilyTextBox" runat="server" Text="مهمان"></asp:TextBox>
                                                <asp:RequiredFieldValidator Font-Size="Small" ID="RequiredFieldValidator4" runat="server" ErrorMessage="*" Text="نام خانوادگی الزامیست" ControlToValidate="FamilyTextBox" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </div>

                                            <asp:TextBox Style="margin-top: 10px" runat="server" CssClass="form-control " ID="MobPhoneTextBox" placeholder="شماره موبایل:  09xxxxxxxxx"></asp:TextBox>
                                            <asp:RequiredFieldValidator Style="margin-top: 10px; margin-bottom: 10px;" Font-Size="Small" ID="RequiredFieldValidator5" runat="server" Text="شماره موبایل الزامیست" ErrorMessage="شماره موبایل" ControlToValidate="MobPhoneTextBox" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>

                                            <asp:TextBox TextMode="Password" Style="margin-top: 10px" runat="server" CssClass="form-control " ID="RegisterPasswordTextBox" placeholder="کلمه عبور"></asp:TextBox>
                                            <asp:RequiredFieldValidator Font-Size="Small" ID="RequiredFieldValidator2" runat="server" ErrorMessage="کلمه عبور الزامیست" ControlToValidate="RegisterPasswordTextBox" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>

                                            <asp:TextBox TextMode="Password" Style="margin-top: 10px" runat="server" CssClass="form-control " ID="RepeatPasswordTextBox" placeholder="تکرار کلمه عبور"></asp:TextBox>
                                            <asp:RequiredFieldValidator Font-Size="Small" ID="RequiredFieldValidator6" runat="server" ErrorMessage="تکرار کلمه عبور الزامیست" ControlToValidate="RepeatPasswordTextBox" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="کلمه عبور را به درستی تکرار کنید" ControlToCompare="RepeatPasswordTextBox" ControlToValidate="RegisterPasswordTextBox" ForeColor="Red" Display="Dynamic"></asp:CompareValidator>

                                            <div style="display: none">
                                                <asp:TextBox Style="margin-top: 10px;" dir="ltr" runat="server" CssClass="form-control text-right" ID="EmailTextBox" placeholder=" پست الکترونیک"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Text="پست الکترونیک شما فاقد اعتبار است!" ErrorMessage="پست الکترونیک شما فاقد اعتبار است!" ControlToValidate="EmailTextBox" Display="Dynamic" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                            </div>

                                            <div dir="rtl" style="padding: 8px">
                                                <a href="<%=SSClass.WebDomain %>/news/5" target="_blank">مطالعه ی شرایط ثبت نام
                                                </a>
                                            </div>
                                            <asp:Button CssClass="btn btn-warning  btn-block" ID="RegisterButton" runat="server" Text="قبول شرایط و ثبت نام" OnClick="RegisterButton_Click" />
                                            <asp:Label ID="MsgLabel" runat="server" ForeColor="Red" Font-Size="Smaller"></asp:Label>
                                        </div>
                                    </div>
                                </asp:Panel>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:UpdatePanel ID="LoginUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <br />
                                    <asp:Panel ID="Panel1" runat="server" DefaultButton="LoginButton">
                                        <div style="font-size: small;padding:15px" class="nice-shadow">
                                            <div style="" class="form-group RegFrame">
                                                <div class="text-right BYekan">
                                                    <h2>قبلا ثبت نام کرده ام:</h2>
                                                    <%--<p class=" help-block">کاربرانی که قبلا از تهران آموز خرید کرده اند.</p>--%>
                                                    <asp:TextBox AutoCompleteType="Email" ValidationGroup="Login" Style="margin-top: 10px" runat="server" CssClass="form-control " ID="LEmailTextBox" placeholder="شماره موبایل یا پست الکترونیک"></asp:TextBox>
                                                    <%--<p class="help-block small hidden-xs">الگوی شماره موبایل: 09xxxxxxxxx   و الگوی ایمیل: your_id@domain.suffix</p>--%>
                                                    <p class="help-block small ">
                                                        الگوی شماره موبایل: 09xxxxxxxxx                                                     
                                                    </p>

                                                    <asp:RequiredFieldValidator ValidationGroup="LoginInRegisterPage" Font-Size="Small" ID="RequiredFieldValidator1" runat="server" Text="شماره موبایل یا پست الکترونیک خود را وارد کنید." ErrorMessage="موبایل یا ایمیل" ControlToValidate="LEmailTextBox" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>

                                                    <asp:TextBox ValidationGroup="LoginInRegisterPage" TextMode="Password" Style="margin-top: 10px" runat="server" CssClass="form-control " ID="LoginPasswordTextBox" placeholder="کلمه عبور"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ValidationGroup="LoginInRegisterPage" Font-Size="Small" ID="RequiredFieldValidator8" runat="server" ErrorMessage="کلمه عبور الزامیست" ControlToValidate="LoginPasswordTextBox" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>

                                                    <div dir="rtl" style="padding: 8px">
                                                        <a href="<%=SSClass.WebDomain %>/PasswordRecovery">کلمه ی عبور را فراموش کرده ام</a>
                                                    </div>
                                                    <asp:Button ValidationGroup="LoginInRegisterPage" CssClass="btn btn-primary btn-block" ID="LoginButton" runat="server" Text="ورود به تهران آموز" OnClick="LoginButton_Click_InRegisterPage" />
                                                    <div style="padding-top: 10px" class="text-center">
                                                        <asp:Label ForeColor="Red" ID="LoginMsgLabel" Visible="false" runat="server" Text=""></asp:Label>
                                                    </div>
                                                    <asp:UpdateProgress ID="UpdateProgress2" runat="server" DisplayAfter="1" AssociatedUpdatePanelID="LoginUpdatePanel">
                                                        <ProgressTemplate>
                                                            <div class="progress progress-striped active" style="height: 12px;">
                                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 100%; height: 15px;">
                                                                </div>
                                                            </div>
                                                        </ProgressTemplate>
                                                    </asp:UpdateProgress>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>


                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

