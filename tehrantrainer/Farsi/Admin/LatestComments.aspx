﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Farsi/Admin/FaAdminMasterPage.master" AutoEventWireup="true" CodeFile="LatestComments.aspx.cs" Inherits="Farsi_Admin_LatestComments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-xs-1">
            <br />
        </div>
        <div class="col-xs-10">
            <asp:UpdateProgress ID="UpdateProgress2" runat="server" DisplayAfter="1" AssociatedUpdatePanelID="CommentsUpdatePanel">
                <ProgressTemplate>
                    <div class="progress progress-striped active container" style="height: 12px; position: absolute; width: 100%">
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 100%; height: 15px; background-color: #36c2b9">
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
        <div class="col-xs-1">
            <br />
        </div>
    </div>
    <asp:UpdatePanel ID="CommentsUpdatePanel" runat="server">
        <ContentTemplate>
            <div class="container-fluid">
                <div class="container">
                    <div class="row ">
                        <div class="col-md-12 text-center">
                            <div class="btn-group" role="group" aria-label="...">
                                <asp:Button ID="NexDayButton" runat="server" Text="روز بعدی" class="btn btn-primary" OnClick="NexDayButton_Click" />
                                <asp:TextBox AutoPostBack="true" ID="TodayTextBox" runat="server" Width="170" class="btn btn-default text-center" OnTextChanged="TodayTextBox_TextChanged"></asp:TextBox>
                                <asp:Button ID="PreDayButton" runat="server" Text="روز قبلی" class="btn btn-primary" OnClick="PreDayButton_Click" />
                                <asp:Label Visible="false" ID="TodayLabel" runat="server" Text=""></asp:Label>
                                <br />
                                <br />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <hr />
                            <a name="comments"></a>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <label class="BYekan" style="padding-right: 15px">نظرات کاربران</label>
                                </div>
                                <div class="panel-body" style="font-size: small;">
                                    <div style="line-height: 25px">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <asp:Repeater ID="CommentsRepeater" runat="server" DataSourceID="CommentsSqlDataSource" OnItemCommand="CommentsRepeater_ItemCommand" OnPreRender="CommentsRepeater_PreRender">
                                                    <ItemTemplate>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <asp:Label ID="pidLabel" runat="server" Text='<%#Eval("pid") %>' Visible="false"></asp:Label>
                                                                <asp:Repeater ID="ProductRepeater" runat="server" DataSourceID="ProductSqlDataSource">
                                                                    <ItemTemplate>
                                                                        <a href="<%=SSClass.WebDomain %>/Farsi/product.aspx?pid=<%#Eval("pid") %>" target="_blank"><%# Eval("fname")+" | " +Eval("model") %></a>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                                <asp:SqlDataSource runat="server" ID="ProductSqlDataSource" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT DISTINCT [pid], [FName], [PageView], [Model] FROM [Product] WHERE ([pid] = @pid)">
                                                                    <SelectParameters>
                                                                        <asp:ControlParameter ControlID="pidLabel" PropertyName="Text" Name="pid" Type="Int32"></asp:ControlParameter>
                                                                    </SelectParameters>
                                                                </asp:SqlDataSource>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <p class="Comments">
                                                                    <strong><%#AdminComment(Eval("userid")) %></strong>
                                                                    <asp:Repeater ID="UserNameRepeater" runat="server" DataSourceID="UserNameSqlDataSource">
                                                                        <ItemTemplate>
                                                                            <strong><%#Eval("MobPhone").ToString()+" | "+Eval("name")+" "+Eval("family").ToString() %> :</strong>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                    <asp:SqlDataSource runat="server" ID="UserNameSqlDataSource" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT MobPhone,[Name], [Family] FROM [Users] WHERE ([UserID] = @UserID)">
                                                                        <SelectParameters>
                                                                            <asp:ControlParameter ControlID="UseridLabel" PropertyName="Text" Name="UserID" Type="Int32"></asp:ControlParameter>
                                                                        </SelectParameters>
                                                                    </asp:SqlDataSource>
                                                                    <asp:Label ID="UseridLabel" Visible="false" runat="server" Text='<%#Eval("userid") %>'></asp:Label>

                                                                    <%#Eval("commentbody") %>
                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <span style="color: gray"><%#DisplayDate(Eval("cdate")) %></span>

                                                                        </div>
                                                                        <div class="col-sm-6 text-left">
                                                                            <asp:Button CssClass="btn btn-sm btn-primary" ID="ReplyButton" CommandName="Reply" CommandArgument='<%#Eval("cid") %>' runat="server" Text="پاسخ به این نظر" />

                                                                        </div>
                                                                    </div>
                                                                </p>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <asp:Label ID="CIDLabel" Visible="false" runat="server" Text='<%#Eval("cid") %>'></asp:Label>
                                                                <asp:Repeater ID="CommentsReplyRepeater" runat="server" DataSourceID="CommentsReplySqlDataSource">
                                                                    <%--<HeaderTemplate>
                                                                        <strong style="color:#72c02c">پاسخ ها:</strong>
                                                                    </HeaderTemplate>--%>
                                                                    <ItemTemplate>
                                                                        <div class="row">
                                                                            <div class="col-md-1"></div>
                                                                            <div class="col-md-11">
                                                                                <p class='<%#CommentCss(Eval("userid")) %>'>
                                                                                    <strong><%#AdminComment(Eval("userid")) %></strong>
                                                                                    <asp:Repeater ID="UserNameRepeater" runat="server" DataSourceID="UserNameSqlDataSource">
                                                                                        <ItemTemplate>
                                                                                            <strong><%#Eval("MobPhone").ToString()+" | "+Eval("name")+" "+Eval("family").ToString() %> :</strong>
                                                                                        </ItemTemplate>
                                                                                    </asp:Repeater>
                                                                                    <asp:SqlDataSource runat="server" ID="UserNameSqlDataSource" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT MobPhone,[Name], [Family] FROM [Users] WHERE ([UserID] = @UserID)">
                                                                                        <SelectParameters>
                                                                                            <asp:ControlParameter ControlID="UseridLabel" PropertyName="Text" Name="UserID" Type="Int32"></asp:ControlParameter>
                                                                                        </SelectParameters>
                                                                                    </asp:SqlDataSource>
                                                                                    <asp:Label ID="UseridLabel" Visible="false" runat="server" Text='<%#Eval("userid") %>'></asp:Label>
                                                                                    <%#Eval("replybody") %>
                                                                                    <br />
                                                                                    <span style="color: gray"><%#DisplayDate(Eval("cdate")) %></span>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                                <asp:SqlDataSource runat="server" ID="CommentsReplySqlDataSource" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT * FROM Commentsreply WHERE (cid = @cid) ">
                                                                    <SelectParameters>
                                                                        <asp:ControlParameter ControlID="CIDLabel" PropertyName="Text" Name="cid" Type="Int32"></asp:ControlParameter>
                                                                    </SelectParameters>
                                                                </asp:SqlDataSource>
                                                            </div>
                                                        </div>
                                                        <hr style="border-top: 1px solid #a4aab4" />

                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <asp:SqlDataSource runat="server" ID="CommentsSqlDataSource" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT * FROM Comments where  CONVERT(DATETIME, FLOOR(CONVERT(FLOAT, cdate)))=@cdate order by cdate desc">
                                                    <SelectParameters>
                                                        <asp:ControlParameter ControlID="TodayLabel" PropertyName="Text" Name="cdate"></asp:ControlParameter>

                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 BYekan">
                                                <div runat="server" id="CommentsDiv" visible="false" class="alert alert-success alert-dismissable text-right">
                                                    <asp:Literal ID="CommentsLiteral" runat="server"></asp:Literal>
                                                </div>
                                            </div>
                                        </div>
                                        <asp:Panel ID="LoggedOutPanel" runat="server">
                                            <%--<div class="row">
                                        <div class="col-md-12 text-center" style="padding-bottom: 8px">
                                            <a class="btn btn-success BYekan" style="text-decoration: none" href="#" data-toggle="modal" data-target="#LoginModal"><span style="margin-left: 10px" class="glyphicon glyphicon-new-window"></span>برای ثبت نظر ابتدا باید وارد مبل و دکور شوید </a>
                                        </div>
                                    </div>--%>
                                        </asp:Panel>
                                        <asp:Panel ID="LoggedInPanel" runat="server">
                                            <div class="row">
                                                <div class="col-md-12" style="padding-bottom: 8px">
                                                    <hr />
                                                    <asp:CheckBox Style="font-weight: 100!important" Checked="true" ID="IsReplyCheckBox" runat="server" AutoPostBack="true" OnCheckedChanged="IsReplyCheckBox_CheckedChanged" Text="این نظر در قالب یک پاسخ ثبت خواهد شد." Visible="false" />

                                                    <asp:TextBox placeholder="با پاسخ به نظرات بازدید کنندگان، سعی در جلب رضایت آنها داشته باشید." CssClass="form-control input-sm" runat="server" ID="CommentBodyTextBox" TextMode="MultiLine" Style="max-width: 100%; min-width: 100%; min-height: 200px" ValidationGroup="CommentSubmit" />
                                                    <asp:RequiredFieldValidator ValidationGroup="CommentSubmit" Style="margin-top: 10px; margin-bottom: 10px;" Font-Size="Small" ID="RequiredFieldValidator5" runat="server" Text="ابتدا نظر خود را وارد نمایید." ErrorMessage="ابتدا نظر خود را وارد نمایید." ControlToValidate="CommentBodyTextBox" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>

                                                </div>
                                                <div class="col-md-8">
                                                    <label style="font-weight: 100!important">
                                                        <asp:CheckBox ID="AgreementCheckBox" runat="server" Checked="true" />
                                                        <span>شرایط و قوانین مبل و دکور را مطالعه نموده و با کلیه موارد آن موافقم.</span>
                                                    </label>

                                                </div>
                                                <div class="col-md-4 text-left">
                                                    <asp:Button CssClass="btn btn-success btn-sm BYekan" ID="SubmitComment" runat="server" Text="ثبت نظر" OnClick="SubmitComment_Click" ValidationGroup="CommentSubmit" />

                                                    <asp:Label ID="CIDReplyLabel" Visible="false" runat="server" Text="0"></asp:Label>

                                                </div>
                                            </div>
                                        </asp:Panel>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

