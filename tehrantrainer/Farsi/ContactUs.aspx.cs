﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Farsi_ContactUs : System.Web.UI.Page
{
    protected string DisplayDate(object Date)
    {
        PersianDateTime PersianDate = new PersianDateTime(Convert.ToDateTime(Date));

        return PersianDate.ToString("dddd d MMMM yyyy ساعت hh:mm tt");
    }
    protected void Page_Init(object sender, EventArgs e)
    {

    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void SubmitButton_Click(object sender, EventArgs e)
    {
        if (!NumCaptcha1.Decide())
        {
            NumCaptcha1.message = "پاسخ اشتباه است";

        }
        else
        {
            DataLayer dl = new DataLayer();
            string CS = dl.dataLayerConnectionString;
            SqlConnection con = new SqlConnection(CS);


            string InsertMsgSqlSyntax = "INSERT INTO feedback (namefamily,phone,email,msg) VALUES (@namefamily,@phone,@email,@msg)";


            SqlCommand InsertMsgCmd = new SqlCommand(InsertMsgSqlSyntax, con);
            SqlParameter[] InsertMsgParam = new SqlParameter[5];

            InsertMsgParam[0] = new SqlParameter("@namefamily", SqlDbType.NVarChar);
            InsertMsgParam[1] = new SqlParameter("@phone", SqlDbType.NVarChar);
            InsertMsgParam[2] = new SqlParameter("@email", SqlDbType.NVarChar);
            InsertMsgParam[3] = new SqlParameter("@msg", SqlDbType.NVarChar);
            InsertMsgParam[4] = new SqlParameter("@sendertype", SqlDbType.NVarChar);

            InsertMsgParam[0].Value = NameFamilyTextBox.Text;
            InsertMsgParam[1].Value = PhoneTextBox.Text;
            InsertMsgParam[2].Value = EmailTextBox.Text;
            InsertMsgParam[3].Value = MsgTextBox.Text;
            InsertMsgParam[4].Value = "default";


            for (int i = 0; i < InsertMsgParam.Length; i++)
            {
                InsertMsgCmd.Parameters.Add(InsertMsgParam[i]);
            }

            try
            {
                con.Open();
                InsertMsgCmd.ExecuteNonQuery();
                NameFamilyTextBox.Text = "";
                PhoneTextBox.Text = "";
                EmailTextBox.Text = "";
                MsgTextBox.Text = "";

                MsgDiv.Visible = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                string msg = "InsertNewPostCmd Error:";
                msg += ex.Message;
                throw new Exception(msg);
            }
            finally
            {
                con.Close();

            }
            NumCaptcha1.Refresh();

        }
    }
}