﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="sm.aspx.cs" Inherits="sm" %>

<?xml version="1.0" encoding="UTF-8" ?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"> 
      <url>
        <loc><%=SSClass.WebDomain %></loc>        
      </url>
<asp:repeater runat="server" datasourceid="SMSqlDataSource">
    <ItemTemplate>
      <url>
        <loc>"<%=SSClass.WebDomain %>/<%#MyFunc.SafeString(Eval("cat2title"))%>/<%#MyFunc.SafeString(Eval("header"))%>/<%#Eval("cid") %>"</loc>
      </url>
    </ItemTemplate>
</asp:repeater>
<asp:sqldatasource runat="server" id="SMSqlDataSource" connectionstring='<%$ ConnectionStrings:DrB_DBConnectionString %>' selectcommand="select content.cid,Header, dbo.Tree.Name AS cat2title FROM dbo.[Content] INNER JOIN dbo.Tree ON dbo.[Content].cat2 = dbo.Tree.NodeID "></asp:sqldatasource>

<asp:repeater runat="server" datasourceid="TagsSqldatasource">
    <ItemTemplate>
      <url>
        <loc><%#SSClass.WebDomain %>/tag/<%#MyFunc.SafeString(Eval("tagname"))%></loc>
      </url>
    </ItemTemplate>
</asp:repeater>
<asp:sqldatasource runat="server" id="TagsSqldatasource" connectionstring='<%$ ConnectionStrings:DrB_DBConnectionString %>' selectcommand="SELECT DISTINCT * FROM [contenttag] inner join tag on contenttag.tid=tag.tid WHERE contenttag.isdeleted=0"></asp:sqldatasource>

</urlset>
