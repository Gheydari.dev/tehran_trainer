﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Farsi_Admin_Tree : System.Web.UI.Page
{

    protected string InsertStartUl(object XName)
    {
        if ((Convert.ToInt32(XName.ToString()) == 1))
        {
            return "<ul class=\"dropdown-menu dropdown-menu-right text-right sss l1dropdown-menu \">";
        }
        else
        {
            return "";
        }
    }
    protected string InsertStartUl2(object XName)
    {
        if ((Convert.ToInt32(XName.ToString()) == 1))
        {
            return "<ul class=\"dropdown-menu dropdown-menu-right text-right sss l2dropdown-menu \">";
        }
        else
        {
            return "";
        }
    }

    protected string InsertEndUl(object XName, object X2Name)
    {
        if (Convert.ToInt32(XName.ToString()) == Convert.ToInt32(X2Name.ToString()))
        {
            return "</ul>";
        }
        else
        {
            return "";
        }
    }
    //private string GenerateUL(IQueryable<Menu> menus)
    //{
    //    var sb = new StringBuilder();

    //    sb.AppendLine("<ul>");
    //    foreach (var menu in menus)
    //    {
    //        if (menu.Menus.Any())
    //        {
    //            sb.AppendLine("<li>" + menu.Text);
    //            sb.Append(GenerateUL(menu.Menus.AsQueryable()));
    //            sb.AppendLine("</li>");
    //        }
    //        else
    //            sb.AppendLine("<li>" + menu.Text + "</li>");
    //    }
    //    sb.AppendLine("</ul>");

    //    return sb.ToString();
    //}
    private TreeNode FindNode(TreeNode root, String NodeValue)
    {
        foreach (TreeNode node in root.ChildNodes)
        {
            if (node.ChildNodes.Count > 0)
                return FindNode(root, NodeValue);
            if (node.Value == NodeValue)
                return node;
        }
        return null;
    }
    private TreeNode FindNode2(TreeNode root, String name)
    {
        foreach (TreeNode node in root.ChildNodes)
        {
            if (node.Value == name)
                return node;
            else
            {
                if (node.ChildNodes.Count > 0)
                    return FindNode2(node, name);
            }
        }
        return null;
    }
    protected string GenerateUL(DataRow[] menu, DataTable table, StringBuilder sb, int lnum, int _nodeid)
    {

        if (lnum == 0)
        {
            sb.AppendLine("<ul class=\"nav navbar-nav navbar-right text-right\">");

        }
        else
        {
            sb.AppendLine("<ul class=\"dropdown-menu dropdown-menu-right text-right sss l" + (lnum).ToString() + "dropdown-menu \">");

        }
        if (menu.Length > 0)
        {
            foreach (DataRow dr in menu)
            {
                string href = dr["href"].ToString();
                string menuText = dr["name"].ToString();
                string line = "";
                string nodeid = dr["nodeid"].ToString();
                string parentId = dr["ParentId"].ToString();
                string ordernum = dr["ordernum"].ToString();
                DataRow[] subMenu = table.Select(String.Format("ParentId = {0}", nodeid));


                if (subMenu.Length > 0)
                {
                    if (lnum == 0)
                    {
                        line = String.Format("<li class=\"dropdown pull-right l{2}dropdown\"><a href=\"#\" data-nodeid={3} data-ordernum={4} class='test' data-href=\"{0}\"><b class=\"caret\"></b>{1}</a>", href, menuText, (lnum + 1).ToString(), nodeid, ordernum);
                        //TreeNode NewTN = new TreeNode
                        //{
                        //    Text = menuText,
                        //    Value = nodeid
                        //};
                        //TreeView1.Nodes.Add(NewTN);

                    }
                    else
                    {
                        //TreeNode NewTN = new TreeNode
                        //{
                        //    Text = menuText,
                        //    Value = nodeid
                        //};

                        //TreeView1.Nodes[0].ChildNodes[0].ChildNodes.Add(NewTN);

                        line = String.Format("<li class=\"l{2}dropdown dropdown-submenu\"><a href=\"#\" data-nodeid={3} data-ordernum={4}  class='test'  data-href=\"{0}\">{1}</a>", href, menuText, (lnum + 1).ToString(), nodeid, ordernum);
                    }
                    //TreeNode ParentTN = new TreeNode
                    //{
                    //    Text = menuText,
                    //    Value = parentId
                    //};
                    //TreeNode CurNode = FindNode(ParentTN, parentId);
                    ////TreeNode CurNode = TreeView1.Nodes.IndexOf();
                    //if (CurNode != null)
                    //{
                    //    CurNode.ChildNodes.Add(NewTN);
                    //}
                }
                else
                {

                    if (lnum == 0)
                    {

                        line = String.Format("<li class=\"dropdown pull-right l{2}dropdown \"><a href=\"#\" data-nodeid={3} data-ordernum={4} class='test' data-href=\"{0}\">{1}</a>", href, menuText, (lnum + 1).ToString(), nodeid, ordernum);
                    }
                    else
                    {
                        line = String.Format("<li class=\"l{2}dropdown dropdown-submenu \"><a href=\"#\" data-nodeid={3} data-ordernum={4} class='test' data-href=\"{0}\">{1}</a>", href, menuText, (lnum + 1).ToString(), nodeid, ordernum);
                    }
                }
                sb.Append(line);





                //if (lnum == 0)
                //{
                //    
                //}
                //else
                //{
                //    try
                //    {
                //        TreeView1.Nodes[0]..ChildNodes.Add(TN);
                //    }
                //    catch (Exception ex)
                //    {

                //        throw new Exception(ordernum.ToString()+" | "+ex.Message);
                //    }

                //}


                if (subMenu.Length > 0 && !nodeid.Equals(parentId))
                {
                    var subMenuBuilder = new StringBuilder();
                    sb.Append(GenerateUL(subMenu, table, subMenuBuilder, ++lnum, Convert.ToInt32(nodeid)));
                    --lnum;
                }
                else
                {
                    var subMenuBuilder = new StringBuilder();
                    sb.Append(GenerateUL(subMenu, table, subMenuBuilder, ++lnum, Convert.ToInt32(nodeid)));
                    --lnum;
                }
                sb.Append("</li>");
            }
        }
        sb.Append(string.Format("<li class=\"l1dropdown \"><a href=\"#\" data-nodeid={0} class='addnewnode test'>افزودن آیتم جدید</a></li>", _nodeid.ToString()));

        sb.Append("</ul>");
        return sb.ToString();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        System.Web.UI.HtmlControls.HtmlGenericControl li = (System.Web.UI.HtmlControls.HtmlGenericControl)this.Page.Master.Master.FindControl("BodyContentPlaceHolder").FindControl("Tree");

        li.Attributes["class"] = "active";

        TreeView1.Nodes.Clear();
        //Response.Redirect("fa/default.aspx");

        DataSet ds = new DataSet();
        SqlConnection connection = new SqlConnection(DataLayer.connectionString );
        SqlCommand command = new SqlCommand("select * from tree where GroupName=N'" + MenuLanSelectorDropDownList.SelectedValue + "' order by ordernum", connection);
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        adapter.Fill(ds);

        DataTable table = ds.Tables[0];
        DataRow[] parentMenus = table.Select("ParentId = 0");

        var sb = new StringBuilder();
        string unorderedList = GenerateUL(parentMenus, table, sb, 0, 0);
        MenuLiteral.Text = unorderedList;
    }
    protected void MenuLanSelectorDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (MenuLanSelectorDropDownList.SelectedIndex != 0)
        {
            MenuDiv.Visible = true;
        }
        else
        {
            Response.Redirect("tree.aspx");
            MenuDiv.Visible = false;
        }
    }
    protected void MenuButton_Click(object sender, EventArgs e)
    {
        if (HrefTextBox.Text.Trim() == "")
        {
            HrefTextBox.Text = "#!";
        }
        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);
        if (AddNewNodeLabel.Text == "true")
        {
            string SQLQuerry = "INSERT INTO [tree] (ParentID,Name,href,GroupName,OrderNum,img) values (@ParentID,@Name,@href,@GroupName,@OrderNum,@img)";
            SQLQuerry += " update tree set haschild=1 where nodeid in (select parentid from tree where groupname=@groupname ) ";
            SqlCommand RegCmd = new SqlCommand(SQLQuerry, con);
            SqlParameter[] param = new SqlParameter[6];

            param[00] = new SqlParameter("@ParentID", SqlDbType.NVarChar);
            param[01] = new SqlParameter("@Name", SqlDbType.NVarChar);
            param[02] = new SqlParameter("@href", SqlDbType.NVarChar);
            param[03] = new SqlParameter("@GroupName", SqlDbType.NVarChar);
            param[04] = new SqlParameter("@OrderNum", SqlDbType.NVarChar);
            param[05] = new SqlParameter("@img", SqlDbType.NVarChar);


            param[00].Value = NodeIDTextBox.Text;
            param[01].Value = NameTextBox.Text;
            param[02].Value = HrefTextBox.Text;
            param[03].Value = MenuLanSelectorDropDownList.SelectedValue;
            param[04].Value = OrderNumTextBox.Text;
            param[05].Value = "noimage";
            if (ImgFileUpload.HasFile)
            {
                string SecCodeString = Guid.NewGuid().ToString("N").Substring(16);

                string fileExt = System.IO.Path.GetExtension(ImgFileUpload.FileName.ToLower());
                //if (fileExt == ".jpg" || fileExt == ".jpeg" || fileExt == ".png" || fileExt == ".gif")
                //{
                //    param[05].Value = SSClass.WebDomainFile + "/images/tree/" + SecCodeString + fileExt;
                //}
                //else
                //{
                //    InfoLiteral.Text = "فقط تصاویر با فرمت های PNG، JPG و JPEG پشتیبانی می شود.";
                //    InfoDiv.Attributes["class"] = " alert alert-danger alert-dismissable text-right";
                //    return;
                //}

                long size = ImgFileUpload.PostedFile.ContentLength;
                size = size / 1024; //to KB
                if (size > 200)
                {
                    InfoLiteral.Text = "حجم تصویر ارسالی شما بیشتر از 200 کیلوبایت است.";
                    InfoDiv.Attributes["class"] = " alert alert-danger alert-dismissable text-right";
                    return;
                }
                param[05].Value = SSClass.WebDomainFile + "/images/tree/" + SecCodeString + fileExt;
                ImgFileUpload.SaveAs(Server.MapPath("~/images/tree/" + SecCodeString + fileExt));
            }

            for (int r = 0; r < param.Length; r++)
            {
                RegCmd.Parameters.Add(param[r]);
            }

            try
            {
                con.Open();
                RegCmd.ExecuteScalar();


                DataSet ds = new DataSet();
                SqlConnection connection = new SqlConnection(DataLayer.connectionString );
                SqlCommand command = new SqlCommand("select * from tree where GroupName=N'" + MenuLanSelectorDropDownList.SelectedValue + "' order by ordernum", connection);
                command.Parameters.Add("@GroupName", MenuLanSelectorDropDownList.SelectedValue);
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);

                DataTable table = ds.Tables[0];
                DataRow[] parentMenus = table.Select("ParentId = 0");

                var sb = new StringBuilder();
                string unorderedList = GenerateUL(parentMenus, table, sb, 0, 0);
                MenuLiteral.Text = unorderedList;


                InfoDiv.Visible = true;
                InfoLiteral.Text = "آیتم جدید با موفقیت افزوده شد.";
                InfoDiv.Attributes["class"] = " alert alert-success alert-dismissable text-right";
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                string msg = "RegCmd Error:";
                msg += ex.Message;
                throw new Exception(msg);
            }
            finally
            {
                con.Close();
            }
        }
        else
        {
            string SQLQuerry = "update [tree] set Name=@Name,href=@href,OrderNum=@OrderNum,img=@img where nodeid=@nodeid ";
            SQLQuerry += " update tree set haschild=1 where nodeid in (select parentid from tree where groupname=@groupname ) ";

            SqlCommand RegCmd = new SqlCommand(SQLQuerry, con);
            SqlParameter[] param = new SqlParameter[6];

            param[00] = new SqlParameter("@nodeid", SqlDbType.NVarChar);
            param[01] = new SqlParameter("@Name", SqlDbType.NVarChar);
            param[02] = new SqlParameter("@href", SqlDbType.NVarChar);
            param[03] = new SqlParameter("@GroupName", SqlDbType.NVarChar);
            param[04] = new SqlParameter("@OrderNum", SqlDbType.NVarChar);
            param[05] = new SqlParameter("@img", SqlDbType.NVarChar);


            param[00].Value = NodeIDTextBox.Text;
            param[01].Value = NameTextBox.Text;
            param[02].Value = HrefTextBox.Text;
            param[03].Value = MenuLanSelectorDropDownList.SelectedValue;
            param[04].Value = OrderNumTextBox.Text;
            param[05].Value = "noimage";

            if (ImgFileUpload.HasFile)
            {
                string SecCodeString = Guid.NewGuid().ToString("N").Substring(16);

                string fileExt = System.IO.Path.GetExtension(ImgFileUpload.FileName.ToLower());
                //if (fileExt == ".jpg" || fileExt == ".jpeg" || fileExt == ".png" || fileExt == ".gif")
                //{
                //    param[05].Value = SSClass.WebDomainFile + "/images/tree/" + SecCodeString + fileExt;
                //}
                //else
                //{
                //    InfoLiteral.Text = "فقط تصاویر با فرمت های PNG، JPG و JPEG پشتیبانی می شود.";
                //    InfoDiv.Attributes["class"] = " alert alert-danger alert-dismissable text-right";
                //    return;
                //}

                long size = ImgFileUpload.PostedFile.ContentLength;
                size = size / 1024; //to KB
                if (size > 200)
                {
                    InfoLiteral.Text = "حجم تصویر ارسالی شما بیشتر از 200 کیلوبایت است.";
                    InfoDiv.Attributes["class"] = " alert alert-danger alert-dismissable text-right";
                    return;
                }
                param[05].Value = SSClass.WebDomainFile + "/images/tree/" + SecCodeString + fileExt;
                ImgFileUpload.SaveAs(Server.MapPath("~/images/tree/" + SecCodeString + fileExt));
            }
            else
            {
                SQLQuerry = SQLQuerry.Replace(",img=@img", "");
            }
            for (int r = 0; r < param.Length; r++)
            {
                RegCmd.Parameters.Add(param[r]);
            }

            try
            {
                con.Open();
                RegCmd.ExecuteScalar();


                DataSet ds = new DataSet();
                SqlConnection connection = new SqlConnection(DataLayer.connectionString );
                SqlCommand command = new SqlCommand("select * from tree where GroupName=N'" + MenuLanSelectorDropDownList.SelectedValue + "' order by ordernum", connection);
                command.Parameters.Add("@GroupName", MenuLanSelectorDropDownList.SelectedValue);
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);

                DataTable table = ds.Tables[0];
                DataRow[] parentMenus = table.Select("ParentId = 0");

                var sb = new StringBuilder();
                string unorderedList = GenerateUL(parentMenus, table, sb, 0, 0);
                MenuLiteral.Text = unorderedList;


                InfoDiv.Visible = true;
                InfoLiteral.Text = "آیتم مورد نظر با موفقیت ویرایش شد.";
                InfoDiv.Attributes["class"] = " alert alert-success alert-dismissable text-right";
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                string msg = "RegCmd Error:";
                msg += ex.Message;
                throw new Exception(msg);
            }
            finally
            {
                con.Close();
            }
        }
    }
    protected void DeleteMenuItemButton_Click(object sender, EventArgs e)
    {
        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);

        string SQLQuerry = "delete from [tree] where nodeid=@nodeid  delete from tree where parentid not in (select nodeid from tree) and parentid!=0   delete from tree where parentid not in (select nodeid from tree) and parentid!=0   delete from tree where parentid not in (select nodeid from tree) and parentid!=0   delete from tree where parentid not in (select nodeid from tree) and parentid!=0   delete from tree where parentid not in (select nodeid from tree) and parentid!=0   delete from tree where parentid not in (select nodeid from tree) and parentid!=0 ";
        SQLQuerry += " update tree set haschild=0 update tree set haschild=1 where nodeid in (select parentid from tree where groupname=@groupname ) ";

        SqlCommand RegCmd = new SqlCommand(SQLQuerry, con);
        SqlParameter[] param = new SqlParameter[5];

        param[00] = new SqlParameter("@nodeid", SqlDbType.NVarChar);
        param[01] = new SqlParameter("@Name", SqlDbType.NVarChar);
        param[02] = new SqlParameter("@href", SqlDbType.NVarChar);
        param[03] = new SqlParameter("@GroupName", SqlDbType.NVarChar);
        param[04] = new SqlParameter("@OrderNum", SqlDbType.NVarChar);


        param[00].Value = NodeIDTextBox.Text;
        param[01].Value = NameTextBox.Text;
        param[02].Value = HrefTextBox.Text;
        param[03].Value = MenuLanSelectorDropDownList.SelectedValue;
        param[04].Value = OrderNumTextBox.Text;

        for (int r = 0; r < param.Length; r++)
        {
            RegCmd.Parameters.Add(param[r]);
        }

        try
        {
            con.Open();
            RegCmd.ExecuteScalar();


            DataSet ds = new DataSet();
            SqlConnection connection = new SqlConnection(DataLayer.connectionString );
            SqlCommand command = new SqlCommand("select * from tree where GroupName=N'" + MenuLanSelectorDropDownList.SelectedValue + "' order by ordernum", connection);
            command.Parameters.Add("@GroupName", MenuLanSelectorDropDownList.SelectedValue);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(ds);

            DataTable table = ds.Tables[0];
            DataRow[] parentMenus = table.Select("ParentId = 0");

            var sb = new StringBuilder();
            string unorderedList = GenerateUL(parentMenus, table, sb, 0, 0);
            MenuLiteral.Text = unorderedList;


            InfoDiv.Visible = true;
            InfoLiteral.Text = "آیتم مورد نظر با موفقیت حذف شد.";
            InfoDiv.Attributes["class"] = " alert alert-success alert-dismissable text-right";
        }
        catch (System.Data.SqlClient.SqlException ex)
        {
            string msg = "RegCmd Error:";
            msg += ex.Message;
            throw new Exception(msg);
        }
        finally
        {
            con.Close();
        }
    }
}

