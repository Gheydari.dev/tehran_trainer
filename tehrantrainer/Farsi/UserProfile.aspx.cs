﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Farsi_UserProfile : System.Web.UI.Page
{
    protected string DisplayDate2(object Date)
    {
        if (Date.ToString() == "")
        {
            return "_";
        }
        else
        {
            PersianDateTime PersianDate = new PersianDateTime(Convert.ToDateTime(Date));
            return PersianDate.ToString(PersianDateTimeFormat.Date);
        }

    }
    protected string DisplayDate3(object Date)
    {

        DateTime dt;

        if (DateTime.TryParseExact(Date.ToString(), "yyyy/mm/dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt))
        {
            PersianDateTime persianDate = PersianDateTime.Parse(Date.ToString());
            DateTime miladiDate = persianDate.ToDateTime();
            return miladiDate.ToString();
        }
        else
        {
            return DateTime.Now.ToShortDateString();
        }
    }
    protected string DisplayDate(object Date)
    {
        if (Date.ToString() == "")
        {
            return "_";
        }
        else
        {
            PersianDateTime PersianDate = new PersianDateTime(Convert.ToDateTime(Date));
            return PersianDate.ToString("dddd d MMMM yyyy");
        }

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "اطلاعات حساب کاربری | تهران آموز";
    }
    protected void EditUserPersonalInfoButton_Click(object sender, EventArgs e)
    {
        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);

        string FetchUserSqlSyntax = "select * from users where userid=@userid";
        SqlCommand FetchUserCmd = new SqlCommand(FetchUserSqlSyntax, con);

        SqlParameter[] param = new SqlParameter[1];

        param[0] = new SqlParameter("@UserID", SqlDbType.BigInt);

        param[0].Value = Session["userid"].ToString();

        FetchUserCmd.Parameters.Add(param[0]);

        try
        {
            con.Open();
            SqlDataReader FetchUserDr = FetchUserCmd.ExecuteReader();
            if (FetchUserDr.Read())
            {
                NameTextBox.Text = FetchUserDr["name"].ToString();
                FamilyTextBox1.Text = FetchUserDr["family"].ToString();
                GenderDropDownList3.SelectedValue = FetchUserDr["gender"].ToString();
                EmailTextBox9.Text = FetchUserDr["Email"].ToString();
                HomeAddressTextBox10.Text = FetchUserDr["homeaddress"].ToString();
                MobPhoneTextBox4.Text = FetchUserDr["mobphone"].ToString();
                HomePhoneTextBox5.Text = FetchUserDr["homephone"].ToString();
                NationalCodeTextBox3.Text = FetchUserDr["nationalcode"].ToString();
                BirthdayTextBox6.Text = DisplayDate2(FetchUserDr["birthday"].ToString());
                BankCardTextBox7.Text = FetchUserDr["bankcard"].ToString();
                NewsLetterDropDownList1.Text = FetchUserDr["newsletter"].ToString();
                //MarriageTextBox3.Text = FetchUserDr["marriage"].ToString();
                //EduTextBox1.Text = FetchUserDr["edu"].ToString();
                //UniTextBox2.Text = FetchUserDr["uni"].ToString();
                OstanDropDownList.DataBind();
                if (FetchUserDr["ostan"] != DBNull.Value)
                {
                    OstanDropDownList.SelectedValue = FetchUserDr["ostan"].ToString();

                }
                if (FetchUserDr["Shahrestan"] != DBNull.Value)
                {
                    ShahrestanDropDownList.SelectedValue = FetchUserDr["Shahrestan"].ToString();

                }

                EditUserPersonalInfoDiv.Visible = true;
                UserPersonalInfoRepeater.Visible = false;
                InfoDiv.Visible = true;
                InfoLiteral.Text = "پس از ویرایش اطلاعات روی دکمه ی ذخیره تغییرات کلیک کنید. جهت ویرایش فیلد های غیر فعال با ما تماس بگیرید.";
                InfoDiv.Attributes["class"] = " alert alert-warning alert-dismissable text-right";
            }
        }

        catch (System.Data.SqlClient.SqlException ex)
        {
            string msg = "FetchUserCmd Error:";
            msg += ex.Message;
            throw new Exception(msg);
        }

        finally
        {
            con.Close();
        }
    }
    protected void UpdateUserPersonalInfoButton_Click(object sender, EventArgs e)
    {
        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);
        string UpdateUserSqlSyntax = "UPDATE [Users] SET [Name] = [Name], [Family] = [Family], [NationalCode] = @NationalCode, [Birthday] = @Birthday, [BankCard] = @BankCard, [NewsLetter] = @NewsLetter, [Gender] = @Gender, [Email] = @Email, [AboutMe] = @AboutMe,[HomeAddress] = @HomeAddress, [MobPhone] = [MobPhone], [HomePhone] = @HomePhone, [edu] = @edu, [uni] = @uni, [marriage] = @marriage, [UpDate] = GetDate(),ostan=@ostan,shahrestan=@shahrestan WHERE [UserID] = @UserID";

        SqlCommand UpdateUserCmd = new SqlCommand(UpdateUserSqlSyntax, con);
        UpdateUserCmd.CommandType = System.Data.CommandType.Text;
        SqlParameter[] param = new SqlParameter[18];

        param[00] = new SqlParameter("@Name", SqlDbType.NVarChar);
        param[01] = new SqlParameter("@Family", SqlDbType.NVarChar);
        param[02] = new SqlParameter("@NationalCode", SqlDbType.NVarChar);
        param[03] = new SqlParameter("@Birthday", SqlDbType.DateTime);
        param[04] = new SqlParameter("@BankCard", SqlDbType.NVarChar);
        param[05] = new SqlParameter("@NewsLetter", SqlDbType.NVarChar);
        param[06] = new SqlParameter("@Gender", SqlDbType.NVarChar);
        param[07] = new SqlParameter("@Email", SqlDbType.NVarChar);
        param[08] = new SqlParameter("@AboutMe", SqlDbType.NVarChar);
        param[09] = new SqlParameter("@HomeAddress", SqlDbType.NVarChar);
        param[10] = new SqlParameter("@MobPhone", SqlDbType.NVarChar);
        param[11] = new SqlParameter("@HomePhone", SqlDbType.NVarChar);
        param[12] = new SqlParameter("@userid", SqlDbType.NVarChar);
        param[13] = new SqlParameter("@edu", SqlDbType.NVarChar);
        param[14] = new SqlParameter("@uni", SqlDbType.NVarChar);
        param[15] = new SqlParameter("@marriage", SqlDbType.NVarChar);
        param[16] = new SqlParameter("@ostan", SqlDbType.NVarChar);
        param[17] = new SqlParameter("@shahrestan", SqlDbType.NVarChar);

        param[00].Value = NameTextBox.Text;
        param[01].Value = FamilyTextBox1.Text;
        param[02].Value = NationalCodeTextBox3.Text;
        param[03].Value = DisplayDate3(BirthdayTextBox6.Text);
        param[04].Value = BankCardTextBox7.Text;
        param[05].Value = NewsLetterDropDownList1.SelectedValue;
        param[06].Value = GenderDropDownList3.SelectedValue;
        param[07].Value = EmailTextBox9.Text;
        param[08].Value = "";// AboutMeTextBox.Text.Replace("\n", "<br />").Replace("\r\n", "<br />");
        param[09].Value = HomeAddressTextBox10.Text;
        param[10].Value = MobPhoneTextBox4.Text;
        param[11].Value = HomePhoneTextBox5.Text;
        param[12].Value = Session["userid"].ToString();
        param[13].Value = "";// EduTextBox1.Text;
        param[14].Value = "";// UniTextBox2.Text;
        param[15].Value = "";// MarriageTextBox3.Text;
        param[16].Value = OstanDropDownList.SelectedValue;// MarriageTextBox3.Text;
        param[17].Value = ShahrestanDropDownList.SelectedValue;// MarriageTextBox3.Text;


        for (int i = 0; i < param.Length; i++)
        {
            UpdateUserCmd.Parameters.Add(param[i]);
        }

        try
        {
            con.Open();
            UpdateUserCmd.ExecuteNonQuery();
            UserPersonalInfoRepeater.DataBind();
            EditUserPersonalInfoDiv.Visible = false;
            UserPersonalInfoRepeater.Visible = true;
            InfoDiv.Visible = true;
            InfoLiteral.Text = "ویرایش اطلاعات شما با موفقیت انجام شد.";
            InfoDiv.Attributes["class"] = " alert alert-success alert-dismissable text-right";
        }
        catch (System.Data.SqlClient.SqlException ex)
        {
            string msg = "UpdateUserCmd Error:";
            msg += ex.Message;
            throw new Exception(msg);
        }
        finally
        {
            con.Close();
        }
    }
    protected void CanselButton_Click(object sender, EventArgs e)
    {
        InfoDiv.Visible = false;
        EditUserPersonalInfoDiv.Visible = false;
        UserPersonalInfoRepeater.Visible = true;
    }

    protected void ToRevDashboardButton_Click(object sender, EventArgs e)
    {
        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);
        string UpdateUserSqlSyntax = "UPDATE [Users] SET [RegSitu] = N'عضو جدید گروه نقد و بررسی تهران آموز', [UpDate] = GetDate() WHERE [UserID] = @UserID";

        SqlCommand UpdateUserCmd = new SqlCommand(UpdateUserSqlSyntax, con);
        UpdateUserCmd.CommandType = System.Data.CommandType.Text;
        SqlParameter[] param = new SqlParameter[1];


        param[00] = new SqlParameter("@userid", SqlDbType.NVarChar);


        param[00].Value = Session["userid"].ToString();



        for (int i = 0; i < param.Length; i++)
        {
            UpdateUserCmd.Parameters.Add(param[i]);
        }

        try
        {
            con.Open();
            UpdateUserCmd.ExecuteNonQuery();
            UserPersonalInfoRepeater.DataBind();
            Response.Redirect("RevDashboard.aspx?ref=NewEditor");

        }
        catch (System.Data.SqlClient.SqlException ex)
        {
            string msg = "UpdateUserCmd Error:";
            msg += ex.Message;
            throw new Exception(msg);
        }
        finally
        {
            con.Close();
        }
    }


    protected void shahrestanDropDownList1_DataBound(object sender, EventArgs e)
    {
        ShahrestanDropDownList.Items.Insert(0, new ListItem("شهرستان مورد نظر را انتخاب کنید...", "0"));
        //if (Session["shahrestan"] != null)
        //{
        //    if (OstanDropDownList.Items.Contains(new ListItem(Session["shahrestan"].ToString())))
        //    {
        //        ShahrestanDropDownList.SelectedValue = Session["shahrestan"].ToString();
        //    }

        //    Session["shahrestan"] = null;
        //}

    }
}