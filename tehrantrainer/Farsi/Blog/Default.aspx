﻿<%@ Page Title="" Language="C#" MasterPageFile="~/farsi/FaMasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Farsi_Blog_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContentPlaceHolder" runat="Server">
    <asp:Literal Text="" ID="CanonicalLiteral" runat="server" />
    <meta name="robots" content="index, follow" />
    <!-- ----------------- <- OPEN GRAPH SHARE TAGs -> ----------------- -->
    <meta property="og:type" content="website" />
    <meta property="og:image" content="<%=SSClass.WebDomainFile %>/Images/Design/main-logo.png" />
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="97" />
    <meta property="og:description" content="تهران آموز" />
    <meta property="og:url" content="<%=SSClass.WebDomain %>/" />
    <meta property="og:site_name" content="تهران آموز" />

    <meta name="description" content="تهران آموز، وبلاگ" />
    <meta name="keywords" content="" />
    <style>
        .sp-caption-container {
            text-align: right !important;
            font-size: 25px;
            font-weight: 500;
            margin-top: 15px;
        }

        .PCName {
            padding-right: 5px;
            text-decoration: none;
            color: #707070;
        }

            .PCName > h2 {
                color: #565656;
                font-size: 15px;
                font-weight: 900;
            }

            .PCName > span {
                margin: 5px;
                color: #989898;
            }

        .cat-landig {
            position: relative;
            display: block;
            background-size: cover;
            background-color: #424242;
            color: white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContentPlaceHolder" runat="Server">
    <div class="cat-landig" style="background-image: url(<%=SSClass.WebDomain %>/images/ex/blog-cover.jpg)">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <h3>دسته ها</h3>
                        <div class="row">
                            <asp:Repeater ID="MainSliderRepeater" runat="server" DataSourceID="MainSliderSqlDataSource">
                                <ItemTemplate>
                                    <div class="col-xs-<%#Eval("colxs") %> col-sm-<%#Eval("colsm") %> col-md-<%#Eval("colmd") %>" data-aos="zoom-out">
                                        <div class="trainer-services">
                                            <a href="<%#Eval("href") %>" class="link-image">
                                                <img class="img-center nice-shadow" src="<%#Eval("src") %>" alt="<%#Eval("Title") %>" title="<%#Eval("Title") %>" />
                                                <h1><%#Eval("Des") %></h1>
                                            </a>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                            <asp:SqlDataSource ID="MainSliderSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DrB_DBConnectionString %>" SelectCommand="SELECT * FROM [sliderimages] WHERE (([SID] = 5 ) and situ=N'show') ORDER BY ordernum; "></asp:SqlDataSource>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <h3>خواندنی ترین‌های هفته</h3>
                        <asp:Repeater ID="TopWeekRepeater" runat="server" DataSourceID="TopWeekSqlDataSource">
                            <ItemTemplate>
                                <div class="row" style="margin-bottom: 15px; border-bottom: 1px solid #fff; padding-bottom: 10px">
                                    <div class="col-sm-2">
                                        <a  href="<%=SSClass.WebDomain %>/<%#MyFunc.SafeString(Eval("cat2title"))%>/<%#MyFunc.SafeString(Eval("header"))%>/<%#Eval("cid") %>">
                                            <img class="zoom" src='<%# Eval("Photo") %>' alt='<%#Eval("Header") %>' title='<%#Eval("Header") %>' style="max-width: 100%" />
                                        </a>
                                    </div>
                                    <div class="col-sm-10">
                                        <a  href="<%=SSClass.WebDomain %>/<%#MyFunc.SafeString(Eval("cat2title"))%>/<%#MyFunc.SafeString(Eval("header"))%>/<%#Eval("cid") %>">
                                            <h1 style="margin: 8px"><%#Eval("Header") %></h1>
                                        </a>
                                        <%--<p class="text-justify small "><%#Eval("Summary")+"" %></p>--%>
                                        <span class="label label-info small"><%# Eval("PageView  ")+" بازدید " %></span>
                                        <span style="color: #ffffff; font-weight: 100!important; margin-right: 5px; float: left" class="small"><%#MyFunc.DisplayDateTime(Eval("cdate")) %></span>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                        <asp:SqlDataSource ID="TopWeekSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DrB_DBConnectionString %>" SelectCommand="SELECT top(6) dbo.[Content].* , dbo.Tree.Name AS cat2title FROM dbo.[Content] INNER JOIN dbo.Tree ON dbo.[Content].cat2 = dbo.Tree.NodeID WHERE cat1=N'fa-blog' and dbo.[Content].cdate<DATEADD(day,7,dbo.[Content].cdate) ORDER BY pageview desc "></asp:SqlDataSource>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid ">
        <div class="container ">
            <hr />
            <div class="row">               
                <div class="col-sm-12">
                    <div class="row">
                        <asp:Repeater runat="server" ID="MoreEventsRepeater" DataSourceID="Top9SqlDataSource">
                            <ItemTemplate>
                                <div class="col-sm-3 text-center" data-aos="zoom-in-up">
                                    <div>
                                        <div class="col-xs-12">
                                            <a  href="<%=SSClass.WebDomain %>/<%#MyFunc.SafeString(Eval("cat2title"))%>/<%#MyFunc.SafeString(Eval("header"))%>/<%#Eval("cid") %>">
                                                <img class="zoom" src='<%# Eval("Photo") %>' alt='<%#Eval("Header") %>' title='<%#Eval("Header") %>' style="max-width: 100%" />
                                            </a>
                                        </div>
                                        <div class="col-xs-12" style="margin-bottom: 15px">
                                            <a  href="<%=SSClass.WebDomain %>/<%#MyFunc.SafeString(Eval("cat2title"))%>/<%#MyFunc.SafeString(Eval("header"))%>/<%#Eval("cid") %>">
                                                <h3 class="NewsH"><%#Eval("Header") %></h3>
                                            </a>
                                            <p class="help-block text-justify small NewsP"><%#Eval("Summary")+"" %></p>
                                            <span class="label label-info small"><%# Eval("PageView  ")+" بازدید " %></span>
                                            <span style="color: #a7a7a7; font-weight: 100!important; margin-right: 5px" class="small"><%#MyFunc.DisplayDate(Eval("cdate")) %></span>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                        <asp:SqlDataSource runat="server" ID="Top9SqlDataSource" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT dbo.[Content].* , dbo.Tree.Name AS cat2title FROM dbo.[Content] INNER JOIN dbo.Tree ON dbo.[Content].cat2 = dbo.Tree.NodeID WHERE cat1=N'fa-blog' and isdeleted=0 order by dbo.[Content].cdate desc">
                            <%-- <SelectParameters>
                                <asp:ControlParameter ControlID="EventIDMainLabel" PropertyName="Text" Name="eventid" Type="Int32"></asp:ControlParameter>
                            </SelectParameters>--%>
                        </asp:SqlDataSource>
                    </div>
                </div>
                <%--<div class="col-sm-2">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="padding: 15px">
                            <h3 class="panel-title">دسته ها</h3>
                        </div>
                        <div class="panel-body text-center">
                            <asp:Repeater runat="server" DataSourceID="ContentSqlDataSource">
                                <ItemTemplate>
                                    <a style="display: block; border-bottom: 1px solid #c2c2c2; padding-bottom: 5px" class="small" target="_blank" href="<%=SSClass.WebDomain %>/<%#Eval("lan") %>/blog/page/<%#Eval("cid") %>/<%#MyFunc.SafeString(Eval("header"))%>">
                                        <img style="margin-bottom: 5px; margin-top: 5px" src='<%#Eval("Photo") %>' alt='<%#Eval("Header") %>' title='<%#Eval("Header") %>' />
                                        <h2 style="font-size: 12px; line-height: 1.5"><%#Eval("Header") %></h2>
                                    </a>
                                </ItemTemplate>
                            </asp:Repeater>
                            <asp:SqlDataSource ID="ContentSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DrB_DBConnectionString %>" SelectCommand="SELECT top(10) * FROM [Content] where cat1=N'fa-blog' order by newid()"></asp:SqlDataSource>
                        </div>
                    </div>
                </div>--%>
                <div class="col-sm-12">
                    <hr />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContentPlaceHolder" runat="Server">
</asp:Content>

