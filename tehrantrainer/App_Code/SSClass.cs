﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SSClass
/// </summary>
public class SSClass
{
    public static string WebDomain = (HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority).Trim('/').ToLower();
    public static string WebDomainFile = (HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority).Trim('/').ToLower();//.Replace("www","file")
    public static string FTitle = "موسسه تهران آموز";
    public static string ETitle = "Tehran Trainer Institute";
    public static string MainLan = "Fa";

    public static string Facebook = "";
    public static string GooglePlus = "";
    public static string Twitter = "https://twitter.com/tehrantrainer";
    public static string Aparat = "";
    public static string Linkedin = "https://www.linkedin.com/in/tehran-trainer-2134b316b/";
    public static string Telegram = "https://t.me/tehrantrainer";
    public static string Instagram = "https://www.instagram.com/tehrantrainer";
    public static string InfoEMail = "info@tehrantrainer.com";
    public static string PhoneNo =  "02136616695";
    public static string PhoneNo2 = "02136616690";
    public static string PhoneNo3 = "";
    public static string MobileNo = "09352881381";
    public static string PostalAddress = "تهران، خیابان آزادی، خیابان اسکندری جنوبی، پلاک 477، طبقه دوم، واحد 24";

    public SSClass()
	{
		//
		// TODO: Add constructor logic here
		//

	}

    
}