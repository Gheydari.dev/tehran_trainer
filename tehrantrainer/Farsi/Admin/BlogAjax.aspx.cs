﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Farsi_Admin_BlogAjax : System.Web.UI.Page
{
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["sqlrev"] == null)
        {
            Session["sqlrev"] = " ";
        }
        string ActString = Request.QueryString["act"].ToString().Trim();
        string ResultLiteralString = "[{\"situ\":\"ok\"},";
        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);
        //string item = e.CommandArgument.ToString();
        string RegSqlSyntax = "";
        switch (ActString)
        {
            case "getphotos":
                //RegSqlSyntax = " select fcat+N' | '+ecat,catid from category where fcat like N'%" + InputString + "%' or ecat like N'%" + InputString + "%' ";
                RegSqlSyntax = Session["sqlrev"] + " select top(20) * from Photos order by cdate desc  ";
                break;
            case "deleteimg":
                string ImgSrcString = Request.QueryString["imgsrc"].ToLower();
                // Response.Write((ImgSrcString).Replace(SSClass.WebDomainFile, "~")); return;
                if (File.Exists(HttpContext.Current.Server.MapPath(ImgSrcString.Replace(SSClass.WebDomainFile, "~"))))
                {
                    File.Delete(Server.MapPath(ImgSrcString.Replace(SSClass.WebDomainFile, "~")));

                }
                RegSqlSyntax = "delete from photos where imgsrc=N'" + ImgSrcString + "'; select top(20) * from Photos order by cdate desc  ";

                //RegSqlSyntax = " select fcat+N' | '+ecat,catid from category where fcat like N'%" + InputString + "%' or ecat like N'%" + InputString + "%' ";
                break;

            default:
                break;
        }
        SqlCommand RegCmd = new SqlCommand(RegSqlSyntax, con);
        SqlParameter[] param = new SqlParameter[1];

        param[00] = new SqlParameter("@input", SqlDbType.NVarChar);

        param[00].Value = "";



        //for (int i = 0; i < param.Length; i++)
        //{
        //    RegCmd.Parameters.Add(param[i]);
        //}

        try
        {
            SqlDataReader Dr;

            con.Open();
            Dr = RegCmd.ExecuteReader();
            PersianDateTime PersianDate = new PersianDateTime(DateTime.Now);
            while (Dr.Read())
            {
                ResultLiteralString += "{ \"ImgTitle\" : \"" + Dr["ImgTitle"].ToString() + "\", \"ImgSrc\" : \"" + Dr["ImgSrc"].ToString() + "\"},";
            }
            ResultLiteralString = ResultLiteralString.Substring(0, ResultLiteralString.Length - 1) + "]";
            Session["sqlrev"] = " ";
            Response.Write(ResultLiteralString);

        }
        catch (System.Data.SqlClient.SqlException ex)
        {
            Response.Write("[{ \"Situ\" : \"" + ex.Message + "\"}]");
        }
        finally
        {
            con.Close();
        }
    }
}