﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NumCaptcha.ascx.cs" Inherits="Controls_Levioza_levcaptcha" %>

<table border="0">
    <tr>
        <td>
            <asp:Image BorderStyle="Solid" BorderColor="#666666" BorderWidth="1" ID="Image1" runat="server" ImageUrl="captcsha.jpg" Height="25px" Width="52px" /></td>
        <td >&nbsp;&nbsp; =&nbsp;</td>
        <td  >
            <asp:TextBox ID="TextBoxNo" runat="server" Width="25px" MaxLength="2"></asp:TextBox>
            <asp:RequiredFieldValidator ID="ReqNo" runat="server" ControlToValidate="TextBoxNo" Display="Dynamic"
                SetFocusOnError="true" Font-Size="Small" ErrorMessage="پاسخ به سوال امنیتی الزامی است!" ForeColor="Red" Style="font-size: small">*</asp:RequiredFieldValidator>
            &nbsp;<asp:Label ID="lblMessage" ForeColor="Red" Font-Size="11px" Font-Bold="False" runat="server" style="font-size: small"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <asp:RegularExpressionValidator ID="RegxNo" ControlToValidate="TextBoxNo" SetFocusOnError="true" Font-Size="11px" Display="Dynamic"
                ValidationExpression="[0-9]{1,2}" runat="server" ErrorMessage="فقط اعداد پذیرفته می شود!" ForeColor="Red" style="font-size: small"></asp:RegularExpressionValidator>
        </td>
    </tr>
</table>
 

