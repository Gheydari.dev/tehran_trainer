﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Farsi/Admin/FaAdminMasterPage.master" AutoEventWireup="true" CodeFile="SMSPanel.aspx.cs" Inherits="Farsi_Admin_SMSPanel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-md-12">
            <div runat="server" id="InfoDiv" class="alert alert-info alert-dismissable text-right">
                <asp:Literal ID="InfoLiteral" runat="server">جهت ارسال پیامک از این قسمت استفاده کنید.</asp:Literal>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
        </div>
        <div class="col-md-6 text-center">
            <asp:TextBox placeholder="شماره موبایل" CssClass="form-control text-right" ID="MobNumsTextBox" runat="server" TextMode="MultiLine" Style="max-width: 100%"></asp:TextBox>
            <hr />
            <asp:TextBox placeholder="متن پیام" CssClass="form-control text-right" ID="MsgTextBox" runat="server" TextMode="MultiLine" Style="max-width: 100%;height:500px"></asp:TextBox>
            <hr />
            <asp:Button CssClass="btn btn-primary " ID="SendSMSButton" runat="server" Text="ارسال پیام" OnClick="SendSMSButton_Click" />
        </div>
        <div class="col-md-3">
        </div>
    </div>
</asp:Content>

