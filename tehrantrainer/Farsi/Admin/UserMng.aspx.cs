﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Farsi_Admin_UserMng : System.Web.UI.Page
{
    protected string DisplayDate(object Date)
    {
        PersianDateTime PersianDate = new PersianDateTime(Convert.ToDateTime(Date));
        return PersianDate.ToString("dddd d MMMM yyyy");
    }
    protected string DisplayDate2(object Date)
    {
        if (Date.ToString() == "")
        {
            return "_";
        }
        else
        {
            PersianDateTime PersianDate = new PersianDateTime(Convert.ToDateTime(Date));


            return PersianDate.ToString(PersianDateTimeFormat.Date);
        }

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //System.Web.UI.HtmlControls.HtmlGenericControl li = (System.Web.UI.HtmlControls.HtmlGenericControl)this.Page.Master.Master.FindControl("BodyContentPlaceHolder").FindControl("UserMng");

        //li.Attributes["class"] = "active";
    }
}