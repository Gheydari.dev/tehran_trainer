﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Farsi_Admin_GenderDefiner : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        System.Web.UI.HtmlControls.HtmlGenericControl li = (System.Web.UI.HtmlControls.HtmlGenericControl)this.Page.Master.Master.FindControl("BodyContentPlaceHolder").FindControl("GenderDefiner");

        li.Attributes["class"] = "active";


    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string genderValue = DataBinder.Eval(e.Row.DataItem, "gender").ToString();

            if (genderValue == "مرد" || genderValue == "زن")
            {
                RadioButtonList rb = (RadioButtonList)e.Row.FindControl("RadioButtonList1");

                rb.Items.FindByText(genderValue.ToString()).Selected = true;
            }
            else
            {

            }

        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataBind();
    }

    protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        RadioButtonList rBtnList = (RadioButtonList)sender;
        GridViewRow gvr = (GridViewRow)rBtnList.Parent.Parent;
        if (rBtnList.SelectedValue == "0")
            SqlDataSource1.UpdateParameters["gender"].DefaultValue = "مرد";
        else if (rBtnList.SelectedValue == "1")
        {
            SqlDataSource1.UpdateParameters["gender"].DefaultValue = "زن";
        }
        //string selectedIndex = GridView1.DataKeys[0].Value.ToString();
        //string customID = (GridView1.DataKeys[selectedIndex]["userid"]).ToString();
        SqlDataSource1.UpdateParameters["userid"].DefaultValue = hdnUserId.Value.ToString();

        SqlDataSource1.Update();
        GridView1.DataBind();



    }



    protected void Button1_Click(object sender, EventArgs e)
    {
        string sql = "";
        for (int i = 0; i < GridView1.Rows.Count; i++)
        {
            RadioButtonList RadioButtonList1 = (RadioButtonList)GridView1.Rows[i].FindControl("RadioButtonList1");
            Label lblUserId = (Label)GridView1.Rows[i].FindControl("lblUserId");
            if (RadioButtonList1.SelectedIndex == 1)
            {
                sql += "update users set gender =N'زن' where userid=" + lblUserId.Text + "   ";
            }
            else if (RadioButtonList1.SelectedIndex == 0)
            {
                sql += "update users set gender =N'مرد' where userid=" + lblUserId.Text + "   ";

            }

        }

        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);


        SqlCommand RegCmd = new SqlCommand(sql, con);
        
        try
        {
            con.Open();
            RegCmd.ExecuteReader();
            GridView1.DataBind();
            //InfoDiv.Visible = true;
            //InfoLiteral.Text = "scode insert.";
            //InfoDiv.Attributes["class"] = " alert alert-success alert-dismissable text-right";
           

            //FNameTextBox.Text =
            //ENameTextBox.Text =
            //PCodeTextBox.Text =
            //ModelTextBox.Text =
            //BrandTextBox.Text =
            //CountryTextBox.Text =
            //YearTextBox.Text =
            //AvailableTextBox.Text =
            //DiscountTextBox.Text =
            //ColorTextBox.Text =
            //PriceTextBox.Text =
            //WeighTextBox.Text =
            //DimensionTextBox.Text = "";
        }
        catch (System.Data.SqlClient.SqlException ex)
        {
            string msg = "RegCmd Error:";
            msg += ex.Message;
            throw new Exception(msg);
        }
        finally
        {
            con.Close();
        }
    }
}