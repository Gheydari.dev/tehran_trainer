﻿<%@ Page Title="" Language="C#" MasterPageFile="~/farsi/Admin/FaAdminMasterPage.master" AutoEventWireup="true" CodeFile="Tree.aspx.cs" Inherits="Farsi_Admin_Tree" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContentPlaceHolder" runat="Server">
    <link href="<%= ResolveUrl("~/css/AllInOne5.css") %>" rel="stylesheet" />
    <style>
        .addnewnode {
            color: #00a3d0 !important;
        }

        .dropdown-submenu {
            position: relative;
        }

            .dropdown-submenu .dropdown-menu {
                top: 0;
                right: 100% !important;
                margin-top: -1px;
            }

        .l1dropdown:hover .l1dropdown-menu {
            display: block;
        }



        .caret {
            display: inline-block;
            width: 0;
            height: 0;
            margin-right: 8px;
            vertical-align: middle;
            border-top: 5px dashed;
            border-color: #bfbfbf;
            border-top: 5px solid\9;
            border-right: 5px solid transparent;
            border-left: 5px solid transparent;
        }

        .l1dropdown > a {
            white-space: nowrap;
            font-weight: 500;
            color: inherit;
        }
    </style>
    <style>
        .telegram-fixed-banner {
            z-index: 1000;
            position: fixed;
            bottom: 10px;
            left: 10px;
        }
        /* Large desktops and laptops */
        @media (min-width: 1200px) {
        }

        /* Landscape tablets and medium desktops */
        @media (min-width: 992px) and (max-width: 1199px) {
        }

        /* Portrait tablets and small desktops */
        @media (min-width: 768px) and (max-width: 991px) {
            .telegram-fixed-banner {
                width: 150px;
            }
        }

        /* Landscape phones and portrait tablets */
        @media (max-width: 767px) {
            .telegram-fixed-banner {
                width: 110px;
            }
        }

        /* Portrait phones and smaller */
        @media (max-width: 480px) {
            .telegram-fixed-banner {
                width: 110px;
            }
        }

        p {
            text-align: justify;
            line-height: 30px;
        }

        h1 {
            font-size: 20px;
        }

        h2 {
            font-size: 17px;
        }

        h3 {
            font-size: 15px;
        }

        h4 {
        }

        h5 {
        }

        h6 {
        }

        .nav > li > a:focus, .nav > li > a:hover {
            text-decoration: none;
            background-color: #eee;
        }

        .navbar-nav > li > a {
            width: auto;
        }

            .navbar-nav > li > a:hover {
                /*background-color: #ffb125;*/
                color: #333 !important;
                background-color: #eee !important;
                text-decoration: none;
                -webkit-transition: background-color 200ms linear;
                -ms-transition: background-color 200ms linear;
                transition: background-color 200ms linear;
            }

        .nav > li {
            text-align: center;
        }

        .is-hover2 {
            color: #333 !important;
            background-color: #eee !important;
            text-decoration: none;
        }

        .sss {
            background-color: #ffffff !important;
            color: white !important;
        }

            .sss > li > a {
                color: #404040;
                line-height: 2;
            }

                .sss > li > a :hover {
                    color: #333 !important;
                    background-color: #eee !important;
                    -webkit-transition: background-color 200ms linear;
                    -ms-transition: background-color 200ms linear;
                    transition: background-color 200ms linear;
                }

        .dropdown-menu > li > a:focus, .dropdown-menu > li > a:hover {
            color: #333 !important;
            background-color: #eee !important;
            text-decoration: none;
            -webkit-transition: background-color 200ms linear;
            -ms-transition: background-color 200ms linear;
            transition: background-color 200ms linear;
        }



        .dropdown-submenu {
            position: relative;
        }

            .dropdown-submenu > .dropdown-menu {
                top: 0;
                right: 100%;
                margin-top: -6px;
                margin-left: -1px;
                -webkit-border-radius: 6px 0 6px 6px;
                -moz-border-radius: 0 6px 6px;
                border-radius: 6px 0 6px 6px;
            }

            .dropdown-submenu:hover > .dropdown-menu {
                display: block;
            }

            .dropdown-submenu > a:after {
                display: block;
                content: " ";
                float: left;
                width: 0;
                height: 0;
                border-color: transparent;
                border-style: solid;
                border-width: 5px 5px 5px 5px;
                border-right-color: #ccc;
                margin-top: 8px;
                margin-left: -10px;
            }

            .dropdown-submenu:hover > a:after {
                border-right-color: #fff;
            }

            .dropdown-submenu.pull-left {
                float: none;
            }

                .dropdown-submenu.pull-left > .dropdown-menu {
                    left: -100%;
                    margin-left: 10px;
                    -webkit-border-radius: 6px 0 6px 6px;
                    -moz-border-radius: 6px 0 6px 6px;
                    border-radius: 6px 0 6px 6px;
                }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div dir="rtl" class="modal fade BYekan" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true" id="MenuItemsModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close pull-left" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <span class="modal-title" id="gridSystemModalLabel">مدیریت آیتم</span>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <asp:Panel ID="Panel2" runat="server" DefaultButton="MenuButton">
                                    <div class="form-group">
                                        <asp:TextBox Text="false" ID="AddNewNodeLabel" CssClass="AddNewNodeLabel" runat="server" Style="display: none" />
                                        <asp:Label Text="عنوان:" CssClass="control-label col-sm-4 input-lg" runat="server" />
                                        <asp:RequiredFieldValidator Style="position: absolute; right: 2px" ValidationGroup="Menu" Font-Size="Small" ID="RequiredFieldValidator9" runat="server" ErrorMessage="" Text="&#9728" ControlToValidate="NameTextBox" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <div class="col-sm-8 input-group">
                                            <asp:TextBox ID="NameTextBox" runat="server" CssClass="input-lg form-control Menu-name" placeholder="عنوان آیتم جهت نمایش در منو" AutoCompleteType="Email"></asp:TextBox>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-tag"></span></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label Text="لینک:" CssClass="control-label col-sm-4 input-lg" runat="server" />
                                        <%--<asp:RequiredFieldValidator Style="position: absolute; right: 2px" ValidationGroup="Menu" Font-Size="Small" ID="RequiredFieldValidator10" runat="server" ErrorMessage="" Text="&#9728" ControlToValidate="HrefTextBox" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                        <div class="col-sm-8 input-group">
                                            <asp:TextBox dir="ltr" ID="HrefTextBox" runat="server" CssClass="input-lg form-control Menu-href" name="tsnPass" placeholder="https://www.google.com"></asp:TextBox>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-link"></span></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label Text="تصویر:" CssClass="control-label col-sm-4 input-lg" runat="server" />
                                        <%--<asp:RequiredFieldValidator Style="position: absolute; right: 2px" ValidationGroup="Menu" Font-Size="Small" ID="RequiredFieldValidator10" runat="server" ErrorMessage="" Text="&#9728" ControlToValidate="HrefTextBox" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                        <div class="col-sm-8 input-group">
                                            <asp:FileUpload ID="ImgFileUpload" runat="server" />
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-picture"></span></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label Text="ترتیب:" CssClass="control-label col-sm-4 input-lg" runat="server" />
                                        <asp:RequiredFieldValidator Style="position: absolute; right: 2px" ValidationGroup="Menu" Font-Size="Small" ID="RequiredFieldValidator2" runat="server" ErrorMessage="" Text="&#9728" ControlToValidate="OrderNumTextBox" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <div class="col-sm-8 input-group">
                                            <asp:TextBox dir="ltr" ID="OrderNumTextBox" runat="server" CssClass="input-lg form-control Menu-ordernum" placeholder="1" AutoCompleteType="Email"></asp:TextBox>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-sort"></span></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label Text="شناسه:" CssClass="control-label col-sm-4 input-lg" runat="server" />
                                        <asp:RequiredFieldValidator Style="position: absolute; right: 2px" ValidationGroup="Menu" Font-Size="Small" ID="RequiredFieldValidator1" runat="server" ErrorMessage="" Text="&#9728" ControlToValidate="NodeIDTextBox" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <div class="col-sm-8 input-group">
                                            <asp:TextBox dir="ltr" ID="NodeIDTextBox" runat="server" CssClass="input-lg form-control Menu-nodeid" placeholder="1" AutoCompleteType="Email"></asp:TextBox>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-barcode"></span></span>
                                        </div>
                                    </div>
                                    <asp:Button ValidationGroup="Menu" CssClass="btn btn-primary btn-block MenuForStorm" ID="MenuButton" runat="server" Text="ثبت" OnClick="MenuButton_Click" />
                                    <div style="padding-top: 10px" class="text-center">
                                        <asp:Label ForeColor="Red" ID="MenuMsgLabel" Visible="false" runat="server" Text=""></asp:Label>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer ">
                    <div class="text-center">
                        <asp:Button ValidationGroup="Menu" CssClass="btn btn-danger btn-block MenuForStorm" ID="DeleteMenuItemButton" runat="server" Text="حذف" OnClick="DeleteMenuItemButton_Click" />
                        <%--<a class="ModalHref" href="http://www.abtingene.com/register">کاربر جدید هستید؟ ثبت نام کنید.</a>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div runat="server" id="InfoDiv" class="alert alert-info alert-dismissable text-right " visible="true">
                <asp:Literal ID="InfoLiteral" runat="server" Text="از این قسمت می توانید منوها و دسته بندی مطالب سایت را مدیریت کنید. ابتدا دسته مورد نظر خود را انتخاب کنید. برای ویرایش هر آیتم روی آن کلیک کنید."></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 text-center">
        </div>
        <div class="col-md-4 text-center gggggg">
            <div class="form-group">
                <asp:DropDownList CssClass="form-control input-sm" ID="MenuLanSelectorDropDownList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="MenuLanSelectorDropDownList_SelectedIndexChanged">
                    <asp:ListItem Text="لطفا انتخاب کنید..." Value="0" />
                    <asp:ListItem Text="منوی اصلی فارسی" Value="Fa-Menu" />
                    <asp:ListItem Text="English main menu" Value="En-Menu" />

                 

                    <asp:ListItem Value="Fa-Pages">صفحات اصلی وبسایت</asp:ListItem>
                    <asp:ListItem Value="Fa-Event">رویدادها</asp:ListItem>
                    <asp:ListItem Value="Fa-Product">محصولات</asp:ListItem>
                    <asp:ListItem Value="Fa-Blog">بلاگ</asp:ListItem>
                    <asp:ListItem Value="Fa-News">اخبار</asp:ListItem>
                    <asp:ListItem Value="Fa-Other">سایر</asp:ListItem>

                    <asp:ListItem Value="En-Pages">Website main pages</asp:ListItem>
                    <asp:ListItem Value="En-Event">Events</asp:ListItem>
                    <asp:ListItem Value="En-Product">Products</asp:ListItem>
                    <asp:ListItem Value="En-Blog">Blog</asp:ListItem>
                    <asp:ListItem Value="En-News">News</asp:ListItem>
                    <asp:ListItem Value="En-Other">Other</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-md-4 text-center">
            <asp:TreeView ID="TreeView1" runat="server">
            </asp:TreeView>
        </div>
    </div>
    <hr />

    <div class="row visible-xs">
        <div class="col-md-12">
            <span onclick='$(this).parent().next().slideToggle(100);$(this).children().first().toggleClass("glyphicon-menu-down glyphicon-menu-up")' style="border-radius: 0px" class="btn btn-warning btn-block"><span style="margin-left: 4px" class="glyphicon glyphicon-menu-down"></span><span>مشاهده منو</span></span>
        </div>
        <div class="col-md-12 ShowCat1" style="display: none; margin-top: 10px">
            <asp:Repeater runat="server" DataSourceID="PhoneCat1SqlDataSource">
                <ItemTemplate>
                    <asp:Label Text='<%#Eval("cat1") %>' runat="server" ID="PhoneCat1Label" Visible="false" />
                    <span onclick='$(this).next().slideToggle(100);$(this).children().first().toggleClass("glyphicon-menu-down glyphicon-menu-up")' style="border-radius: 0px" class="btn btn-warning btn-block text-right"><span style="margin-left: 4px" class="glyphicon glyphicon-menu-down" runat="server" visible='<%#(Eval("sub").ToString()=="True"?true:false) %>'></span><span><%#Eval("cat1") %></span></span>
                    <div class="col-md-12" style="display: none">
                        <asp:Repeater runat="server" DataSourceID="PhoneCat2SqlDataSource">
                            <ItemTemplate>
                                <a style="text-decoration: none" href="<%#Eval("Cat2Href")%>"><span style="text-align: right!important; padding-right: 10px; border-radius: 0px" class="btn btn-warning btn-block "><span style="margin-left: 5px"></span><%#Eval("cat2") %></span></a>
                            </ItemTemplate>
                        </asp:Repeater>
                        <asp:SqlDataSource runat="server" ID="PhoneCat2SqlDataSource" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT *,count(cat2) OVER ()  as ItemCount FROM [Cat2Menu]  where cat1id=@cat1id  order by [Cat2order]">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="Cat1IDLabel" PropertyName="Text" Name="cat1id"></asp:ControlParameter>
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <asp:Label Text='<%#Eval("cat1id") %>' runat="server" Visible="false" ID="Cat1IDLabel" />
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            <asp:SqlDataSource runat="server" ID="PhoneCat1SqlDataSource" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT * FROM [Cat1Menu] where lan=N'fa' order by [Cat1order]"></asp:SqlDataSource>
            <hr />
        </div>
    </div>
    <div class="row hidden-xs" runat="server" id="MenuDiv" visible="false">
        <div class="col-md-12">
            <nav style="padding: 0px; margin: 0px;" class="navbar" role="navigation">
                <asp:Literal ID="MenuLiteral" Text="" runat="server" />
                <%--<ul class="nav navbar-nav navbar-right text-right">
                    <asp:Repeater runat="server" ID="L1Repeater" DataSourceID="L1SqlDataSource">
                        <ItemTemplate>
                            <li class="dropdown pull-right l1dropdown">
                                <a style="white-space: nowrap; font-weight: 500; color: inherit;" href="<%#Eval("href")%>"><b runat="server" class="caret" visible='<%#(Eval("sub").ToString()=="True"?true:false) %>'></b><%#Eval("name")%></a>
                                <asp:Repeater ID="L2Repeater" runat="server" DataSourceID="L2SqlDataSource">
                                    <ItemTemplate>
                                        <%#InsertStartUl(Container.ItemIndex+1) %>
                                        <li class="l2dropdown  <%#(Eval("sub").ToString()=="True"?"dropdown-submenu":"ops") %>"><a href="<%#Eval("Href")%>"><%#Eval("name")%></a>
                                            <asp:Repeater ID="L3Repeater" runat="server" DataSourceID="L3SqlDataSource">
                                                <ItemTemplate>
                                                    <%#InsertStartUl2(Container.ItemIndex+1) %>
                                                    <li class="l3dropdown"><a href="<%#Eval("Href")%>"><%#Eval("name")%></a></li>
                                                    <%#InsertEndUl(Container.ItemIndex+1,Eval("ItemCount")) %>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <asp:SqlDataSource runat="server" ID="L3SqlDataSource" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT *,count(*) OVER ()  as ItemCount FROM [tree]  where ParentID=@ParentID  order by [ordernum]">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="ParentIDLabel" PropertyName="Text" Name="ParentID"></asp:ControlParameter>
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                            <asp:Label Text='<%#Eval("NodeID") %>' runat="server" Visible="false" ID="ParentIDLabel" />
                                        </li>
                                        <%#InsertEndUl(Container.ItemIndex+1,Eval("ItemCount")) %>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:SqlDataSource runat="server" ID="L2SqlDataSource" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT *,count(*) OVER ()  as ItemCount FROM [tree]  where ParentID=@ParentID  order by [ordernum]">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="ParentIDLabel" PropertyName="Text" Name="ParentID"></asp:ControlParameter>
                                    </SelectParameters>
                                </asp:SqlDataSource>
                                <asp:Label Text='<%#Eval("NodeID") %>' runat="server" Visible="false" ID="ParentIDLabel" />
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                    <asp:SqlDataSource runat="server" ID="L1SqlDataSource" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT * FROM [tree] where lan=N'fa' and ParentID=0 order by [ordernum]"></asp:SqlDataSource>
                </ul>--%>
                <div class="navbar-header pull-left">

                    <%--<a class="navbar-brand" style="font-size: 17px; padding: 10px 5px" href="http://www.abtingene.com/fa/default.aspx">
                                    <img src="http://www.abtingene.com/Images/Design/fa-Version.png" alt="researchimpact-Persian" title="مشاهده نسخه فارسی" style="width: 17px;" />
                                </a>--%>
                </div>
            </nav>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContentPlaceHolder" runat="Server">
    <script type="text/javascript">
        $(document).ready(function (e) {
            $('.sss').on('mouseover', function () {
                $(this).prev().addClass('is-hover2');
            }).on('mouseout', function () {
                $(this).prev().removeClass('is-hover2');
            });

            $('.test').on('click', function (e) {
                //$(this).prev().addClass('is-hover2');
                $('.Menu-name').val($(this).text());
                $('.Menu-nodeid').val($(this).attr('data-nodeid'));
                $('.Menu-href').val($(this).attr('data-href'));
                $('.Menu-ordernum').val($(this).attr('data-ordernum'));
                $('#MenuItemsModal').modal('show');
                $('#MenuItemsModal').on('shown.bs.modal', function () {
                    $('.Menu-Name').focus()
                })
                if ($(this).hasClass('addnewnode')) {
                    $('.AddNewNodeLabel').val('true');
                }
                else {
                    $('.AddNewNodeLabel').val('false');
                }
            })

            var val = getUrlVars()["val"];
            if (val != null && $('#<%=MenuLanSelectorDropDownList.ClientID%>').val() == 0) {
                $('#<%=MenuLanSelectorDropDownList.ClientID%>').val(val).change()
            }

        });
        function getUrlVars() {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }
    </script>
</asp:Content>



