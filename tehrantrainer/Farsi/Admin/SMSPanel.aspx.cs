﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Farsi_Admin_SMSPanel : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        System.Web.UI.HtmlControls.HtmlGenericControl li = (System.Web.UI.HtmlControls.HtmlGenericControl)this.Page.Master.Master.FindControl("BodyContentPlaceHolder").FindControl("SMSPanel");

        li.Attributes["class"] = "active";

        if (Request.QueryString["ordersId"] != null)
        {
            DataLayer dl = new DataLayer();
            string CS = dl.dataLayerConnectionString;
            SqlConnection con = new SqlConnection(CS);
            con.Open();

            string query = @"Declare @OrderDate nvarchar(10) 
                                select @OrderDate=CONVERT(VARCHAR(10), CDate , 111) from Orders 
                                select Users.Name+' '+Users.Family as FullName ,Users.Gender, Orders.OrdersId, Orders.ShippingMode, CDate=dbo.GregorianToPersian(@OrderDate) 
                                from users inner join orders on users.userid=orders.userid 
                                where ordersid=" + Request.QueryString["ordersId"];

            SqlDataAdapter da = new SqlDataAdapter(query, con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            string FullName = dt.Rows[0]["FullName"].ToString();
            string Gender = dt.Rows[0]["Gender"].ToString();
            string ShippingMode = dt.Rows[0]["ShippingMode"].ToString();
            string OrdersId = dt.Rows[0]["OrdersId"].ToString();
            string CDate = dt.Rows[0]["CDate"].ToString();

            if (Gender == "مرد")
            {
                Gender = "جناب آقای";
            }
            else
            {
                Gender = "سرکار خانم";
            }

            if (ShippingMode == "")
            {
                ShippingMode = "-";
            }

            switch (ShippingMode)
            {
                case "ارسال رایگان مبل و دکور فقط برای شهر تهران!": ShippingMode = "پیک مبل و دکور"; break;
                case "ارسال با تیپاکس به صورت پس کرایه (درب منزل)": ShippingMode = "تیپاکس"; break;
                case "ارسال با پست پیش تاز به صورت پس کرایه(درب منزل)": ShippingMode = "پست پیش تاز"; break;
                case "ارسال با باربری به صورت پیش کرایه (تا نزدیکترین باربری به محل)": ShippingMode = "باربری"; break;

                default:
                    break;
            }
            MsgTextBox.Text = Gender + " " + FullName + " با سلام سفارش شما از طریق " + ShippingMode + " با شماره پیگیری ....... در تاریخ " + CDate + " ارسال گردید.";
            MsgTextBox.Text += " با تشکر از خرید شما.";
            MsgTextBox.Text += " وبسایت مبل و دکور.";
        }
    }
    protected void SendSMSButton_Click(object sender, EventArgs e)
    {
        int retval = -1;
        try
        {
            WebReference.Send sms = new WebReference.Send();
            long[] rec = null;
            byte[] status = null;
            retval = sms.SendSms("9358177180", "4837", MobNumsTextBox.Text.Split('\n'), "50001333263333", MsgTextBox.Text, false, "", ref rec, ref status);

        }
        catch (Exception)
        {
            InfoDiv.Visible = true;
            InfoLiteral.Text = "خطا در انجام عملیات. کد خطا: " + retval.ToString();
            InfoDiv.Attributes["class"] = " alert alert-danger alert-dismissable text-right";
        }
        finally
        {
            if (retval == 1)
            {
                InfoDiv.Visible = true;
                InfoLiteral.Text = "پیام شما با موفقیت ارسال شد.";
                InfoDiv.Attributes["class"] = " alert alert-success alert-dismissable text-right";
            }
            else
            {
                InfoDiv.Visible = true;
                InfoLiteral.Text = "خطا در ارسال. کد خطا: " + retval.ToString();
                InfoDiv.Attributes["class"] = " alert alert-danger alert-dismissable text-right";
            }
        }
    }
}