﻿<%@ Page Title="" Language="C#" MasterPageFile="~/farsi/FaMasterPage.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Farsi_Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContentPlaceHolder" runat="Server">
    <div class="container-fluid">
        <div class="container">
            <div class="row" style="margin-top:70px">
                <div class="col-md-4">
                </div>
                <div class="col-md-4 text-right">
                    <asp:Panel ID="Panel1" runat="server" DefaultButton="LoginButton">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="padding-bottom: 0px; padding-right: 0px">
                                <label class="BYekan" style="padding-right: 15px">ورود مدیر</label>
                            </div>
                            <div style="padding: 15px; font-size: small" class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group ">
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="!نام کاربری" ControlToValidate="UserNameTextBox" ForeColor="Red" Display="Dynamic">*</asp:RequiredFieldValidator>
                                            <asp:TextBox runat="server" class="form-control input-sm" ID="UserNameTextBox" placeholder="نام کاربری"></asp:TextBox>
                                        </div>
                                        <div class="form-group ">
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="!کلمه عبور" ControlToValidate="PasswordTextBox" ForeColor="Red" Display="Dynamic">*</asp:RequiredFieldValidator>
                                            <asp:TextBox runat="server" TextMode="Password" class="form-control input-sm" ID="PasswordTextBox" placeholder="کلمه عبور"></asp:TextBox>
                                        </div>

                                        <%--<a href="<%=SSClass.WebDomain %>/PasswordRecovery">
                                            <p style="color: #0094ff" class="help-block"><small>کلمه عبور را فراموش کرده ام!</small></p>
                                        </a>--%>
                                        <div class="form-group text-center">
                                            <asp:Button CssClass="btn btn-primary btn-sm btn-block" ID="LoginButton" runat="server" Text="ورود به بخش مدیریت" OnClick="LoginButton_Click" />
                                            <asp:Label ID="MsgLabel" runat="server" ForeColor="Red" Font-Size="Smaller"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <div class="col-md-4">
                </div>
            </div>
        </div>
    </div>
</asp:Content>

