﻿<%@ Page Title="" Language="C#" MasterPageFile="~/farsi/UserMasterPage.master" AutoEventWireup="true" CodeFile="UserProfile.aspx.cs" Inherits="Farsi_UserProfile" %>


<asp:Content ID="Content1" ContentPlaceHolderID="BodyContentPlaceHolder" runat="Server">
    <div class="row BYekan">
        <div class="col-md-12">
            <h2>اطلاعات حساب کاربری</h2>
            <div runat="server" id="InfoDiv" visible="false" class="alert alert-danger alert-dismissable text-right">
                <asp:Literal ID="InfoLiteral" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <hr />
    <div class="row" style="padding-bottom:150px">
        <div class="col-md-12">
            <asp:Repeater ID="UserPersonalInfoRepeater" runat="server" DataSourceID="UserPersonalInfoSqlDataSource">
                <ItemTemplate>
                    <div class="row">
                        <div class="col-sm-6" style="padding-bottom: 15px">
                            <asp:Label CssClass="FieldLabel" ID="Label1" runat="server" Text="نام و نام خانوادگی:"></asp:Label>
                            <asp:Label CssClass="" ID="Label2" runat="server" Text='<%#Eval("Name")+" "+Eval("Family") %>'></asp:Label>
                        </div>
                        <div class="col-sm-6" style="padding-bottom: 15px">
                            <asp:Label CssClass="FieldLabel" ID="Label15" runat="server" Text="تاریخ تولد:"></asp:Label>
                            <asp:Label CssClass="" ID="Label16" runat="server" Text='<%#DisplayDate(Eval("Birthday"))%>'></asp:Label>
                        </div>
                        <div class="col-sm-6" style="padding-bottom: 15px">
                            <asp:Label CssClass="FieldLabel" ID="Label5" runat="server" Text="پست الکترونیک:"></asp:Label>
                            <asp:Label CssClass="" ID="Label6" runat="server" Text='<%#Eval("Email") %>'></asp:Label>
                        </div>
                        <div class="col-sm-6" style="padding-bottom: 15px">
                            <asp:Label CssClass="FieldLabel" ID="Label11" runat="server" Text="جنسیت:"></asp:Label>
                            <asp:Label CssClass="" ID="Label12" runat="server" Text='<%#Eval("Gender") %>'></asp:Label>
                        </div>
                        <div class="col-sm-6" style="padding-bottom: 15px">
                            <asp:Label CssClass="FieldLabel" ID="Label13" runat="server" Text="کد ملی:"></asp:Label>
                            <asp:Label CssClass="" ID="Label14" runat="server" Text='<%#Eval("NationalCode") %>'></asp:Label>
                        </div>
                        <div class="col-sm-6" style="padding-bottom: 15px">
                            <asp:Label CssClass="FieldLabel" ID="Label19" runat="server" Text="دریافت خبرنامه:"></asp:Label>
                            <asp:Label CssClass="" ID="Label20" runat="server" Text='<%#Eval("NewsLetter") %>'></asp:Label>
                        </div>
                        <div class="col-sm-6" style="padding-bottom: 15px">
                            <asp:Label CssClass="FieldLabel" ID="Label9" runat="server" Text="تلفن ثابت:"></asp:Label>
                            <asp:Label CssClass="" ID="Label10" runat="server" Text='<%#Eval("HomePhone") %>'></asp:Label>
                        </div>
                        <div class="col-sm-6" style="padding-bottom: 15px">
                            <asp:Label CssClass="FieldLabel" ID="Label17" runat="server" Text="شماره کارت:"></asp:Label>
                            <asp:Label CssClass="" ID="Label18" runat="server" Text='<%#Eval("BankCard") %>'></asp:Label>
                        </div>
                        <div class="col-sm-6" style="padding-bottom: 15px">
                            <asp:Label CssClass="FieldLabel" ID="Label7" runat="server" Text="تلفن همراه:"></asp:Label>
                            <asp:Label CssClass="" ID="Label8" runat="server" Text='<%#Eval("MobPhone") %>'></asp:Label>
                        </div>
                        <%--<div class="col-sm-6" style="padding-bottom: 15px">
                            <asp:Label CssClass="FieldLabel" ID="Label27" runat="server" Text="سطح تحصیلات:"></asp:Label>
                            <asp:Label CssClass="" Style="font-family: Tahoma" ID="Label28" runat="server" Text='<%#Eval("Edu") %>'></asp:Label>
                        </div>
                        <div class="col-sm-6" style="padding-bottom: 15px">
                            <asp:Label CssClass="FieldLabel" ID="Label29" runat="server" Text="دانشگاه محل اخذ:"></asp:Label>
                            <asp:Label CssClass="" Style="font-family: Tahoma" ID="Label30" runat="server" Text='<%#Eval("Uni") %>'></asp:Label>
                        </div>
                        <div class="col-sm-6" style="padding-bottom: 15px">
                            <asp:Label CssClass="FieldLabel" ID="Label31" runat="server" Text="وضعیت تاهل:"></asp:Label>
                            <asp:Label CssClass="" Style="font-family: Tahoma" ID="Label32" runat="server" Text='<%#Eval("Marriage") %>'></asp:Label>
                        </div>--%>
                    </div>
                    <div class="row">
                        <div class="col-md-12" style="padding-bottom: 15px">
                            <asp:Label CssClass="FieldLabel" ID="Label21" runat="server" Text="محل سکونت:"></asp:Label>
                            <asp:Label CssClass="" ID="Label22" runat="server" Text='<%#Eval("ostan")+", "+Eval("shahrestan")+", "+Eval("HomeAddress") %>'></asp:Label>
                        </div>
                        <%--<div class="col-md-12" style="padding-bottom: 15px">
                            <asp:Label CssClass="FieldLabel" ID="Label23" runat="server" Text="درباره من:"></asp:Label>
                            <br />
                            <asp:Label CssClass="" ID="Label24" runat="server" Text='<%#Eval("AboutMe") %>'></asp:Label>
                        </div>--%>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-left">
                                                    <hr />

                            <%--<asp:Button Visible='<%#SetEditorVisible(Eval("regsitu")) %>' OnClick="ToRevDashboardButton_Click" Enabled="true" Font-Bold="true" Text="مایل به همکاری در گروه نقد و بررسی تهران آموز هستم" runat="server" ID="ToRevDashboardButton" CssClass="btn btn-info BYekan" />
                                            <asp:Button Visible='<%#!SetEditorVisible(Eval("regsitu")) %>' OnClick="ToRevDashboardButton_Click" Enabled="true" Font-Bold="true" Text="ورود به پنل نقد و بررسی" runat="server" ID="Button1" CssClass="btn btn-info BYekan" />--%>
                            <asp:Button OnClick="EditUserPersonalInfoButton_Click" Enabled="true" Text="ویرایش اطلاعات فردی" runat="server" ID="EditUserPersonalInfoButton" CssClass="btn btn-default" />
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            <asp:SqlDataSource runat="server" ID="UserPersonalInfoSqlDataSource" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT * FROM [Users] WHERE ([UserID] = @UserID)">
                <SelectParameters>
                    <asp:SessionParameter SessionField="userid" Name="UserID" Type="Int32"></asp:SessionParameter>
                </SelectParameters>
            </asp:SqlDataSource>
            <div runat="server" id="EditUserPersonalInfoDiv" visible="false">
                <div class="row">
                    <div class="col-md-3" style="padding-bottom: 15px">
                        <asp:Label CssClass="FieldLabel" ID="Label1" runat="server" Text="نام:" Enabled="true"></asp:Label>
                        <asp:TextBox CssClass="form-control " ID="NameTextBox" runat="server" Text=""></asp:TextBox>
                    </div>
                    <div class="col-md-3" style="padding-bottom: 15px">
                        <asp:Label CssClass="FieldLabel" ID="Label3" runat="server" Text="نام خانوادگی:"></asp:Label>
                        <asp:TextBox CssClass="form-control " ID="FamilyTextBox1" runat="server" Text="" Enabled="true"></asp:TextBox>
                    </div>
                    <div class="col-md-6" style="padding-bottom: 15px">
                        <asp:Label CssClass="FieldLabel" ID="Label15" runat="server" Text="تاریخ تولد:"></asp:Label>
                        <asp:TextBox CssClass="form-control " ID="BirthdayTextBox6" runat="server" Text=""></asp:TextBox>
                    </div>
                    <div class="col-md-6" style="padding-bottom: 15px;">
                        <asp:Label CssClass="FieldLabel" ID="Label5" runat="server" Text="پست الکترونیک:"></asp:Label>
                        <asp:TextBox CssClass="form-control " ID="EmailTextBox9" runat="server" Text=""></asp:TextBox>
                    </div>
                    <div class="col-md-6" style="padding-bottom: 15px">
                        <asp:Label CssClass="FieldLabel" ID="Label11" runat="server" Text="جنسیت:"></asp:Label>
                        <asp:DropDownList CssClass="form-control " ID="GenderDropDownList3" runat="server">
                            <asp:ListItem>زن</asp:ListItem>
                            <asp:ListItem>مرد</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-6" style="padding-bottom: 15px">
                        <asp:Label CssClass="FieldLabel" ID="Label13" runat="server" Text="کد ملی:"></asp:Label>
                        <asp:TextBox CssClass="form-control " ID="NationalCodeTextBox3" runat="server" Text=""></asp:TextBox>
                    </div>
                    <div class="col-md-6" style="padding-bottom: 15px">
                        <asp:Label CssClass="FieldLabel" ID="Label19" runat="server" Text="دریافت خبرنامه:"></asp:Label>
                        <asp:DropDownList CssClass="form-control " ID="NewsLetterDropDownList1" runat="server">
                            <asp:ListItem Value="بلی" Text="بلی"></asp:ListItem>
                            <asp:ListItem Value="خیر" Text="خیر"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-6" style="padding-bottom: 15px">
                        <asp:Label CssClass="FieldLabel" ID="Label9" runat="server" Text="تلفن ثابت:"></asp:Label>
                        <asp:TextBox CssClass="form-control " ID="HomePhoneTextBox5" runat="server" Text=""></asp:TextBox>
                    </div>
                    <div class="col-md-6" style="padding-bottom: 15px">
                        <asp:Label CssClass="FieldLabel" ID="Label17" runat="server" Text="شماره کارت بانکی:"></asp:Label>
                        <asp:TextBox CssClass="form-control " ID="BankCardTextBox7" runat="server" Text=""></asp:TextBox>
                    </div>
                    <div class="col-md-6" style="padding-bottom: 15px">
                        <asp:Label CssClass="FieldLabel" ID="Label7" runat="server" Text="تلفن همراه:"></asp:Label>
                        <asp:TextBox CssClass="form-control " ID="MobPhoneTextBox4" runat="server" Text="" Enabled="false"></asp:TextBox>
                    </div>
                    <%--<div class="col-md-3" style="padding-bottom: 15px">
                        <asp:Label CssClass="FieldLabel" ID="Label27" runat="server" Text="سطح تحصیلات:"></asp:Label>
                        <asp:TextBox CssClass="form-control " ID="EduTextBox1" runat="server" Text=""></asp:TextBox>
                    </div>
                    <div class="col-md-3" style="padding-bottom: 15px">
                        <asp:Label CssClass="FieldLabel" ID="Label29" runat="server" Text="دانشگاه محل اخذ:"></asp:Label>
                        <asp:TextBox CssClass="form-control " ID="UniTextBox2" runat="server" Text=""></asp:TextBox>
                    </div>
                    <div class="col-md-3" style="padding-bottom: 15px">
                        <asp:Label CssClass="FieldLabel" ID="Label31" runat="server" Text="وضعیت تاهل:"></asp:Label>
                        <asp:TextBox CssClass="form-control " ID="MarriageTextBox3" runat="server" Text=""></asp:TextBox>
                    </div>--%>
                </div>
                <div class="row">
                    <div class="col-md-12" style="padding-bottom: 15px">
                        <div class="row">
                            <div class="col-sm-12">
                                <asp:Label CssClass="FieldLabel" ID="Label21" runat="server" Text="محل سکونت:"></asp:Label>
                            </div>
                            <div class="col-sm-6">
                                <asp:DropDownList AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control" ID="OstanDropDownList" runat="server" DataSourceID="OstanSqlDataSource" DataTextField="ostan" DataValueField="ostan">
                                    <asp:ListItem Text="استان مورد نظر را انتخاب کنید..." Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:SqlDataSource runat="server" ID="OstanSqlDataSource" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT DISTINCT [ostan] FROM [Iran] order by ostan"></asp:SqlDataSource>
                                <asp:RequiredFieldValidator InitialValue="0" ValidationGroup="address" Font-Size="Small" ID="RequiredFieldValidator5" runat="server" Text="انتخاب استان مقصد الزامیست" ControlToValidate="OstanDropDownList" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                            </div>
                            <div class="col-sm-6">
                                <asp:DropDownList AutoPostBack="true" CssClass="form-control " ID="ShahrestanDropDownList" runat="server" DataSourceID="ShahrestanSqlDataSource1" DataTextField="shahrestan" DataValueField="shahrestan" OnDataBound="shahrestanDropDownList1_DataBound">
                                </asp:DropDownList>
                                <asp:SqlDataSource runat="server" ID="ShahrestanSqlDataSource1" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT DISTINCT [shahrestan] FROM [Iran] WHERE ([ostan] = @ostan) order by shahrestan">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="OstanDropDownList" PropertyName="SelectedValue" Name="ostan" Type="String"></asp:ControlParameter>
                                    </SelectParameters>
                                </asp:SqlDataSource>
                                <asp:RequiredFieldValidator InitialValue="0" ValidationGroup="address" Font-Size="Small" ID="RequiredFieldValidator6" runat="server" Text="انتخاب شهرستان مقصد الزامیست" ControlToValidate="ShahrestanDropDownList" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                            </div>
                            <div class="col-sm-12">
                                <asp:TextBox CssClass="form-control " ID="HomeAddressTextBox10" runat="server" Placeholder="ادامه ی نشانی"></asp:TextBox>
                            </div>
                        </div>

                    </div>
                    <%--<div class="col-md-12" style="padding-bottom: 15px">
                        <asp:Label CssClass="FieldLabel" ID="Label23" runat="server" Text="درباره من:"></asp:Label>
                        <asp:TextBox TextMode="MultiLine" Style="max-width: 100%; min-width: 100%; min-height: 150px" CssClass="form-control " ID="AboutMeTextBox" runat="server" Text=""></asp:TextBox>
                    </div>--%>
                </div>
                <div class="row">
                    <div class="col-md-12 text-left">
                                            <hr />

                        <asp:Button OnClick="CanselButton_Click" Enabled="true" Text="انصراف" runat="server" ID="CanselButton" CssClass="btn btn-default" />
                        <asp:Button OnClick="UpdateUserPersonalInfoButton_Click" Enabled="true" Text="ذخیره تغییرات" runat="server" ID="UpdateUserPersonalInfoButton" CssClass="btn btn-success" />
                    </div>
                </div>
            </div>
        </div>
    </div>   
</asp:Content>

