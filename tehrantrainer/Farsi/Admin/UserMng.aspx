﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Farsi/Admin/FaAdminMasterPage.master" AutoEventWireup="true" CodeFile="UserMng.aspx.cs" Inherits="Farsi_Admin_UserMng" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container-fluid ">
        <div class="container">
            <div class="row ">
                <div class="col-md-12" style="overflow: auto">

                    <asp:GridView CssClass="text-center table table-bordered table-condensed" ID="GridView2" runat="server" AutoGenerateColumns="False" DataKeyNames="UserID" DataSourceID="SqlDataSource1" EmptyDataText="There are no data records to display." AllowPaging="True" PageSize="100" AllowSorting="True">
                        <Columns>
                            <asp:TemplateField HeaderText="ر">
                                <HeaderStyle CssClass="text-center" Font-Bold="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="پروفایل">
                                <HeaderStyle CssClass="text-center" Font-Bold="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                <ItemTemplate>
                                    <span style="cursor: pointer" data-toggle="modal" data-target="#<%#Container.DataItemIndex.ToString()+"UserProfile" %>" class="glyphicon glyphicon-user "></span>
                                    <div class="modal fade BYekan" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true" id="<%#Container.DataItemIndex.ToString()+"UserProfile" %>">
                                        <div class="modal-dialog" style="width: 80%">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close pull-left" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="gridSystemModalLabel">پروفایل کاربر</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <asp:Repeater ID="UserProfileRepeater" runat="server" DataSourceID="UserProfileSqlDataSource">
                                                        <ItemTemplate>
                                                            <div class="container-fluid text-right">
                                                                <div class="row">
                                                                    <div class="col-sm-6" style="padding-bottom: 15px">
                                                                        <asp:Label CssClass="FieldLabel" ID="Label1" runat="server" Text="نام و نام خانوادگی:"></asp:Label>
                                                                        <asp:Label CssClass="" ID="Label2" runat="server" Text='<%#Eval("Name")+" "+Eval("Family") %>'></asp:Label>
                                                                    </div>
                                                                    <%--<div class="col-sm-6" style="padding-bottom: 15px">
                                                                        <asp:Label CssClass="FieldLabel" ID="Label15" runat="server" Text="تاریخ تولد:"></asp:Label>
                                                                        <asp:Label CssClass="" ID="Label16" runat="server" Text='<%#DisplayDate(Eval("BDate"))%>'></asp:Label>
                                                                    </div>--%>
                                                                    <div class="col-sm-6" style="padding-bottom: 15px">
                                                                        <asp:Label CssClass="FieldLabel" ID="Label5" runat="server" Text="پست الکترونیک:"></asp:Label>
                                                                        <asp:Label CssClass="" ID="Label6" runat="server" Text='<%#Eval("Email") %>'></asp:Label>
                                                                    </div>
                                                                    <div class="col-sm-6" style="padding-bottom: 15px">
                                                                        <asp:Label CssClass="FieldLabel" ID="Label11" runat="server" Text="جنسیت:"></asp:Label>
                                                                        <asp:Label CssClass="" ID="Label12" runat="server" Text='<%#Eval("Gender") %>'></asp:Label>
                                                                    </div>
                                                                    <%--<div class="col-sm-6" style="padding-bottom: 15px">
                            <asp:Label CssClass="FieldLabel" ID="Label13" runat="server" Text="کد ملی:"></asp:Label>
                            <asp:Label CssClass="" ID="Label14" runat="server" Text='<%#Eval("NationalCode") %>'></asp:Label>
                        </div>--%>
                                                                    <%--<div class="col-sm-6" style="padding-bottom: 15px">
                            <asp:Label CssClass="FieldLabel" ID="Label19" runat="server" Text="دریافت خبرنامه:"></asp:Label>
                            <asp:Label CssClass="" ID="Label20" runat="server" Text='<%#Eval("NewsLetter") %>'></asp:Label>
                        </div>--%>
                                                                    <%--<div class="col-sm-6" style="padding-bottom: 15px">
                                                                        <asp:Label CssClass="FieldLabel" ID="Label9" runat="server" Text="تلفن ثابت:"></asp:Label>
                                                                        <asp:Label CssClass="" ID="Label10" runat="server" Text='<%#Eval("FixPhone") %>'></asp:Label>
                                                                    </div>--%>
                                                                    <%--<div class="col-sm-6" style="padding-bottom: 15px">
                                                                        <asp:Label CssClass="FieldLabel" ID="Label27" runat="server" Text="مقطع تحصیلی:"></asp:Label>
                                                                        <asp:Label CssClass="" Style="font-family: Tahoma" ID="Label28" runat="server" Text='<%#Eval("EducationGrade") %>'></asp:Label>
                                                                    </div>--%>
                                                                    <div class="col-sm-6" style="padding-bottom: 15px">
                                                                        <asp:Label CssClass="FieldLabel" ID="Label7" runat="server" Text="تلفن همراه:"></asp:Label>
                                                                        <asp:Label CssClass="" ID="Label8" runat="server" Text='<%#Eval("MobPhone") %>'></asp:Label>
                                                                    </div>

                                                                    <%--<div class="col-sm-6" style="padding-bottom: 15px">
                                                                        <asp:Label CssClass="FieldLabel" ID="Label29" runat="server" Text="دانشگاه / موسسه آموزشی:"></asp:Label>
                                                                        <asp:Label CssClass="" Style="font-family: Tahoma" ID="Label30" runat="server" Text='<%#Eval("University") %>'></asp:Label>
                                                                    </div>--%>
                                                                    <div class="col-sm-6" style="padding-bottom: 15px">
                                                                        <asp:Label CssClass="FieldLabel" ID="Label17" runat="server" Text="شماره کارت بانکی:"></asp:Label>
                                                                        <asp:Label CssClass="" ID="Label18" runat="server" Text='<%#Eval("BankCard") %>' dir="ltr"></asp:Label>
                                                                    </div>
                                                                    <%--<div class="col-sm-6" style="padding-bottom: 15px">
                                                                        <asp:Label CssClass="FieldLabel" ID="Label4" runat="server" Text="رشته تحصیلی:"></asp:Label>
                                                                        <asp:Label CssClass="" Style="font-family: Tahoma" ID="Label13" runat="server" Text='<%#Eval("EducationField") %>'></asp:Label>
                                                                    </div>--%>
                                                                    <%--<div class="col-sm-6" style="padding-bottom: 15px">
                            <asp:Label CssClass="FieldLabel" ID="Label31" runat="server" Text="وضعیت تاهل:"></asp:Label>
                            <asp:Label CssClass="" Style="font-family: Tahoma" ID="Label32" runat="server" Text='<%#Eval("Marriage") %>'></asp:Label>
                        </div>--%>
                                                                </div>
                                                                <%--<div class="row">
                                                                    <div class="col-md-12" style="padding-bottom: 15px">
                                                                        <asp:Label CssClass="FieldLabel" ID="Label21" runat="server" Text="محل سکونت:"></asp:Label>
                                                                        <asp:Label CssClass="" ID="Label22" runat="server" Text='<%#Eval("ostan")+", "+Eval("shahrestan")+", "+Eval("PostalAddress") %>'></asp:Label>
                                                                    </div>
                                                                    <div class="col-md-12" style="padding-bottom: 15px">
                            <asp:Label CssClass="FieldLabel" ID="Label23" runat="server" Text="درباره من:"></asp:Label>
                            <br />
                            <asp:Label CssClass="" ID="Label24" runat="server" Text='<%#Eval("AboutMe") %>'></asp:Label>
                        </div>
                                                                </div>--%>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                    <asp:SqlDataSource runat="server" ID="UserProfileSqlDataSource" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT * FROM [Users] WHERE ([UserID] = @UserID)">
                                                        <SelectParameters>
                                                            <asp:ControlParameter ControlID="UserIDLabel6" PropertyName="Text" Name="UserID" Type="Int32"></asp:ControlParameter>
                                                        </SelectParameters>
                                                    </asp:SqlDataSource>
                                                </div>
                                                <div class="modal-footer ">
                                                    <div class="text-right">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:Label runat="server" Visible="false" Text='<%# Bind("UserID") %>' ID="UserIDLabel6"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="UserID" HeaderText="کد کاربری" ReadOnly="True" SortExpression="UserID">
                                <HeaderStyle CssClass="text-center" Font-Bold="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Name" HeaderText="نام" SortExpression="Name">
                                <HeaderStyle CssClass="text-center" Font-Bold="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Family" HeaderText="نام خانوادگی" SortExpression="Family">
                                <HeaderStyle CssClass="text-center" Font-Bold="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:BoundField>
                            <asp:BoundField DataField="MobPhone" HeaderText="موبایل" SortExpression="MobPhone">
                                <HeaderStyle CssClass="text-center" Font-Bold="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:BoundField>

                        </Columns>
                        <EmptyDataTemplate>
                            <asp:Label CssClass="alert alert-warning" ID="Label2" runat="server" Text="در حال حاضر اطلاعاتی جهت نمایش وجود ندارد."></asp:Label>
                        </EmptyDataTemplate>
                        <AlternatingRowStyle BackColor="#efefef" />
                        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                        <HeaderStyle BackColor="#333333" Font-Bold="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" Height="0px" />
                        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                        <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                        <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                        <SortedDescendingHeaderStyle BackColor="#242121" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DrB_DBConnectionString %>" SelectCommand="select * from users">
                    </asp:SqlDataSource>


                </div>
            </div>
        </div>
    </div>
</asp:Content>

