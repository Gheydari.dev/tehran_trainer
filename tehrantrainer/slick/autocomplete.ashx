﻿<%@ WebHandler Language="C#" Class="autocomplete" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Linq;
using System.Web.UI.WebControls;
using System.Collections.Generic;
public class autocomplete : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "application/javascript";

        //load your items from somewhere...
        var myList = new List<string>();

        string[] items;
        //{
        //    "ActionScript",
        //    "AppleScript",
        //    "Ruby",
        //    "Scala",
        //    "Scheme"
        //};
        
        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(CS);

        string FetchUserSqlSyntax = "SELECT AllImages.ImgSrc, Product.FName, Product.EName, Product.pid FROM AllImages INNER JOIN Product ON AllImages.pid = Product.pid where allimages.imgorder=1";

        System.Data.SqlClient.SqlCommand FetchUserCmd = new System.Data.SqlClient.SqlCommand(FetchUserSqlSyntax, con);

        
        
        //System.Data.SqlClient.SqlParameter[] param = new System.Data.SqlClient.SqlParameter[1];
        //param[0] = new System.Data.SqlClient.SqlParameter("@UserID", System.Data.SqlDbType.BigInt);
        //param[0].Value = Session["detailsuserid"].ToString();
        //FetchUserCmd.Parameters.Add(param[0]);
        try
        {
            con.Open();
            System.Data.SqlClient.SqlDataReader FetchUserDr = FetchUserCmd.ExecuteReader();
            while (FetchUserDr.Read())
            {
                myList.Add(FetchUserDr["fname"].ToString());
                myList.Add(FetchUserDr["ename"].ToString());
                //myList.Add(FetchUserDr["ImgSrc"].ToString());
                
            }
            items = myList.ToArray();
            //FetchUserDr.NextResult();
            //while (FetchUserDr.Read())
            //{
            //    RoleAccess.Add(FetchUserDr["roleid"] + "#" + FetchUserDr["RoleAccess"]);
            //}
            //FetchUserDr.NextResult();
            //int i = 1;
            //while (FetchUserDr.Read())
            //{
            //    Application["daysen" + i.ToString()] = FetchUserDr["daysen"].ToString();
            //    Application["daysenvis" + i.ToString()] = FetchUserDr["visible"].ToString();
            //    i++;
            //}
        }

        catch (System.Data.SqlClient.SqlException ex)
        {
            string msg = "FetchUserCmd Error:";
            msg += ex.Message;
            throw new Exception(msg);
        }

        finally
        {
            con.Close();
        }
        

        //jQuery will pass in what you've typed in the search box in the "term" query string
        string term = context.Request.QueryString["term"];

        if (!String.IsNullOrEmpty(term))
        {
            //find any matches
            var result = items.Where(i => i.StartsWith(term, StringComparison.CurrentCultureIgnoreCase)).ToArray();
            
            //convert the string array to Javascript
            context.Response.Write(new JavaScriptSerializer().Serialize(result));
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}