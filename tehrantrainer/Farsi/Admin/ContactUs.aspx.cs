﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Farsi_Admin_ContactUs : System.Web.UI.Page
{
    protected string DisplayDate(object Date)
    {
        PersianDateTime PersianDate = new PersianDateTime(Convert.ToDateTime(Date));

        return PersianDate.ToString("dddd d MMMM yyyy ساعت hh:mm tt");
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        System.Web.UI.HtmlControls.HtmlGenericControl li = (System.Web.UI.HtmlControls.HtmlGenericControl)this.Page.Master.Master.FindControl("BodyContentPlaceHolder").FindControl("ContactUs");

        li.Attributes["class"] = "active";
    }
}