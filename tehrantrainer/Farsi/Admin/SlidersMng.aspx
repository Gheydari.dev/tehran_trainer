﻿<%@ Page Title="" Language="C#" MasterPageFile="~/farsi/Admin/FaAdminMasterPage.master" AutoEventWireup="true" CodeFile="SlidersMng.aspx.cs" Inherits="Farsi_Admin_SlidersMng" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <%--<div style="color: #0072c4" class="col-md-12">
        <span class="glyphicon glyphicon-info-sign"></span>
        <asp:Label Text="Valid formats includes JPG, JPEG, GIF and PNG with sizes less than 200KB." runat="server" />
        <br />
        <span class="glyphicon glyphicon-info-sign"></span>
        <asp:Label Text="Best dimension is 755x240. Be sure all your images have same dimension for better display." runat="server" />
        <br />     
        <br />
    </div>--%>
    <div dir="rtl" class="modal fade BYekan" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true" id="SlidesModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close pull-left" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel">آمار اسلایدرها</h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <asp:Repeater runat="server" ID="SlidesStaticRepeater" DataSourceID="SlidesStaticSqlDataSource">
                            <ItemTemplate>
                                <div class="row">
                                    <div class="col-md-8">
                                        <h4><%#InsertTitle("عنوان اسلایدر",Container.ItemIndex) %></h4>
                                        <asp:Label ID="TitleLabel" runat="server" Text='<%#Eval("title") %>'></asp:Label>
                                        <asp:Label ID="SIDLabel" runat="server" Text='<%#Eval("sid") %>' Visible="false"></asp:Label>
                                    </div>
                                    <div class="col-md-4">
                                        <h4><%#InsertTitle("تعداد تصاویر",Container.ItemIndex) %></h4>
                                        <asp:Repeater runat="server" ID="slidesCountsRepeater" DataSourceID="SlidesCountSqlDataSource">
                                            <ItemTemplate>
                                                <asp:Label ID="Label9" runat="server" Text='<%#Eval("siicount") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <asp:SqlDataSource runat="server" ID="SlidesCountSqlDataSource" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT count(siid) as siicount FROM [SliderImages] WHERE ([SID] = @SID)">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="SIDLabel" PropertyName="Text" Name="SID" Type="Int32"></asp:ControlParameter>
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                        <asp:SqlDataSource runat="server" ID="SlidesStaticSqlDataSource" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT * FROM [Slider]  order by title"></asp:SqlDataSource>
                    </div>

                </div>
                <div class="modal-footer ">
                    <div class="text-center">
                        <%--<a href="../farsi/Register.aspx">کاربر جدید هستید؟ ثبت نام کنید.</a>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div dir="rtl" class="modal fade BYekan" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true" id="NewSliderModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close pull-left" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">افزودن اسلایدر جدید</h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <asp:Panel CssClass="text-center" ID="Panel1" runat="server" DefaultButton="AddNewSliderButton">
                                    <div class="input-group">
                                        <div dir="rtl">
                                            <asp:TextBox ID="NewSliderTextBox" placeholder="عنوان اسلایدر" CssClass="form-control input-sm text-right BYekan" Style="max-width: 100% !important" runat="server" Height="40" Font-Size="Small"></asp:TextBox>
                                        </div>
                                        <span class="input-group-btn">
                                            <asp:LinkButton ID="AddNewSliderButton" OnClick="AddNewSliderButton_Click" runat="server" class="btn btn-default" Text="ثبت" Height="40" Font-Size="Small"><span style="padding-top:2px;padding-right:15px;padding-left:15px;font-size:20px" class="glyphicon glyphicon-plus"></span></asp:LinkButton>
                                        </span>

                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer ">
                    <div class="text-center">
                        <%--<a href="../farsi/Register.aspx">کاربر جدید هستید؟ ثبت نام کنید.</a>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <%--onclick="$('.MySlider').html($('.sqwsaqw').html())"--%>
            <div runat="server" id="InfoDiv" class="alert alert-info alert-dismissable ">
                <asp:Literal ID="InfoLiteral" runat="server">مدیریت اسلایدرهای وبسایت.</asp:Literal>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 text-center">
            <a class="btn btn-default btn-sm" style="text-decoration: none" href="#" data-toggle="modal" data-target="#NewSliderModal"><span style="margin-left: 5px" class="glyphicon glyphicon-plus"></span>افزودن اسلایدر جدید</a>
        </div>
        <div class="col-md-4 text-center gggggg">
            <div class="form-group">
                <asp:DropDownList AppendDataBoundItems="false" CssClass="form-control input-sm" ID="SliderSelectorDropDownList" runat="server" AutoPostBack="True" DataSourceID="ListOfCoursesSqlDataSource" DataTextField="title" DataValueField="SID" OnSelectedIndexChanged="SliderSelectorDropDownList_SelectedIndexChanged" OnDataBound="SliderSelectorDropDownList_DataBound">
                    <asp:ListItem Text="لطفا اسلایدر مورد نظر خود را انتخاب کنید..." Value="" />
                </asp:DropDownList>
                <asp:SqlDataSource ID="ListOfCoursesSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DrB_DBConnectionString %>" SelectCommand="SELECT * FROM [slider] where Situ=N'ok' and isdeleted=0 order by title"></asp:SqlDataSource>
            </div>
        </div>
        <div class="col-md-4 text-center">
            <a class="btn btn-info btn-sm" style="text-decoration: none" href="#" data-toggle="modal" data-target="#SlidesModal"><span style="margin-left: 5px" class="glyphicon glyphicon-new-window"></span>مشاهده آمار اسلایدرها</a>
        </div>
    </div>
    <hr />
    <div class="row" runat="server" id="SlideShow" visible="false">
        <div runat="server" visible="false" id="legendDiv" class="col-md-12 text-center" style="padding-bottom: 8px">
            <span style="color: green" class="glyphicon glyphicon-stop"></span>
            <asp:Label ForeColor="Green" ID="Label1" runat="server" Text="عملیت موفقیت آمیز."></asp:Label>
            <span style="color: orange" class="glyphicon glyphicon-stop"></span>
            <asp:Label ForeColor="Orange" ID="Label3" runat="server" Text="حجم بالا تر از 200KB."></asp:Label>
            <span style="color: red" class="glyphicon glyphicon-stop"></span>
            <asp:Label ForeColor="Red" ID="Label4" runat="server" Text="قالب غیرمجاز."></asp:Label>
            <span style="color: black" class="glyphicon glyphicon-stop"></span>
            <asp:Label ForeColor="Black" ID="Label5" runat="server" Text="شماره غیرمعتبر."></asp:Label>
            <hr />
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12 text-center">
                        </div>
                        <div class="col-md-12 text-right">
                            <asp:LinkButton ID="UploadPhotosButton" CssClass="btn btn-sm btn-primary UploadPhotosButton" runat="server" Text="Upload photos and save order changes" OnClick="UploadPhotosButton_Click"><span class="glyphicon glyphicon-upload"></span>&nbsp;&nbsp;بارگذاری تصاویر و ذخیره تغییرات</asp:LinkButton>
                            <asp:LinkButton ToolTip="افزودن اسلاید جدید" ID="AddNewItemLinkButton" CssClass="btn btn-sm btn-success" runat="server" Text="افزودن اسلاید جدید" OnClick="AddNewItemLinkButton_Click"><span style="margin-left:10px" class="glyphicon glyphicon-plus"></span>افزودن اسلاید جدید</asp:LinkButton>
                            <label style="cursor: pointer; margin-right: 15px">

                                <asp:CheckBox ID="ShowAllCheckBox" runat="server" OnCheckedChanged="ShowAllCheckBox_CheckedChanged" AutoPostBack="true" />
                                <span>نمایش همه ی اسلایدها</span>
                            </label>
                            <asp:LinkButton ID="DeleteLinkButton" CssClass="btn btn-sm btn-danger pull-left" runat="server" Text="Delete slider" OnClick="DeleteLinkButton_Click"><span class="glyphicon glyphicon-trash"></span>&nbsp;&nbsp;حذف اسلایدر</asp:LinkButton>

                        </div>
                        <div class="col-md-12 small" style="font-family: Tahoma; font-size: 10px; font-weight: 100">
                            <textarea runat="server" id="SliderCodeTextArea" style="width: 100%; max-width: 100%; height: 250px; display: none" class="MySlider" dir="ltr"> </textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <asp:Repeater OnItemCommand="SlidesRepeater_ItemCommand" ID="SlidesRepeater" runat="server" DataSourceID="SqlDataSource1">
                    <ItemTemplate>
                        <div class="col-md-2" style="padding: 2px; float: right; font-size: small">
                            <div runat="server" id="PhotoDiv" style="background-color: #e6e6e6; border: solid; border-width: 2px; border-color: #b0b0b0; border-radius: 4px 5px; margin: 0px; padding: 5px" class="form-group">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <asp:Label Visible="false" ID="SIIDLabel" runat="server" Text='<%# Eval("siid").ToString() %>'></asp:Label>
                                        <%--<asp:Label ID="Label2" runat="server" Text="ترتیب:"></asp:Label>--%>
                                        <asp:TextBox CssClass="text-center" Width="35px" ID="OrderNumTextBox" runat="server" Text='<%#Eval("ordernum").ToString() %>'></asp:TextBox>
                                    </div>
                                    <div class="col-xs-7">
                                        <asp:CheckBox Text="نمایش" ID="SituCheckBox" runat="server" Checked='<%#SetSituChecked(Eval("situ")) %>'  CssClass="cp"/>
                                    </div>
                                    <div class="col-xs-2">
                                        <asp:LinkButton CommandArgument='<%#Eval("siid").ToString()+"-"+Eval("src").ToString() %>' CommandName="deletSlide" ID="DeleteButton" CssClass="btn btn-danger btn-xs pull-left" runat="server" ToolTip="حذف اسلاید"><span class="glyphicon glyphicon-trash"></span></asp:LinkButton>
                                    </div>
                                </div>
                                <hr class="divider" />
                                <div class="row text-center">
                                    <div class="col-xs-4">
                                        <span>md</span>
                                        <asp:TextBox CssClass="text-center" Width="30px" ID="ColmdTextBox" runat="server" Text='<%#Eval("colmd").ToString() %>'></asp:TextBox>
                                    </div>
                                    <div class="col-xs-4">
                                        <span>sm</span>
                                        <asp:TextBox CssClass="text-center" Width="30px" ID="ColsmTextBox" runat="server" Text='<%#Eval("colsm").ToString() %>'></asp:TextBox>
                                    </div>
                                    <div class="col-xs-4">
                                        <span>xs</span>
                                        <asp:TextBox CssClass="text-center" Width="30px" ID="ColxsTextBox" runat="server" Text='<%#Eval("colxs").ToString() %>'></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <asp:FileUpload Width="100%" Style="margin-top: 4px; margin-bottom: 4px" ID="PhotoFileUpload" runat="server" />
                                        لینک مقصد:
                                        <div dir="ltr">
                                            <asp:TextBox Style="margin-top: 2px; margin-bottom: 2px" CssClass="text-left form-control input-sm" ID="HrefTextBox" runat="server" Text='<%#Eval("href").ToString() %>'></asp:TextBox>
                                        </div>
                                        عنوان تصویر:
                                        <asp:TextBox Style="margin-top: 2px; margin-bottom: 2px" CssClass="text-center form-control input-sm" ID="TitleTextBox" runat="server" Text='<%#Eval("Title").ToString() %>'></asp:TextBox>
                                        توضیحات تصویر:
                                        <asp:TextBox Style="margin-top: 2px; margin-bottom: 2px" CssClass="text-center form-control input-sm" ID="DesTextBox" runat="server" Text='<%#Eval("Des").ToString() %>'></asp:TextBox>
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <asp:Image Height="100" Width="100%" ID="SlideImage" runat="server" ImageUrl='<%#Eval("src") %>' />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <div style="display: none">
                                            <label class="btn btn-primary btn-xs btn-block" style="margin-top: 5px; cursor: auto">
                                                <span class="glyphicon glyphicon-time "></span><span style="margin-right: 8px">تنظیمات شمارنده فروش ویژه</span>
                                            </label>
                                            <asp:Label ID="Label2" runat="server" Text="نمایش شمارنده:"></asp:Label>
                                            <asp:CheckBox ID="ShowCnterCheckBox" runat="server" Checked='<%#SetCnterChecked(Eval("cnter")) %>' />
                                            <br />
                                            <asp:Label ID="Label7" runat="server" Text="دوره فروش ویژه:"></asp:Label>
                                            <br />
                                            <asp:Label ID="Label8" runat="server" Text="تا:"></asp:Label>
                                            <asp:TextBox ID="StartDateTextBox" runat="server" CssClass="sss" Text='<%#Eval("cnter") %>'></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DrB_DBConnectionString %>" SelectCommand="SELECT * FROM [sliderimages] WHERE [SID] = @SID ORDER BY situ,[ordernum] desc">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="SliderSelectorDropDownList" PropertyName="SelectedValue" Name="SID"></asp:ControlParameter>
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>
        </div>
    </div>
    <div class="delay-01s fadeInUp wow sqwsaqw" style="display: none">
        <div dir="ltr" class="slider-pro MakeThisSlider2">
            <div class="sp-slides ">
                <asp:Repeater ID="MainSliderRepeater" runat="server" DataSourceID="MainSliderSqlDataSource">
                    <ItemTemplate>
                        <div class="sp-slide text-center">
                            <a href='<%#Eval("href") %>' target="_blank" style="z-index: 500; cursor: pointer" class="sp-selectable">
                                <img alt='<%#Eval("des") %>' class="sp-image" src='<%#Eval("src").ToString().Replace("~","../..") %>' title='<%#Eval("title") %>'
                                    data-src='<%#Eval("src").ToString().Replace("~","../..") %>'
                                    data-retina='<%#Eval("src").ToString().Replace("~","../..") %>' />
                            </a>
                            <p class="sp-caption ">
                                <label dir="rtl" style="border-bottom: solid 2px #a7a7a7; border-top: solid 2px #a7a7a7; padding-left: 25px; padding-right: 25px" class="BYekan"><%#Eval("des") %></label>
                            </p>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <asp:SqlDataSource ID="MainSliderSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DrB_DBConnectionString %>" SelectCommand="SELECT * FROM [sliderimages] WHERE [SID] = @SID and situ=N'show'  ORDER BY [ordernum] desc">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="SliderSelectorDropDownList" PropertyName="SelectedValue" Name="SID"></asp:ControlParameter>
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>
        </div>
    </div>
    <hr />

    <script type="text/javascript">
        $(document).ready(function () { $(".sss").datepicker(); })
    </script>
    <script type="text/javascript">
        function pageLoad() {
            $(".UploadPhotosButton").click(function () {
                myHTML = $('.sqwsaqw').html().replace(/[<>&\n]/g, function (x) {
                    return {
                        '<': 'kamtar',
                        '>': 'bishtar',
                        '&': '&amp;',
                        '\n': ''
                    }[x];
                });
                $('.MySlider').html(myHTML);
            });
        }

        //$(document).ready(function () {

        //});
    </script>
    <%--<script type="text/javascript">
        //function pageLoad() {
        //    myHTML = $('.sqwsaqw').html().replace(/[<>&\n]/g, function (x) {
        //        return {
        //            '<': 'kamtar',
        //            '>': 'bishtar',
        //            '&': '&amp;',
        //            '\n': ''
        //        }[x];
        //    });
        //    $('.MySlider').html(myHTML);
        //}

        $(document).ready(function () {
            $(".UploadPhotosButton").click(function () {
                myHTML = $('.sqwsaqw').html().replace(/[<>&\n]/g, function (x) {
                    return {
                        '<': 'kamtar',
                        '>': 'bishtar',
                        '&': '&amp;',
                        '\n': ''
                    }[x];
                });
                $('.MySlider').html(myHTML);
            });
        });
    </script>--%>
</asp:Content>

