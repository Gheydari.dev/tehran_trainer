﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

/// <summary>
/// Summary description for MyFunc
/// </summary>
public static class MyFunc
{
	
    public static string makeprice(object inp)
    {
        try
        {
            if (inp.ToString() != "" && Double.Parse(inp.ToString()) > 0)
            {
                string num = string.Format("{0:0,0}", Double.Parse(inp.ToString()));
                return CorrectNumbers(num) + " تومان";
            }
            else
            {
                return "0";
            }
        }
        catch { return "0"; }
    }
    public static string CorrectNumbers(object number)
    {
        number = number.ToString().Replace("0", "۰");
        number = number.ToString().Replace("1", "۱");
        number = number.ToString().Replace("2", "۲");
        number = number.ToString().Replace("3", "۳");
        number = number.ToString().Replace("4", "۴");
        number = number.ToString().Replace("5", "۵");
        number = number.ToString().Replace("6", "۶");
        number = number.ToString().Replace("7", "۷");
        number = number.ToString().Replace("8", "۸");
        number = number.ToString().Replace("9", "۹");
        return number.ToString();
    }
    public static string CorrectNumbersEn(object number)
    {
        number = number.ToString().Replace("۰", "0");
        number = number.ToString().Replace("۱", "1");
        number = number.ToString().Replace("۲", "2");
        number = number.ToString().Replace("۳", "3");
        number = number.ToString().Replace("۴", "4");
        number = number.ToString().Replace("۵", "5");
        number = number.ToString().Replace("۶", "6");
        number = number.ToString().Replace("۷", "7");
        number = number.ToString().Replace("۸", "8");
        number = number.ToString().Replace("۹", "9");
        return number.ToString();
    }
    public static string TimeHourRemain(object DateSend)
    {
        string Rval = "";

        DateTime DT = Convert.ToDateTime(DateSend);
        DateTime DTNow = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, 0);
        if (Convert.ToInt32(Math.Truncate((DTNow - DT).TotalSeconds)) > 0)
        {
            int Second = Convert.ToInt32(Math.Truncate((DTNow - DT).TotalSeconds));
            int Minute = Convert.ToInt32(Math.Truncate((DTNow - DT).TotalMinutes));
            int Hour = Convert.ToInt32(Math.Truncate((DTNow - DT).TotalHours));
            int Day = Convert.ToInt32(Math.Truncate((DTNow - DT).TotalDays));

            if (Day == 0 && Hour == 0 && Minute == 0)
                Rval = Second.ToString() + " ثانیه پیش ";
            else if (Day == 0 && Hour == 0)
                Rval = Minute.ToString() + " دقیقه پیش ";
            else if (Day == 0)
                Rval = Hour.ToString() + " ساعت پیش ";
            else
                Rval = Day.ToString() + " روز پیش ";
        }
        else
        {
            Rval = " لحظاتی پیش ";
        }
        return CorrectNumbers(Rval);
    }
    public static string ValidPersian(this string str)
    {
        if (string.IsNullOrEmpty(str))
        {
            return str;
        }
        else
        {
            str = Regex.Replace(Regex.Replace(Regex.Replace(str, "ك", "ک", RegexOptions.IgnoreCase), "ي", "ی"), "\\s{2,}", "\\s", RegexOptions.IgnoreCase);
            return str;

        }
    }
    public static string DisplayTime(object Date)
    {
        PersianDateTime PersianDate = new PersianDateTime(Convert.ToDateTime(Date));
        return PersianDate.ToString("hh:mm");
    }
    public static string DisplayDate(object Date)
    {
        PersianDateTime PersianDate = new PersianDateTime(Convert.ToDateTime(Date));
        return PersianDate.ToString("yyyy/MM/dd");
    }
    public static string DisplayDateTime(object Date)
    {
        PersianDateTime PersianDate = new PersianDateTime(Convert.ToDateTime(Date));
        return PersianDate.ToString("yyyy/MM/dd - HH:mm");
    }
    public static string SafeString(object Input)
    {

        return Input.ToString().Trim()
            .Replace("\"", "-")
            .Replace("،", "-")
            .Replace("َ", "-") //a
            .Replace("ُ", "-") //e
            .Replace("ِ", "-") //o
            .Replace("#", "-")
            .Replace("؟", "-")
            .Replace("?", "-")
            .Replace("!", "-")
            .Replace("\r\n", "-")
            .Replace("\n\r", " -")
            .Replace("\n\r", " -")
            .Replace("\r", "-")
            .Replace("\n", "-")
            .Replace(".", "-")
            .Replace("\\", "-")
            .Replace(":", "-")
            .Replace("+", "-")
            .Replace("  ", " ")
            .Replace(" ", "-")
            .Replace("--", "-")
            .Trim();

    }
}