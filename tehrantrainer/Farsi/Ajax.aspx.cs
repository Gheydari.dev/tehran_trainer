﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Farsi_Ajax : System.Web.UI.Page
{
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["name"] != null)
        {
            string ActString = Request.QueryString["act"];
            string XIDTypeString = Request.QueryString["xidtype"];
            string XIDString = Request.QueryString["xid"];
            string SqlString = "";

            DataLayer dl = new DataLayer();
            string CS = dl.dataLayerConnectionString;
            SqlConnection con = new SqlConnection(CS);

            switch (ActString)
            {
                case "like":
                    {
                        SqlString = "if not exists (select * from userlikes where userid=@userid and xid=@xid and xidtype=@xidtype) begin insert into userlikes (userid,xid,xidtype) values (@userid,@xid,@xidtype) select N'liked' end else begin delete from userlikes where userid=@userid and xid=@xid and xidtype=@xidtype  select N'unliked' end";
                        SqlCommand FetchUserCmd = new SqlCommand(SqlString, con);
                        SqlParameter[] FetchUserParam = new SqlParameter[3];

                        FetchUserParam[0] = new SqlParameter("@xid", SqlDbType.NVarChar);
                        FetchUserParam[1] = new SqlParameter("@userid", SqlDbType.NVarChar);
                        FetchUserParam[2] = new SqlParameter("@xidtype", SqlDbType.NVarChar);
                        
                        FetchUserParam[0].Value = XIDString;
                        FetchUserParam[1].Value = Session["userid"];
                        FetchUserParam[2].Value = XIDTypeString;

                        for (int i = 0; i < FetchUserParam.Length; i++)
                        {
                            FetchUserCmd.Parameters.Add(FetchUserParam[i]);
                        }
                        SqlDataReader SqlDr;
                        try
                        {
                            con.Open();
                            SqlDr = FetchUserCmd.ExecuteReader();

                            if (SqlDr.Read())
                            {
                                Response.Write("[{\"situ\":\"" + SqlDr[0].ToString() + "\"}]");
                            }
                        }
                        catch (System.Data.SqlClient.SqlException ex)
                        {
                            string msg = "FetchTagsCmd Error:";
                            msg += ex.Message;
                            throw new Exception(msg);
                        }

                        finally
                        {
                            con.Close();
                        }
                    }
                    break;
                case "checklike":
                    {
                        SqlString = "if exists (select * from userlikes where userid=@userid and xid=@xid and xidtype=@xidtype) select N'liked' else select N'unliked' ";
                        SqlCommand FetchUserCmd = new SqlCommand(SqlString, con);
                        SqlParameter[] FetchUserParam = new SqlParameter[3];

                        FetchUserParam[0] = new SqlParameter("@xid", SqlDbType.NVarChar);
                        FetchUserParam[1] = new SqlParameter("@userid", SqlDbType.NVarChar);
                        FetchUserParam[2] = new SqlParameter("@xidtype", SqlDbType.NVarChar);

                        FetchUserParam[0].Value = XIDString;
                        FetchUserParam[1].Value = Session["userid"];
                        FetchUserParam[2].Value = XIDTypeString;

                        for (int i = 0; i < FetchUserParam.Length; i++)
                        {
                            FetchUserCmd.Parameters.Add(FetchUserParam[i]);
                        }
                        SqlDataReader SqlDr;
                        try
                        {
                            con.Open();
                            SqlDr = FetchUserCmd.ExecuteReader();

                            if (SqlDr.Read())
                            {
                                Response.Write("[{\"situ\":\"" + SqlDr[0].ToString() + "\"}]");
                            }
                        }
                        catch (System.Data.SqlClient.SqlException ex)
                        {
                            string msg = "FetchTagsCmd Error:";
                            msg += ex.Message;
                            throw new Exception(msg);
                        }

                        finally
                        {
                            con.Close();
                        }
                    }
                    break;
                default:
                    break;
            }

        }
        else if (Request.QueryString["act"]=="like")        
        {
            Response.Write("[{\"situ\":\"login\"}]");
        }
    }
}