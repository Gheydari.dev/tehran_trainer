﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Farsi_Register : System.Web.UI.Page
{
    protected string ShowName(object XName)
    {
        if (XName.ToString().Length > 45)
        {
            return XName.ToString().Substring(0, 45) + " ... ";
        }
        else
        {
            return XName.ToString();
        }
    }
    protected string SetPriceStyle(object InputPrice, object C2, object Cnt)
    {
        int Count = Convert.ToInt32(Cnt.ToString());
        string Situ = C2.ToString();
        string Price = InputPrice.ToString();
        string Sub = "";
        if (Count == 0)
        {
            if (Situ == "توقف تولید")
            {
                return ("<span class='glyphicon glyphicon-remove'></span> توقف تولید");
            }
            else if (Situ == "به زودی")
            {
                return ("<span class='glyphicon glyphicon-time'></span> به زودی!");
            }
            else
            {
                return (":( ناموجود!");
            }
        }
        else
        {
            if (Convert.ToInt64(InputPrice.ToString()) != 0)
            {
                int until = Price.Length / 3;
                if (Price.Length % 3 == 0)
                {
                    until = until - 1;
                }
                for (int i = 0; i < until; i++)
                {
                    Sub = Price.Substring(Price.Length - (((i + 1) * 3) + i), (((i + 1) * 3) + i));
                    Price = Price.Substring(0, Price.Length - Sub.Length);
                    Price = Price + "," + Sub;
                }
                return Price + " تومان";
            }
            else
            {
                if (Situ == "توقف تولید")
                {
                    return ("<span class='glyphicon glyphicon-remove-circle'></span> توقف تولید");
                }
                else if (Situ == "به زودی")
                {
                    return ("<span class='glyphicon glyphicon-time'></span> به زودی!");
                }
                else
                {
                    return ("ناموجود!");
                }
            }
        }
    }
    protected bool SetDiscountStyle(object DiscountPrice, object C2, object Cnt)
    {
        string Price = DiscountPrice.ToString();
        if (Price == "0" || C2.ToString() == "به زودی" || C2.ToString() == "توقف تولید" || Convert.ToInt32(Cnt.ToString()) == 0)
        {
            return (false);
        }
        else
        {
            return (true);
        }
    }
    protected System.Drawing.Color SetPriceColor(object InputPrice, object C2, object Cnt)
    {
        int Count = Convert.ToInt32(Cnt.ToString());
        string Situ = C2.ToString();
        string Price = InputPrice.ToString();
        string Sub = "";
        if (Count == 0)
        {
            if (Situ == "توقف تولید")
            {
                return System.Drawing.Color.Gray;
            }
            else if (Situ == "به زودی")
            {
                return System.Drawing.Color.RoyalBlue;
            }
            else
            {
                return System.Drawing.Color.Red;
            }
        }
        else
        {
            if (Convert.ToInt64(InputPrice.ToString()) != 0)
            {
                int until = Price.Length / 3;
                if (Price.Length % 3 == 0)
                {
                    until = until - 1;
                }
                for (int i = 0; i < until; i++)
                {
                    Sub = Price.Substring(Price.Length - (((i + 1) * 3) + i), (((i + 1) * 3) + i));
                    Price = Price.Substring(0, Price.Length - Sub.Length);
                    Price = Price + "," + Sub;
                }
                return System.Drawing.Color.Green;
            }
            else
            {
                if (Situ == "توقف تولید")
                {
                    return System.Drawing.Color.Gray;
                }
                else if (Situ == "به زودی")
                {
                    return System.Drawing.Color.RoyalBlue;
                }
                else
                {
                    return System.Drawing.Color.Red;
                }
            }
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "ثبت نام و ورود | تهران آموز";

        if (Request.QueryString["Ref"] == "shipping")
        {
            StepBarDiv.Visible = true;
        }
        else
        {
            StepBarDiv.Visible = false;

        }

        if (Request.QueryString["Ref"] == "StormPool")
        {
            ContactUsDiv.Visible = false;
        }
        else
        {
            ContactUsDiv.Visible = true;

        }
        //string StringStatus = "";
        //switch (retval)
        //{
        //    case 0:
        //        StringStatus = "نام کاربری و یا رمز عبور اشتباه است";
        //        break;
        //    case 1:
        //        StringStatus = "ارسال با موفقیت انجام شده است";
        //        break;
        //    case 2:
        //        StringStatus = "اعتبار کافی نیست";
        //        break;
        //    case 3:
        //        StringStatus = "محدودیت در ارسال روزانه";
        //        break;
        //    case 4:
        //        StringStatus = "محدودیت در حجم ارسال";
        //        break;
        //    case 5:
        //        StringStatus = "شماره فرستنده معتبر نیست";
        //        break;
        //}


    }
    protected void LoginButton_Click(object sender, EventArgs e)
    {

    }
    protected void RegisterButton_Click(object sender, EventArgs e)
    {
        string EncodedResponse = Request.Form["g-Recaptcha-Response"];
        bool IsCaptchaValid = (ReCaptchaClass.Validate(EncodedResponse) == "True" ? true : false);

        Int64 MobNumberInt64;
        bool MobNumberResult = Int64.TryParse(MobPhoneTextBox.Text.ToString().ToLower().Trim(), out MobNumberInt64);
        string saveStat = "false";

        //if (IsCaptchaValid)
        if (MobPhoneTextBox.Text.ToString().ToLower().Trim().Length == 11 && MobNumberResult && RegisterPasswordTextBox.Text.Length != 0 && RepeatPasswordTextBox.Text.Length != 0 && NameTextBox.Text.Length != 0 && FamilyTextBox.Text.Length != 0)
        {
            DataLayer dl = new DataLayer();
            string CS = dl.dataLayerConnectionString;
            SqlConnection con = new SqlConnection(CS);

            string step = "";
            string FetchKodemelliSqlSyntax = "select * from users where Email=@Email or MobPhone=@MobPhone;";
            SqlCommand FetchUseridCmd = new SqlCommand(FetchKodemelliSqlSyntax, con);

            SqlParameter[] UseridParam = new SqlParameter[2];

            UseridParam[0] = new SqlParameter("@Email", SqlDbType.NVarChar);
            UseridParam[1] = new SqlParameter("@MobPhone", SqlDbType.NVarChar);

            if (EmailTextBox.Text.ToString().ToLower().Trim() != "")
            {
                UseridParam[0].Value = EmailTextBox.Text.ToString().ToLower().Trim();
            }
            else
            {
                UseridParam[0].Value = "this mail never existed!";
            }
            UseridParam[1].Value = MobPhoneTextBox.Text.ToString().ToLower().Trim();

            FetchUseridCmd.Parameters.Add(UseridParam[0]);
            FetchUseridCmd.Parameters.Add(UseridParam[1]);

            try
            {
                con.Open();
                SqlDataReader FetchUserDr = FetchUseridCmd.ExecuteReader();
                if (FetchUserDr.Read())
                {

                    step = "stop";
                }
                else
                {
                    step = "go";
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                string msg = "FetchUserCmd Error:";
                msg += ex.Message;
                throw new Exception(msg);
            }

            finally
            {
                con.Close();
            }
            if (step == "go")
            {
                string RegSqlSyntax = "begin DECLARE @myInsertedTable table (insertedid  bigint NOT NULL)  INSERT INTO Users (name,family,email,password,mobphone,gender,newsletter,birthday) OUTPUT inserted.Userid into @myInsertedTable VALUES (@name,@family,@email,@password,@mobphone,N'مرد',N'بلی',getdate()) update [orders] set userid=(select insertedid from @myInsertedTable),ordersitu=N'تایید' where ordersid=@ordersid  select insertedid from @myInsertedTable delete from @myInsertedTable  end ";
                if (Request.QueryString["Ref"] == "StormPool")
                {
                    RegSqlSyntax = "begin DECLARE @myInsertedTable table (insertedid  bigint NOT NULL)  INSERT INTO Users (name,family,email,password,mobphone,gender,newsletter,birthday) OUTPUT inserted.Userid into @myInsertedTable VALUES (@name,@family,@email,@password,@mobphone,N'مرد',N'بلی',getdate()) update [orders] set userid=(select insertedid from @myInsertedTable),ordersitu=N'تایید' where ordersid=@ordersid  select insertedid from @myInsertedTable  if not exists (select SPID from stormpool where userid=(select insertedid from @myInsertedTable) and smid=(select top(1) smid from stormmain where ssitu=N'true' order by cdate desc)) insert into stormpool (SMID,PID,Userid) values ((select top(1) smid from stormmain where ssitu=N'true' order by cdate desc),@PID,(select insertedid from @myInsertedTable)) delete from @myInsertedTable  end ";
                }
                SqlCommand RegCmd = new SqlCommand(RegSqlSyntax, con);
                SqlParameter[] param = new SqlParameter[7];

                param[00] = new SqlParameter("@name", SqlDbType.NVarChar);
                param[01] = new SqlParameter("@family", SqlDbType.NVarChar);
                param[02] = new SqlParameter("@email", SqlDbType.NVarChar);
                param[03] = new SqlParameter("@password", SqlDbType.NVarChar);
                param[04] = new SqlParameter("@mobphone", SqlDbType.NVarChar);
                param[05] = new SqlParameter("@ordersid", SqlDbType.NVarChar);
                param[06] = new SqlParameter("@pid", SqlDbType.NVarChar);


                param[00].Value = NameTextBox.Text.Trim();
                param[01].Value = FamilyTextBox.Text.Trim();
                if (EmailTextBox.Text.ToLower().Trim() != "")
                {
                    param[02].Value = EmailTextBox.Text.ToLower().Trim();
                }
                else
                {
                    param[02].Value = "";
                }

                param[03].Value = RegisterPasswordTextBox.Text;
                param[04].Value = MobPhoneTextBox.Text.Trim();
                param[05].Value = Session["ordersid"].ToString();
                if (Request.QueryString["Ref"] == "StormPool")
                {
                    param[06].Value = Request.QueryString["pid"];
                }
                else
                {
                    param[06].Value = "";

                }


                for (int i = 0; i < param.Length; i++)
                {
                    RegCmd.Parameters.Add(param[i]);
                }

                try
                {
                    con.Open();
                    Session["userid"] = RegCmd.ExecuteScalar().ToString();
                    Session["name"] = NameTextBox.Text;
                    Session["ordercount"] = 1;
                    Session["ispublic"] = "false";
                    Session["addressid"] = 0;
                    Session["mobphone"] = MobPhoneTextBox.Text.Trim();


                    InfoDiv.Visible = true;
                    InfoSpan.Attributes["class"] = "glyphicon glyphicon-check";
                    InfoLiteral.Text = "سلام. ثبت نام شما با موفقیت انجام شد. به تهران آموز خوش آمدید.";
                    InfoDiv.Attributes["class"] = " alert alert-success alert-dismissable text-right";

                    if (MobPhoneTextBox.Text != "")
                    {
                        Int64 number;
                        bool result = Int64.TryParse(MobPhoneTextBox.Text.Trim(), out number);
                        if (result)
                        {
                            WebReference.Send sms = new WebReference.Send();
                            long[] rec = null;
                            byte[] status = null;
                            int retval = sms.SendSms("9358177180", "4837", MobPhoneTextBox.Text.Trim().Split('\n'), "50001333263333", "به تهران آموز خوش آمدید. ثبت نام شما انجام شد. کلمه عبور: " + RegisterPasswordTextBox.Text + " | "+SSClass.FTitle+"  |  "+SSClass.PhoneNo, false, "", ref rec, ref status);
                        }
                    }

                    if (Request.QueryString["Ref"] == "shipping")
                    {
                        Response.Redirect(SSClass.WebDomain+"/shipping/?Ref=newreg");
                    }
                    if (Request.QueryString["Ref"] == "StormPool")
                    {
                        Response.Redirect(SSClass.WebDomain+"/?Ref=StormPool");
                    }
                    Response.Redirect(SSClass.WebDomain+"/?Ref=newreg");


                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    string msg = "RegCmd Error:";
                    msg += ex.Message;
                    throw new Exception(msg);
                }
                finally
                {
                    con.Close();
                }
            }
            else
            {
                InfoDiv.Visible = true;
                InfoLiteral.Text = "پست الکترونیک  و یا شماره موبایل وارد شده قبلا در سامانه ثبت شده." + "" + "در صورتی که کلمه عبور خود را فراموش کرده اید،" + "<strong><a href='PasswordRecovery.aspx'>کلیک کنید</a></strong>" + ".";
                InfoDiv.Attributes["class"] = " alert alert-danger alert-dismissable text-right";
            }
        }
        else
        {
            InfoDiv.Visible = true;
            InfoLiteral.Text = "تلفن همرا با 0 شروع می شود و 11 رقم دارد. نمونه ی صحیح: 09xxxxxxxxx";
            InfoDiv.Attributes["class"] = " alert alert-danger alert-dismissable text-right";
        }

    }
    protected void LoginButton_Click_InRegisterPage(object sender, EventArgs e)
    {
        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);

        string FetchUserSqlSyntax = "SELECT email,password,name,ordercount,userid,regsitu,mobphone From users Where (email=@email and password=@password) or (MobPhone=@email and password=@password)";



        SqlCommand FetchUserCmd = new SqlCommand(FetchUserSqlSyntax, con);
        SqlParameter[] FetchUserParam = new SqlParameter[2];

        FetchUserParam[0] = new SqlParameter("@email", SqlDbType.NVarChar);
        FetchUserParam[1] = new SqlParameter("@password", SqlDbType.NVarChar);

        FetchUserParam[0].Value = LEmailTextBox.Text.ToLower();
        FetchUserParam[1].Value = LoginPasswordTextBox.Text; ;

        for (int i = 0; i < FetchUserParam.Length; i++)
        {
            FetchUserCmd.Parameters.Add(FetchUserParam[i]);
        }
        SqlDataReader UserDr;
        try
        {
            con.Open();
            UserDr = FetchUserCmd.ExecuteReader();
            if (UserDr.Read())
            {
                string TempUserID = Session["userid"].ToString();
                Session["userid"] = UserDr["userid"].ToString();
                Session["name"] = UserDr["name"].ToString();
                Session["ordercount"] = UserDr["ordercount"];
                Session["regsitu"] = UserDr["regsitu"];
                Session["mobphone"] = UserDr["mobphone"];
                Session["ispublic"] = "false";
                Session["addressid"] = 0;

                con.Close();

                // set public order to signed in user in shopping cart items

                if (true)
                {
                    string SetPublicToUserSqlSyntax = @"if exists (select ordersid from orders where userid=@userid and ordersitu=N'انتخاب محصولات')
                        begin                         
                             update orders set ordersitu=N'Canseled' where userid=@userid and ordersitu=N'انتخاب محصولات' 
                             update [orders] set userid=@userid,ordersitu=N'تایید' where ordersid=@ordersid
                        end
                        else 
                             update [orders] set userid=@userid,ordersitu=N'تایید' where ordersid=@ordersid ";
                    if (Request.QueryString["Ref"] == "StormPool")
                    {
                        SetPublicToUserSqlSyntax += "  if not exists (select SPID from stormpool where userid=@userid and smid=(select top(1) smid from stormmain where ssitu=N'true' order by cdate desc)) insert into stormpool (SMID,PID,Userid) values ((select top(1) smid from stormmain where ssitu=N'true' order by cdate desc),@PID,@userid) ";
                    }
                    SqlCommand SetPublicCmd = new SqlCommand(SetPublicToUserSqlSyntax, con);
                    SqlParameter[] SetPublicParam = new SqlParameter[3];

                    SetPublicParam[0] = new SqlParameter("@userid", SqlDbType.NVarChar);
                    SetPublicParam[1] = new SqlParameter("@ordersid", SqlDbType.NVarChar);
                    SetPublicParam[2] = new SqlParameter("@pid", SqlDbType.Int);

                    SetPublicParam[0].Value = Session["userid"].ToString();
                    SetPublicParam[1].Value = Session["ordersid"].ToString();
                    if (Request.QueryString["Ref"] == "StormPool")
                    {
                        SetPublicParam[2].Value = Request.QueryString["pid"];
                    }
                    else
                    {
                        SetPublicParam[2].Value = 0;
                    }

                    for (int i = 0; i < SetPublicParam.Length; i++)
                    {
                        SetPublicCmd.Parameters.Add(SetPublicParam[i]);
                    }
                    try
                    {
                        con.Open();
                        SetPublicCmd.ExecuteReader();
                        con.Close();

                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        string msg = "FetchUserCmd Error:";
                        msg += ex.Message;
                        throw new Exception(msg);
                    }

                }
                if (Request.QueryString["Ref"] == "StormPool")
                {
                    Response.Redirect(SSClass.WebDomain+"/?Ref=StormPool");
                }
                if (Request.QueryString["Ref"] == "shipping")
                {
                    Response.Redirect(SSClass.WebDomain+"/shipping?Ref=shipping-login");
                }
                Response.Redirect(SSClass.WebDomain+"");
            }
            else
            {
                LoginMsgLabel.Visible = true;
                LoginMsgLabel.Text = "اطلاعات ورودی معتبر نیست!";
            }
        }

        catch (System.Data.SqlClient.SqlException ex)
        {
            string msg = "FetchUserCmd Error:";
            msg += ex.Message;
            throw new Exception(msg);
        }

        finally
        {
            //con.Close();
        }
    }

}

