﻿<%@ Page Title="" Language="C#" MasterPageFile="~/farsi/FaMasterPage.master" AutoEventWireup="true" CodeFile="PasswordRecovery.aspx.cs" Inherits="Farsi_PasswordRecovery" %>

<%@ Register Src="~/farsi/NumCaptcha.ascx" TagPrefix="uc1" TagName="NumCaptcha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContentPlaceHolder" runat="Server">
    <div class="container-fluid BYekan">
        <div class="container">
            <div class="row " style="margin-top:70px">
                <div class="col-md-12">
                    <div runat="server" id="InfoDiv" class="alert alert-info alert-dismissable text-right" visible="false">
                        <span runat="server" id="InfoSpan" class="glyphicon glyphicon-remove"></span>
                        <asp:Literal ID="InfoLiteral" runat="server" Text=""></asp:Literal>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 text-right">
                </div>
                <div class="col-md-4 text-right">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="padding-bottom: 0px; padding-right: 0px">
                            <label class="BYekan" style="padding-right: 15px">بازیابی کلمه عبور</label>
                        </div>
                        <div style="padding: 15px; font-size: small" class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="پست الکترونیک شما فاقد اعتبار است!" ControlToValidate="EmailTextBox" Display="Dynamic" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="!پست الکترونیک" ControlToValidate="EmailTextBox" ForeColor="Red" Display="Dynamic">*</asp:RequiredFieldValidator>
                                        <div dir="ltr">
                                            <asp:TextBox runat="server" class="form-control input-sm text-right" ID="EmailTextBox" placeholder="پست الکترونیک"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label2" runat="server" Text="سوال امنیتی"></asp:Label>
                                        <uc1:NumCaptcha runat="server" ID="NumCaptcha1" />
                                    </div>
                                    <div class="form-group">
                                        <asp:Button CssClass="btn btn-primary btn-sm btn-block" ID="PassRecButton" runat="server" Text="بازیابی کلمه عبور" OnClick="PassRecButton_Click" />
                                        <asp:Label ID="MsgLabel" runat="server" ForeColor="Red" Font-Size="Smaller"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 text-right">
                </div>
            </div>
        </div>
    </div>
</asp:Content>

