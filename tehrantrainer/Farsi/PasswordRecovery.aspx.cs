﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Farsi_PasswordRecovery : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "بازیابی کلمه ی عبور | تهران آموز";

    }
    protected void PassRecButton_Click(object sender, EventArgs e)
    {
        if (!NumCaptcha1.Decide())
        {
            NumCaptcha1.message = "پاسخ اشتباه است";

        }
        else
        {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("mail.Tehrantrainer.ir");
            SmtpServer.EnableSsl = false;
            mail.From = new MailAddress("Info@Tehrantrainer.ir");
            mail.To.Add(EmailTextBox.Text);
            mail.Subject = "بازیابی کلمه عبور";

            string pass = "";
            DataLayer dl = new DataLayer();
            string CS = dl.dataLayerConnectionString;
            SqlConnection con = new SqlConnection(CS);
            string FetchUserPassSqlSyntax = "select Password from Users where (Email=@Email) ";
            SqlParameter[] param2 = new SqlParameter[1];
            param2[0] = new SqlParameter("@Email", SqlDbType.NVarChar);
            param2[0].Value = EmailTextBox.Text;
            SqlCommand FetchPassCmd = new SqlCommand(FetchUserPassSqlSyntax, con);
            FetchPassCmd.Parameters.Add(param2[0]);
            SqlDataReader dr;
            con.Open();
            dr = FetchPassCmd.ExecuteReader();
            if (dr.Read())
            {
                pass = dr[0].ToString();
                con.Close();

                mail.IsBodyHtml = true;
                mail.Body = "<strong>تهران آموز</strong>" + "<br/><br/>" + "کاربر گرامی، سلام" + "<br/><br/>" + ":گذرواژه ی حساب کاربری شما" + "<br/><br/>" + pass + "<br/><br/>";
                SmtpServer.EnableSsl = false;
                SmtpServer.Send(mail);

                InfoDiv.Visible = true;
                InfoSpan.Attributes["class"] = "glyphicon glyphicon-check";
                InfoLiteral.Text = "گذرواژه با موفقیت به پست الکترونیک شما ارسال شد. در صورت عدم مشاهده ی پیام، اسپم باکس خود را بررسی کنید!";
                InfoDiv.Attributes["class"] = "alert alert-success text-right";
                EmailTextBox.Text = "";
            }

            else
            {
                InfoDiv.Visible = true;
                InfoSpan.Attributes["class"] = "glyphicon glyphicon-remove";
                InfoLiteral.Text = "پست الکترونیک شما در سامانه ثبت نشده است!" + " برای ثبت نام در تهران آموز، " + "<strong><a href='Register.aspx'>کلیک کنید</a></strong>" + ".";
                InfoDiv.Attributes["class"] = "alert alert-danger text-right";
                con.Close();
            }


            con.Close();
            NumCaptcha1.Refresh();
        }
    }
}