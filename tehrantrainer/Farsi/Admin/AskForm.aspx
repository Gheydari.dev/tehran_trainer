﻿<%@ Page Title="" Language="C#" MasterPageFile="~/farsi/Admin/FaAdminMasterPage.master" AutoEventWireup="true" CodeFile="AskForm.aspx.cs" Inherits="Farsi_Admin_AskForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContentPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-sm-12">
            <div runat="server" id="InfoDiv" class="alert alert-success alert-dismissable text-right" visible="false" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <asp:Label ID="InfoLabel" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <asp:Button ID="DeleteButton" Text="حذف" runat="server" CssClass="btn btn-danger" OnClick="DeleteButton_Click" ValidationGroup="delete" />
            <asp:GridView ID="AskFormGridView" runat="server" AutoGenerateColumns="False" DataKeyNames="AFID" DataSourceID="AskFormSqlDataSource" AllowSorting="True" CssClass="table table-bordered table-condensed table-responsive">
                <Columns>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:CheckBox ID="SelectCheckBox" runat="server" />
                            <asp:Label Text='<%#Eval("AFID") %>' runat="server" ID="AFIDLabel" Visible="false" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="AFID" HeaderText="شناسه" ReadOnly="True" InsertVisible="False" SortExpression="AFID"></asp:BoundField>
                    <asp:BoundField DataField="Name" HeaderText="نام و نام خانوادگی" SortExpression="Name"></asp:BoundField>
                    <asp:BoundField DataField="Field" HeaderText="رشته تحصیلی" SortExpression="Field"></asp:BoundField>
                    <asp:BoundField DataField="PhoneNo" HeaderText="شماره تماس" SortExpression="PhoneNo"></asp:BoundField>
                    <asp:BoundField DataField="SCode" HeaderText="کد تخفِف" SortExpression="SCode"></asp:BoundField>
                    <asp:TemplateField HeaderText="تاریخ ثبت" SortExpression="CDate">
                        <ItemTemplate>
                            <asp:Label runat="server" Text='<%# MyFunc.DisplayDateTime(Eval("CDate")) %>' ID="Label1"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
                <AlternatingRowStyle BackColor="#efefef" />
                <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                <HeaderStyle BackColor="#616161" Font-Bold="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" Height="0px" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#242121" />
            </asp:GridView>
            <asp:SqlDataSource runat="server" ID="AskFormSqlDataSource" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT * FROM [AskForm] where isdeleted=0"></asp:SqlDataSource>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContentPlaceHolder" runat="Server">
</asp:Content>

