﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Farsi_Admin_LatestComments : System.Web.UI.Page
{
    protected string DisplayDate3(object Date)
    {
        PersianDateTime persianDate = PersianDateTime.Parse(Date.ToString());
        DateTime miladiDate = persianDate.ToDateTime();
        return miladiDate.ToString();
    }
    protected string DisplayDate(object Date)
    {
        PersianDateTime PersianDate = new PersianDateTime(Convert.ToDateTime(Date));
        return PersianDate.ToString("dddd d MMMM yyyy");
    }
    protected string DisplayDate2(object Date)
    {
        PersianDateTime PersianDate = new PersianDateTime(Convert.ToDateTime(Date));
        return PersianDate.ToString(PersianDateTimeFormat.Date);
    }
    protected void NexDayButton_Click(object sender, EventArgs e)
    {
        TodayTextBox.Text = DisplayDate2(Convert.ToDateTime(DisplayDate3(TodayTextBox.Text)).AddDays(+1));
        TodayLabel.Text = Convert.ToDateTime((TodayLabel.Text)).AddDays(+1).ToShortDateString().ToString();
        TodayLabel.DataBind();

        //OrdersSqlDataSource.DataBind();
        CommentsRepeater.DataBind();
    }
    protected void PreDayButton_Click(object sender, EventArgs e)
    {
        TodayTextBox.Text = DisplayDate2(Convert.ToDateTime(DisplayDate3(TodayTextBox.Text)).AddDays(-1));
        TodayLabel.Text = Convert.ToDateTime((TodayLabel.Text)).AddDays(-1).ToShortDateString().ToString();
        TodayLabel.DataBind();

        //OrdersSqlDataSource.DataBind();
        CommentsRepeater.DataBind();
    }
    protected void TodayTextBox_TextChanged(object sender, EventArgs e)
    {
        //TodayTextBox.Text = DisplayDate2(Convert.ToDateTime(DisplayDate3(TodayTextBox.Text)));
        TodayLabel.Text = (Convert.ToDateTime(DisplayDate3(TodayTextBox.Text))).ToShortDateString().ToString();
        TodayLabel.DataBind();

        //OrdersSqlDataSource.DataBind();
        CommentsRepeater.DataBind();

        //TodayLabel.Text = DisplayDate3(Convert.ToDateTime((TodayTextBox.Text)).ToShortDateString()).ToString();
        //TodayLabel.DataBind();
        ////OrdersSqlDataSource.DataBind();
        //CommentsRepeater.DataBind();
    }
    protected string AdminComment(object UserID)
    {
        if (Convert.ToInt32(UserID.ToString())==0)
        {
            return "مبل و دکور - واحد پاسخ گویی به سوالات: ";
        }
        else
        {
            return "";
        }
        
    }

    protected string CommentCss(object UserID)
    {
        if (Convert.ToInt32(UserID.ToString())==0)
        {
            return "CommentsReplyAdmin";
        }
        else
        {
            return "CommentsReply";
        }
        
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        System.Web.UI.HtmlControls.HtmlGenericControl li = (System.Web.UI.HtmlControls.HtmlGenericControl)this.Page.Master.Master.FindControl("BodyContentPlaceHolder").FindControl("LatestComments");

        li.Attributes["class"] = "active";
        if (!IsPostBack)
        {
            TodayTextBox.Text = DisplayDate2(DateTime.Now);
            TodayLabel.Text = DateTime.Now.ToShortDateString().ToString();
        }
    }
    protected void CommentsRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Reply")
        {
            CIDReplyLabel.Text = e.CommandArgument.ToString();
            IsReplyCheckBox.Visible = true;
            IsReplyCheckBox.Checked = true;
            SubmitComment.Text = "ثبت پاسخ";
            CommentBodyTextBox.Focus();


        }
    }
    protected void IsReplyCheckBox_CheckedChanged(object sender, EventArgs e)
    {
        if (!IsReplyCheckBox.Checked)
        {
            IsReplyCheckBox.Visible = false;
            CIDReplyLabel.Text = "0";
            SubmitComment.Text = "ثبت نظر";
        }

    }
    protected void CommentsRepeater_PreRender(object sender, EventArgs e)
    {
      
    }
    protected void SubmitComment_Click(object sender, EventArgs e)
    {
        string pid = CIDReplyLabel.Text;
        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);

        string SQLQuerry = "INSERT INTO comments (pid,commentbody,userid)values(@pid,@commentbody,@userid)";

        if (Convert.ToInt32(CIDReplyLabel.Text) == 0)
        {

        }
        else
        {
            SQLQuerry = "INSERT INTO commentsreply (cid,replybody,userid)values(@cid,@replybody,@userid)";
        }
        SqlCommand RegCmd = new SqlCommand(SQLQuerry, con);
        SqlParameter[] param = new SqlParameter[5];

        param[00] = new SqlParameter("@pid", SqlDbType.Int);
        param[01] = new SqlParameter("@commentbody", SqlDbType.NVarChar);
        param[02] = new SqlParameter("@userid", SqlDbType.NVarChar);

        param[03] = new SqlParameter("@cid", SqlDbType.NVarChar);
        param[04] = new SqlParameter("@replybody", SqlDbType.NVarChar);

        param[00].Value = pid;
        param[01].Value = CommentBodyTextBox.Text.Replace("\n", "<br />").Replace("\r\n", "<br />");
        param[02].Value = 0;
        param[03].Value = CIDReplyLabel.Text;
        param[04].Value = CommentBodyTextBox.Text.Replace("\n", "<br />").Replace("\r\n", "<br />");

        for (int r = 0; r < param.Length; r++)
        {
            RegCmd.Parameters.Add(param[r]);
        }

        try
        {
            con.Open();
            RegCmd.ExecuteScalar();
            CommentsRepeater.DataBind();
            CommentsDiv.Visible = true;

            if (Convert.ToInt32(CIDReplyLabel.Text) == 0)
            {
                CommentsLiteral.Text = "نظر شما با موفقیت ثبت شد.";

            }
            else
            {
                CommentsLiteral.Text = "پاسخ شما با موفقیت ثبت شد.";
            }


            CommentsDiv.Attributes["class"] = " alert alert-success alert-dismissable text-right";
            CommentBodyTextBox.Text = "";

            IsReplyCheckBox.Visible = false;
            CIDReplyLabel.Text = "0";
            SubmitComment.Text = "ثبت نظر";

        }
        catch (System.Data.SqlClient.SqlException ex)
        {
            string msg = "RegCmd Error:";
            msg += ex.Message;
            throw new Exception(msg);
        }
        finally
        {
            con.Close();
        }
    }

}