﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Farsi_Blog_Category : System.Web.UI.Page
{

    protected string DisplayDate(object Date)
    {
        DateTime CDate = Convert.ToDateTime(Date);
        if ((DateTime.Now - CDate).TotalDays < 1)
        {
            return "امروز";
        }
        else if ((DateTime.Now - CDate).TotalDays < 2)
        {
            return "دیروز";
        }
        else if ((DateTime.Now - CDate).TotalDays < 3)
        {
            return "2 روز پیش";
        }
        else
        {
            PersianDateTime PersianDate = new PersianDateTime(Convert.ToDateTime(Date));
            return PersianDate.ToString("dddd d MMMM yyyy");
        }

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //H1H1.InnerText = Request.QueryString["cat1"];

        //if (HttpContext.Current.Request.Url.AbsoluteUri.ToLower().Contains("news.aspx"))
        //{
        //    if (Request.QueryString["cat1"] != null)
        //    {
        //        if (Request.QueryString["cat1"] == "اخبار عمومی")
        //        {
        //            Response.Redirect(SSClass.WebDomain+"/all-news");
        //        }
        //        else if (Request.QueryString["cat1"] == "مقالات عمومی")
        //        {
        //            Response.Redirect(SSClass.WebDomain+"/all-articles");
        //        }
        //    }
        //    else
        //    {
        //        Response.Redirect(SSClass.WebDomain+"/all-articles");
        //    }
        //}
        //string cat1 = Page.RouteData.Values["cat1"].ToString().Trim();
        //Cat1Label.Text = cat1;

        string cat2 = "";
        if (Page.RouteData.Values["cat2"] != null)
        {
            cat2 = Page.RouteData.Values["cat2"].ToString().Trim().Replace("-", " ");
            Cat2Label.Text = cat2;
        }
        else
        {
            Top9SqlDataSource.SelectCommand = "SELECT top(50) * FROM [content] where cat1=@cat1 order by cdate desc";
            Top9Repeater.DataBind();
        }
        Page.Title = cat2 + " | وبسایت تهران آموز";
        //if (cat1 == "مقالات اصلی")
        //{
        //    Cat1LatinLabel.Text = "articles";
        //    Page.Title = "مقالات عمومی توسعه صنعت نمایشگاه و رویداد";

        //}
        //else if (cat1 == "اخبار عمومی توسعه صنعت نمایشگاه و رویداد")
        //{
        //    Cat1LatinLabel.Text = "news";
        //    Page.Title = "";

        //}
        //else
        //{
        //    Cat1LatinLabel.Text = "news";
        //}
    }
}