﻿<%@ Page Title="" Language="C#" MasterPageFile="~/farsi/faMasterPage.master" AutoEventWireup="true" CodeFile="Page.aspx.cs" Inherits="Farsi_Blog_Page" %>

<asp:Content ID="Content4" ContentPlaceHolderID="TopHeadContentPlaceHolder" runat="Server">
    <asp:Literal Text="" ID="CanonicalLiteral" runat="server" />
    <asp:Literal Text="" ID="KeyWordsLiteral" runat="server" />
    <asp:Literal Text="" ID="DescriptionLiteral" runat="server" />
    <meta name="robots" content="index, follow" />

    <!-- ----------------- <- OPEN GRAPH SHARE TAGs -> ----------------- -->
    <meta property="og:type" content="blog post" />
    <meta property="og:image" content="<%=SSClass.WebDomainFile %>/Images/Design/main-logo.png" />
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="200" />
    <meta property="og:description" content="<%=DescriptionLiteral.Text.Replace("<meta name=\"description\" content=\"","").Replace("\" /> ","") %>" />
    <meta property="og:url" content="<%=Request.Url %>/" />
    <meta property="og:site_name" content="<%=SSClass.FTitle %>" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContentPlaceHolder" runat="Server">
    <style>
        figcaption {
            background: rgba(250,250,250, .8);
            bottom: 0;
            box-sizing: border-box;
            /*position: absolute;*/
            width: 100%;
            padding: 1rem;
            -webkit-transition: max-height 0.3s ease-out;
        }

        figure {
            margin: 1rem;
            position: relative;
        }

        .blog-keywords {
            display:inline-block;
            float: right;
            padding: 7px;
            border: solid 1px #c2c2c2;
            background-color: #ededed;
            color: gray;
            margin-right:5px;
            text-decoration:none!important;
            cursor:pointer;
        }
        .blog-keywords:hover {
            border: solid 1px #268ea9;
            background-color: #3ba8c4;
            color: #fff;

        }
        .collapsible-item-title-link {
            padding: 10px;
            display: block;
        }

            .collapsible-item-title-link:hover {
                text-decoration: none
            }

            .collapsible-item-title-link:focus {
                text-decoration: none
            }

        .panel-heading {
            cursor: pointer;
        }

        .panel-default > .panel-heading {
            padding: 0px;
        }

        .panel-heading > h2 {
            display: block;
            padding: 15px;
            text-decoration: none
        }

        .collapsible-item-title-link-icon {
            padding: 10px
        }

        h2 {
            font-size: 25px;
        }

        .Comments {
        }

        .CommentsReply {
        }

        .CommentsReplyAdmin {
        }

        details[open] summary ~ * {
            animation: sweep .4s ease-in-out;
        }

        @keyframes sweep {
            0% {
                opacity: 0;
                margin-left: -200px;
                margin-right: 200px
            }

            100% {
                opacity: 1;
                margin-left: 0px;
                margin-right: 0px
            }
        }

        summary {
            cursor: pointer;
            border: 1px solid #cccccc;
            background-color: #dedede;
            padding-right: 15px
        }

        .InfoLabel {
            color: #ffffff;
            padding: 30px;
            display: block;
            font-weight: 900;
            font-size: 20px;
            text-shadow: 6px -9px 12px 8px black;
            text-shadow: 0 0 7px black;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContentPlaceHolder" runat="Server">
    <asp:Label ID="CIDMainLabel" Visible="false" runat="server" Text="Label"></asp:Label>
    <meta name="robots" content="index, follow" />
    <asp:Repeater ID="ContentRepeater" runat="server" DataSourceID="ContentSqlDataSource">
        <ItemTemplate>
            <div style="margin: 0px; padding: 0px;" class="container-fluid ">
                <div class="container MyHR" style="height: 8px; margin-bottom: 0px">
                </div>
            </div>
            <div class="container-fluid">
                <div class="container" style="min-height: 500px">
                    <div class="row UnitShadow">
                        <div class="col-sm-10">
                            <div class="row">
                                <div style="padding: 15px" class="col-xs-12">
                                    <div class="row">
                                        <div class="col-sm-8 ">
                                           <%-- <asp:Repeater runat="server" DataSourceID="Cat2SqlDataSource">
                                                <ItemTemplate>
                                                    <span style="color: #e91e63">&#x25CF</span>
                                                    <span id="Cat2Span" style="font-size: 17px"><%#Eval("cat2") %></span>
                                                    <span style="color: #e91e63">&#x25CF</span>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <asp:SqlDataSource runat="server" ID="Cat2SqlDataSource" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT  DISTINCT top(1) * FROM [ContentCat] WHERE ([CID] = @CID)">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="CIDMainLabel" PropertyName="Text" Name="CID" Type="String"></asp:ControlParameter>
                                                </SelectParameters>
                                            </asp:SqlDataSource>--%>

                                            <span style="display: block;"><%#Eval("headerup")%></span>
                                            <h1 style="margin-top: 5px; display: inline-block"><%#Eval("header")%></h1>
                                            <span style="display: block; margin-bottom: 15px;"><%#Eval("headerdown")%></span>
                                        </div>
                                        <div class="col-sm-4 small">
                                            <div style="color: gray; display: inline-block; padding-right: 15px; float: left">
                                                <span><%# DisplayDate( Eval("cdate")) %></span>
                                                <span>/</span>
                                                <span><%#Eval("pageview") %></span>
                                                <span class="glyphicon glyphicon-eye-open"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 DetailsBody text-justify" style="line-height: 2.428571; font-weight: 300;">
                                            <hr />
                                            <%#SetSlider(Eval("body")) %>
                                        </div>
                                        <%--<div class="col-xs-12 text-center">
                                            <span style="color: #9a9a9a; font-size: 20px">این مطلب رو دوست داشتید؟</span>
                                            <span style="font-size: 25px; color: #e91e63" class="glyphicon glyphicon-heart-empty"></span>
                                        </div>--%>
                                    </div>
                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                            <div class="row " style="margin: 50px 15px; text-shadow: none; background-color: #f7f7f7; border-radius: 10px;"
                                                data-aos="zoom-out" data-toggle="validator" role="form">
                                                <div class="col-xs-12 form-group text-center">
                                                    <h2 style="color: #3ba8c4">در کوتاهترین زمان با شما تماس خواهیم گرفت.</h2>
                                                </div>
                                                <div class="col-sm-3 form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                                                        <asp:TextBox runat="server" placeholder="نام و نام خانوادگی" class="form-control input-lg" ID="QNameFamilyTextBox" Font-Size="Small"></asp:TextBox>
                                                    </div>
                                                    <asp:RequiredFieldValidator ValidationGroup="qr" ErrorMessage="نام و نام خانوادگی خود را وارد کنید" ControlToValidate="QNameFamilyTextBox" runat="server" />
                                                </div>
                                                <div class="col-sm-3 form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><span class="glyphicon glyphicon-book"></span></div>
                                                        <asp:TextBox runat="server" placeholder="رشته ی تحصیلی" class="form-control input-lg" ID="QFieldTextBox" Font-Size="Small"></asp:TextBox>
                                                    </div>
                                                    <asp:RequiredFieldValidator ValidationGroup="qr" ErrorMessage="رشته ی تحصیلی خود را وارد کنید" ControlToValidate="QFieldTextBox" runat="server" />
                                                </div>
                                                <div class="col-sm-3 form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <span class="fas fa-phone"></span>
                                                        </div>
                                                        <asp:TextBox dir="ltr" runat="server" placeholder="09..." class="form-control input-lg" ID="QMobTextBox" Font-Size="Small"></asp:TextBox>
                                                    </div>
                                                    <asp:RequiredFieldValidator ValidationGroup="qr" ErrorMessage="شماره تماس خود را وارد کنید" ControlToValidate="QMobTextBox" runat="server" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="QMobTextBox" Display="Dynamic" ErrorMessage="*" ForeColor="#ff9900" Font-Bold="true" Style="position: absolute; right: 2px; bottom: -20px" Text="شماره تلفن همراه یازده رقمی و با 09 آغاز می شود. نمونه صحیح: 09123456789" ValidationExpression="^09[0-9]{9}$" ValidationGroup="qr"></asp:RegularExpressionValidator>
                                                </div>
                                                <div class="col-sm-3 form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><span class="glyphicon glyphicon-star"></span></div>
                                                        <asp:TextBox runat="server" placeholder="کد تخفیف" class="form-control input-lg" ID="SCodeTextBox" Font-Size="Small"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 text-center form-group">
                                                    <asp:Label ID="InfoLabel" Text="" runat="server" CssClass="InfoLabel" Visible="false" />
                                                </div>
                                                <div class="col-xs-12 form-group">
                                                    <div class="col-sm-5 form-group">
                                                    </div>
                                                    <div class="col-sm-2 form-group">
                                                        <asp:Button ValidationGroup="qr" ID="QuickRegButton" Text="ثبت" runat="server" CssClass="btn btn-primary btn-block btn-lg" OnClick="QuickRegButton_Click" />
                                                    </div>
                                                    <div class="col-sm-5 form-group">
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <%--<hr style="padding: 2px; margin: 10px" />
                                <div class="col-xs-6 col-sm-5 text-center">
                                    <asp:Repeater runat="server" DataSourceID="PrevPostSqlDataSource">
                                        <ItemTemplate>
                                            <a style="display: block; padding-bottom: 5px; text-decoration: none" class="small" href="<%=SSClass.WebDomain %>/<%#Eval("lan") %>/blog/page/<%#Eval("cid") %>/<%#MyFunc.SafeString(Eval("header"))%>">
                                                <strong class="well well-sm" style="display: block; font-size: 15px;"><span class="glyphicon glyphicon-menu-right"></span>مطلب قبلی</strong>
                                                <img class="zoom" style="margin-bottom: 5px; max-width: 200px!important" src='<%#Eval("Photo") %>' alt='<%#Eval("Header") %>' title='<%#Eval("Header") %>' />
                                                <h2 style="font-size: 16px"><%#Eval("Header") %></h2>
                                            </a>
                                            <p class="help-block text-justify small NewsP"><%#Eval("Summary")+"" %></p>
                                            <span class="label label-info small"><%# Eval("PageView  ")+" بازدید " %></span>
                                            <span style="color: #a7a7a7; font-weight: 100!important; margin-right: 5px" class="small"><%#MyFunc.DisplayDate(Eval("cdate")) %></span>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <asp:SqlDataSource ID="PrevPostSqlDataSource" runat="server" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT top(1) * FROM [content] WHERE ([cid] < @cid) and cat1=(select cat1 from content where cid=@cid) and isdeleted=0 order by cid desc">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="CIDMainLabel" PropertyName="Text" Name="cid" Type="Int32"></asp:ControlParameter>
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </div>
                                <div class="col-sm-2">
                                </div>
                                <div class="col-xs-6 col-sm-5 text-center">
                                    <asp:Repeater runat="server" DataSourceID="NextPostSqlDataSource">
                                        <ItemTemplate>
                                            <a style="display: block; padding-bottom: 5px; text-decoration: none" class="small" href="<%=SSClass.WebDomain %>/<%#Eval("lan") %>/blog/page/<%#Eval("cid") %>/<%#MyFunc.SafeString(Eval("header"))%>">
                                                <strong class="well well-sm" style="display: block; font-size: 15px">مطلب بعدی <span class="glyphicon glyphicon-menu-left"></span></strong>
                                                <img class="zoom" style="margin-bottom: 5px; max-width: 200px!important" src='<%#Eval("Photo") %>' alt='<%#Eval("Header") %>' title='<%#Eval("Header") %>' />
                                                <h2 style="font-size: 16px"><%#Eval("Header") %></h2>
                                            </a>
                                            <p class="help-block text-justify small NewsP"><%#Eval("Summary")+"" %></p>
                                            <span class="label label-info small"><%# Eval("PageView  ")+" بازدید " %></span>
                                            <span style="color: #a7a7a7; font-weight: 100!important; margin-right: 5px" class="small"><%#MyFunc.DisplayDate(Eval("cdate")) %></span>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <asp:SqlDataSource ID="NextPostSqlDataSource" runat="server" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT top(1) * FROM [content] WHERE ([cid] > @cid) and cat1=(select cat1 from content where cid=@cid) and isdeleted=0  order by cid asc">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="CIDMainLabel" PropertyName="Text" Name="cid" Type="Int32"></asp:ControlParameter>
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </div>--%>
                                <div class="col-xs-12">
                                    <hr />
                                </div>
                                <div class="col-xs-12 col-sm-12">
                                    <asp:Repeater ID="NewsTagRepeater" runat="server" DataSourceID="NewsTagSqlDataSource">
                                        <HeaderTemplate>
                                            <span class="blog-keywords"><span class="glyphicon glyphicon-tags small"></span>برچسب ها</span>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <a href="<%=SSClass.WebDomain %>/tag/<%#MyFunc.SafeString(Eval("tagname"))%>" class="blog-keywords"><span class="glyphicon"></span>#<%#Eval("tagname").ToString().Replace(" ","_")%></a>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <asp:SqlDataSource ID="NewsTagSqlDataSource" runat="server" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT DISTINCT * FROM [contenttag] inner join tag on contenttag.tid=tag.tid WHERE ([cid] = @cid) and contenttag.isdeleted=0">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="CIDMainLabel" PropertyName="Text" Name="cid" Type="Int32"></asp:ControlParameter>
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="panel panel-default">
                                <div class="panel-heading" style="padding: 15px">
                                    <h3 class="panel-title">مقالات بیشتر</h3>
                                </div>
                                <div class="panel-body text-center">
                                    <asp:Repeater runat="server" DataSourceID="ContentSqlDataSource">
                                        <ItemTemplate>
                                            <a style="display: block; border-bottom: 1px solid #c2c2c2; padding-bottom: 5px" class="small" href="<%=SSClass.WebDomain %>/<%#MyFunc.SafeString(Eval("cat2title"))%>/<%#MyFunc.SafeString(Eval("header"))%>/<%#Eval("cid") %>">
                                                <img style="margin-bottom: 5px; margin-top: 5px" src='<%#Eval("Photo") %>' alt='<%#Eval("Header") %>' title='<%#Eval("Header") %>' />
                                                <h2 style="font-size: 12px; line-height: 1.5"><%#Eval("Header") %></h2>
                                            </a>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <asp:SqlDataSource ID="ContentSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DrB_DBConnectionString %>" SelectCommand="SELECT top(10) dbo.[Content].* , dbo.Tree.Name AS cat2title FROM dbo.[Content] INNER JOIN dbo.Tree ON dbo.[Content].cat2 = dbo.Tree.NodeID WHERE   cat1=N'fa-blog' and isdeleted=0  order by newid()"></asp:SqlDataSource>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <hr />
                        </div>
                    </div>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <asp:SqlDataSource runat="server" ID="ContentSqlDataSource" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT DISTINCT * FROM [Content] WHERE ([CID] = @CID) and isdeleted=0 ">
        <SelectParameters>
            <asp:ControlParameter ControlID="CIDMainLabel" PropertyName="Text" Name="cid" Type="Int32"></asp:ControlParameter>
        </SelectParameters>
    </asp:SqlDataSource>
    <div class="container-fluid" style="display: none">
        <div class="container">
            <asp:UpdatePanel ID="CommentsUpdatePanel" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-xs-12">
                            <hr />
                            <a name="comments"></a>
                        </div>
                    </div>
                    <asp:Repeater ID="CommentsRepeater" runat="server" DataSourceID="CommentsSqlDataSource" OnItemCommand="CommentsRepeater_ItemCommand" OnPreRender="CommentsRepeater_PreRender">
                        <ItemTemplate>
                            <div class="row">
                                <div class="col-sm-1">
                                    <img src="<%=SSClass.WebDomain %>/Images/Design/default-female-profile.JPG" />
                                </div>
                                <div class="col-sm-11">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <p class="Comments">
                                                <strong><%#AdminComment(Eval("userid")) %></strong>
                                                <asp:Repeater ID="UserNameRepeater" runat="server" DataSourceID="UserNameSqlDataSource">
                                                    <ItemTemplate>
                                                        <strong><%#Eval("name")+" "+Eval("family") %> :</strong>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <asp:SqlDataSource runat="server" ID="UserNameSqlDataSource" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT [Name], [Family] FROM [Users] WHERE ([UserID] = @UserID)">
                                                    <SelectParameters>
                                                        <asp:ControlParameter ControlID="UseridLabel" PropertyName="Text" Name="UserID" Type="Int32"></asp:ControlParameter>
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                                <asp:Label ID="UseridLabel" Visible="false" runat="server" Text='<%#Eval("userid") %>'></asp:Label>
                                                <%#Eval("commentbody") %>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <span style="font-size: 15px; color: #e91e63" class="glyphicon glyphicon-heart-empty"></span>
                                                        <span style="color: #e91e63">●</span>
                                                        <span style="color: #e91e63; cursor: pointer">پاسخ دادن</span>
                                                        <span style="color: #e91e63">●</span>
                                                        <span style="color: gray"><%#DisplayCommentDate(Eval("cdate")) %></span>
                                                    </div>
                                                    <div class="col-sm-6 text-left">
                                                        <asp:Button CssClass="btn btn-sm btn-primary" ID="ReplyButton" CommandName="Reply" CommandArgument='<%#Eval("cid") %>' runat="server" Text="پاسخ به این نظر" />
                                                    </div>
                                                </div>
                                            </p>
                                        </div>
                                    </div>
                                    <asp:Repeater ID="CommentsReplyRepeater" runat="server" DataSourceID="CommentsReplySqlDataSource">
                                        <ItemTemplate>
                                            <div class="row">
                                                <div class="col-sm-1">
                                                    <img src="<%=SSClass.WebDomain %>/Images/Design/default-female-profile.JPG" />
                                                </div>
                                                <div class="col-sm-11">
                                                    <p class='<%#CommentCss(Eval("userid")) %>'>
                                                        <strong><%#AdminComment(Eval("userid")) %></strong>
                                                        <asp:Repeater ID="UserNameRepeater" runat="server" DataSourceID="UserNameSqlDataSource">
                                                            <ItemTemplate>
                                                                <strong><%#Eval("name")+" "+Eval("family") %> :</strong>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                        <asp:SqlDataSource runat="server" ID="UserNameSqlDataSource" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT [Name], [Family] FROM [Users] WHERE ([UserID] = @UserID)">
                                                            <SelectParameters>
                                                                <asp:ControlParameter ControlID="UseridLabel" PropertyName="Text" Name="UserID" Type="Int32"></asp:ControlParameter>
                                                            </SelectParameters>
                                                        </asp:SqlDataSource>
                                                        <asp:Label ID="UseridLabel" Visible="false" runat="server" Text='<%#Eval("userid") %>'></asp:Label>
                                                        <%#Eval("replybody") %>
                                                        <br />
                                                        <span style="font-size: 15px; color: #e91e63" class="glyphicon glyphicon-heart-empty"></span>
                                                        <span style="color: #e91e63">●</span>
                                                        <span style="color: gray"><%#DisplayCommentDate(Eval("cdate")) %></span>
                                                    </p>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <asp:SqlDataSource runat="server" ID="CommentsReplySqlDataSource" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT * FROM Commentsreply WHERE (cid = @cid)">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="CIDLabel" PropertyName="Text" Name="cid" Type="Int32"></asp:ControlParameter>
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </div>
                            </div>
                            <hr style="border-top: 1px solid #a4aab4" />
                            <asp:Label ID="CIDLabel" Visible="false" runat="server" Text='<%#Eval("cid") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:Repeater>
                    <asp:SqlDataSource runat="server" ID="CommentsSqlDataSource" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT * FROM Comments WHERE (OID = @OID)">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="CIDMainLabel" PropertyName="Text" Name="OID" Type="Int32"></asp:ControlParameter>
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <div class="row">
                        <div class="col-xs-12 BYekan">
                            <div runat="server" id="CommentsDiv" visible="false" class="alert alert-success alert-dismissable text-right">
                                <asp:Literal ID="CommentsLiteral" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <asp:Panel ID="LoggedOutPanel" runat="server">
                        <div class="row">
                            <div class="col-xs-12 text-center" style="padding-bottom: 8px">
                                <a class="btn btn-success BYekan" style="text-decoration: none" href="#" data-toggle="modal" data-target="#LoginModal"><span style="margin-left: 10px" class="glyphicon glyphicon-new-window"></span>برای ثبت نظر، وارد حساب کاربری خود شوید </a>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="LoggedInPanel" runat="server">
                        <div class="row">
                            <div class="col-xs-12" style="padding-bottom: 8px">
                                <hr />
                                <asp:CheckBox Style="font-weight: 100!important" Checked="true" ID="IsReplyCheckBox" runat="server" AutoPostBack="true" OnCheckedChanged="IsReplyCheckBox_CheckedChanged" Text="این نظر در قالب یک پاسخ ثبت خواهد شد." Visible="false" />

                                <asp:TextBox placeholder="با نظرات خود، سایر کاربران را در یک خرید خوب راهنمایی کنید." CssClass="form-control input-sm" runat="server" ID="CommentBodyTextBox" TextMode="MultiLine" Style="max-width: 100%; min-width: 100%; min-height: 200px" ValidationGroup="CommentSubmit" />
                                <asp:RequiredFieldValidator ValidationGroup="CommentSubmit" Style="margin-top: 10px; margin-bottom: 10px;" Font-Size="Small" ID="RequiredFieldValidator5" runat="server" Text="ابتدا نظر خود را وارد نمایید." ErrorMessage="ابتدا نظر خود را وارد نمایید." ControlToValidate="CommentBodyTextBox" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>

                            </div>
                            <div class="col-sm-8">
                                <label style="font-weight: 100!important">
                                    <asp:CheckBox ID="AgreementCheckBox" runat="server" Checked="true" />
                                    <span>شرایط و قوانین را مطالعه نموده و با کلیه موارد آن موافقم.</span>
                                </label>

                            </div>
                            <div class="col-sm-4 text-left">
                                <asp:Button CssClass="btn btn-success btn-sm BYekan" ID="SubmitComment" runat="server" Text="ثبت نظر" OnClick="SubmitComment_Click" ValidationGroup="CommentSubmit" />
                                <asp:Label ID="CIDReplyLabel" Visible="false" runat="server" Text="0"></asp:Label>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContentPlaceHolder" runat="Server">
    <script type="text/javascript" src='<%= ResolveUrl(SSClass.WebDomainFile+"/js/jquery.sliderPro.min.js") %>'></script>
    <script type="text/javascript">
        $(document).ready(function ($) {
            $('.MakeThisSlider2').sliderPro({
                keyboard: false,
                fullscreen: true,
                autoHeight: true,
                width: '100%',
                arrows: true,
                centerImage: true,
                fade: true,
            });
            $('.snip1135').children().each(function (index) {
                if ($('#Cat2Span').text() == $(this).text()) {
                    $(this).addClass("current")
                }
            });
            //$('.pre-title').click(function (event) {
            //    event.preventDefault();

            //    //alert($(this).next().html())
            //    $(this).next().slideToggle(100)
            //});
            //$('.panel-heading').click(function () {

            //    $(this).children().first().children().first().click();
            //});
        });
    </script>
</asp:Content>
