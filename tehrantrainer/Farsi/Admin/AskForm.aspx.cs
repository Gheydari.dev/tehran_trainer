﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Farsi_Admin_AskForm : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void DeleteButton_Click(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection(DataLayer.connectionString);

        string sc = "";

        for (int i = 0; i < AskFormGridView.Rows.Count; i++)
        {
            CheckBox SelectCheckBox = (CheckBox)AskFormGridView.Rows[i].FindControl("SelectCheckBox");
            if (SelectCheckBox != null)
            {
                if (SelectCheckBox.Checked)
                {
                    Label AFIDLabel = (Label)AskFormGridView.Rows[i].FindControl("AFIDLabel");

                    sc += string.Format(@" 
                    update  AskForm set isdeleted = 1 where AFID ={0} ", AFIDLabel.Text);
                }
            }
        }
        if (sc != "")
        {

            string SQLQuerry = sc;

            SqlCommand RegCmd = new SqlCommand(SQLQuerry, con);

            try
            {
                con.Open();
                RegCmd.ExecuteScalar();
                AskFormGridView.DataBind();
                InfoDiv.Visible = true;
                InfoLabel.Text = "موفقیت آمیز.";
                InfoDiv.Attributes["class"] = " alert alert-success alert-dismissable";

            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                string msg = "RegCmd Error:";
                msg += ex.Message;
                throw new Exception(msg);
            }
            finally
            {
                con.Close();
            }
        }
        else
        {
            InfoDiv.Visible = true;
            InfoLabel.Text = "No items selected!";
            InfoDiv.Attributes["class"] = " alert alert-warning alert-dismissable ";
        }
    }
}