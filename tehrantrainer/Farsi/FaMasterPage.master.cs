﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class FaMasterPage : System.Web.UI.MasterPage
{

    protected string ShowName(object XName)
    {
        if (XName.ToString().Length > 30)
        {
            return XName.ToString().Substring(0, 30) + " ... ";
        }
        else
        {
            return XName.ToString();
        }
    }
    protected string InsertARow(object XName)
    {
        if ((Convert.ToInt32(XName.ToString()) % 7 == 0 && Convert.ToInt32(XName.ToString()) <= 7) || (Convert.ToInt32(XName.ToString()) % 7 == 6 && Convert.ToInt32(XName.ToString()) > 7))
        {
            return "<div class=\"col-md-12\"><hr class=\"Menu-divider\" /></div>";
        }
        else
        {
            return "";
        }
    }


    protected string DisplayDate(object Date)
    {
        PersianDateTime PersianDate = new PersianDateTime(Convert.ToDateTime(Date));
        return PersianDate.ToString("dddd d MMMM yyyy ساعت hh:mm tt");
    }
    protected bool IsAdmin()
    {
        if (Session["admin"] != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //Response.Cookies["telegram"].Value = "false";
        if (Session["CartItemsCount"] != null)
        {
            //CartLinkButton.Text = "(" + Session["CartItemsCount"].ToString() + ")" + "<span style='padding-top:2px;padding-right:15px;padding-left:15px;font-size:20px' class='glyphicon glyphicon-shopping-cart'></span>";
            //CartItemsCountLabel.Text = Session["CartItemsCount"].ToString();
            //   CartImageButton.ImageUrl = SSClass.WebDomainFile+"/Images/Design/ShoppingCartItems/" + Convert.ToInt32(Session["CartItemsCount"].ToString()).ToString() + ".png";
        }
        else
        {
            Session["CartItemsCount"] = "00";
            //  CartImageButton.ImageUrl = SSClass.WebDomainFile + "/Images/Design/ShoppingCartItems/" + Convert.ToInt32(Session["CartItemsCount"].ToString()).ToString() + ".png";

        }
        ShoppingCartBadgeLabel.Text = Session["CartItemsCount"].ToString();

    }
    protected void Page_Init(object sender, EventArgs e)
    {
        //if (this.ContentPlaceHolder1.Page.ToString().ToLower() == "asp.Farsi_default_aspx")
        //{
        //    H1Literal.Text = "<h1 style=\"margin:3px;padding:0px;text-align: center;width: 150px;display: block;\">" + SSClass.FTitle + "</h1>";

        //}
        //else
        //{
        //    H1Literal.Text = "<span style=\"margin:3px;padding:0px;text-align: center;width: 150px;display: block;\">" + SSClass.FTitle + "</span>";

        //}

        DataSet ds = new DataSet();
        SqlConnection connection = new SqlConnection(DataLayer.connectionString );
        SqlCommand command = new SqlCommand("select * from tree where GroupName=N'Fa-Menu' order by ordernum", connection);
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        adapter.Fill(ds);

        DataTable table = ds.Tables[0];
        DataRow[] parentMenus = table.Select("ParentId = 0");

        var sb = new StringBuilder();
        string unorderedList = GenerateUL(parentMenus, table, sb, 0, 0);
        MenuLiteral.Text = unorderedList;


        // CartImageButton.DataBind();
        ShoppingCartBadgeLabel.DataBind();

        //CartImageButton2.DataBind();
        //SessionSituLabel.Text = "userid:" + Session["userid"].ToString() + " and ispublic:" + Session["ispublic"].ToString();
        if (Session["name"] != null)
        {
            LoggedOutLi.Visible = false;
            LoggedInLi.Visible = true;
            //WelcomeLabel.Text = "سلام " + Session["name"].ToString();
            WelcomeLabel.Text = "حساب کاربری من";


        }
        else
        {
            LoggedOutLi.Visible = true;
            LoggedInLi.Visible = false;

        }
        //TodayDateLabel.Text = "امروز " + DisplayDate(DateTime.Now);
        //MiladiTodayDateLabel.Text = "Today "+ DateTime.Today.DayOfWeek+ " "+DateTime.Today ;
        //SqlDataSource AllProducts =(SqlDataSource) Application["allproducts"];
        //AllProducts.DataBind();
        //SqlDataSource1.SelectCommand = Application["sc"].ToString();
        //SqlDataSource1.DataBind();





    }

    //protected void SearchTextBox_TextChanged(object sender, EventArgs e)
    //{
    //    IsSearchingDiv.Attributes["style"] = "display:block";
    //    string SearchKeyword = SearchTextBox.Text.ToString().Trim();
    //    string SelectCommand = "";

    //    if (SearchKeyword.Length >= 2)
    //    {
    //        DataTable SearchedData = new DataTable();
    //        if (Application["ProductTable"] != null)
    //        {
    //            SearchTabDiv.Visible = true;
    //            Application.Lock();
    //            DataTable ProductTableDataTable = (DataTable)Application["ProductTable"];
    //            Application.UnLock();
    //            DataTable table = new DataTable();
    //            Int64 number;
    //            bool result = Int64.TryParse(SearchKeyword, out number);
    //            if (result)
    //            {
    //                SelectCommand = "((fname LIKE '%" + SearchKeyword + "%' or  ename LIKE '%" + SearchKeyword + "%' or model like '%" + SearchKeyword + "%' or pid= " + SearchKeyword + " ))";
    //            }
    //            else
    //            {
    //                SelectCommand = "((fname LIKE '%" + SearchKeyword + "%' or  ename LIKE '%" + SearchKeyword + "%' or model like '%" + SearchKeyword + "%' ) )";
    //            }
    //            if (ProductTableDataTable.Select(SelectCommand).Length > 0)
    //            {
    //                table = ProductTableDataTable.Select(SelectCommand).Take(20).CopyToDataTable();
    //                SearchRepeater.DataSource = table;
    //                SearchRepeater.DataBind();
    //                NoSearchLabel.Visible = false;
    //                NoSearchLabel.Text = "&nbsp;";
    //                IsSearchingDiv.Attributes["style"] = "display:none";
    //            }
    //            else
    //            {
    //                SearchRepeater.DataSource = table;
    //                SearchRepeater.DataBind();
    //                NoSearchLabel.Visible = true;
    //                NoSearchLabel.Text = "موردی یافت نشد!";
    //                IsSearchingDiv.Attributes["style"] = "display:none";

    //            }
    //            //ProductTableDataTable.Select("((fname LIKE '%" + SearchTextBox.Text.ToString() + "%' or  ename LIKE '%" + SearchTextBox.Text.ToString() + "%') )");


    //            //Label1.Text = "search is starting";
    //            //SqlDataSource1.SelectCommand = "SELECT TOP (10) AllImages.ImgSrc, Product.FName, Product.EName, Product.pid FROM AllImages INNER JOIN Product ON AllImages.pid = Product.pid where (fname LIKE N'%" + SearchTextBox.Text + "%' or  ename LIKE N'%" + SearchTextBox.Text + "%') and (allimages.imgorder=1)";

    //        }
    //        else
    //        {
    //Response.Redirect(SSClass.WebDomain);
    //        }
    //    }
    //    else
    //    {
    //        SearchTabDiv.Visible = false;
    //        IsSearchingDiv.Attributes["style"] = "display:none";
    //    }
    //    if (SearchRepeater.Items.Count < 1)
    //    {
    //        SearchTabDiv.Visible = true;
    //        NoSearchLabel.Visible = true;
    //    }
    //    else
    //    {
    //        NoSearchLabel.Visible = false;
    //    }
    //}
    protected void LoginButton_Click(object sender, EventArgs e)
    {
        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);

        string FetchUserSqlSyntax = "SELECT email,password,name,ordercount,userid,regsitu,MobPhone From users Where (email=@email and password=@password) or (MobPhone=@email and password=@password)";
        SqlCommand FetchUserCmd = new SqlCommand(FetchUserSqlSyntax, con);
        SqlParameter[] FetchUserParam = new SqlParameter[2];

        FetchUserParam[0] = new SqlParameter("@email", SqlDbType.NVarChar);
        FetchUserParam[1] = new SqlParameter("@password", SqlDbType.NVarChar);

        FetchUserParam[0].Value = EmailTextBox.Text.ToLower();
        FetchUserParam[1].Value = PasswordTextBox.Text; ;

        for (int i = 0; i < FetchUserParam.Length; i++)
        {
            FetchUserCmd.Parameters.Add(FetchUserParam[i]);
        }
        SqlDataReader UserDr;
        try
        {
            con.Open();
            UserDr = FetchUserCmd.ExecuteReader();
            if (UserDr.Read())
            {
                string TempUserID = Session["userid"].ToString();
                Session["userid"] = UserDr["userid"].ToString();
                Session["name"] = UserDr["name"].ToString();
                Session["ordercount"] = UserDr["ordercount"];
                Session["regsitu"] = UserDr["regsitu"];
                Session["mobphone"] = UserDr["mobphone"];
                Session["ispublic"] = "false";
                Session["addressid"] = 0;
                con.Close();



                if (Request.QueryString["Ref"] == "shipping")
                {
                    Response.Redirect(SSClass.WebDomain + "/shipping?Ref=shipping-login");
                }
                if (this.BodyContentPlaceHolder.Page.ToString().ToLower() == "asp.Farsi_register_aspx")
                {
                    Response.Redirect(SSClass.WebDomain);
                }
                Page.Response.Redirect(Page.Request.Url.ToString().ToLower());
            }
            else
            {
                //LoginMsgLabel.Visible = true;
                //LoginMsgLabel.Text = "اطلاعات ورودی معتبر نیست!";
            }
        }

        catch (System.Data.SqlClient.SqlException ex)
        {
            string msg = "FetchUserCmd Error:";
            msg += ex.Message;
            throw new Exception(msg);
        }

        finally
        {
            //con.Close();
        }
    }
    protected void LogOutLinkButton_Click(object sender, EventArgs e)
    {
        Session["userid"] = null;
        Session["name"] = null;
        Session["ispublic"] = "true";
        //Page.Response.Redirect(Page.Request.Url.ToString().ToLower());
        Session["CartItemsCount"] = "00";
        Session.Clear();
        Page.Response.Redirect(SSClass.WebDomain);
    }
    protected void TelegramImageButton_Click(object sender, ImageClickEventArgs e)
    {
        Response.Cookies["telegram"].Value = "true";
        Response.Cookies["telegram"].Expires = DateTime.Now.AddHours(24);
        Response.Redirect(SSClass.Telegram);
    }
    protected void SkipTelegramButton_Click(object sender, EventArgs e)
    {
        Response.Cookies["telegram"].Value = "true";
        Response.Cookies["telegram"].Expires = DateTime.Now.AddHours(10);
        Response.Redirect(Request.Url.ToString());


    }
    protected void SubscribeLinkButton_Click(object sender, EventArgs e)
    {
        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);

        string SQLQuerry = "if not exists (select * from subscribe where email=@email) INSERT into  Subscribe (email,situ) values (@email,N'new')";

        SqlCommand RegCmd = new SqlCommand(SQLQuerry, con);
        SqlParameter[] param = new SqlParameter[1];

        param[00] = new SqlParameter("@email", SqlDbType.NVarChar);

        param[00].Value = SubscribeTextBox.Text;

        for (int r = 0; r < param.Length; r++)
        {
            RegCmd.Parameters.Add(param[r]);
        }
        try
        {
            con.Open();
            RegCmd.ExecuteScalar();
            SubscribeLabel.Text = "ایمیل شما با موفقیت ثبت شد.";

        }
        catch (System.Data.SqlClient.SqlException ex)
        {
            string msg = "RegCmd Error:";
            msg += ex.Message;
            throw new Exception(msg);
        }
        finally
        {
            con.Close();
        }
    }
    protected void RegisterButton_Click(object sender, EventArgs e)
    {
        //string EncodedResponse = Request.Form["g-Recaptcha-Response"];
        //bool IsCaptchaValid = (ReCaptchaClass.Validate(EncodedResponse) == "True" ? true : false);


        string saveStat = "false";

        //if (IsCaptchaValid)
        if (RegMobTextBox.Text.ToString().ToLower().Trim().Length > 4 && RegRepeatPasswordTextBox.Text.Length != 0)
        {
            DataLayer dl = new DataLayer();
            string CS = dl.dataLayerConnectionString;
            SqlConnection con = new SqlConnection(CS);

            string step = "";
            string FetchKodemelliSqlSyntax = "select * from users where MobPhone=@MobPhone;";
            SqlCommand FetchUseridCmd = new SqlCommand(FetchKodemelliSqlSyntax, con);

            SqlParameter[] UseridParam = new SqlParameter[1];

            UseridParam[0] = new SqlParameter("@MobPhone", SqlDbType.NVarChar);
            UseridParam[0].Value = RegMobTextBox.Text.ToString().ToLower().Trim();

            FetchUseridCmd.Parameters.Add(UseridParam[0]);

            try
            {
                con.Open();
                SqlDataReader FetchUserDr = FetchUseridCmd.ExecuteReader();
                if (FetchUserDr.Read())
                {

                    step = "stop";
                }
                else
                {
                    step = "go";
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                string msg = "FetchUserCmd Error:";
                msg += ex.Message;
                throw new Exception(msg);
            }

            finally
            {
                con.Close();
            }
            if (step == "go")
            {
                string RegSqlSyntax = "begin DECLARE @myInsertedTable table (insertedid  bigint NOT NULL)  INSERT INTO Users (name,family,email,password,mobphone,gender,newsletter,birthday,EduGroup,Field) OUTPUT inserted.Userid into @myInsertedTable VALUES (@name,@family,@email,@password,@mobphone,N'مرد',N'بلی',getdate(),@EduGroup,@Field) update [orders] set userid=(select insertedid from @myInsertedTable),ordersitu=N'تایید' where ordersid=@ordersid  select insertedid from @myInsertedTable delete from @myInsertedTable  end ";
                
                SqlCommand RegCmd = new SqlCommand(RegSqlSyntax, con);
                SqlParameter[] param = new SqlParameter[8];

                param[00] = new SqlParameter("@name", SqlDbType.NVarChar);
                param[01] = new SqlParameter("@family", SqlDbType.NVarChar);
                param[02] = new SqlParameter("@email", SqlDbType.NVarChar);
                param[03] = new SqlParameter("@password", SqlDbType.NVarChar);
                param[04] = new SqlParameter("@mobphone", SqlDbType.NVarChar);
                param[05] = new SqlParameter("@ordersid", SqlDbType.NVarChar);
                param[06] = new SqlParameter("@EduGroup", SqlDbType.NVarChar);
                param[07] = new SqlParameter("@Field", SqlDbType.NVarChar);

                param[00].Value = NameTextBox.Text; ;
                param[01].Value = FamilyTextBox.Text; ;
                param[02].Value = "";
                param[03].Value = RegPasswordTextBox.Text;
                param[04].Value = RegMobTextBox.Text.Trim();
                param[05].Value = Session["ordersid"].ToString();
                param[06].Value = EduGroupDropDownList.SelectedValue;
                param[07].Value = FieldDropDownList.SelectedValue;


                for (int i = 0; i < param.Length; i++)
                {
                    RegCmd.Parameters.Add(param[i]);
                }

                try
                {
                    con.Open();
                    Session["userid"] = RegCmd.ExecuteScalar().ToString();
                    Session["name"] = "کاربر مهمان";
                    Session["ordercount"] = 1;
                    Session["ispublic"] = "false";
                    Session["addressid"] = 0;
                    Session["mobphone"] = RegMobTextBox.Text.Trim();




                    if (RegMobTextBox.Text != "")
                    {
                        Int64 number;
                        bool result = Int64.TryParse(RegMobTextBox.Text.Trim(), out number);
                        if (result)
                        {
                            WebReference.Send sms = new WebReference.Send();
                            long[] rec = null;
                            byte[] status = null;
                            int retval = sms.SendSms("9358177180", "4837", RegMobTextBox.Text.Trim().Split('\n'), "50001333263333", "به تهران آموز خوش آمدید. ثبت نام شما انجام شد. کلمه عبور: " + PasswordTextBox.Text + " | " + SSClass.FTitle + "  |  " + SSClass.PhoneNo, false, "", ref rec, ref status);
                        }
                    }

                    if (Request.QueryString["Ref"] == "shipping")
                    {
                        Response.Redirect(SSClass.WebDomain + "/shipping/?Ref=newreg");
                    }
                    if (Request.QueryString["Ref"] == "StormPool")
                    {
                        Response.Redirect(SSClass.WebDomain + "/?Ref=StormPool");
                    }
                    Response.Redirect(SSClass.WebDomain + "/?Ref=newreg");


                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    string msg = "RegCmd Error:";
                    msg += ex.Message;
                    throw new Exception(msg);
                }
                finally
                {
                    con.Close();
                }
            }
            else
            {
                RegMsgLabel.Visible = true;
                RegMsgLabel.Text = "پست الکترونیک  و یا شماره موبایل وارد شده قبلا در سامانه ثبت شده." + "" + "در صورتی که کلمه عبور خود را فراموش کرده اید،" + "<strong><a href='PasswordRecovery.aspx'>کلیک کنید</a></strong>" + ".";
            }
        }
        else
        {
            RegMsgLabel.Visible = true;
            RegMsgLabel.Text = "خطا در اطلاعات ورودی!";
        }
    }
    protected string GenerateUL(DataRow[] menu, DataTable table, StringBuilder sb, int lnum, int _nodeid)
    {
        if (lnum == 0)
        {
            sb.AppendLine("<ul class=\"nav navbar-nav text-center\">");

        }
        else
        {
            sb.AppendLine("<ul class=\"dropdown-menu dropdown-menu-right text-center sss l" + (lnum).ToString() + "dropdown-menu \">");

        }
        if (menu.Length > 0)
        {
            foreach (DataRow dr in menu)
            {
                string handler = dr["href"].ToString();
                string menuText = dr["name"].ToString();
                string line = "";
                string nodeid = dr["nodeid"].ToString();
                string parentId = dr["ParentId"].ToString();
                DataRow[] subMenu = table.Select(String.Format("ParentId = {0}", nodeid));

                if (subMenu.Length > 0)
                {
                    if (lnum == 0)
                    {
                        line = String.Format("<li class=\"dropdown l{2}dropdown\"><a href=\"{0}\"><b class=\"caret\"></b>{1}</a>", handler, menuText, (lnum + 1).ToString());
                    }
                    else
                    {
                        line = String.Format("<li class=\"l{2}dropdown dropdown-submenu\"><a href=\"{0}\">{1}</a>", handler, menuText, (lnum + 1).ToString());
                    }
                }
                else
                {
                    if (lnum == 0)
                    {
                        line = String.Format("<li class=\"dropdown l{2}dropdown \"><a href=\"{0}\">{1}</a>", handler, menuText, (lnum + 1).ToString());
                    }
                    else
                    {
                        line = String.Format("<li class=\"l{2}dropdown \"><a href=\"{0}\">{1}</a>", handler, menuText, (lnum + 1).ToString());
                    }
                }
                sb.Append(line);

                if (subMenu.Length > 0 && !nodeid.Equals(parentId))
                {
                    var subMenuBuilder = new StringBuilder();
                    sb.Append(GenerateUL(subMenu, table, subMenuBuilder, ++lnum, Convert.ToInt32(nodeid)));
                    --lnum;
                }

                sb.Append("</li>");
            }
        }
        //sb.Append(string.Format("<li class=\"l1dropdown \"><a href=\"\" data-nodeid={0} class='addnewnode'>افزودن آیتم جدید</a></li>", _nodeid.ToString()));

        sb.Append("</ul>");
        return sb.ToString();
    }


    protected void FieldDropDownList_DataBound(object sender, EventArgs e)
    {
        FieldDropDownList.Items.Insert(0, new ListItem("انتخاب رشته ی تحصیلی...", "-1"));
    }

    protected void EduGroupDropDownList_DataBound(object sender, EventArgs e)
    {
        EduGroupDropDownList.Items.Insert(0, new ListItem("انتخاب گروه آموزشی...", "-1"));
    }
}
