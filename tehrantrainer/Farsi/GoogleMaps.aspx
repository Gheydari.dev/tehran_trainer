﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GoogleMaps.aspx.cs" Inherits="Farsi_GoogleMaps" %>

<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style>
        #map {
            height: 400px;
            width: 100%;
        }
    </style>
</head>
<body>    
    <div id="map"></div>
    <script type="text/javascript" src='<%= ResolveUrl("~/js/jquery.min.js") %>'></script>
    <script>
        
        <asp:Literal ID="MapScriptLiteral" runat="server" ></asp:Literal>
    </script>
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBtMJlGHLzOeiGBd2R0D2FgnmZsbwmK-aE&callback=initMap">
    </script>
</body>
</html>
