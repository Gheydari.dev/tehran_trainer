﻿<%@ Page Title="" Language="C#" MasterPageFile="~/farsi/UserMasterPage.master" AutoEventWireup="true" CodeFile="UserPassword.aspx.cs" Inherits="Farsi_UserPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContentPlaceHolder" runat="Server">
    <div class="row BYekan">
        <div class="col-md-12">
            <h2>تغییر رمز عبور</h2>
            <div runat="server" id="InfoDiv" visible="false" class="alert alert-danger alert-dismissable text-right">
                <asp:Literal ID="InfoLiteral" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <hr />
    <div class="row" style="padding-bottom: 150px">
        <div class="col-md-3">
        </div>
        <div class="col-md-6 text-right">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <%--<asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="ClientValidate" ControlToValidate="NewPassTextBox" Display="Dynamic" ErrorMessage="حداقل طول گذر واژه 6 کاراکتر است" ForeColor="Red">*</asp:CustomValidator>--%>
                        <asp:TextBox ID="CurPassTextBox" CssClass="form-control input-sm" placeholder="کلمه عبور قدیم" runat="server" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="CurPassTextBox" ForeColor="Red" Display="Dynamic">وارد کردن کلمه عبور الزامیست</asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="NewPassTextBox" CssClass="form-control input-sm" placeholder="کلمه عبور جدید" runat="server" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="NewPassTextBox" ForeColor="Red" Display="Dynamic">وارد کردن عبور جدید الزامیست</asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="RepeatNewPassTextBox" CssClass="form-control input-sm" placeholder="تکرار کلمه عبور جدید" runat="server" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="RepeatNewPassTextBox" ForeColor="Red" Display="Dynamic">تکرار عبور جدید الزامیست</asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="RepeatNewPassTextBox" ControlToValidate="NewPassTextBox" Display="Dynamic" ForeColor="Red">گذرواژه به درستی تکرار نشده</asp:CompareValidator>
                    </div>
                    <asp:Button CssClass="btn btn-warning btn-sm btn-block" ID="SubmitButton" runat="server" Text="تغییر رمز عبور" OnClick="SubmitButton_Click" />
                </div>
            </div>
        </div>
        <div class="col-md-3">
        </div>
    </div>
</asp:Content>

