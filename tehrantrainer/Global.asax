﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="System.Web.Security" %>
<%@ Import Namespace="System.Web.SessionState" %>
<%@ Import Namespace="System.Web.Routing" %>


<script RunAt="server">
    void Application_Start(object sender, EventArgs e)
    {
        RegisterRoutes(RouteTable.Routes);

        //RegisterRoutes(RouteTable.Routes);
        Application.Lock();
        // Code that runs on application startup
        //Application["RoleChange"] = "false";
        Application["OnlineUsers"] = 0;

        StringCollection NowLookingSC = new StringCollection();
        Application["NowLookingSC"] = NowLookingSC;

        StringCollection RoleName = new StringCollection();
        Application["RoleName"] = RoleName;

        StringCollection RoleAccess = new StringCollection();
        Application["RoleAccess"] = RoleAccess;

        //DataLayer dl = new DataLayer();
        //string CS = dl.dataLayerConnectionString;
        //System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(CS);

        //string FetchUserSqlSyntax = "SELECT Product.FName, Product.EName,Product.Model, Product.pid,Cat4 FROM Product ";

        //System.Data.SqlClient.SqlCommand FetchUserCmd = new System.Data.SqlClient.SqlCommand(FetchUserSqlSyntax, con);

        //System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(FetchUserCmd);

        //System.Data.DataTable dt = new System.Data.DataTable();

        //da.Fill(dt);
        //if (dt.Rows.Count > 0)
        //{
        //    Application["ProductTable"] = dt;
        //}
        //Application["SC"] = FetchUserSqlSyntax;

        //SqlDataSource AllProducts = new SqlDataSource();
        //AllProducts.SelectCommand = FetchUserSqlSyntax;
        //AllProducts.ConnectionString = CS;
        //AllProducts.DataBind();
        //Application["AllProducts"] = AllProducts;

        //try
        //{
        //    con.Open();
        //    System.Data.SqlClient.SqlDataReader FetchUserDr = FetchUserCmd.ExecuteReader();

        //}

        //catch (System.Data.SqlClient.SqlException ex)
        //{
        //    string msg = "FetchUserCmd Error:";
        //    msg += ex.Message;
        //    throw new Exception(msg);
        //}

        //finally
        //{
        //    con.Close();
        //}
        Application.UnLock();
    }

    void Application_End(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown

    }

    void Application_Error(object sender, EventArgs e)
    {
        //string LogFileName = Server.MapPath(".") + "\\ErrorLog.txt";
        //if (!System.IO.File.Exists(LogFileName))
        //{
        //    System.IO.File.Create(LogFileName);
        //}
        //System.IO.StreamWriter swriter = new System.IO.StreamWriter(LogFileName, true);
        //swriter.Write(DateTime.Now + "_" + Server.GetLastError() + "\n\r\n\r");
        //swriter.Close();
        //Response.Redirect("~/pages/error.aspx");
    }

    void Session_Start(object sender, EventArgs e)
    {
        // Code that runs when a new session is started
        Application.Lock();
        Application["OnlineUsers"] = (int)Application["OnlineUsers"] + 1;
        Application.UnLock();

    }

    void Session_End(object sender, EventArgs e)
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.
        Application.Lock();
        Application["OnlineUsers"] = (int)Application["OnlineUsers"] - 1;
        Application.UnLock();

    }
    void RegisterRoutes(RouteCollection routes)
    {
        routes.MapPageRoute("home", "", "~/farsi/default.aspx");


        routes.MapPageRoute("Product", "Product/{pid}/{*fname}", "~/farsi/Product.aspx", true, new RouteValueDictionary { { "pid", "2" }, { "fname", "-" } });
        routes.MapPageRoute("Event", "Event/{eventid}/{*eventtitle}", "~/farsi/event.aspx", true, new RouteValueDictionary { { "eventid", "2" }, { "eventtitle", "-" } });
        routes.MapPageRoute("Brand", "Brand/{bid}/{*brandfname}", "~/farsi/brand.aspx", true, new RouteValueDictionary { { "bid", "6" }, { "brandfname", "-" } });
        routes.MapPageRoute("Articles", "Articles/{cid}/{*header}", "~/farsi/NewsDetails.aspx", true, new RouteValueDictionary { { "cid", "1" }, { "header", "پدر-پیام-کوتاه-در-۶۳-سالگی-درگذشت" } });
        routes.MapPageRoute("News", "News/{cid}/{*header}", "~/farsi/NewsDetails.aspx", true, new RouteValueDictionary { { "cid", "1" }, { "header", "پدر-پیام-کوتاه-در-۶۳-سالگی-درگذشت" } });
        routes.MapPageRoute("Policy", "Policy/{id}/{*header}", "~/farsi/Tehrantrainer.aspx", true, new RouteValueDictionary { { "id", "1" }, { "header", "حمل-رایگان" } });
        routes.MapPageRoute("All-News", "All-News/{cat1}", "~/farsi/News.aspx", true, new RouteValueDictionary { { "cat1", "اخبار اصلی" } });
        routes.MapPageRoute("All-Articles", "All-Articles/{cat1}", "~/farsi/News.aspx", true, new RouteValueDictionary { { "cat1", "مقالات اصلی" } });

        routes.MapPageRoute("Fa-Blog", "Fa/blog", "~/farsi/blog/default.aspx");
        routes.MapPageRoute("Fa-Page", "Fa/blog/page/{cid}/{*header}", "~/farsi/blog/page.aspx", true, new RouteValueDictionary { { "cid", "0" }, { "header", "-" } });
        routes.MapPageRoute("Fa-Category", "Fa/blog/category/{cat2}", "~/farsi/blog/category.aspx", true, new RouteValueDictionary { { "cat2", "اخبار عمومی" } });
        routes.MapPageRoute("Fa-Tag", "tag/{tag}", "~/farsi/blog/tag.aspx", true, new RouteValueDictionary { { "tag", "خوش پوشی" } });

        routes.MapPageRoute("En-Blog", "En/blog", "~/english/blog/default.aspx");
        routes.MapPageRoute("En-Page", "En/blog/page/{cid}/{*header}", "~/english/blog/page.aspx", true, new RouteValueDictionary { { "cid", "0" }, { "header", "-" } });
        routes.MapPageRoute("En-Category", "En/blog/category/{cat2}", "~/english/blog/category.aspx", true, new RouteValueDictionary { { "cat2", "اخبار عمومی" } });
        routes.MapPageRoute("En-Tag", "En/blog/tag/{tag}", "~/english/blog/category.aspx", true, new RouteValueDictionary { { "tag", "خوش پوشی" } });

        //seo stuff
        routes.MapPageRoute("Blog", "blog", "~/farsi/blog/default.aspx");
        routes.MapPageRoute("Page", "blog/page/{cid}/{*header}", "~/farsi/blog/page.aspx", true, new RouteValueDictionary { { "cid", "0" }, { "header", "-" } });
        routes.MapPageRoute("Category", "blog/category/{cat2}", "~/farsi/blog/category.aspx", true, new RouteValueDictionary { { "cat2", "اخبار عمومی" } });
        routes.MapPageRoute("Tag", "blog/tag/{tag}", "~/farsi/blog/category.aspx", true, new RouteValueDictionary { { "tag", "خوش پوشی" } });
        //end


        routes.MapPageRoute("PasswordRecovery", "PasswordRecovery", "~/farsi/PasswordRecovery.aspx");
        routes.MapPageRoute("Register", "Register", "~/farsi/Register.aspx");
        routes.MapPageRoute("ProfileOld", "Profile", "~/farsi/UserProfile.aspx");
        routes.MapPageRoute("PersonalInfo", "Profile/PersonalInfo", "~/farsi/UserProfile.aspx");
        routes.MapPageRoute("Orders", "Profile/Orders", "~/farsi/UserOrders.aspx");
        routes.MapPageRoute("UserPassword", "Profile/Password", "~/farsi/UserPassword.aspx");
        routes.MapPageRoute("Favorites", "Profile/Favorites", "~/farsi/UserLikes.aspx");
        routes.MapPageRoute("Logout", "Logout", "~/farsi/Logout.aspx");
        routes.MapPageRoute("Login", "Login", "~/farsi/Login.aspx");
        routes.MapPageRoute("Cart", "Cart", "~/farsi/Shoppingcart.aspx");
        routes.MapPageRoute("Payment", "Payment", "~/farsi/Payment.aspx");
        routes.MapPageRoute("Shipping", "Shipping", "~/farsi/shipping.aspx");
        routes.MapPageRoute("404", "404", "~/farsi/Tehrantrainer404.aspx");
        routes.MapPageRoute("ContactUs", "ContactUs", "~/farsi/ContactUs.aspx");
        routes.MapPageRoute("AllProducts", "AllProducts/{PageNum}", "~/farsi/AllProducts.aspx", true, new RouteValueDictionary { { "PageNum", "1" } });
        routes.MapPageRoute("Search", "Search/{*filters}", "~/farsi/Products.aspx", true, new RouteValueDictionary { { "filters", "null" } });
        routes.MapPageRoute("Tehrantrainer-Borns", "Tehrantrainer-Borns", "~/farsi/landing2.aspx");


        //routes.MapPageRoute("PriceChart", "PriceChart/{pid}", "~/Default3.aspx", true, new RouteValueDictionary { { "pid", "2" } });
        //routes.MapPageRoute("", "Search/Category-{cat}/{fname}/{ename}", "~/farsi/Search.aspx", true, new RouteValueDictionary { { "fname", "-" }, { "ename", "-" } });
        //routes.MapPageRoute("", "Product/{pid}/{fname}/{ename}", "~/farsi/Product.aspx", true, new RouteValueDictionary { { "fname", "-" }, { "ename", "-" } });
        //routes.MapPageRoute("", "Product/{pid}/{fname}/{ename}", "~/farsi/Product.aspx", true, new RouteValueDictionary { { "fname", "-" }, { "ename", "-" } });
        //routes.MapPageRoute("", "Product/{pid}/{fname}/{ename}", "~/farsi/Product.aspx", true, new RouteValueDictionary { { "fname", "-" }, { "ename", "-" } });
        //routes.MapPageRoute("", "Product/{pid}/{fname}/{ename}", "~/farsi/Product.aspx", true, new RouteValueDictionary { { "fname", "-" }, { "ename", "-" } });
        routes.MapPageRoute("FriendlyUrl", "{cat2}/{header}/{cid}/", "~/farsi/blog/page.aspx", true, new RouteValueDictionary { { "cat2", "-" }, { "header", "-" }, { "cid", "0" } });

    }
    void Application_BeginRequest(object sender, EventArgs e)
    {
        // Code that runs on every request
        var currentURL = HttpContext.Current.Request.Url.ToString().ToLower();
        if (currentURL.Contains("/blog/page/"))
        {

            string CID = currentURL.Split('/')[5];
            if (currentURL.Contains("fa/blog/page/"))
            {
                CID = currentURL.Split('/')[6];
            }
            string Cat2Title = "null";
            string Header = "null";


            DataLayer dl = new DataLayer();
            string CS = dl.dataLayerConnectionString;
            System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(CS);

            string FetchUserSqlSyntax = " select content.cid,Header, dbo.Tree.Name AS cat2title FROM dbo.[Content] INNER JOIN dbo.Tree ON dbo.[Content].cat2 = dbo.Tree.NodeID WHERE cid=@cid   ";

            System.Data.SqlClient.SqlCommand FetchUserCmd = new System.Data.SqlClient.SqlCommand(FetchUserSqlSyntax, con);

            System.Data.SqlClient.SqlParameter[] FetchUserParam = new System.Data.SqlClient.SqlParameter[1];

            FetchUserParam[0] = new System.Data.SqlClient.SqlParameter("@cid", System.Data.SqlDbType.NVarChar);

            FetchUserParam[0].Value = CID;

            for (int i = 0; i < FetchUserParam.Length; i++)
            {
                FetchUserCmd.Parameters.Add(FetchUserParam[i]);
            }

            try
            {
                con.Open();
                System.Data.SqlClient.SqlDataReader dr;
                dr = FetchUserCmd.ExecuteReader();


                if (dr.Read())
                {
                    Cat2Title = dr["Cat2Title"].ToString();
                    Header = dr["Header"].ToString();

                    var newurl = SSClass.WebDomain + "/" + MyFunc.SafeString(Cat2Title) + "/" + MyFunc.SafeString(Header) + "/" + CID;
                    HttpContext.Current.Response.Status = "301 Moved Permanently";
                    HttpContext.Current.Response.AddHeader("Location", newurl);
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                string msg = "FetchUserCmd Error:";
                msg += ex.Message;
                throw new Exception(msg);
            }

            finally
            {
                con.Close();
            }
        }
        else if (currentURL.Contains("/blog/tag/") || currentURL.Contains("fa/blog/tag/"))
        {
            var tag = currentURL.Split('/').Last();
            
            var newurl = SSClass.WebDomain + "/tag/" + tag;
            HttpContext.Current.Response.Status = "301 Moved Permanently";
            HttpContext.Current.Response.AddHeader("Location", newurl);
        }

    }
</script>
