﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Farsi_Admin_FaAdminMasterPage : System.Web.UI.MasterPage
{
    protected void Page_Init(object sender, EventArgs e)
    {


    }
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["superadmin"] = "true";

        Session["AdminID"] = 1;
        Session["AdminName"] = "مهدی";
        Session["AdminIP"] = "0.0.0.0";



        if (true)
        {
            DataLayer dl = new DataLayer();
            string CS = dl.dataLayerConnectionString;
            SqlConnection con = new SqlConnection(CS);

            if (!Page.IsPostBack)
            {
                string SQLQuerry = @" if not exists (select * from RM_Pages where PageName=@PageName and ActName=@ActName) 
                                    insert into RM_Pages (PageTitle,PageName,ActName,Situ) values (@PageTitle,@PageName,@ActName,@Situ) ";

                SqlCommand RegCmd = new SqlCommand(SQLQuerry, con);
                SqlParameter[] param = new SqlParameter[4];

                param[0] = new SqlParameter(@"PageTitle", SqlDbType.NVarChar);
                param[1] = new SqlParameter(@"PageName", SqlDbType.NVarChar);
                param[2] = new SqlParameter(@"ActName", SqlDbType.NVarChar);
                param[3] = new SqlParameter(@"Situ", SqlDbType.NVarChar);


                param[0].Value = "بدون عنوان";
                param[1].Value = Page.ToString();
                if (Request.QueryString["act"] == null)
                {
                    param[2].Value = "null";
                }
                else
                {
                    param[2].Value = Request.QueryString["act"];
                }
                param[3].Value = "ok";



                for (int r = 0; r < param.Length; r++)
                {
                    RegCmd.Parameters.Add(param[r]);
                }

                try
                {
                    con.Open();
                    RegCmd.ExecuteNonQuery();

                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    string msg = "RegCmd Error:";
                    msg += ex.Message;
                    throw new Exception(msg);
                }
                finally
                {
                    con.Close();
                }
            }
            else
            {
                Control PostBackControl = null;
                // first we will check the "__EVENTTARGET" because if post back made by the controls
                // which used "_doPostBack" function also available in Request.Form collection.
                string controlName = Page.Request.Params["__EVENTTARGET"];
                if (!String.IsNullOrEmpty(controlName))
                {
                    PostBackControl = Page.FindControl(controlName);
                }
                else
                {
                    // if __EVENTTARGET is null, the control is a button type and we need to
                    // iterate over the form collection to find it

                    // ReSharper disable TooWideLocalVariableScope
                    string controlId;
                    Control foundControl;
                    // ReSharper restore TooWideLocalVariableScope

                    foreach (string ctl in Page.Request.Form)
                    {
                        // handle ImageButton they having an additional "quasi-property" 
                        // in their Id which identifies mouse x and y coordinates
                        if (ctl.EndsWith(".x") || ctl.EndsWith(".y"))
                        {
                            controlId = ctl.Substring(0, ctl.Length - 2);
                            foundControl = Page.FindControl(controlId);
                        }
                        else
                        {
                            foundControl = Page.FindControl(ctl);
                        }

                        if (!(foundControl is IButtonControl)) continue;

                        PostBackControl = foundControl;
                        break;
                    }
                }
                if (PostBackControl == null)
                {
                    string smUniqueId = ScriptManager.GetCurrent(Page).UniqueID;
                    string smFieldValue = Request.Form[smUniqueId];
                    string UpdatePanelControlID = "";
                    if (!String.IsNullOrEmpty(smFieldValue) && smFieldValue.Contains("|"))
                    {
                        UpdatePanelControlID = smFieldValue.Split('|')[1];
                        PostBackControl = Page.FindControl(UpdatePanelControlID);

                    }
                }
                if (PostBackControl == null)
                {
                    return;
                }
                string SQLQuerry = @"if not exists (select * from RM_Controls where clientid=@clientid and PageName=@PageName) 
                                    insert into RM_Controls (ClientID,PageName,ControlName,ControlType) values (@ClientID,@PageName,@ControlName,@ControlType) ";

                SqlCommand RegCmd = new SqlCommand(SQLQuerry, con);
                SqlParameter[] param = new SqlParameter[5];

                param[0] = new SqlParameter(@"ClientID", SqlDbType.NVarChar);
                param[1] = new SqlParameter(@"PageName", SqlDbType.NVarChar);
                param[2] = new SqlParameter(@"ControlName", SqlDbType.NVarChar);
                param[3] = new SqlParameter(@"ControlType", SqlDbType.NVarChar);
                param[4] = new SqlParameter(@"Situ", SqlDbType.NVarChar);

                if (PostBackControl.ClientID.Contains("GridView") || PostBackControl.ClientID.Contains("Repeater"))
                {
                    string ClienIDString = PostBackControl.ClientID;
                    ClienIDString = ClienIDString.Replace("_" + ClienIDString.Split('_').Last(), "");
                    param[0].Value = ClienIDString;
                }
                else
                {
                    param[0].Value = PostBackControl.ClientID;
                }
                param[1].Value = PostBackControl.Page.ToString();

                if (PostBackControl is Button)
                {
                    Button BB = (Button)PostBackControl;
                    param[2].Value = BB.Text;
                }
                else if (PostBackControl is LinkButton)
                {
                    LinkButton BB = (LinkButton)PostBackControl;
                    param[2].Value = BB.Text;
                }
                else
                {
                    param[2].Value = "Need to Check";

                }
                param[3].Value = PostBackControl.GetType().ToString();
                param[4].Value = "ok";


                for (int r = 0; r < param.Length; r++)
                {
                    RegCmd.Parameters.Add(param[r]);
                }

                try
                {
                    con.Open();
                    RegCmd.ExecuteNonQuery();

                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    string msg = "RegCmd Error:";
                    msg += ex.Message;
                    throw new Exception(msg);
                }
                finally
                {
                    con.Close();
                }
            }
            //Label1.DataBind();
            //Session.Timeout = 60;
            //if (Session["admin"] != "true")
            //{
            //    Response.Redirect("../../farsi/Login.aspx");
            //}

        }


        if (Session["superadmin"] == null)
        {
            if (Session["AdminID"] == null)
            {
                Response.Redirect(SSClass.WebDomain + "/login");
            }
            HiAdminSpan.InnerText = "سلام " + Session["adminname"].ToString();
            DataLayer dl = new DataLayer();
            string CS = dl.dataLayerConnectionString;
            SqlConnection con = new SqlConnection(CS);

            if (!Page.IsPostBack)
            {
                string SQLQuerry = @" if exists (select * from rm_RolePages where pageid=(select pageid from rm_pages where PageName=@PageName and ActName=@ActName) and roleid in (select roleid from rm_AdminRoles where Adminid=@AdminID ))
                                    begin insert into rm_logs (adminid,pageid) values (@adminid,(select pageid from rm_pages where PageName=@PageName and ActName=@ActName)) select N'allowed' as result end else select N'notallowed' as result ";


                SqlCommand RegCmd = new SqlCommand(SQLQuerry, con);
                SqlParameter[] param = new SqlParameter[4];

                param[0] = new SqlParameter(@"AdminID", SqlDbType.NVarChar);
                param[1] = new SqlParameter(@"PageName", SqlDbType.NVarChar);
                param[2] = new SqlParameter(@"ActName", SqlDbType.NVarChar);
                param[3] = new SqlParameter(@"Situ", SqlDbType.NVarChar);


                param[0].Value = Session["AdminID"];
                param[1].Value = Page.ToString();
                if (Request.QueryString["act"] == null)
                {
                    param[2].Value = "null";
                }
                else
                {
                    param[2].Value = Request.QueryString["act"];
                }
                param[3].Value = "ok";



                for (int r = 0; r < param.Length; r++)
                {
                    RegCmd.Parameters.Add(param[r]);
                }

                try
                {
                    SqlDataReader dr;
                    con.Open();

                    dr = RegCmd.ExecuteReader();
                    if (dr.Read())
                    {
                        if (dr["result"].ToString() == "notallowed" && Page.ToString() != "ASP.Farsi_admin_default_aspx")
                        {
                            Response.Redirect("Default.aspx?ref=not-allowed&page=" + Page.ToString().Replace("ASP.", "") + "&clientid=null");
                        }
                    }

                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    string msg = "RegCmd Error:";
                    msg += ex.Message;
                    throw new Exception(msg);
                }
                finally
                {
                    con.Close();
                }
            }
            else
            {
                Control PostBackControl = null;
                // first we will check the "__EVENTTARGET" because if post back made by the controls
                // which used "_doPostBack" function also available in Request.Form collection.
                string controlName = Page.Request.Params["__EVENTTARGET"];
                if (!String.IsNullOrEmpty(controlName))
                {
                    PostBackControl = Page.FindControl(controlName);
                }
                else
                {
                    // if __EVENTTARGET is null, the control is a button type and we need to
                    // iterate over the form collection to find it

                    // ReSharper disable TooWideLocalVariableScope
                    string controlId;
                    Control foundControl;
                    // ReSharper restore TooWideLocalVariableScope

                    foreach (string ctl in Page.Request.Form)
                    {
                        // handle ImageButton they having an additional "quasi-property" 
                        // in their Id which identifies mouse x and y coordinates
                        if (ctl.EndsWith(".x") || ctl.EndsWith(".y"))
                        {
                            controlId = ctl.Substring(0, ctl.Length - 2);
                            foundControl = Page.FindControl(controlId);
                        }
                        else
                        {
                            foundControl = Page.FindControl(ctl);
                        }

                        if (!(foundControl is IButtonControl)) continue;

                        PostBackControl = foundControl;
                        break;
                    }
                }
                if (PostBackControl == null)
                {
                    string smUniqueId = ScriptManager.GetCurrent(Page).UniqueID;
                    string smFieldValue = Request.Form[smUniqueId];
                    string UpdatePanelControlID = "";
                    if (!String.IsNullOrEmpty(smFieldValue) && smFieldValue.Contains("|"))
                    {
                        UpdatePanelControlID = smFieldValue.Split('|')[1];
                        PostBackControl = Page.FindControl(UpdatePanelControlID);
                    }
                }

                string SQLQuerry = @"if exists (select * from RM_RoleControls where ControlID=(select ControlID from RM_Controls where clientid=@clientid and PageName=@PageName) and RoleID in (select roleid from rm_AdminRoles where Adminid=@AdminID ) ) 
                                          begin insert into rm_logs (adminid,pageid,controlid)
                                                              values (@adminid,(select pageid from rm_pages where PageName=@PageName and ActName=@ActName),(select ControlID from RM_Controls where clientid=@clientid and PageName=@PageName)) select N'allowed' as result end else select N'notallowed' as result ";
                SqlCommand RegCmd = new SqlCommand(SQLQuerry, con);
                SqlParameter[] param = new SqlParameter[5];

                param[0] = new SqlParameter(@"ClientID", SqlDbType.NVarChar);
                param[1] = new SqlParameter(@"PageName", SqlDbType.NVarChar);
                param[2] = new SqlParameter(@"AdminID", SqlDbType.NVarChar);
                param[3] = new SqlParameter(@"ActName", SqlDbType.NVarChar);
                param[4] = new SqlParameter(@"Situ", SqlDbType.NVarChar);

                if (PostBackControl.ClientID.Contains("GridView") || PostBackControl.ClientID.Contains("Repeater"))
                {
                    string ClienIDString = PostBackControl.ClientID;
                    ClienIDString = ClienIDString.Replace("_" + ClienIDString.Split('_').Last(), "");
                    param[0].Value = ClienIDString;
                }
                else
                {
                    param[0].Value = PostBackControl.ClientID;
                }
                param[1].Value = PostBackControl.Page.ToString();
                param[2].Value = Session["AdminID"];
                if (Request.QueryString["act"] == null)
                {
                    param[3].Value = "null";
                }
                else
                {
                    param[3].Value = Request.QueryString["act"];
                }
                param[4].Value = "ok";


                for (int r = 0; r < param.Length; r++)
                {
                    RegCmd.Parameters.Add(param[r]);
                }

                try
                {
                    SqlDataReader dr;
                    con.Open();

                    dr = RegCmd.ExecuteReader();
                    if (dr.Read())
                    {
                        if (dr["result"].ToString() == "notallowed" && Page.ToString() != "ASP.Farsi_admin_default_aspx")
                        {
                            Response.Redirect("Default.aspx?ref=not-allowed&page=" + Page.ToString().Replace("ASP.", "") + "&clientid" + PostBackControl.ClientID);
                        }
                    }

                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    string msg = "RegCmd Error:";
                    msg += ex.Message;
                    throw new Exception(msg);
                }
                finally
                {
                    con.Close();
                }
            }
        }


    }

}
