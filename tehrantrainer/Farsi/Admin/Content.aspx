﻿<%@ Page Title="" Language="C#" MasterPageFile="~/farsi/Admin/FaAdminMasterPage.master" AutoEventWireup="true" CodeFile="Content.aspx.cs" Inherits="Farsi_Admin_Content" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=SSClass.WebDomainFile %>/css/jquery-ui.css" rel="stylesheet" />
    <link href="<%=SSClass.WebDomainFile %>/css/dropzone.css" rel="stylesheet" />
    <style type="text/css">
        .dz-max-files-reached {
            background-color: red;
        }

        #dropZone {
            background: gray;
            border: black dashed 3px;
            width: 200px;
            padding: 50px;
            text-align: center;
            color: white;
        }

        div.relative {
            position: relative;
            display: inline-block;
            padding: 2px;
            margin-bottom: -7px;
        }

        div.absolute {
            position: absolute;
            top: 5px;
            left: 5px;
        }

        .photocards {
            float: left;
            height: 5em;
            border: 1px solid lightgray;
        }

        h2 {
            font-size: 25px;
        }

        .dropzone {
            min-height: 150px
        }

        #photos {
            color: #ddd;
            padding: 15px;
            max-height: 300px;
            overflow-y: scroll;
            margin: 0px;
        }

        figcaption {
            background: rgba(250,250,250, .8);
            bottom: 0;
            box-sizing: border-box;
            /*position: absolute;*/
            width: 100%;
            padding: 1rem;
            -webkit-transition: max-height 0.3s ease-out;
        }

        figure {
            margin: 1rem;
            position: relative;
        }

        .selected-cat {
            color: #00b9ff
        }

        .list-group-item > input[type=checkbox] {
            margin-left: 10px;
        }

        .list-group-item {
            cursor: pointer;
        }

            .list-group-item:hover {
                background-color: #f5f5f5;
            }

            .list-group-item.list-group-item-success:hover {
                background-color: #a9ca9c;
            }

            .list-group-item.CatBack {
                background-color: #7d7d7d;
                color: #f5f5f5;
                text-align: left;
            }

            .list-group-item.Selected {
                background-color: #e0e0e0;
            }


        #BodyContentPlaceHolder_ContentPlaceHolder1_CatTreeView {
            background-color: white;
            padding: 15px;
            border: 1px solid #ccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            position: absolute;
            width: 100%;
            z-index: 5;
        }

        input[type='checkbox'] {
            margin-left: 5px
        }

        .inserted-li {
            display: inline-block;
            float: right;
            border: solid 1px #c7c7c7;
            padding: 2px 5px;
            margin-right: 6px;
            background-color: white;
            cursor: pointer;
        }

            .inserted-li:hover {
                border: solid 1px #fca6a6;
                background-color: #ffcaca;
            }

        .search-result {
            padding-right: 0px;
            max-height: 600px;
            overflow-y: auto;
            padding-right: 0px;
            position: absolute;
            z-index: 3;
            width: max-content;
            width: fit-content;
        }

        .delete-cid {
            margin-right: 5px;
            color: red !important;
        }
    </style>

    <div class="row">
        <div class="col-sm-12">
            <div runat="server" id="InfoDiv" class="alert alert-success alert-dismissable text-right" visible="false">
                <asp:Literal ID="InfoLiteral" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class=" well well-sm">
                <div class="form-group">
                    <label>*بخش مربوطه:</label>
                    <asp:DropDownList AutoPostBack="true" CssClass="form-control input-sm" ID="Cat1DropDownList" runat="server" OnSelectedIndexChanged="Cat1DropDownList_SelectedIndexChanged">
                         <asp:ListItem Value="0">لطفا انتخاب کنید...</asp:ListItem>
                        <asp:ListItem Value="Fa-Pages">صفحات اصلی وبسایت</asp:ListItem>
                        <asp:ListItem Value="Fa-Event">رویدادها</asp:ListItem>
                        <asp:ListItem Value="Fa-Product">محصولات</asp:ListItem>
                        <asp:ListItem Value="Fa-Blog">بلاگ</asp:ListItem>
                        <asp:ListItem Value="Fa-News">اخبار</asp:ListItem>
                        <asp:ListItem Value="Fa-Other">سایر</asp:ListItem>

                        <asp:ListItem Value="En-Pages">Website main pages</asp:ListItem>
                        <asp:ListItem Value="En-Event">Events</asp:ListItem>
                        <asp:ListItem Value="En-Product">Products</asp:ListItem>
                        <asp:ListItem Value="En-Blog">Blog</asp:ListItem>
                        <asp:ListItem Value="En-News">News</asp:ListItem>
                        <asp:ListItem Value="En-Other">Other</asp:ListItem>


                    </asp:DropDownList>
                    <hr style="padding: 2px; margin: 3px" />
                </div>
                <div runat="server" id="ContentFormDiv" visible="false">
                    <div class="row">
                        <div class="col-sm-6" style="border-left: 1px #cfcfcf solid">
                            <div class="form-group">
                                <label>تیترهای مطلب:</label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="لطفا عنوان مطلب را وارد کنید!" ControlToValidate="HeaderTextBox" Display="Dynamic" ForeColor="Red" ValidationGroup="content">*</asp:RequiredFieldValidator>
                                <asp:TextBox placeholder="روتیتر" CssClass="form-control input-sm" ID="HeaderUpTextBox" runat="server" Style="margin-bottom: 3px"></asp:TextBox>
                                <asp:TextBox placeholder="*تیتر اصلی" CssClass="form-control input-lg" ID="HeaderTextBox" runat="server" Style="margin-bottom: 3px"></asp:TextBox>
                                <asp:TextBox placeholder="زیر تیتر" CssClass="form-control input-sm" ID="HeaderDownTextBox" runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <hr style="padding: 2px; margin: 3px" />
                                <label>تصویر مطلب</label>
                                <asp:FileUpload ID="NewsFileUpload" runat="server" Width="100%" />
                                <asp:TextBox ID="PhotoTextBox" runat="server" Style="display: none"></asp:TextBox>
                                <p class="help-block small">
                                    <span class="glyphicon glyphicon-info-sign"></span>
                                    از تصاویر JPG و به صورت مربع (300x300) و زیر 70KB استفاده کنید.
                                </p>
                                <label style="cursor: pointer">
                                    <asp:CheckBox ID="PhotoVizCheckBox" runat="server" Checked="true" />
                                    <span>این تصویر را در ابتدای مطلب درج کن.</span>
                                </label>
                                <hr style="padding: 2px; margin: 8px" />
                            </div>
                            <div class="form-group">
                                <label>خلاصه مطلب (Meta Description Tag):</label>
                                <span class="glyphicon glyphicon-question-sign cp" data-target="#MetaDescriptionTagModal" data-toggle="modal"></span>
                                <asp:TextBox CssClass="form-control input-sm" runat="server" ID="SummaryTextBox" TextMode="MultiLine" Style="max-width: 100%" />
                                <label style="cursor: pointer;">
                                    <asp:CheckBox ID="SummaryVizCheckBox" runat="server" Checked="true" />
                                    <span>خلاصه مطلب را در ابتدای مطلب درج کن.</span>
                                </label>
                            </div>
                            <div class="form-group">
                                <label>منبع مطلب:</label>
                                <asp:TextBox CssClass="form-control input-sm" runat="server" ID="ResourceTextBox" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <label>*دسته بندی:</label>
                                        <a onmouseover="$(this).attr('href','Tree.aspx?val='+$('#<%=Cat1DropDownList.ClientID %>').val())" href="Tree.aspx?val=fa-category" class="pull-left btn btn-xs btn-info " target="_blank">+ دسته بندی جدید</a>
                                        <p style="margin: 0px" class="help-block small">دسته های مورد نظر را تیک زده و در نهایت روی دسته اصلی کلیک کنید.</p>
                                        <div style="position: relative">
                                            <asp:TreeView ShowCheckBoxes="All" CssClass="" RootNodeStyle-CssClass="selected-cat" ID="CatTreeView" runat="server" OnTreeNodePopulate="CatTreeView_TreeNodePopulate" OnSelectedNodeChanged="CatTreeView_SelectedNodeChanged">
                                                <Nodes>
                                                    <asp:TreeNode PopulateOnDemand="True" Text="لطفا انتخاب کنید" Value="null"></asp:TreeNode>
                                                </Nodes>
                                            </asp:TreeView>
                                        </div>
                                        <br />
                                        <br />
                                        <br />
                                        <asp:Button CssClass="btn btn-block" Text="بروز رسانی" runat="server" ID="GetCheckedNodesButton" OnClick="GetCheckedNodesButton_Click" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <hr style="padding: 2px; margin: 3px" />
                                <label>کلیدواژه ها:</label>
                                <asp:TextBox CssClass="form-control input-sm" runat="server" ID="KeywordTextBox" placeholder="کلیدواژه ها را با کاراکتر  -  از هم جدا کنید. مثال: دستور پخت قرمه سبزی-چگونه قرمه سبزی بپزیم-آموزش آشپزی-نکات مهم پخت خورش قرمه سبزی" />
                                <p class="help-block small"><span style="margin-left: 5px" class="glyphicon glyphicon-info-sign"></span>این مطلب با چه کلمات و عباراتی در موتورهای جستجو نمایش داده شود؟</p>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <hr style="padding: 2px; margin: 3px" />
                        </div>
                        <div class="col-sm-2">
                            <label>برچسب ها:</label>
                            <div class="input-group">
                                <asp:TextBox AutoCompleteType="None" autocomplete="off" placeholder="برچسب ها" CssClass="form-control" runat="server" ID="TagTextBox" onkeyup="SearchAjax(this)" onclick="SearchAjax(this)" data-act="tag"></asp:TextBox>
                                <div data-toggle="modal" data-target="#TagHelpModal" class="input-group-addon cp"><span class="glyphicon glyphicon-question-sign"></span></div>
                            </div>
                            <ul class="list-group search-result" style="padding-right: 0px" id="TagUl">
                            </ul>
                        </div>
                        <div class="col-sm-10">
                            <asp:TextBox runat="server" ID="InsertedTagTextBox" Style="display: none" />
                            <ul class="list-group" style="margin-top: 25px;" id="InsertedTagUl">
                            </ul>
                        </div>
                        <div class="col-sm-12">
                            <hr style="padding: 2px; margin: 3px" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-2">
                                <label>درج اسلایدر:</label>
                                <div class="input-group">
                                    <asp:TextBox AutoCompleteType="None" autocomplete="off" placeholder="عنوان اسلایدر" CssClass="form-control" runat="server" ID="SliderTextBox" onkeyup="SearchAjax(this)" onclick="SearchAjax(this)" data-act="slider"></asp:TextBox>
                                    <div data-toggle="modal" data-target="#SliderHelpModal" class="input-group-addon cp"><span class="glyphicon glyphicon-question-sign"></span></div>
                                </div>
                                <ul class="list-group search-result" style="padding-right: 0px" id="SliderUl">
                                </ul>
                            </div>
                            <div class="col-sm-10 ">
                                <label>درج کادرهای کشویی:</label>
                                <br />
                                <span onclick="AddCollapsible('default')" class="btn btn-xs btn-default ">+ افزودن Collapsible</span>
                                <span onclick="AddCollapsible('primary')" class="btn btn-xs btn-primary ">+ افزودن Collapsible</span>
                                <span onclick="AddCollapsible('success')" class="btn btn-xs btn-success ">+ افزودن Collapsible</span>
                                <span onclick="AddCollapsible('info')" class="btn btn-xs btn-info ">+ افزودن Collapsible</span>
                                <span onclick="AddCollapsible('warning')" class="btn btn-xs btn-warning ">+ افزودن Collapsible</span>
                                <span onclick="AddCollapsible('danger')" class="btn btn-xs btn-danger ">+ افزودن Collapsible</span>
                            </div>

                        </div>
                    </div>
                    <div class="form-group">

                        <asp:TextBox runat="server" ID="CKTempTextBox" TextMode="MultiLine" ClientIDMode="Static" Style="display: none" />
                        <textarea name="RevTextTextBox" id="RevTextTextBox" rows="10" cols="80"></textarea>
                        <%--<CKEditor:CKEditorControl ID="BodyTextBox" runat="server" DefaultLanguage="fa" Language="fa" ResizeMinHeight="230" Height="187"></CKEditor:CKEditorControl>--%>
                        <br />
                        <asp:Button OnClientClick="$('#CKTempTextBox').val(CKEDITOR.instances.RevTextTextBox.getData() )" CssClass="btn btn-primary btn-block" ID="SubmitNewsButton" runat="server" Text="ثبت مطلب" ValidationGroup="content" OnClick="SubmitNewsButton_Click" />

                        <asp:Label ID="CIDLabel" runat="server" Text="" Visible="false"></asp:Label>
                        <asp:Label ID="PhotoLabel" runat="server" Text="" Visible="false"></asp:Label>
                        <asp:ValidationSummary CssClass="alert alert-danger" ID="LoginValidationSummary" runat="server" DisplayMode="List" Font-Bold="False" ForeColor="Red" ValidationGroup="content" />
                    </div>
                    <div class="row">
                        <div class="col-sm-12" style="margin-bottom: 5px">
                            <div class="dropzone text-center" id="dropzoneForm">
                                <span style="display: inline-block">تصاویر خود را اینجا رها کنید!</span>
                                <div class="fallback">
                                    <input name="file" type="file" multiple />
                                    <input type="submit" value="Upload" />
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="row text-center PleaseWait" style="display: none; top: 100px; position: fixed; display: block; z-index: 100; width: 100%; left: 0; right: 0;">
                                <div class="col-lg-4 col-sm-4 col-sm-2 col-xs-1">
                                </div>
                                <div class="col-lg-4 col-sm-4 col-sm-8 col-xs-10">
                                    <div style="border: 1px solid #cfcfcf; padding: 15px; background-color: #ffffff; color: black">
                                        <img style="max-width: 200px!important" src="<%=SSClass.WebDomainFile %>/Images/Design/main-logo.png" title="<%=SSClass.FTitle+" | "+SSClass.ETitle %>" alt="<%=SSClass.FTitle+" | "+SSClass.ETitle %>" />
                                        <br />
                                        <label class="BYekan" style="padding: 10px">
                                            لطفا منتظر باشید...
                                        </label>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-4 col-sm-2 col-xs-1">
                                </div>
                            </div>
                            <div id="photos" class="row">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr style="padding: 2px; margin: 3px" />
    <div class="row">
        <div class="col-sm-12">
            <label>جستجوی مطالب:</label>
            <div class="input-group">
                <asp:TextBox AutoCompleteType="None" autocomplete="off" placeholder="عنوان مطلب" CssClass="form-control" runat="server" ID="SearchContentTextBox" onkeyup="SearchAjax(this)" onclick="SearchAjax(this)" data-act="content"></asp:TextBox>
                <div class="input-group-addon "><span class="glyphicon glyphicon-search"></span></div>
            </div>
        </div>
        <div class="col-sm-12 text-center" style="height: 600px; overflow-y: auto">

            <asp:Button ID="UpdateContentGridViewButton" OnClick="UpdateContentGridViewButton_Click" CssClass="btn btn-warning btn-xs" runat="server" ToolTip="ویرایش" Style="display: none"></asp:Button>
            <asp:GridView Font-Size="Small" CssClass="text-center table table-bordered table-condensed" ID="ContentGridView" runat="server" AutoGenerateColumns="False" DataSourceID="ContentSqlDataSource" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal" Width="100%" AllowPaging="True" PageSize="70">
                <Columns>
                    <asp:BoundField DataField="CID" HeaderText="کد" ReadOnly="True" InsertVisible="False" SortExpression="CID" />
                    <asp:TemplateField HeaderText="عنوان" SortExpression="Header">
                        <ItemTemplate>
                            <%--<a title="Friendly URL" target="_blank" href="<%=SSClass.WebDomain %>/<%#MyFunc.SafeString(Eval("cat2title"))%>/<%#MyFunc.SafeString(Eval("header"))%>/<%#Eval("cid") %>/">
                                <span class="glyphicon glyphicon-link"></span>
                            </a>--%>
                            <a target="_blank" href="<%=SSClass.WebDomain %>/<%#MyFunc.SafeString(Eval("cat2title"))%>/<%#MyFunc.SafeString(Eval("header"))%>/<%#Eval("cid") %>">
                                <%#Eval("Header") %>
                            </a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="PageView" HeaderText="بازدید" SortExpression="PageView" />
                    <asp:TemplateField HeaderText="تاریخ" SortExpression="CDate">
                        <ItemTemplate>
                            <span><%#MyFunc.DisplayDateTime(Eval("CDate")) %></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="عملیات">
                        <ItemTemplate>
                            <span data-cid="<%#Eval("CID")%>" class="btn btn-xs btn-default edit-cid glyphicon glyphicon-edit"></span>
                            <span data-cid="<%#Eval("CID")%>" class="btn btn-xs btn-default delete-cid glyphicon glyphicon-trash"></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    <asp:Label Font-Size="Small" ID="Label2" runat="server" Text="در حال حاضر اطلاعاتی جهت نمایش وجود ندارد."></asp:Label>
                </EmptyDataTemplate>
                <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                <HeaderStyle BackColor="#666666" Font-Bold="false" ForeColor="White" VerticalAlign="Middle" />
                <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#242121" />
            </asp:GridView>
            <asp:SqlDataSource ID="ContentSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DrB_DBConnectionString %>" SelectCommand="SELECT dbo.[Content].CID, dbo.[Content].HeaderUp, dbo.[Content].Header, dbo.[Content].HeaderDown, dbo.[Content].Resource, dbo.[Content].Summary, dbo.[Content].SummaryViz, dbo.[Content].PageView, dbo.[Content].PageViewViz, dbo.[Content].Photo, dbo.[Content].Photoviz, dbo.[Content].Cat1, dbo.[Content].Cat2, dbo.[Content].Situ, dbo.[Content].Lan, dbo.[Content].UpdateDate, dbo.[Content].CDateViz, dbo.[Content].CDate, dbo.[Content].IsDeleted, dbo.[Content].AdminID, dbo.Tree.Name AS cat2title FROM dbo.[Content] INNER JOIN dbo.Tree ON dbo.[Content].cat2 = dbo.Tree.NodeID WHERE (dbo.[Content].Cat1 = @cat1) AND (dbo.[Content].IsDeleted = 0) ORDER BY dbo.[Content].CDate DESC">
                <SelectParameters>
                    <asp:ControlParameter ControlID="Cat1DropDownList" PropertyName="SelectedValue" Name="cat1"></asp:ControlParameter>
                </SelectParameters>
            </asp:SqlDataSource>


        </div>
        <div class="col-sm-12 text-right hide">
            <span>لینک صفحه این بخش</span>
            <div dir="ltr" class="text-left">
                <asp:TextBox placeholder="جهت نمایش لینک ابتدا بخش مربوطه را انتخاب کنید" CssClass="form-control input-sm" ID="Cat1TextBox" runat="server"></asp:TextBox>
            </div>
        </div>
    </div>
    <hr />
    <div class="modal fade BYekan" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true" id="SliderHelpModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close pull-left" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <span class="modal-title">راهنمای درج اسلایدر</span>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="help-block">
                                    از این قسمت می توانید اسلایدرهای ایجاد شده در بخش <a href="SlidersMng.aspx">مدیریت اسلایدر</a> را در مطلب خود نمایش دهید. کافیست قسمتی از عنوان اسلایدر را تایپ کنید و روی اسلایدر مورد نظر خود کلیک کنید. عبارتی مانند ###242### به متن اضافه می شود. این عبارت در وبسایت تبدیل به اسلایدر انتخابی شما می شود.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade BYekan" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true" id="TagHelpModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close pull-left" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <span class="modal-title">راهنمای برچسب ها</span>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="help-block">
                                    <h2>برچسب چیست؟</h2>

                                    <p>دسته بندی و برچسب گذاری هردو کاربردهای خود را در ارائه مطالب سایت یا وبلاگ دارا هستند. برای حفظ سادگی و دسترسی بهتر کاربران به مطالب سایت پیشنهاد میشود که تعداد دسته ها را بیشتر از پنج عدد قرار ندهید. کاربران برای دسترسی به موضوع مشخصی در سایت شما از دسته ها استفاده میکنند و زیاد بودن تعداد آن میتواند موجب سردردگمی آنها شود، همچنین محدود بودن آن به نویسنده این امکان را میدهد تا موضوع مقاله بعدی خود را براحتی انتخاب کرده و محل نمایش صحیح آن را تعیین کند. برچسب گذاری نیز با هدفی مشابه به کار میرود و ترکیب این دو میتواند راهنمای مناسبی برای کاربران در یافتن مطالب مورد نظرشان در سایت شما باشد.</p>

                                    <p>برچسب ها دسته بندی های خاص و کوچک تر را برای مطالب سایت شما انجام میدهند و به محتوای هر نوشته بستگی خواهند داشت در حالیکه دسته بندی براساس موضوع نوشته انجام میگیرد. برای ایجاد ارتباط بین مطالب در دسته های مختلف باید از برچسب گذاری استفاده کنید.</p>

                                    <p>برای مثال فرض کنید که شما مطلبی در مورد فیلم &ldquo;گذشته&rdquo; به کارگردانی &ldquo;اصغر فرهادی&rdquo; نوشته اید، بطور حتم دسته بندی این مطلب چیزی مانند &ldquo;نقد فیلم&rdquo; یا &ldquo;معرفی فیلم&rdquo; خواهد بود و انتخاب عبارت &ldquo;اصغر فرهادی&rdquo; بعنوان دسته نمیتواند انتخاب مناسبی باشد. با این حال در دسته &ldquo;اخبار سینما&rdquo; شما مطلبی در مورد تقدیم جایزه اسکار به اصغر فرهادی منتشر کرده اید. حال فرض کنید که کاربر سایت شما در حال خواندن مطلب مربوط به فیلم &ldquo;گذشته&rdquo; بوده و تمایل دارد که اطلاعات بیشتری در مورد کارگردان آن داشته باشد.</p>

                                    <p>اگر شما در هردو مطلب ذکر شده برچسب &ldquo;اصغر فرهادی&rdquo; را تعریف کرده باشید کاربر میتواند براحتی و با کلیک بر روی آن تمام مطالبی که به نوعی با &ldquo;اصغر فرهادی&rdquo; در ارتباط است را مشاهده کند. همین مثال را میتوانید در مورد هر زمینه کاری یا موضوع دیگری بکار ببرید. در واقع برچسب ها کلمات مهم در متن شما هستند (با کلمات کلیدی اشتباه نگیرید).</p>

                                    <p>با امید به اینکه مثال بالا شما را در انتخاب برچسب برای مطالب سایت راهنمایی کرده باشد، 4 نکته اساسی که در برچسب گذاری اهمیت ویژه دارند را در ادامه بیان خواهیم کرد:</p>

                                    <ol>
                                        <li>
                                            <p><strong>برچسب باید کوتاه باشد</strong></p>
                                        </li>
                                        <li>
                                            <p><strong>تعداد برچسب های یک نوشته کم باشد</strong></p>
                                        </li>
                                        <li>
                                            <p><strong>از Tag های قبلی استفاده کنید</strong></p>
                                        </li>
                                        <li>
                                            <p><strong>به تفاوت آنها با دسته دقت کنید</strong></p>
                                        </li>
                                    </ol>

                                    <h3>سخن پایانی</h3>

                                    <p>برچسب ها میتوانند تاثیر زیادی بر سئو سایت شما داشته باشند و همچنین به هدف افزایش کارایی سایت استفاده میشوند؛ البته به شرطی که به درستی از آنها استفاده کنید.</p>

                                    <p>قرار دادن کلمات کلیدی مورد نظرتان بعنوان Tag و استفاده بیش از حد از آنها تاثیری بر بهبود رتبه شما در نتایج گوگل نخواهد داشت و حتی ممکن است برای شما جریمه بهمراه داشته باشد. امروزه در اینترنت با صفحاتی مواجه میشویم که در پایین مطلب تعداد بسیار زیادی برچسب مشابه را بکار میبرند و برخی از آنها رتبه مناسبی نیز در نتایج گوگل بدست آورده اند.</p>

                                    <p>ما به شما اطمینان میدهیم که رتبه کسب شده به دلیل استفاده از برچسب های زیاد و مشابه نبوده و احتمالا به دلایل دیگری مانند ضعیف بودن سایر رقبا در این جایگاه هستند. پیشنهاد میکنیم که به خوانندگان سایت خود احترام بگذارید و کارایی سایت را فدای تکنیک های نادرست سئو نکنید.</p>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade BYekan" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true" id="MetaDescriptionTagModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close pull-left" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <span class="modal-title">Meta Description Tag چیست؟</span>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <strong class="help-block">متخصصین سئو در <a style="color: #00b9ff" href="https://moz.com/learn/seo/meta-description">وب سایت مرجع <span dir="ltr">Moz</span></a> می گویند: متا تگ توضیحات یک تگ <span dir="ltr">html</span> است که توضیح مختصری درباره محتویات یک صفحه وب ارائه می کند.
                                </strong>
                                <img class="table-bordered" src="<%=SSClass.WebDomainFile %>/images/help/meta-description.jpg" alt="Alternate Text" />

                                <h3>معرفی متا تگ Description:</h3>

                                <p>تعریف متا تگ توضیحات بطور ملموس تر بدین صورت است که وقتی عبارتی را در گوگل جستجو می کنیم، همانطور که در تصویر فوق مشخص شده است، به آن توضیح کوتاهی که بعد از لینک وب سایت و بعد از&nbsp;URL&nbsp;آن سایت آمده است متاتگ توضیحات (description meta tag) گفته می شود. به بیان دیگر، متا تگ توضیحات می تواند یک کاربر را متقاعد کند که روی لینک وب سایت ما کلیک کند یا خیر. کاربر می تواند با مطالعه شرح مختصر محتوای صفحه وب، تصمیم بگیرد که روی لینک سایت شما کلیک کند یا اینکه سایت دیگری را برای مطالعه انتخاب کند. در صورتی که متا تگ توضیحات شامل کلمات کلیدی که کاربر جستجو کرده است باشد، بسیار می تواند در متقاعد کردن کاربر برای کلیک روی لینک سایت شما کمک کننده باشد.</p>



                                <h3>آیا تعریف متا تگ توضیحات ضروری است؟</h3>

                                <p>اگر بخواهیم به سوال بالا یک پاسخ کوتاه بدهیم، باید بگوییم بله! و برای شرح دادن دلایل این سوال، به موارد زیر توجه کنید:</p>



                                <h4>۱- تعداد&nbsp;CTR (Click Through Rate)&nbsp;را افزایش دهید و تعداد بازدیدکنندگان وب سایت تان را از طریق جستجوی ارگانیک زیاد کنید.&nbsp;</h4>

                                <p>هر اندازه که در صفحه اول نتایج جستجوی گوگل، به پایان صفحه نزدیک تر می شویم، تعداد کلیک های وب سایت ها کاهش می یابد. زیرا وب سایت های پرطرفدارتر و نیز مرتبط به عبارت سرچ شده بترتیب در رده های بالای نتایج جستجوی گوگل قرار می گیرند.&nbsp; یکی از روش های جذب کاربران برای کلیک کردن روی لینک وب سایت، داشتن توضیحات مختصر و مفید درباره محتویات آن صفحه وب می باشد. بنابراین هر اندازه متای توضیحات آن صفحه پربار تر و جذاب تر باشد، کاربران بیشتری روی لینک وب سایت شما کلیک خواهند کرد.</p>



                                <h4>۲- به کاربران واقعی اطلاعات درست را در زمان مناسب ارائه دهید.</h4>

                                <p>هنگامی که کاربران از جستجوی پیشرفته (Advanced Search) برای یافتن اطلاعات موردنظر خود استفاده می کنند، گوگل از متاتگ توضیحات (Description Meta Tag) وب سایت ها برای نمایش وب سایت ها در نتایج خود استفاده خواهد کرد.</p>



                                <h4>۳- تعداد بازدیدکنندگان وب سایت خود را از طریق رسانه های اجتماعی افزایش دهید.</h4>

                                <p>رسانه های اجتماعی مانند فیسبوک، از متاتگ توضیحات (description meta tag) برای نمایش مطلب به اشتراک گذاشته شده استفاده می کند. بدون تعریف این متا تگ، این شبکه های اجتماعی احتمالا چند کلمه ابتدایی هر صفحه وب را بعنوان متا تگ توضیحات در نظر می گیرند. با توجه به جملات ابتدایی هر مقاله، نمایش آنها به کاربران شبکه های اجتماعی بعنوان خلاصه ای از آن صفحه وب نمی تواند تجربه کاربری خوبی بدنبال داشته باشد و نمی تواند کاربر را برای کلیک بر روی لینک آن وب سایت متقاعد کند.</p>



                                <h4>۴- &nbsp;از متا تگ توضیحات برای ارائه و فروش محتوای وب سایت تان استفاده کنید.</h4>

                                <p>اگرچه متا تگ توضیحات به طور مستقیم بر روی رتبه بندی نتایج گوگل تاثیر ندارد، اما عوارض جانبی تعریف این متا تگ می تواند رتبه های وب سایت ها را بطور غیرمستقیم جابجا کند. در صورتی که متا تگ توضیحات بدرستی (مختصر و مفید و مرتبط) تعریف شده باشد، می تواند&nbsp;CTR-Click Through Rate&nbsp;را افزایش دهد. بنابراین مسلما به مرور زمان می تواند بر الگوریتم های سرچ و رتبه بندی گوگل تاثیرگذار باشد.</p>





                                <h3>چه چیزی باعث قدرتمند شدن متا تگ توضیحات می شود؟</h3>

                                <p>برای تمام صفحات یک وبلاگ و یا وب سایت باید متا تگ توضیحات را بطور صحیح و بهینه تعریف کنیم، دلایل اهمیت تعریف متا تگ توضیحات را در بالا بیان کردیم. اکنون نوبت آنست که یاد بگیریم چگونه می توان متا تگ&nbsp;description&nbsp;را بصورت بهینه تعریف کرد.</p>



                                <h4>۱) محتوای متقاعدکننده برای متا تگ&nbsp;description&nbsp;درنظر بگیریم.</h4>

                                <p>در جمله ای کوتاه و متقاعدکننده بیان کنید که چرا کاربر باید پست شما را بخواند. به کاربران فواید مطالعه پست تان را بطور صریح نمایش دهید. بنابراین بهتر است از این فرصت استفاده کنید و اطلاعات آگاهی دهنده و مفید را در قالب متا تگ توضیحات به کاربران ارائه دهید.</p>



                                <h4>۲) از یک یا دو کلمه کلیدی استفاده کنید.</h4>

                                <p>تگ عنوان (title tag) و متا تگ توضیحات (description meta tag) باید شامل کلمات کلیدی مرتبط با محتوای صفحه وب باشند. تعریف تگ های مذکور به شکل مناسب، باعث می شود خزنده های موتورهای جستجو بفهمند وب سایت شما راجع به چه چیزی است و با کلمات کلیدی تعریف شده در آنها، وب سایت تان را ایندکس و شناسایی کند. متا تگ توضیحات باید پیرامون یک کلمه کلیدی که در محتوای آن صفحه خاص مهم است تعریف شود و نباید از کلمات کلیدی بصورت بیش از حد و غیر مرتبط در آن استفاده کرد.</p>



                                <h4>۳) &nbsp;برای ۳۲۰ کاراکتر هدف گذاری کنید.</h4>

                                <p>در حقیقت گوگل طول تگ ها را با تعداد کاراکترشان اندازه نمی گیرد. بلکه با پیکسل اندازه گیری می کند. متا تگ توضیحات بعد از طول (width) مشخصی، بریده (cut) می شود. بنابراین توصیه می شود تعداد کاراکترهای توضیحات متا بین ۳۰۰ تا ۳۲۰ کاراکتر باشد.</p>

                                <p>نکته: حداکثر تعداد کاراکتر متای توضیحات تا قبل از تاریخ دسامبر ۲۰۱۷ مقدار ۱۶۰ کاراکتر بود اما در آپدیتی که گوگل در سال ۲۰۱۸ ارائه کرده، این تعداد به ۳۲۰ کاراکتر افزایش یافته است. برای کسب اطلاعات بیشتر به&nbsp;<a href="https://www.inc.com/john-lincoln/googles-new-meta-description-length-what-you-need-to-know.html">این مقاله</a>&nbsp;مراجعه شود.</p>



                                <h4>۴) از تکرار کلمات کلیدی اجتناب کنید.&nbsp;</h4>

                                <p>گاهی اوقات خوب است که در عنوان صفحه (title) از عبارات توصیفی (Descriptive Terms) استفاده شود اما تکرار آنها در متا تگ توضیحات نه تنها نمی تواند مفید باشد، بلکه ممکن است موجب اسپم شناخته شدن سایت تان از نظر گوگل و سایر موتورهای جستجو شود.</p>



                                <h4>۵) از کاراکترهای غیر الفبایی استفاده نکنید.</h4>

                                <p>گوگل کاراکترهای&nbsp;AlphaNumeric&nbsp;را در متا تگ توضیحات بعنوان کدهای&nbsp;HTML&nbsp;در نظر می گیرد و در واقع اینگونه کاراکترها معنا و مفهومی برای گوگل ندارند. بنابراین توصیه می شود در تعریف متا تگ توضیحات تنها از متن ساده (plain text) استفاده شود. در صورتی که در متاتگ&nbsp;description&nbsp;صفحه وب سایت خود از کوتیشن (quotation marks) استفاده کنید، گوگل متا تگ توضیحات شما را قطع می کند و ناقص خواهد شد. بنابراین برای جلوگیری از بریده شدن متاتگ توضیحات، سعی کنید کاراکترهای&nbsp;non-alphanumeric&nbsp;را از متاتگ توضیحات حذف کنید.</p>





                                <p>در نهایت این بخش را با جملاتی از&nbsp;<a href="https://support.google.com/webmasters/answer/35624?hl=en">گوگل</a>&nbsp;به پایان می رسانیم:</p>

                                <p>&quot; &nbsp;متا تگ توضیحات را با کیفیت تعریف کنید، مطمئن شوید که متا تگ توضیحات بصورت توصیفی (descriptive) تعریف شده باشد. متا تگ توضیحات (description meta tag) بطور مستقیم به کاربر نمایش داده نمی شود و تنها در نتایج جستجوی گوگل به کاربران ارائه می شود. بنابراین با داشتن یک متا تگ مناسب و توصیفی و مرتبط، می توانید ترافیک بازدیدکنندگان سایت خود را بطور غیرمستقیم افزایش دهید.&nbsp;&nbsp;&quot;</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div dir="rtl" class="modal fade BYekan" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true" id="DeleteFGModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close pull-left" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <span class="modal-title">آیا مطمئن هستید؟</span>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group text-center">
                                            <asp:Button ID="EditContentButton" OnClick="EditContentButton_Click" CssClass="btn btn-warning btn-xs" runat="server" ToolTip="ویرایش" Style="display: none"></asp:Button>
                                            <asp:TextBox ID="CIDTextBox" runat="server" Style="display: none"></asp:TextBox>
                                            <asp:Button CssClass="btn btn-danger" ID="DeleteCIDButton" Text="بله" runat="server" OnClick="DeleteCIDButton_Click" ValidationGroup="DeleteCIDButton" />
                                            <button type="button" class="btn btn-default" data-dismiss="modal">انصراف</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer ">
                    <%--<div class="text-center">
                        <a class="ModalHref" href="<%=SSClass.WebDomain %>/Register?ref=Modal">کاربر جدید هستید؟ ثبت نام کنید.</a>
                    </div>--%>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ContentPlaceHolderID="FooterContentPlaceHolder" ID="FooterContent" runat="server">
    <script type="text/javascript" src='<%= SSClass.WebDomainFile+"/js/dropzone.min.js" %>'></script>
    <script type="text/javascript" src='<%= SSClass.WebDomainFile+"/ckeditor/ckeditor.js" %>'></script>
    <script type="text/javascript">
        var searched = false;
        //File Upload response from the server
        Dropzone.options.dropzoneForm = {
            maxFiles: 15,
            url: "Content.aspx",
            //thumbnail: function(file, dataUrl) {
            //    alert(file.name)
            //},
            init: function () {
                this.on("queuecomplete", function (file) {
                    LoadPhotos('getphotos', 'null');
                });

                this.on("maxfilesexceeded", function (data) {
                    var res = eval('(' + data.xhr.responseText + ')');
                });
                this.on("addedfile", function (file) {
                    // Create the remove button
                    var removeButton = Dropzone.createElement("<button  class='btn btn-xs btn-danger btn-block'>x</button>");
                    // Capture the Dropzone instance as closure.
                    var _this = this;
                    // Listen to the click event
                    removeButton.addEventListener("click", function (e) {
                        // Make sure the button click doesn't submit the form:
                        e.preventDefault();
                        e.stopPropagation();
                        // Remove the file preview.
                        //_this.removeFile(file);
                        //var oEditor = CKEDITOR.instances.RevTextTextBox;
                        //var html = "<img src='sss' />";

                        ////var newElement = CKEDITOR.dom.element.createFromHtml(html, oEditor.document);
                        //oEditor.insertElement("asd"+ + "sadasd");
                        //alert(dropzoneForm.files);
                        CKEDITOR.instances.RevTextTextBox.insertText('some text here');



                        // If you want to the delete the file on the server as well,
                        // you can do the AJAX request here.
                    });
                    // Add the button to the file preview element.
                    file.previewElement.appendChild(removeButton);
                    file.previewElement.remove();
                });
            }
        };
        function LoadPhotos(act, imgsrc) {
            var url = decodeURIComponent('BlogAjax.aspx?');
            url += 'act=' + act + '&imgsrc=' + imgsrc;

            //var table = $(Clicked_Tag).attr('data-table');
            //var field = $(Clicked_Tag).attr('data-field');
            //var fnid = $(Clicked_Tag).attr('data-fnid');
            //var input = $(Clicked_Tag).val();


            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    myFunction(this.responseText);
                }
            }
            xmlhttp.open("POST", url, true);
            xmlhttp.send();

            $('.PleaseWait').css('display', 'block');

            function myFunction(response) {
                var arr = JSON.parse(response);
                if (arr[0].situ == "ok") {

                    var PhotosOut = ''

                    for (var b = 1; b < arr.length; b++) {
                        PhotosOut += '<div class="relative"><img class="photocards" src="' + arr[b].ImgSrc + '" alt="' + arr[b].ImgTitle + '" title="' + arr[b].ImgTitle + '" /><div class="absolute"><span onclick="AddImg(' + "'" + arr[b].ImgSrc + "'" + ',' + "'" + arr[b].ImgTitle + "'" + ')" class="glyphicon glyphicon-plus-sign btn btn-default btn-xs"></span><span onclick="LoadPhotos(' + "'deleteimg','" + arr[b].ImgSrc.replace('"+<%=SSClass.WebDomainFile %>+"', '../..') + "'" + ')" class="glyphicon glyphicon-trash btn btn-default btn-xs" style="margin-right: 4px;" ></span></div></div>';
                    }
                    $('#photos').html(PhotosOut);
                }
                else {
                    alert('خطا!\n' + arr[0].situ);
                }
                $('.PleaseWait').css('display', 'none');
            }
        }
        function AddImg(ImgSrc, ImgTitle) {
            var element = CKEDITOR.dom.element.createFromHtml('<div style="text-align:center"><figure class="image" style="display:inline-block"><img src="' + ImgSrc + '" alt="' + ImgTitle + '" /><figcaption>' + ImgTitle + '</figcaption></figure></div>');
            CKEDITOR.instances.RevTextTextBox.insertElement(element);
        }
        $(document).ready(function ($) {

            CKEDITOR.replace('RevTextTextBox');
            CKEDITOR.instances.RevTextTextBox.setData($('#CKTempTextBox').val());
            LoadPhotos('getphotos', '');
            CKEDITOR.config.allowedContent = true;
            CKEDITOR.config.extraAllowedContent = true;
            CKEDITOR.dtd.$removeEmpty.span = 0;
            CKEDITOR.config.extraPlugins = 'pre';

            $('*[id^="CatTreeViewt"]').mouseover(function (event) {
                alert(this.innerHTML);
            });


        })
        function AddCollapsible(panelclass) {
            var RandomID = Math.floor((Math.random() * 999846464) + 1);

            //var element = CKEDITOR.dom.element.createFromHtml('<h2 class="collaps-h2" aria-controls="col' + RandomID + '" aria-expanded="false" class="collapsed" data-target="#col' + RandomID + '" data-toggle="collapse">عنوان</h2>');
            //CKEDITOR.instances.RevTextTextBox.insertElement(element);

            //var element = CKEDITOR.dom.element.createFromHtml('<div aria-expanded="true" class="collapse in" id="col' + RandomID + '" ><div class="well">محتوا...</div></div>');


            //CKEDITOR.instances.RevTextTextBox.insertElement(element);
            var element = CKEDITOR.dom.element.createFromHtml('<div class="panel panel-' + panelclass + '"><div class="panel-heading" id="heading' + RandomID + '" role="tab"><h2 class="panel-title" aria-controls="collapseOne" aria-expanded="true" data-parent="#accordion" data-toggle="collapse" data-target="#col' + RandomID + '" role="button">عنوان</h2></div><div aria-expanded="true" class="panel-collapse collapse in" id="col' + RandomID + '" role="tabpanel" style=""><div class="panel-body"><p>محتوا...</p></div></div></div>');
            CKEDITOR.instances.RevTextTextBox.insertElement(element);


        }

        function SearchAjax(Input) {
            var InpuElement0 = arguments[0];
            //var act = $(InpuElement0).attr('data-act');
            var keyword = $(InpuElement0).val();

            var act = $(InpuElement0).data('act');
            if (act == "newtag") {
                keyword = $('#<%=TagTextBox.ClientID%>').val();
            }
            else if (act == "tag") {
                keyword = $('#<%=InsertedTagTextBox.ClientID%>').val() + "|" + keyword
            }
            else if (act == "content") {
                keyword = $('#<%=SearchContentTextBox.ClientID%>').val() + "|" + $('#<%=Cat1DropDownList.ClientID%>').val();
            }
            $.ajax({
                type: "POST",
                url: "Content.aspx/SearchAjax",
                context: InpuElement0,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                //data: {'baseInfo':BaseInfo,fields:FieldsString},
                data: "{keyword:" + JSON.stringify(keyword) + ",act:" + JSON.stringify(act) + "}",
                success: OnSuccessSearchAjax,
                failure: function (response) {
                    alert("error 542:" + response.d);
                },
                error: function (response) {
                    alert("error 576:" + response.d);
                }
            });
        }
        function OnSuccessSearchAjax(response) {
            var arr;

            try {
                arr = JSON.parse(response.d);
            } catch (e) {
                alert('خطا: ' + e.message)
            }
            var OutString = "";
            if (arr[0].situ == "ok") {
                if (arr[1].act == "product") {

                    for (var i = 2; i < arr.length; i++) {
                        OutString += '<li class="list-group-item" data-act="product" data-jpid="' + arr[i].jpid + '"  onclick="PushIt(this)">' + arr[i].title + '</li><span class="badge"><a target="_blank" href="http://www.jahazchin.com/products/detail/jpid/' + arr[i].jpid + '/view/">' + 'مشاهده' + '</a></span>';
                    }
                    if (OutString != "") {
                        $('#CatsFilterUl').html(OutString);
                        OutString = "";
                    }
                    else {
                        $('#CatsFilterUl').html('<li class="list-group-item" >نتیجه ای یافت نشد</li>');
                    }
                }
                else if (arr[1].act == "guarantee") {

                    for (var i = 2; i < arr.length; i++) {
                        OutString += '<li class="list-group-item" data-act="guarantee" data-jgid="' + arr[i].jgid + '"  onclick="PushIt(this)">' + arr[i].title + '</li>';
                    }
                    if (OutString != "") {
                        $('#CatsFilterUl').html(OutString);
                        OutString = "";
                    }
                    else {
                        $('#CatsFilterUl').html('<li class="list-group-item" >نتیجه ای یافت نشد</li>');
                    }
                }
                else if (arr[1].act == "color") {

                    for (var i = 2; i < arr.length; i++) {
                        OutString += '<li class="list-group-item" data-act="color" data-jcid="' + arr[i].jcid + '"  onclick="PushIt(this)"><span style="height: 15px; width: 15px;margin-left:5px; background-color:' + arr[i].colorcode + '"></span>' + arr[i].title + '</li>';
                    }
                    if (OutString != "") {
                        $('#CatsFilterUl').html(OutString);
                        OutString = "";
                    }
                    else {
                        $('#CatsFilterUl').html('<li class="list-group-item" >نتیجه ای یافت نشد</li>');
                    }
                }
                else if (arr[1].act == "brand") {

                    for (var i = 2; i < arr.length; i++) {
                        OutString += '<li class="list-group-item" data-act="brand" data-bid="' + arr[i].bid + '"  onclick="PushIt(this)">' + arr[i].title + '</li>';
                    }
                    if (OutString != "") {
                        $('#BrandsUl').html(OutString);
                        OutString = "";
                    }
                    else {
                        $('#BrandsUl').html('<li class="list-group-item" >نتیجه ای یافت نشد</li>');
                    }
                }
                else if (arr[1].act == "slider") {

                    for (var i = 2; i < arr.length; i++) {
                        OutString += '<li class="list-group-item" data-act="slider" data-sid="' + arr[i].sid + '"  onclick="PushIt(this)">' + arr[i].title + '</li>';
                    }
                    if (OutString != "") {
                        $('#SliderUl').html(OutString);
                        OutString = "";
                    }
                    else {
                        $('#SliderUl').html('<li class="list-group-item" >نتیجه ای یافت نشد</li>');
                    }
                }
                else if (arr[1].act == "tag") {

                    for (var i = 2; i < arr.length; i++) {
                        OutString += '<li class="list-group-item" data-act="tag" data-tid="' + arr[i].tid + '"  onclick="PushIt(this)">' + arr[i].title + '</li>';
                    }
                    if (OutString != "") {
                        $('#TagUl').html(OutString);
                        OutString = "";
                    }
                    else {
                        if ($('#<%=TagTextBox.ClientID%>').val().trim() != "") {
                            $('#TagUl').html('<li class="list-group-item" onclick="SearchAjax(this)" data-act="newtag" >نتیجه ای یافت نشد. در صورتی که این یک برچسب جدید است، کلیک کنید!</li>');

                        }
                        else {
                            $('#TagUl').html('<li class="list-group-item" >لطفا برچسب مورد نظر خود را تایپ کنید...</li>');

                        }
                    }
                }
                else if (arr[1].act == "newtag") {

                    for (var i = 2; i < arr.length; i++) {
                        OutString += '<li class="list-group-item newtag-inserted" data-act="tag" data-tid="' + arr[i].tid + '"  onclick="PushIt(this)">' + arr[i].title + '</li>';
                    }
                    if (OutString != "") {
                        $('#TagUl').html(OutString);
                        $('.newtag-inserted').click();
                        $('#<%=TagTextBox.ClientID%>').val('');
                        OutString = "";
                    }
                    else {
                        $('#TagUl').html('<li class="list-group-item" onclick="SearchAjax(this)" data-act="newtag" >نتیجه ای یافت نشد. در صورتی که این یک برچسب جدید است، کلیک کنید!</li>');
                    }
                }
                else if (arr[1].act == "content") {
                    OutString += '<tbody><tr valign="middle" style="color:White;background-color:#666666;font-weight:normal;"><th scope="col">کد</th><th scope="col">عنوان</th><th scope="col">بازدید</th><th scope="col">تاریخ</th><th scope="col">عملیات</th></tr>'
                    var flag = false;
                    for (var i = 2; i < arr.length; i++) {
                        OutString += '<tr><td>' + arr[i].cid + '</td><td><a href="<%=SSClass.WebDomain%>/' + arr[i].cat2title + '/' + arr[i].dashedtitle + '/' + arr[i].cid + '" target="_blank">' + arr[i].title + '</a></td><td>' + arr[i].pageview + '</td><td><span>' + arr[i].cdate + '</span></td><td><span data-cid="' + arr[i].cid + '" class="btn btn-xs btn-default edit-cid glyphicon glyphicon-edit"></span><span data-cid="' + arr[i].cid + '" class="btn btn-xs btn-default delete-cid glyphicon glyphicon-trash"></span></td></tr>';
                        flag = true;
                    }
                    OutString += '</tbody>';
                    if (flag) {
                        $('#<%=ContentGridView.ClientID%>').html(OutString);
                        $('.newtag-inserted').click();
                        $('#<%=TagTextBox.ClientID%>').val('');
                        OutString = "";
                        searched = true;
                    }
                    else {
                        OutString = '<tbody><tr valign="middle" style="color:White;background-color:#666666;font-weight:normal;"><th scope="col">کد</th><th scope="col">عنوان</th><th scope="col">بازدید</th><th scope="col">تاریخ</th><th scope="col">عملیات</th></tr><tr><td colspan="5">نتیجه ای یافت نشد!</td></tr></tbody>'
                        $('#<%=ContentGridView.ClientID%>').html(OutString);
                    }
                    $(".delete-cid").click(function (e) {
                        var InpuElement0 = arguments[0];
                        var cid = this.dataset.cid;
                        $('#<%=CIDTextBox.ClientID %>').val(cid)
                        $('#DeleteFGModal').modal('show')

                    });
                    $(".edit-cid").click(function (e) {
                        var InpuElement0 = arguments[0];
                        var cid = this.dataset.cid;
                        $('#<%=CIDTextBox.ClientID %>').val(cid)
                        $('#<%=EditContentButton.ClientID %>').click()
                    });
                }
            }
            else {
                alert("error8745: " + response.d)
            }
        }
        function NewTag() {
            var NewTagName = $('#<%=TagTextBox.Text%>');

        }
        function RemoveTag() {
            var InpuElement0 = arguments[0];
            var tid = $(InpuElement0).data('tid');

            $(InpuElement0).fadeOut(300, function () { $(this).remove(); })
            $('#<%=InsertedTagTextBox.ClientID%>').val($('#<%=InsertedTagTextBox.ClientID%>').val().replace(tid + ",", ""));

        }
        function PushIt() {
            var InpuElement0 = arguments[0];
            var act = $(InpuElement0).data('act');
            switch (act) {

                case 'slider': {
                    var sid = $(InpuElement0).data('sid');
                    var element = CKEDITOR.dom.element.createFromHtml('<div>###' + sid + '###</div>');
                    CKEDITOR.instances.RevTextTextBox.insertElement(element);
                    $("#SliderUl").html('');

                }
                    break;
                case 'tag': {
                    var tid = $(InpuElement0).data('tid');

                    $('#InsertedTagUl').append('<li onclick="RemoveTag(this)" class="inserted-li" data-act="removetag" data-tid="' + tid + '" >' + $(InpuElement0).text() + '</li>')

                    $('#<%=InsertedTagTextBox.ClientID%>').val($('#<%=InsertedTagTextBox.ClientID%>').val() + tid + ",");
                    $(InpuElement0).remove();
                }
                    break;
                default:
            }
            var bcpid = $('#BCPIDSpan').text();
            if ($(InpuElement0).attr('data-act') == "product") {
                $('.jpid_' + bcpid).prev().val($(InpuElement0).attr('data-jpid'));
                $('.jgid_' + bcpid).attr('data-jpid', $(InpuElement0).attr('data-jpid'));
                $('.jcid_' + bcpid).attr('data-jpid', $(InpuElement0).attr('data-jpid'));
                $('.jpid_' + bcpid).click();

            }
            else if ($(InpuElement0).attr('data-act') == "guarantee") {
                $('.jgid_' + bcpid).prev().val($(InpuElement0).attr('data-jgid'));
                $('.jgid_' + bcpid).click();

            }
            else if ($(InpuElement0).attr('data-act') == "color") {
                $('.jcid_' + bcpid).prev().val($(InpuElement0).attr('data-jcid'));
                $('.jcid_' + bcpid).click();

            }
            $('#SearchModal').modal('hide')

        }
        function pageLoad() {
            if ($('#BodyContentPlaceHolder_ContentPlaceHolder1_CatTreeViewn0Nodes').css('display') == 'block') {
                $('[id^=BodyContentPlaceHolder_ContentPlaceHolder1_CatTreeView]').children().click();

            }
            var InsertedTags = $('#<%=InsertedTagTextBox.ClientID%>').val().toLocaleLowerCase().split(",");
            var tids = "";
            for (var i = 0; i < InsertedTags.length; i++) {
                if (InsertedTags[i].length > 0) {

                    $('#InsertedTagUl').append('<li onclick="RemoveTag(this)" class="inserted-li" data-act="removetag" data-tid="' + InsertedTags[i].split("-")[0] + '" >' + InsertedTags[i].split("-")[1] + '</li>')
                    tids += InsertedTags[i].split("-")[0] + ",";
                }
            }
            $('#<%=InsertedTagTextBox.ClientID%>').val(tids);

        }
        $("body").click(function () {
            $("#SliderUl").html('');
            $("#TagUl").html('');

        });

        $("#SliderUl").click(function (e) {
            e.stopPropagation();
        });
        $("#TagUl").click(function (e) {
            e.stopPropagation();
        });
        $(".delete-cid").click(function (e) {
            var InpuElement0 = arguments[0];
            var cid = this.dataset.cid;
            $('#<%=CIDTextBox.ClientID %>').val(cid)
            $('#DeleteFGModal').modal('show')

        });
        $(".edit-cid").click(function (e) {
            var InpuElement0 = arguments[0];
            var cid = this.dataset.cid;
            $('#<%=CIDTextBox.ClientID %>').val(cid)
            $('#<%=EditContentButton.ClientID %>').click()
        });
    </script>

</asp:Content>
