﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Farsi_Admin_Content : System.Web.UI.Page
{
    protected string DisplayDate(object Date)
    {
        PersianDateTime PersianDate = new PersianDateTime(Convert.ToDateTime(Date));
        return PersianDate.ToString("hh:mm tt - yyyy/MM/dd");
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["sqlrev"] == null)
        {
            Session["sqlrev"] = " ";
        }
        if (Session["irev"] == null)
        {
            Session["irev"] = 1;
        }
        Session["pidrev"] = Request.QueryString["pid"];
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        System.Web.UI.HtmlControls.HtmlAnchor li = (System.Web.UI.HtmlControls.HtmlAnchor)this.Page.Master.Master.FindControl("BodyContentPlaceHolder").FindControl("content");

        li.Attributes["class"] = "btn btn-primary";

        if (!IsPostBack)
        {
            SaveUploadedFile(Request.Files);
        }
        if (Session["sqlrev"] == null)
        {
            Session["sqlrev"] = " ";
        }
        if (Session["irev"] == null)
        {
            Session["irev"] = 1;
        }
        if (Session["tempCK"] == null)
        {
            Session["tempCK"] = " ";
        }
        Session["pidrev"] = Request.QueryString["pid"];

        InfoDiv.Visible = true;
        InfoLiteral.Text = "مدیریت محتوای اصلی سایت از قبیل صفحات، اخبار، وبلاگ و ... از این قسمت انجام می گیرد.";
        InfoDiv.Attributes["class"] = " alert alert-info alert-dismissable text-right";
    }
    protected void SubmitNewsButton_Click(object sender, EventArgs e)
    {

        if (CatTreeView.SelectedNode == null)
        {
            InfoDiv.Visible = true;
            InfoLiteral.Text = "خطا! تعیین دسته اصلی الزامیست جهت تعیین دسته اصلی در قسمت دسته بندی روی گزینه ی مورد نظر کلیک کنید.";
            InfoDiv.Attributes["class"] = " alert alert-danger alert-dismissable text-right";
            return;
        }
        if (CatTreeView.CheckedNodes.Count == 0)
        {
            InfoDiv.Visible = true;
            InfoLiteral.Text = "خطا! از قسمت دسته بندی حداقل یک دسته را انتخاب کنید.";
            InfoDiv.Attributes["class"] = " alert alert-danger alert-dismissable text-right";
            return;
        }
        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);

        string CID = "noPage";

        string Header = HeaderTextBox.Text;
        string BodyString = CKTempTextBox.Text;
        string PhotoString = SSClass.WebDomainFile + "/images/NewsImages/Default.JPG";
        string SecCodeString = Guid.NewGuid().ToString("N").Substring(16);
        if (NewsFileUpload.HasFile)
        {
            string fileExt = System.IO.Path.GetExtension(NewsFileUpload.FileName.ToLower());
            if (fileExt == ".jpg" || fileExt == ".jpeg" || fileExt == ".png" || fileExt == ".gif")
            {
                PhotoString = SSClass.WebDomainFile + "/images/NewsImages/" + SecCodeString + fileExt;
            }
            else
            {
                InfoLiteral.Text = "فقط تصاویر با فرمت های PNG، JPG و JPEG پشتیبانی می شود.";
                InfoDiv.Attributes["class"] = " alert alert-danger alert-dismissable text-right";
                return;
            }

            long size = NewsFileUpload.PostedFile.ContentLength;
            size = size / 1024; //to KB
            if (size > 70)
            {
                InfoLiteral.Text = "حجم تصویر ارسالی شما بیشتر از 70 کیلوبایت است.";
                InfoDiv.Attributes["class"] = " alert alert-danger alert-dismissable text-right";
                return;
            }
            NewsFileUpload.SaveAs(Server.MapPath("~/images/newsimages/" + SecCodeString + fileExt));

        }
        else if (PhotoTextBox.Text != "")
        {
            PhotoString = PhotoTextBox.Text;
        }

        string NewsSqlSyntax = "";
        if (CIDTextBox.Text == "")
        {
            NewsSqlSyntax = @"INSERT INTO Content (
                                                     Body
                                                    ,headerUp
                                                    ,Header
                                                    ,headerdown
                                                    ,resource
                                                    ,Summary
                                                    ,summaryviz
                                                    ,pageview
                                                    ,pageviewviz
                                                    ,Photo
                                                    ,photoviz
                                                    ,cat1
                                                    ,cat2
                                                    ,situ
                                                    ,lan
                                                    ,cdate
                                                    ,cdateviz
                                                    ) output inserted.cid VALUES 
                                                    (@Body
                                                    ,@headerUp
                                                    ,@Header
                                                    ,@headerdown
                                                    ,@resource
                                                    ,@Summary
                                                    ,@summaryviz
                                                    ,@pageview
                                                    ,@pageviewviz
                                                    ,@Photo
                                                    ,@photoviz
                                                    ,@cat1
                                                    ,@cat2
                                                    ,@situ
                                                    ,@lan
                                                    ,@cdate
                                                    ,@cdateviz)";

        }
        else
        {
            NewsSqlSyntax = @"update content set
                                               Body       =@Body
                                              ,headerUp   =@headerUp
                                              ,Header     =@Header
                                              ,headerdown =@headerdown
                                              ,resource   =@resource
                                              ,Summary    =@Summary
                                              ,summaryviz =@summaryviz
                                              ,pageviewviz=@pageviewviz
                                              ,Photo      =@Photo
                                              ,photoviz   =@photoviz
                                              ,cat1       =@cat1
                                              ,cat2       =@cat2
                                              ,situ       =@situ
                                              ,lan        =@lan
                                              ,cdate      =@cdate
                                              ,cdateviz   =@cdateviz
                                               where cid=@cid

";

        }



        SqlCommand NewsCmd = new SqlCommand(NewsSqlSyntax, con);
        SqlParameter[] param = new SqlParameter[19];

        param[00] = new SqlParameter("@CID", SqlDbType.NVarChar);
        param[01] = new SqlParameter("@Body", SqlDbType.NVarChar);
        param[02] = new SqlParameter("@HeaderUp", SqlDbType.NVarChar);
        param[03] = new SqlParameter("@Header", SqlDbType.NVarChar);
        param[04] = new SqlParameter("@HeaderDown", SqlDbType.NVarChar);
        param[05] = new SqlParameter("@Resource", SqlDbType.NVarChar);
        param[06] = new SqlParameter("@Summary", SqlDbType.NVarChar);
        param[07] = new SqlParameter("@SummaryViz", SqlDbType.NVarChar);
        param[08] = new SqlParameter("@PageView", SqlDbType.NVarChar);
        param[09] = new SqlParameter("@PageViewViz", SqlDbType.NVarChar);
        param[10] = new SqlParameter("@Photo", SqlDbType.NVarChar);
        param[11] = new SqlParameter("@Photoviz", SqlDbType.NVarChar);
        param[12] = new SqlParameter("@Cat1", SqlDbType.NVarChar);
        param[13] = new SqlParameter("@Situ", SqlDbType.NVarChar);
        param[14] = new SqlParameter("@Lan", SqlDbType.NVarChar);
        param[15] = new SqlParameter("@UpdateDate", SqlDbType.NVarChar);
        param[16] = new SqlParameter("@CDateViz", SqlDbType.NVarChar);
        param[17] = new SqlParameter("@CDate", SqlDbType.NVarChar);
        param[18] = new SqlParameter("@Cat2", SqlDbType.NVarChar);



        param[00].Value = CIDTextBox.Text;
        param[01].Value = BodyString;
        param[02].Value = HeaderUpTextBox.Text;
        param[03].Value = HeaderTextBox.Text;
        param[04].Value = HeaderDownTextBox.Text;
        param[05].Value = ResourceTextBox.Text;
        param[06].Value = SummaryTextBox.Text;
        param[07].Value = SummaryVizCheckBox.Checked;
        param[08].Value = "0";
        param[09].Value = true;
        param[10].Value = PhotoString;
        param[11].Value = PhotoVizCheckBox.Checked;
        param[12].Value = Cat1DropDownList.SelectedValue;
        param[13].Value = "ok";
        param[14].Value = Cat1DropDownList.SelectedValue.Split('-')[0];
        param[15].Value = DateTime.Now;
        param[16].Value = true;
        param[17].Value = DateTime.Now;
        param[18].Value = CatTreeView.SelectedNode.Value;



        for (int i = 0; i < param.Length; i++)
        {
            NewsCmd.Parameters.Add(param[i]);
        }

        try
        {
            con.Open();
            if (CIDTextBox.Text == "")
            {
                CID = NewsCmd.ExecuteScalar().ToString();

            }
            else
            {
                NewsCmd.ExecuteNonQuery();
                CID = CIDTextBox.Text;
            }


            InfoDiv.Visible = true;
            InfoLiteral.Text = "مطلب با موفقیت ثبت شد. جهت مشاهده در سایت" + "<a href=\"" + SSClass.WebDomain + "/" + Cat1DropDownList.SelectedValue.Split('-')[0] + "/blog/page" + CID + "\" target=\"_blank\">کلیک کنید</a>";
            InfoDiv.Attributes["class"] = " alert alert-success alert-dismissable text-right";

            SubmitNewsButton.Text = "ثبت مطلب";
            SubmitNewsButton.CssClass = "btn btn-primary btn-block";
        }
        catch (System.Data.SqlClient.SqlException ex)
        {
            string msg = "NewsCmd Error:";
            msg += ex.Message;
            throw new Exception(msg);
        }
        finally
        {
            con.Close();
            CIDTextBox.Text = "";
        }

        string ContentTagCatKeywordSqlString = " ";

        ContentTagCatKeywordSqlString += " delete from ContentKeyword where cid= " + CID;
        ContentTagCatKeywordSqlString += " delete from ContentTag where cid= " + CID;
        ContentTagCatKeywordSqlString += " delete from ContentCat where cid= " + CID;

        foreach (string item in KeywordTextBox.Text.Trim().Split('-'))
        {
            if (item != "")
            {
                ContentTagCatKeywordSqlString += "  INSERT INTO [ContentKeyword] ([cid], [keyword]) VALUES (" + CID + ",N' " + item.ToString().Trim() + "')  ";
            }
        }
        foreach (string item in InsertedTagTextBox.Text.Trim().Split(','))
        {
            if (item != "")
            {
                ContentTagCatKeywordSqlString += "  INSERT INTO [ContentTag] ([cid], [tid]) VALUES (" + CID + "," + item.ToString().Trim() + ")  ";
            }
        }

        if (CatTreeView.CheckedNodes.Count > 0)
        {
            foreach (TreeNode node in CatTreeView.CheckedNodes)
            {
                if (node.Parent != null)
                {
                    if (node.Value != "null")
                    {
                        ContentTagCatKeywordSqlString += "  INSERT INTO [ContentCat] ([cid], [nodeid]) VALUES (" + CID + ", " + node.Value.ToString().Trim() + ")  ";
                    }
                }
            }
        }

        SqlCommand NewsTagAndGroupAssignmentCmd = new SqlCommand(ContentTagCatKeywordSqlString, con);
        try
        {
            con.Open();
            NewsTagAndGroupAssignmentCmd.ExecuteScalar();

            ContentGridView.DataBind();

            CKTempTextBox.Text = "";
            HeaderUpTextBox.Text = "";
            HeaderTextBox.Text = "";
            HeaderDownTextBox.Text = "";
            ResourceTextBox.Text = "";
            SummaryTextBox.Text = "";
            SummaryVizCheckBox.Checked = true;
            PhotoTextBox.Text = "";
            PhotoVizCheckBox.Checked = true;

            InsertedTagTextBox.Text = "";
            KeywordTextBox.Text = "";

            SelectNodeByValue(CatTreeView.Nodes, "null");
            foreach (TreeNode node in CatTreeView.Nodes)
            {
                node.Checked = false;
                CheckChildren(node, false);
            }
        }
        catch (System.Data.SqlClient.SqlException ex)
        {
            string msg = "insert ContentTagCatKeywordSqlString Error:";
            msg += ex.Message;
            throw new Exception(msg);
        }
        finally
        {
            con.Close();
        }
    }





    protected void Cat1DropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Cat1DropDownList.SelectedValue != "0")
        {
            ContentFormDiv.Visible = true;
            CatTreeView.Nodes.Clear();
            TreeNode NewNode = new
                        TreeNode("لطفا انتخاب کنید", "null");
            NewNode.PopulateOnDemand = true;
            NewNode.SelectAction = TreeNodeSelectAction.Select;
            CatTreeView.Nodes.Add(NewNode);
            CatTreeView.DataBind();
        }
        else
        {
            ContentFormDiv.Visible = false;
        }

    }

    public void SaveUploadedFile(HttpFileCollection httpFileCollection)
    {

        bool isSavedSuccessfully = true;
        string fName = "";
        string SQLString = "";

        foreach (string fileName in httpFileCollection)
        {
            HttpPostedFile file = httpFileCollection.Get(fileName);
            //Save file content goes here
            fName = file.FileName;
            if (file != null && file.ContentLength > 0)
            {

                //var originalDirectory = new DirectoryInfo(string.Format("{0}Images\\WallImages", Server.MapPath(@"\")));

                //string pathString = System.IO.Path.Combine(originalDirectory.ToString(), "imagepath");

                //var fileName1 = Path.GetFileName(file.FileName);


                //bool isExists = System.IO.Directory.Exists(pathString);

                //if (!isExists)
                //    System.IO.Directory.CreateDirectory(pathString);


                //var path = string.Format("{0}\\{1}", pathString, file.FileName);

                string SecCode = Guid.NewGuid().ToString("N").Substring(16);
                string fileExt = System.IO.Path.GetExtension(file.FileName.ToLower());
                file.SaveAs(Server.MapPath("~/images/Reviews/" + SecCode + fileExt));

                Session["sqlrev"] = Session["sqlrev"].ToString() + "   insert into photos (imgsrc,imgtitle) values (N'" + SSClass.WebDomainFile + "/Images/Reviews/" + SecCode + fileExt + "',N'" + fName + "')   ";
                Session["irev"] = Convert.ToInt32(Session["irev"].ToString()) + 1;

                //Session["tempCK"] += "<div class=\"text-center\"><img src=\"" + SSClass.WebDomainFile +"/Images/Reviews/" + SecCode + fileExt + "\" alt=\"" + "####" + "\" title=\"" + "####" + "\" /></div>";
                //SqlParameter[] param = new SqlParameter[2];

                //param[00] = new SqlParameter("@imgsrc", SqlDbType.NVarChar);
                //param[01] = new SqlParameter("@imgorder", SqlDbType.Int);

                //param[00].Value = SSClass.WebDomainFile +"/Images/products/" + SecCode + fileExt;
                //param[01].Value = i;



                //for (int r = 0; r < param.Length; r++)
                //{
                //    RegCmd.Parameters.Add(param[r]);
                //}



            }

        }

        //InsertIntoPhotos();
        //if (isSavedSuccessfully)
        //{
        //    return Json(new { Message = fName });
        //}
        //else
        //{
        //    return Json(new { Message = "Error in saving file" });
        //}
    }
    [WebMethod]
    public static string SearchAjax(string keyword, string act)
    {
        keyword = keyword.Trim();

        string CS = DataLayer.connectionString;
        SqlConnection con = new SqlConnection(CS);

        string retvalString = "[{\"situ\":\"ok\"},{\"act\":\"" + act + "\"},";

        //string SQLQuerry = @"if not exists (select * from productlinkage where bcpid=@bcpid) insert into productlinkage (bcpid,pid) values (@bcpid,@pid) else update productlinkage set pid=@pid where bcpid=@bcpid ";
        string SQLQuerry = "";
        if (act == "product")
        {
            SQLQuerry = @" select top(20)  id,title from product where isdeleted=0 and (title like N'%" + keyword + "%' or entitle like N'%" + keyword + "%')";
        }
        else if (act == "guarantee")
        {
            SQLQuerry = @" select top(20)  [Id],[Title] from [Guaranty] where isdeleted=0 and (title like N'%" + keyword + "%')";
        }
        else if (act == "color")
        {
            SQLQuerry = @" select top(20)  [Id],[Title],[ColorCode] from [Color] where isdeleted=0 and (title like N'%" + keyword + "%')";
        }
        else if (act == "brand")
        {
            SQLQuerry = @" select top(20)  [BID],[BrandFName],[BrandEName] from [AllBrands] where (BrandFName like N'%" + keyword + "%') or (BrandEName like N'%" + keyword + "%')";
        }
        else if (act == "slider")
        {
            SQLQuerry = @" select top(20)  [SID],[Title] from [Slider] where (Title like N'%" + keyword + "%')";
        }
        else if (act == "tag")
        {

            SQLQuerry = @" select top(20)  [TID],[TagName] from [Tag] where (TagName like N'%" + keyword.Split('|')[1] + "%') and tid not in (" + keyword.Split('|')[0] + "0)";
        }
        else if (act == "newtag")
        {
            if (keyword.Trim()!="")
            {
                SQLQuerry = @"begin
                    DECLARE @myInsertedTable table (insertedid  bigint NOT NULL)
                    insert into tag (tagname) OUTPUT inserted.tid into @myInsertedTable  values (N'" + keyword + @"')  
                    select insertedid as tid,N'" + keyword + @"' as tagname from  @myInsertedTable delete from @myInsertedTable
                    end";
            }
            
        }
        else if (act == "content")
        {
            if (keyword.Split('|')[0].Trim() == "")
            {
                SQLQuerry = "SELECT top(40) [CID],[Header],pageview,dbo.[Content].cdate,lan, dbo.Tree.Name AS cat2title FROM dbo.[Content] INNER JOIN dbo.Tree ON dbo.[Content].cat2 = dbo.Tree.NodeID WHERE  cat1=N'" + keyword.Split('|')[1] + "'  and isdeleted=0 order by dbo.[Content].cdate desc";
            }
            else
            {
                SQLQuerry = @" select top(20)  [CID],[Header],pageview,dbo.[Content].cdate,lan, dbo.Tree.Name AS cat2title FROM dbo.[Content] INNER JOIN dbo.Tree ON dbo.[Content].cat2 = dbo.Tree.NodeID WHERE  (Header like N'%" + keyword.Split('|')[0] + "%' and cat1=N'" + keyword.Split('|')[1] + "')";
            }

        }
        SqlCommand RegCmd = new SqlCommand(SQLQuerry, con);
        RegCmd.CommandTimeout = 60;

        SqlParameter[] param = new SqlParameter[2];

        param[00] = new SqlParameter("@jpid", SqlDbType.NVarChar);
        param[01] = new SqlParameter("@bcpid", SqlDbType.NVarChar);



        param[00].Value = "";
        param[01].Value = "";





        for (int r = 0; r < param.Length; r++)
        {
            RegCmd.Parameters.Add(param[r]);
        }

        try
        {
            SqlDataReader dr;
            con.Open();
            dr = RegCmd.ExecuteReader();


            if (act == "product")
            {
                while (dr.Read())
                {
                    retvalString += "{\"jpid\":\"" + dr["id"].ToString() + "\",\"title\":\"" + dr["title"].ToString() + "\"},";
                }
            }
            else if (act == "guarantee")
            {
                while (dr.Read())
                {
                    retvalString += "{\"jgid\":\"" + dr["id"].ToString() + "\",\"title\":\"" + dr["title"].ToString() + "\"},";
                }
            }
            else if (act == "color")
            {
                while (dr.Read())
                {
                    retvalString += "{\"jcid\":\"" + dr["id"].ToString() + "\",\"title\":\"" + dr["title"].ToString() + "\",\"colorcode\":\"" + dr["colorcode"].ToString() + "\"},";
                }
            }
            else if (act == "brand")
            {
                while (dr.Read())
                {
                    retvalString += "{\"bid\":\"" + dr["bid"].ToString() + "\",\"title\":\"" + dr["BrandFname"].ToString() + " | " + dr["BrandEname"].ToString() + "\"},";
                }
            }
            else if (act == "slider")
            {
                while (dr.Read())
                {
                    retvalString += "{\"sid\":\"" + dr["sid"].ToString() + "\",\"title\":\"" + dr["Title"].ToString() + "\"},";
                }
            }
            else if (act == "tag")
            {
                while (dr.Read())
                {
                    retvalString += "{\"tid\":\"" + dr["tid"].ToString() + "\",\"title\":\"" + dr["tagname"].ToString() + "\"},";
                }
            }
            else if (act == "newtag")
            {
                while (dr.Read())
                {
                    retvalString += "{\"tid\":\"" + dr["tid"].ToString() + "\",\"title\":\"" + dr["tagname"].ToString() + "\"},";
                }
            }
            else if (act == "content")
            {
                while (dr.Read())
                {
                    retvalString += "{\"cid\":\"" + dr["cid"].ToString() + "\",\"title\":\"" + MyFunc.SafeString(dr["header"].ToString()).Replace("-", " ") + "\",\"dashedtitle\":\"" + MyFunc.SafeString(dr["header"].ToString()) + "\",\"pageview\":\"" + dr["pageview"].ToString() + "\",\"cdate\":\"" + MyFunc.DisplayDateTime(dr["cdate"].ToString()) + "\",\"cat2title\":\"" + MyFunc.SafeString(dr["cat2title"].ToString()) + "\"},";
                }
            }

            retvalString = retvalString.Remove(retvalString.Length - 1);
            retvalString += "]";
        }
        catch (System.Data.SqlClient.SqlException ex)
        {
            string msg = "RegCmd Error:";
            msg += ex.Message + " ||| " + SQLQuerry;
            return msg;
        }
        finally
        {
            con.Close();
        }
        return retvalString;
    }
    private void CheckChildren(TreeNode rootNode, bool isChecked)
    {
        foreach (TreeNode node in rootNode.ChildNodes)
        {
            CheckChildren(node, isChecked);
            node.Checked = isChecked;
        }
    }
    protected void CatTreeView_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        if (e.Node.ChildNodes.Count == 0)
        {
            if (e.Node.Depth == 0)
            {
                PopulateCategories(e.Node);
            }
            else if (e.Node.Depth > 0)
            {
                PopulateSubCategories(e.Node);
            }
            else
            {
                InfoDiv.Visible = true;
                InfoLiteral.Text = "خطا-45218.";
                InfoDiv.Attributes["class"] = " alert alert-Danger alert-dismissable text-right";
            }
        }
    }
    void PopulateCategories(TreeNode node)
    {
        SqlCommand sqlQuery = new SqlCommand(
            "Select name, nodeid From tree where groupname=N'" + Cat1DropDownList.SelectedValue + "' and parentid=0");
        DataSet resultSet;
        resultSet = RunQuery(sqlQuery);
        if (resultSet.Tables.Count > 0)
        {
            foreach (DataRow row in resultSet.Tables[0].Rows)
            {
                TreeNode NewNode = new
                    TreeNode(row["name"].ToString(), row["nodeid"].ToString());

                NewNode.PopulateOnDemand = true;
                NewNode.SelectAction = TreeNodeSelectAction.Select;
                node.ChildNodes.Add(NewNode);
            }
            //TreeNode AddNewNode = new TreeNode("افزودن آیتم جدید", "0");
            //AddNewNode.PopulateOnDemand = false;
            //AddNewNode.SelectAction = TreeNodeSelectAction.None;
            //node.ChildNodes.Add(AddNewNode);
        }
    }
    void PopulateSubCategories(TreeNode node)
    {
        SqlCommand sqlQuery = new SqlCommand();
        sqlQuery.CommandText = "Select name,nodeid From tree  Where parentid= @nodeid";
        sqlQuery.Parameters.Add("@nodeid", SqlDbType.Int).Value = node.Value;
        DataSet ResultSet = RunQuery(sqlQuery);
        if (ResultSet.Tables.Count > 0)
        {
            foreach (DataRow row in ResultSet.Tables[0].Rows)
            {
                TreeNode NewNode = new
                    TreeNode(row["name"].ToString(), row["nodeid"].ToString());
                NewNode.PopulateOnDemand = true;
                NewNode.SelectAction = TreeNodeSelectAction.Select;
                node.ChildNodes.Add(NewNode);
            }
            //TreeNode AddNewNode = new TreeNode("افزودن آیتم جدید", node.Value);
            //AddNewNode.PopulateOnDemand = false;
            //AddNewNode.SelectAction = TreeNodeSelectAction.None;
            //node.ChildNodes.Add(AddNewNode);
        }
    }
    private DataSet RunQuery(SqlCommand sqlQuery)
    {

        SqlConnection DBConnection =
            new SqlConnection(DataLayer.connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;
        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return resultsDataSet;
    }

    protected void CatTreeView_SelectedNodeChanged(object sender, EventArgs e)
    {


        CatTreeView.Nodes[0].Text = CatTreeView.SelectedNode.Text;
        CatTreeView.Nodes[0].Value = CatTreeView.SelectedValue;


    }
    protected void SelectNodeByValue(TreeNodeCollection Nodes, string ValueToSelect)
    {
        foreach (TreeNode n in Nodes)
        {
            if (n.Value == ValueToSelect)
            {
                //NodeLabel.Text += "<br/>" + n.Value;
                n.Selected = true;
                CatTreeView.Nodes[0].Text = n.Text;
                CatTreeView.Nodes[0].Value = n.Value;
                break;
            }

            else if (n.ChildNodes.Count > 0)
                SelectNodeByValue(n.ChildNodes, ValueToSelect);
            else
            {
                //NodeLabel.Text += "<br/>" + "fuck";

            }
        }
    }
    protected void CheckedNodeByValue(TreeNodeCollection Nodes, string ValueToSelect)
    {
        foreach (TreeNode n in Nodes)
        {
            if (n.Value == ValueToSelect)
            {
                //NodeLabel.Text += "<br/>" + n.Value;
                n.Checked = true;
            }

            else if (n.ChildNodes.Count > 0)
                CheckedNodeByValue(n.ChildNodes, ValueToSelect);
            else
            {
                //NodeLabel.Text += "<br/>" + "fuck";

            }
        }
    }

    protected void GetCheckedNodesButton_Click(object sender, EventArgs e)
    {
        CatTreeView.Nodes.Clear();
        TreeNode NewNode = new TreeNode("لطفا انتخاب کنید", "null");
        NewNode.PopulateOnDemand = true;
        NewNode.SelectAction = TreeNodeSelectAction.Select;
        CatTreeView.Nodes.Add(NewNode);
        CatTreeView.DataBind();
    }

    protected void DeleteCIDButton_Click(object sender, EventArgs e)
    {
        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);


        string DefineCourseSqlSyntax = " Update  content set isdeleted=1 where cid=@cid ";

        SqlCommand DefineCourseCmd = new SqlCommand(DefineCourseSqlSyntax, con);
        SqlParameter[] param = new SqlParameter[1];

        param[00] = new SqlParameter("@cid", SqlDbType.Int);

        param[00].Value = CIDTextBox.Text;

        for (int i = 0; i < param.Length; i++)
        {
            DefineCourseCmd.Parameters.Add(param[i]);
        }

        try
        {
            con.Open();
            DefineCourseCmd.ExecuteNonQuery();
            CIDTextBox.Text = "";
        }
        catch (System.Data.SqlClient.SqlException ex)
        {
            string msg = "deletecontent Error:";
            msg += ex.Message;
            throw new Exception(msg);
        }
        finally
        {
            con.Close();
            ContentGridView.DataBind();

            InfoDiv.Visible = true;
            InfoLiteral.Text = "مطلب با موفقیت حذف شد.";
            InfoDiv.Attributes["class"] = " alert alert-success alert-dismissable text-right";

            //Page.Response.Redirect(HttpContext.Current.Request.Url.ToString(), true);
        }
    }

    protected void EditContentButton_Click(object sender, EventArgs e)
    {
        InfoDiv.Visible = true;
        InfoLiteral.Text = "لطفا تغییرات لازم را انجام داده و روی دکمه ذخیره تغییرات کلیک کنید. در صورت انتخاب تصویر، تصویر جدید جایگزین تصویر قبل می شود.";
        InfoDiv.Attributes["class"] = " alert alert-warning alert-dismissable text-right";
        SubmitNewsButton.Visible = true;
        SubmitNewsButton.Text = "ذخیره تغییرات";
        SubmitNewsButton.CssClass = "btn btn-success btn-block";


        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);


        string FetchCourseSqlSyntax = "SELECT * FROM [content] WHERE ([cid] = @cid);SELECT tag.tid,tagname FROM [contenttag] inner join tag on contenttag.tid=tag.tid WHERE [cid] = @cid and contenttag.isdeleted=0 ;SELECT nodeid FROM [contentcat] WHERE ([cid] = @cid and isdeleted=0 );SELECT keyword FROM [contentkeyword] WHERE ([cid] = @cid)";
        SqlCommand FetchCourseCmd = new SqlCommand(FetchCourseSqlSyntax, con);
        SqlParameter[] FetchCourseParam = new SqlParameter[1];

        FetchCourseParam[0] = new SqlParameter("@cid", SqlDbType.NVarChar);
        FetchCourseParam[0].Value = CIDTextBox.Text;

        for (int i = 0; i < FetchCourseParam.Length; i++)
        {
            FetchCourseCmd.Parameters.Add(FetchCourseParam[i]);
        }

        SqlDataReader FetchDataDr;
        try
        {
            con.Open();
            FetchDataDr = FetchCourseCmd.ExecuteReader();

            if (FetchDataDr.Read())
            {
                CatTreeView.DataBind();
                SelectNodeByValue(CatTreeView.Nodes, FetchDataDr["cat2"].ToString());

                CIDTextBox.Text = FetchDataDr["CID"].ToString();
                CKTempTextBox.Text = FetchDataDr["Body"].ToString();
                HeaderUpTextBox.Text = FetchDataDr["HeaderUp"].ToString();
                HeaderTextBox.Text = FetchDataDr["Header"].ToString();
                HeaderDownTextBox.Text = FetchDataDr["HeaderDown"].ToString();
                ResourceTextBox.Text = FetchDataDr["Resource"].ToString();
                SummaryTextBox.Text = FetchDataDr["Summary"].ToString();
                SummaryVizCheckBox.Checked = Convert.ToBoolean(FetchDataDr["SummaryViz"].ToString());
                // PageViewTextBox.Text = FetchDataDr["PageView"].ToString();
                // PageViewVizTextBox.Text = FetchDataDr["PageViewViz"].ToString();
                PhotoTextBox.Text = FetchDataDr["Photo"].ToString();
                PhotoVizCheckBox.Checked = Convert.ToBoolean(FetchDataDr["Photoviz"].ToString());
                //Cat1DropDownList.SelectedValue= FetchDataDr["MainCatID"].ToString();
                //SituTextBox.Text = FetchDataDr["Situ"].ToString();
                //LanTextBox.Text = FetchDataDr["Lan"].ToString();
                //UpdateDateTextBox.Text = FetchDataDr["UpdateDate"].ToString();
                //CDateVizTextBox.Text = FetchDataDr["CDateViz"].ToString();
                //CDateTextBox.Text = FetchDataDr["CDate"].ToString();



            }
            KeywordTextBox.Text = "";
            InsertedTagTextBox.Text = "";

            FetchDataDr.NextResult();
            while (FetchDataDr.Read())
            {
                InsertedTagTextBox.Text += FetchDataDr["tid"].ToString() + "-" + FetchDataDr["tagname"].ToString() + ",";
            }
            FetchDataDr.NextResult();
            while (FetchDataDr.Read())
            {
                CheckedNodeByValue(CatTreeView.Nodes, FetchDataDr["nodeid"].ToString());
            }
            FetchDataDr.NextResult();
            while (FetchDataDr.Read())
            {
                KeywordTextBox.Text += FetchDataDr["keyword"].ToString() + "-";
            }

        }


        catch (System.Data.SqlClient.SqlException ex)
        {
            string msg = "FetchCourseCmd Error:";
            msg += ex.Message;
            throw new Exception(msg);
        }

        finally
        {
            con.Close();
        }
    }

    protected void UpdateContentGridViewButton_Click(object sender, EventArgs e)
    {
        ContentGridView.DataBind();
    }
}