﻿<%@ Page Title="" Language="C#" MasterPageFile="~/farsi/faMasterPage.master" AutoEventWireup="true" CodeFile="Category.aspx.cs" Inherits="Farsi_Blog_Category" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContentPlaceHolder" runat="Server">
    <style>
        /* Large desktops and laptops */
        @media (min-width: 1200px) {
            .blog-cover {
                background-position: center;
                background-size: cover;
                border: 1px solid #dcdcdc;
                height: 250px;
                background-repeat: no-repeat;
            }

            .blog-acover {
                display: block;
                width: 100%;
                height: 250px;
            }
        }

        /* Landscape tablets and medium desktops */
        @media (min-width: 992px) and (max-width: 1199px) {
            .blog-cover {
                background-position: center;
                background-size: cover;
                border: 1px solid #dcdcdc;
                height: 250px;
                background-repeat: no-repeat;
            }

            .blog-acover {
                display: block;
                width: 100%;
                height: 250px;
            }

            .blog-hr {
                width: 320px;
                float: right;
            }
        }

        /* Portrait tablets and small desktops */
        @media (min-width: 768px) and (max-width: 991px) {
            .blog-cover {
                background-position: center;
                background-size: cover;
                height: 250px;
                background-repeat: no-repeat;
            }

            .blog-acover {
                display: block;
                width: 100%;
                height: 250px;
            }

            .blog-hr {
                width: 320px;
                float: right;
            }
        }

        /* Landscape phones and portrait tablets */
        @media (max-width: 767px) {
            .blog-cover {
                background-position: center;
                background-size: contain;
                height: 400px;
                background-repeat: no-repeat;
            }

            .blog-acover {
                display: block;
                width: 100%;
                height: 400px;
            }

            .blog-hr {
                width: 100%;
            }
        }

        /* Portrait phones and smaller */
        @media (max-width: 480px) {
        }

        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContentPlaceHolder" runat="Server">

    <div class="container-fluid">
        <div class="container">
            <asp:Label ID="Cat1Label" Visible="false" runat="server" Text="Label"></asp:Label>
            <asp:Label ID="Cat2Label" Visible="false" runat="server" Text="Label"></asp:Label>
            <asp:Label ID="Cat1LatinLabel" Visible="false" runat="server" Text="Label"></asp:Label>
            <asp:Repeater runat="server" DataSourceID="Top9SqlDataSource" ID="Top9Repeater">
                <ItemTemplate>
                    <div class="row">
                        <div class="col-sm-2">
                            <a  href="<%=SSClass.WebDomain %>/<%#MyFunc.SafeString(Eval("cat2title"))%>/<%#MyFunc.SafeString(Eval("header"))%>/<%#Eval("cid") %>">
                                <img class="zoom" src="<%#Eval("Photo") %>" alt="<%#Eval("header") %>" title="<%#Eval("header") %>">
                            </a>
                        </div>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-sm-6">
                                    <span style="font-size: 10px"><%#DisplayDate(Eval("cdate"))+" | " %></span>
                                    <a href="<%#SSClass.WebDomain %>/<%#Eval("lan") %>/blog/category/<%#Eval("cat2") %>" style="color: #3ba8c4; font-size: 10px"><%#Eval("cat2") %></a>
                                </div>
                                <div class="col-sm-6 text-left" style="color: #3ba8c4">
                                    <span><%#Eval("pageview") %></span>
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </div>
                                <div class="col-sm-12">
                                    <a style="text-decoration: none; color: inherit"  href="<%=SSClass.WebDomain %>/<%#MyFunc.SafeString(Eval("cat2title"))%>/<%#MyFunc.SafeString(Eval("header"))%>/<%#Eval("cid") %>">
                                        <h2 style="font-size: 22px;"><%#Eval("Header").ToString().Replace("\n","<br />") %></h2>
                                    </a>
                                </div>
                                <div class="col-sm-12">
                                    <p class="help-block"><%#Eval("summary") %></p>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <hr />
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            <asp:SqlDataSource runat="server" ID="Top9SqlDataSource" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT TOP (50) dbo.[Content].CID, dbo.[Content].Body, dbo.[Content].HeaderUp, dbo.[Content].Header, dbo.[Content].HeaderDown, dbo.[Content].Resource, dbo.[Content].Summary, dbo.[Content].SummaryViz, dbo.[Content].PageView, dbo.[Content].Photo, dbo.[Content].Photoviz, dbo.[Content].Cat1, dbo.[Content].Situ, dbo.[Content].Lan, dbo.[Content].UpdateDate, dbo.[Content].CDate, dbo.ContentCat.Cat2, dbo.Tree.Name AS cat2title FROM dbo.[Content] INNER JOIN dbo.ContentCat ON dbo.[Content].CID = dbo.ContentCat.CID INNER JOIN dbo.Tree ON dbo.[Content].CID = dbo.Tree.NodeID WHERE (dbo.[Content].Cat1 = N'fa-blog') AND (dbo.ContentCat.Cat2 = @cat2) ORDER BY dbo.[Content].CDate DESC">
                <SelectParameters>
                    <asp:ControlParameter ControlID="Cat2Label" PropertyName="Text" Name="cat2" Type="String"></asp:ControlParameter>
                </SelectParameters>
            </asp:SqlDataSource>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContentPlaceHolder" runat="Server">
</asp:Content>

