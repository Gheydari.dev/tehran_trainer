﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Farsi/Admin/FaAdminMasterPage.master" AutoEventWireup="true" CodeFile="FileManager.aspx.cs" Inherits="Farsi_Admin_FileManager" %>

<%-- Add content controls here --%>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div class="row">
        <div class="col-md-12">
            <div runat="server" id="InfoDiv" class="alert alert-info alert-dismissable text-right" visible="true">
                <asp:Literal ID="InfoLiteral" runat="server" Text="در این قسمت می توانید فایل های مورد نظر خود را بارگذاری کرده و از لینک آنها در مطالب وبسایت استفاده کنید."></asp:Literal>
            </div>
        </div>
    </div>

    <div class="row GRayDiv">

        <div class="col-md-2">
            <asp:Label ID="Label3" runat="server" Text="بارگذاری فایل جدید:"></asp:Label>
        </div>
        <div class="col-md-3">
            <asp:FileUpload ID="DocumentsFileUpload" runat="server" Width="100%" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="انتخاب فایل الزامیست!" ControlToValidate="DocumentsFileUpload" Display="Dynamic" ForeColor="Red">انتخاب فایل الزامیست!</asp:RequiredFieldValidator>
        </div>
        <div class="col-md-1">
            <asp:Button CssClass="btn btn-xs btn-primary" ID="SubmitButton" runat="server" Text="بارگذاری" OnClick="SubmitButton_Click" />
        </div>

        <div class="col-md-6">
            <asp:Label ID="Label4" runat="server" Text="قالب های مجاز: jpg, jpeg, png, gif, pdf, doc, docx, xls, xlsx, ppt, pptx, zip, rar, swf" ForeColor="Red"></asp:Label>
        </div>
    </div>
    <div class="row ">
        <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto">
            <div class="col-md-12 text-right">
                <asp:GridView AllowPaging="true" PageSize="50" OnRowCommand="FileMngGridView_RowCommand" Font-Size="Small" ShowHeaderWhenEmpty="true" ID="FileMngGridView" CssClass="text-center table table-bordered table-condensed" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataSourceID="UsersSqlDataSource" ForeColor="Black" GridLines="Horizontal" Width="100%" AllowSorting="True">
                    <Columns>
                        <asp:TemplateField HeaderText="ر">
                            <HeaderStyle CssClass="text-center" Font-Bold="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="نام فایل" SortExpression="fpath">
                            <ItemTemplate>
                                <div dir="ltr">
                                    <asp:Label ID="Label121" runat="server" Text='<%# GName( Eval("fpath")) %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="text-center" Font-Bold="False"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:BoundField DataField="FSize" HeaderText="حجم فایل" SortExpression="FSize">
                            <HeaderStyle CssClass="text-center" Font-Bold="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="تاریخ بارگذاری" SortExpression="CDate">
                            <ItemTemplate>
                                <asp:Label ID="Label134" runat="server" Text='<%# DisplayDate( Eval("CDate")) %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="text-center" Font-Bold="False"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="دریافت لینک فایل" SortExpression="fpath">
                            <ItemTemplate>
                                <div dir="ltr">
                                    <asp:Label ID="Label156" runat="server" Text='<%# GLink( Eval("fpath")) %>'></asp:Label>
                                    <hr class="HrDivider" />
                                    <asp:Label ID="Label1" runat="server" Text='<%# GLink2( Eval("fpath")) %>'></asp:Label>
                                    <hr class="HrDivider" />
                                    <asp:TextBox CssClass="form-control" ID="ImgLinkTextBox" runat="server" Text='<%# GetImgLink( Eval("fpath")) %>'></asp:TextBox>
                                </div>
                            </ItemTemplate>
                            <HeaderStyle CssClass="text-center" Font-Bold="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="حذف" SortExpression="CDate">
                            <ItemTemplate>
                                <asp:LinkButton ValidationGroup="delete" CommandArgument='<%#Eval("fpath").ToString().ToLower() %>' CommandName="deletefile" ID="DeleteButton" CssClass="btn btn-danger btn-xs" runat="server" ToolTip="حذف"><span class="glyphicon glyphicon-remove"></span></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="text-center" Font-Bold="False"></HeaderStyle>
                        </asp:TemplateField>

                        <%--<asp:TemplateField HeaderText="گزینه ها">
                        <HeaderStyle CssClass="text-center" Font-Bold="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                        <ItemTemplate>
                            <div class="btn-group  text-right">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                    <span class="glyphicon glyphicon-align-justify">                                        
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right dropdown-menu-right text-right" role="menu">
                                    <li>
                                        <a href="UserDetails.aspx?NationalCode=<%#Eval("kodemelli") %>&userid=<%#Eval("userid") %>"><span class='<%#IsOnline(Eval("kodemelli")) %>'></span>&nbsp پروفایل</a>
                                    </li>
                                    <li>
                                        <a href="ResultUser.aspx?NationalCode=<%#Eval("kodemelli") %>&userid=<%#Eval("userid") %>"><span class="glyphicon glyphicon-book"></span>&nbsp نتایج</a>
                                    </li>
                                    <li>
                                        <a href="UserReg.aspx?UserID=<%#Eval("UserID") %>"><span class="glyphicon glyphicon-edit"></span>&nbsp ویرایش</a>
                                    </li>
                                </ul>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    </Columns>
                    <EmptyDataTemplate>
                        <asp:Label Font-Size="Small" ID="Label2" runat="server" Text="در حال حاضر اطلاعاتی جهت نمایش وجود ندارد."></asp:Label>
                    </EmptyDataTemplate>
                    <AlternatingRowStyle BackColor="#efefef" />
                    <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                    <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                    <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                    <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                    <SortedDescendingHeaderStyle BackColor="#242121" />
                </asp:GridView>
                <asp:SqlDataSource ID="UsersSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DrB_DBConnectionString %>" SelectCommand="SELECT * FROM [FileMng] order by cdate desc"></asp:SqlDataSource>
            </div>
        </asp:Panel>
    </div>
</asp:Content>

