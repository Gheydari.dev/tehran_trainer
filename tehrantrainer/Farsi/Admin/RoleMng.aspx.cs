﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Farsi_Admin_RoleMng : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        System.Web.UI.HtmlControls.HtmlGenericControl li = (System.Web.UI.HtmlControls.HtmlGenericControl)this.Page.Master.Master.FindControl("BodyContentPlaceHolder").FindControl("AdminProfile");

        li.Attributes["class"] = "active";
    }

    protected void AddSelectedRolesButton_Click(object sender, EventArgs e)
    {
        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);

        string sc = "";
        string Color = string.Empty;
        string ColorCode = string.Empty;
        //loop the ColorsRepeater Items
        for (int i = 0; i < AdminsRepeater.Items.Count; i++)
        {
            CheckBox AdminSelectCheckBox = (CheckBox)AdminsRepeater.Items[i].FindControl("AdminSelectCheckBox");
            if (AdminSelectCheckBox != null)
            {
                if (AdminSelectCheckBox.Checked)
                {
                    Label AdminIDLabel = (Label)AdminsRepeater.Items[i].FindControl("AdminIDLabel");

                    for (int j = 0; j < RolesRepeater.Items.Count; j++)
                    {
                        CheckBox RoleSelectCheckBox = (CheckBox)RolesRepeater.Items[j].FindControl("RoleSelectCheckBox");
                        if (RoleSelectCheckBox != null)
                        {
                            if (RoleSelectCheckBox.Checked)
                            {
                                Label RoleIDLabel = (Label)RolesRepeater.Items[j].FindControl("RoleIDLabel");

                                sc += @" if not exists (select * from rm_adminroles where RoleID=" + RoleIDLabel.Text + " and AdminID=" + AdminIDLabel.Text + @" )
                                        insert into rm_adminroles (roleid,adminid,situ) values (" + RoleIDLabel.Text + "," + AdminIDLabel.Text + ",N'ok') ";

                            }
                        }
                    }


                }
            }
        }
        if (sc != "")
        {


            string SQLQuerry = sc;

            SqlCommand RegCmd = new SqlCommand(SQLQuerry, con);


            try
            {
                con.Open();
                RegCmd.ExecuteScalar();
                AdminsRepeater.DataBind();
                RolesRepeater.DataBind();
                InfoDiv.Visible = true;
                InfoLiteral.Text = "عملیات با موفقیت انجام شد.";
                InfoDiv.Attributes["class"] = " alert alert-success alert-dismissable text-right";

            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                string msg = "RegCmd Error:";
                msg += ex.Message;
                throw new Exception(msg);
            }
            finally
            {
                con.Close();
            }
        }
        else
        {
            InfoDiv.Visible = true;
            InfoLiteral.Text = "شما هیچ نقش یا کاربری را انتخاب نکرده اید!";
            InfoDiv.Attributes["class"] = " alert alert-warning alert-dismissable text-right";
        }
    }

    protected void AddSelectedPagesButton_Click(object sender, EventArgs e)
    {
        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);

        string sc = "";
        string Color = string.Empty;
        string ColorCode = string.Empty;
        //loop the ColorsRepeater Items
        for (int k = 0; k < RolesRepeater.Items.Count; k++)
        {
            CheckBox RoleSelectCheckBox = (CheckBox)RolesRepeater.Items[k].FindControl("RoleSelectCheckBox");
            if (RoleSelectCheckBox != null)
            {
                if (RoleSelectCheckBox.Checked)
                {
                    Label RoleIDLabel = (Label)RolesRepeater.Items[k].FindControl("RoleIDLabel");
                    for (int i = 0; i < PagesRepeater.Items.Count; i++)
                    {
                        CheckBox PageSelectCheckBox = (CheckBox)PagesRepeater.Items[i].FindControl("PageSelectCheckBox");
                        if (PageSelectCheckBox != null)
                        {
                            if (PageSelectCheckBox.Checked)
                            {
                                Label PageIDLabel = (Label)PagesRepeater.Items[i].FindControl("PageIDLabel");
                                sc += @" if not exists (select * from rm_rolePages where RoleID=" + RoleIDLabel.Text + " and PageID=" + PageIDLabel.Text + @" )
                                        insert into rm_rolePages (roleid,PageID,situ) values (" + RoleIDLabel.Text + "," + PageIDLabel.Text + ",N'ok') ";

                                Repeater ControlsRepeater = (Repeater)PagesRepeater.Items[i].FindControl("ControlsRepeater");

                                for (int j = 0; j < ControlsRepeater.Items.Count; j++)
                                {
                                    CheckBox ControlSelectCheckBox = (CheckBox)ControlsRepeater.Items[j].FindControl("ControlSelectCheckBox");
                                    if (ControlSelectCheckBox != null)
                                    {
                                        if (ControlSelectCheckBox.Checked)
                                        {
                                            Label ControlIDLabel = (Label)ControlsRepeater.Items[j].FindControl("ControlIDLabel");

                                            sc += @" if not exists (select * from rm_roleControls where RoleID=" + RoleIDLabel.Text + " and ControlID=" + ControlIDLabel.Text + @" )
                                                     insert into rm_roleControls (roleid,ControlID,click,visible,situ) values (" + RoleIDLabel.Text + "," + ControlIDLabel.Text + ",N'True',N'True',N'ok') ";

                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            }
        }
        if (sc != "")
        {


            string SQLQuerry = sc;

            SqlCommand RegCmd = new SqlCommand(SQLQuerry, con);


            try
            {
                con.Open();
                RegCmd.ExecuteScalar();
                PagesRepeater.DataBind();
                RolesRepeater.DataBind();
                InfoDiv.Visible = true;
                InfoLiteral.Text = "عملیات با موفقیت انجام شد.";
                InfoDiv.Attributes["class"] = " alert alert-success alert-dismissable text-right";

            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                string msg = "RegCmd Error:";
                msg += ex.Message;
                throw new Exception(msg);
            }
            finally
            {
                con.Close();
            }
        }
        else
        {
            InfoDiv.Visible = true;
            InfoLiteral.Text = "شما هیچ نقش، کنترل و یا صفحه ای را انتخاب نکرده اید!";
            InfoDiv.Attributes["class"] = " alert alert-warning alert-dismissable text-right";
        }
    }

    protected void AdminsRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "ChangePassword")
        {
            TextBox NewPassTextBox = (TextBox)e.Item.FindControl("NewPassTextBox");
            TextBox RepeatNewPassTextBox = (TextBox)e.Item.FindControl("RepeatNewPassTextBox");
            if (NewPassTextBox.Text != "" && RepeatNewPassTextBox.Text != "")
            {
                string NewPass = NewPassTextBox.Text;
                DataLayer dl = new DataLayer();
                string CS = dl.dataLayerConnectionString;
                SqlConnection con = new SqlConnection(CS);
                string UpdatePassSqlSyntax = "update rm_admins set password=@password where adminid=@adminid";
                SqlParameter[] param2 = new SqlParameter[2];

                param2[0] = new SqlParameter("@password", SqlDbType.NVarChar);
                param2[1] = new SqlParameter("@adminid", SqlDbType.BigInt);

                param2[0].Value = NewPass;
                param2[1].Value = e.CommandArgument.ToString();

                SqlCommand UpdatePassCmd = new SqlCommand(UpdatePassSqlSyntax, con);
                UpdatePassCmd.Parameters.Add(param2[0]);
                UpdatePassCmd.Parameters.Add(param2[1]);

                try
                {
                    con.Open();
                    UpdatePassCmd.ExecuteNonQuery();
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    string msg = "UpdatePassCmd Error:";
                    msg += ex.Message;
                    throw new Exception(msg);
                }
                finally
                {
                    con.Close();
                    InfoDiv.Visible = true;
                    InfoLiteral.Text = "کلمه عبور کاربر با موفقیت تغییر یافت.";
                    InfoDiv.Attributes["class"] = " alert alert-success alert-dismissable text-right";

                }
            }
        }
        else if (e.CommandName == "DeleteAdminRoles")
        {
            Repeater AdminRolesRepeater = (Repeater)e.Item.FindControl("AdminRolesRepeater");

            DataLayer dl = new DataLayer();
            string CS = dl.dataLayerConnectionString;
            SqlConnection con = new SqlConnection(CS);

            string sc = "";
            string Color = string.Empty;
            string ColorCode = string.Empty;
            //loop the ColorsRepeater Items
            for (int i = 0; i < AdminRolesRepeater.Items.Count; i++)
            {
                CheckBox AdminRoleSelectCheckBox = (CheckBox)AdminRolesRepeater.Items[i].FindControl("AdminRoleSelectCheckBox");
                if (AdminRoleSelectCheckBox != null)
                {
                    if (AdminRoleSelectCheckBox.Checked)
                    {
                        Label URIDLabel = (Label)AdminRolesRepeater.Items[i].FindControl("URIDLabel");

                        sc += string.Format(@" delete from rm_adminroles where URID={0} ", URIDLabel.Text);

                    }
                }
            }
            if (sc != "")
            {


                string SQLQuerry = sc;

                SqlCommand RegCmd = new SqlCommand(SQLQuerry, con);


                try
                {
                    con.Open();
                    RegCmd.ExecuteScalar();
                    AdminRolesRepeater.DataBind();
                    RolesRepeater.DataBind();
                    InfoDiv.Visible = true;
                    InfoLiteral.Text = "عملیات با موفقیت انجام شد.";
                    InfoDiv.Attributes["class"] = " alert alert-success alert-dismissable text-right";

                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    string msg = "RegCmd Error:";
                    msg += ex.Message;
                    throw new Exception(msg);
                }
                finally
                {
                    con.Close();
                }
            }
            else
            {
                InfoDiv.Visible = true;
                InfoLiteral.Text = "شما هیچ نقشی را انتخاب نکرده اید!";
                InfoDiv.Attributes["class"] = " alert alert-warning alert-dismissable text-right";
            }
        }

    }

    protected void RolesRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "DeleteRoleAdmins")
        {
            Repeater RoleAdminsRepeater = (Repeater)e.Item.FindControl("RoleAdminsRepeater");

            DataLayer dl = new DataLayer();
            string CS = dl.dataLayerConnectionString;
            SqlConnection con = new SqlConnection(CS);

            string sc = "";
            string Color = string.Empty;
            string ColorCode = string.Empty;
            //loop the ColorsRepeater Items
            for (int i = 0; i < RoleAdminsRepeater.Items.Count; i++)
            {
                CheckBox RoleSelectCheckBox = (CheckBox)RoleAdminsRepeater.Items[i].FindControl("RoleSelectCheckBox");
                if (RoleSelectCheckBox != null)
                {
                    if (RoleSelectCheckBox.Checked)
                    {
                        Label URIDLabel = (Label)RoleAdminsRepeater.Items[i].FindControl("URIDLabel");

                        sc += string.Format(@" delete from RM_AdminRoles where URID={0} ", URIDLabel.Text);

                    }
                }
            }
            if (sc != "")
            {


                string SQLQuerry = sc;

                SqlCommand RegCmd = new SqlCommand(SQLQuerry, con);


                try
                {
                    con.Open();
                    RegCmd.ExecuteScalar();
                    RoleAdminsRepeater.DataBind();
                    RolesRepeater.DataBind();
                    InfoDiv.Visible = true;
                    InfoLiteral.Text = "عملیات با موفقیت انجام شد.";
                    InfoDiv.Attributes["class"] = " alert alert-success alert-dismissable text-right";

                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    string msg = "RegCmd Error:";
                    msg += ex.Message;
                    throw new Exception(msg);
                }
                finally
                {
                    con.Close();
                }
            }
            else
            {
                InfoDiv.Visible = true;
                InfoLiteral.Text = "شما هیچ کاربری را انتخاب نکرده اید!";
                InfoDiv.Attributes["class"] = " alert alert-warning alert-dismissable text-right";
            }
        }
        else if (e.CommandName == "DeleteRoleAccesses")
        {
            DataLayer dl = new DataLayer();
            string CS = dl.dataLayerConnectionString;
            SqlConnection con = new SqlConnection(CS);

            string sc = "";

            Repeater RolePagesRepeater = (Repeater)e.Item.FindControl("RolePagesRepeater");
            Label PageIDLabel = (Label)e.Item.FindControl("PageIDLabel");
            Label RoleIDLabel = (Label)e.Item.FindControl("RoleIDLabel");
            Label PageNameLabel = (Label)e.Item.FindControl("PageNameLabel");




            for (int i = 0; i < RolePagesRepeater.Items.Count; i++)
            {
                CheckBox PageSelectCheckBox = (CheckBox)e.Item.FindControl("PageSelectCheckBox");

                if (PageSelectCheckBox != null)
                {
                    if (PageSelectCheckBox.Checked)
                    {
                        sc += string.Format(@" delete from rm_rolepages where pageid={0} and roleid={1} delete from rm_rolecontrols where roleid={1} and controlid in (select controlid from rm_controls where PageName={2})  ", PageIDLabel.Text, RoleIDLabel.Text, PageNameLabel.Text);
                    }
                }
                Repeater ControlsRepeater = (Repeater)RolePagesRepeater.Items[i].FindControl("ControlsRepeater");
                for (int j = 0; j < ControlsRepeater.Items.Count; j++)
                {
                    CheckBox ControlSelectCheckBox = (CheckBox)ControlsRepeater.Items[j].FindControl("ControlSelectCheckBox");
                    Label ControlIDLabel = (Label)ControlsRepeater.Items[j].FindControl("ControlIDLabel");

                    if (ControlSelectCheckBox != null)
                    {
                        if (ControlSelectCheckBox.Checked)
                        {
                            sc += string.Format(" delete from rm_rolecontrols where roleid={0} and controlid={1} ", RoleIDLabel.Text, ControlIDLabel.Text);
                        }
                    }
                }

            }

            if (sc != "")
            {


                string SQLQuerry = sc;

                SqlCommand RegCmd = new SqlCommand(SQLQuerry, con);


                try
                {
                    con.Open();
                    RegCmd.ExecuteScalar();
                    RolesRepeater.DataBind();
                    InfoDiv.Visible = true;
                    InfoLiteral.Text = "عملیات با موفقیت انجام شد.";
                    InfoDiv.Attributes["class"] = " alert alert-success alert-dismissable text-right";

                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    string msg = "RegCmd Error:";
                    msg += ex.Message;
                    throw new Exception(msg);
                }
                finally
                {
                    con.Close();
                }
            }
            else
            {
                InfoDiv.Visible = true;
                InfoLiteral.Text = "شما هیچ کنترل و یا صفحه ای را انتخاب نکرده اید!";
                InfoDiv.Attributes["class"] = " alert alert-warning alert-dismissable text-right";
            }
        }
    }

    protected void AddNewAdminButton_Click(object sender, EventArgs e)
    {
        try
        {
            System.Drawing.Color NewColor = System.Drawing.ColorTranslator.FromHtml(ColorCodeTextBox.Text);

        }
        catch (Exception ss)
        {

            InfoDiv.Visible = true;
            InfoLiteral.Text = "خطا. کد رنگ وارد شده معتبر نمی باشد. " + ss.Message;
            InfoDiv.Attributes["class"] = " alert alert-danger alert-dismissable text-right";
        }
        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);

        string SQLQuerry = "INSERT INTO rm_admins (name,family,mobphone,password,colorcode) OUTPUT inserted.adminid  values (@name,@family,@mobphone,@password,@colorcode)";

        SqlCommand RegCmd = new SqlCommand(SQLQuerry, con);
        SqlParameter[] param = new SqlParameter[5];

        param[00] = new SqlParameter("@name", SqlDbType.NVarChar);
        param[01] = new SqlParameter("@family", SqlDbType.NVarChar);
        param[02] = new SqlParameter("@mobphone", SqlDbType.NVarChar);
        param[03] = new SqlParameter("@password", SqlDbType.NVarChar);
        param[04] = new SqlParameter("@colorcode", SqlDbType.NVarChar);

        param[00].Value = NameTextBox.Text;
        param[01].Value = FamilyTextBox.Text;
        param[02].Value = MobPhoneTextBox.Text;
        param[03].Value = PasswordTextBox.Text;
        param[04].Value = ColorCodeTextBox.Text;


        for (int r = 0; r < param.Length; r++)
        {
            RegCmd.Parameters.Add(param[r]);
        }

        try
        {

            con.Open();
            int AdminidInt = Convert.ToInt32(RegCmd.ExecuteScalar());
            AdminsRepeater.DataBind();
            InfoDiv.Visible = true;
            InfoLiteral.Text = "کاربر جدید با موفقیت افزوده شد.";
            InfoDiv.Attributes["class"] = " alert alert-success alert-dismissable text-right";
            if (AdminPhotoFileUpload.HasFile)
            {
                string PhotoPathString = "";
                PhotoPathString = SSClass.WebDomain + "~/images/AdminPhoto/" + AdminidInt + ".jpg";

                string fileExt =
                System.IO.Path.GetExtension(AdminPhotoFileUpload.FileName.ToLower());

                if (fileExt == ".jpg")
                {
                    long size = AdminPhotoFileUpload.PostedFile.ContentLength;
                    size = size / 1024; //to KB
                    if (size <= 300)
                    {
                        AdminPhotoFileUpload.SaveAs(Server.MapPath(PhotoPathString));
                    }
                }
            }


        }
        catch (System.Data.SqlClient.SqlException ex)
        {
            string msg = "RegCmd Error:";
            msg += ex.Message;
            throw new Exception(msg);
        }
        finally
        {
            con.Close();
        }
    }

    protected void AddNewRoleButton_Click(object sender, EventArgs e)
    {
        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);

        string SQLQuerry = "INSERT INTO RM_Roles (RoleName,situ) values (@RoleName,N'ok')";

        SqlCommand RegCmd = new SqlCommand(SQLQuerry, con);
        SqlParameter[] param = new SqlParameter[1];

        param[00] = new SqlParameter("@RoleName", SqlDbType.NVarChar);


        param[00].Value = RoleNameTextBox.Text;



        for (int r = 0; r < param.Length; r++)
        {
            RegCmd.Parameters.Add(param[r]);
        }

        try
        {
            con.Open();
            RegCmd.ExecuteScalar();
            RolesRepeater.DataBind();
            InfoDiv.Visible = true;
            InfoLiteral.Text = "نقش جدید با موفقیت افزوده شد.";
            InfoDiv.Attributes["class"] = " alert alert-success alert-dismissable text-right";
        }
        catch (System.Data.SqlClient.SqlException ex)
        {
            string msg = "RegCmd Error:";
            msg += ex.Message;
            throw new Exception(msg);
        }
        finally
        {
            con.Close();
        }
    }

    protected void SelectAllAccessesCheckBox_CheckedChanged(object sender, EventArgs e)
    {
        if (SelectAllAccessesCheckBox.Checked)
        {
            for (int i = 0; i < PagesRepeater.Items.Count; i++)
            {
                CheckBox PageSelectCheckBox = (CheckBox)PagesRepeater.Items[i].FindControl("PageSelectCheckBox");
                if (PageSelectCheckBox != null)
                {
                    PageSelectCheckBox.Checked = true;

                    Repeater ControlsRepeater = (Repeater)PagesRepeater.Items[i].FindControl("ControlsRepeater");

                    for (int j = 0; j < ControlsRepeater.Items.Count; j++)
                    {
                        CheckBox ControlSelectCheckBox = (CheckBox)ControlsRepeater.Items[j].FindControl("ControlSelectCheckBox");
                        if (ControlSelectCheckBox != null)
                        {
                            ControlSelectCheckBox.Checked = true;

                        }
                    }
                }
            }

        }
        else
        {
            for (int i = 0; i < PagesRepeater.Items.Count; i++)
            {
                CheckBox PageSelectCheckBox = (CheckBox)PagesRepeater.Items[i].FindControl("PageSelectCheckBox");
                if (PageSelectCheckBox != null)
                {
                    PageSelectCheckBox.Checked = false;

                    Repeater ControlsRepeater = (Repeater)PagesRepeater.Items[i].FindControl("ControlsRepeater");

                    for (int j = 0; j < ControlsRepeater.Items.Count; j++)
                    {
                        CheckBox ControlSelectCheckBox = (CheckBox)ControlsRepeater.Items[j].FindControl("ControlSelectCheckBox");
                        if (ControlSelectCheckBox != null)
                        {
                            ControlSelectCheckBox.Checked = false;

                        }
                    }
                }
            }
        }
    }
}