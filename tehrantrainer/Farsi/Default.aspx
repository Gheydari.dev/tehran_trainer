﻿<%@ Page Title="وبسایت موسسه تهران آموز" Language="C#" MasterPageFile="~/farsi/FaMasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Farsi_Default" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContentPlaceHolder" runat="Server">

    <link href="<%=SSClass.WebDomain %>/css/slick.css" rel="stylesheet" />
    <asp:Literal Text="" ID="CanonicalLiteral" runat="server" />
    <meta name="robots" content="index, follow" />
    <!-- ----------------- <- OPEN GRAPH SHARE TAGs -> ----------------- -->
    <meta property="og:type" content="website" />
    <meta property="og:image" content="<%=SSClass.WebDomainFile %>/Images/Design/main-logo.png" />
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="97" />
    <meta property="og:description" content="" />
    <meta property="og:url" content="<%=SSClass.WebDomain %>/" />
    <meta property="og:site_name" content="وبسایت موسسه تهران آموز" />

    <meta name="description" content="وبسایت موسسه تهران آموز ارایه دهنده ی خدمات مشاوره ای آموزشی و پژوهشی با تیمی مجرب از دانشگاه های تهران، شریف و امیرکبیر" />
    <meta name="keywords" content="وبسایت موسسه تهران آموز,پایان نامه,مقاله,آی اس آی,ISI,نگارش مقاله ISI,اکسپت و چاپ,تحلیل آماری,طراحی پرسشنامه,ترجمه تخصصی,خدمات مشاوره ای,آموزش,پژوهش,مدلسازی,شبیه سازش,پرژه های دانشجویی,چاپ کتاب" />


    <style>
        h2 {
            color: inherit;
            font-size: 27px;
            margin-bottom: 30px;
        }

        .PCDiv {
            padding: 17px;
            padding-bottom: 2px;
        }

        .PCDImageDiv {
            padding: 0px;
            padding-bottom: 15px;
        }

        .Shadow {
            padding-bottom: 15px;
            background-color: #ffffff;
            margin-bottom: -13px;
        }

        .PCName {
            padding-right: 5px;
            text-decoration: none;
            color: #707070;
        }

            .PCName > h2 {
                margin: 5px;
                color: #737373;
                font-size: 19px;
            }

            .PCName > span {
                margin: 5px;
                color: #989898;
            }

        .PCPrice {
            padding-left: 5px;
            text-decoration: none;
            color: #ff0046;
        }

        .PCDiscount {
            font-size: smaller;
            color: #707070;
        }


        .AskForm {
            padding: 15px;
        }

        .home-service {
        }

            .home-service > div {
                text-align: center;
                font-weight: 900;
                border: 3px solid white;
                font-size: 16px;
                padding: 30px 0px;
                margin: 10px 0px;
                white-space: nowrap
            }

            .home-service hr {
                margin: 15px 0px !important;
                padding: 10px 0px !important;
                border-top: 3px solid white
            }

        .main-header {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            padding: 0;
            z-index: 10;
            transition: all 0.5s ease-in-out !important;
            background-color: transparent;
            color: #ffffff;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            box-shadow: none;
        }

            .main-header.active {
                background: #fff;
                -webkit-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
                -moz-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
                box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
                color: #5f5f5f
            }

        .navbar-brand {
            display: -webkit-box;
        }

            .navbar-brand.active {
                display: inline-block;
            }

        .main-container {
            min-height: 700px;
            width: 100%;
            margin: 0px;
            margin-top: 0px
        }

        @-webkit-keyframes fadeIn {
            from {
                opacity: 0;
            }

            to {
                opacity: 1;
            }
        }

        @-moz-keyframes fadeIn {
            from {
                opacity: 0;
            }

            to {
                opacity: 1;
            }
        }

        @keyframes fadeIn {
            from {
                opacity: 0;
            }

            to {
                opacity: 1;
            }
        }

        .fade-in {
            opacity: 0;
            -webkit-animation: fadeIn ease-in 1;
            -moz-animation: fadeIn ease-in 1;
            animation: fadeIn ease-in 1;
            -webkit-animation-fill-mode: forwards;
            -moz-animation-fill-mode: forwards;
            animation-fill-mode: forwards;
            -webkit-animation-duration: 0.5s;
            -moz-animation-duration: 0.5s;
            animation-duration: 0.5s;
        }

            .fade-in.f1 {
                -webkit-animation-delay: 0.7s;
                -moz-animation-delay: 0.7s;
                animation-delay: 0.7s;
            }

            .fade-in.f2 {
                -webkit-animation-delay: 1.2s;
                -moz-animation-delay: 1.2s;
                animation-delay: 1.2s;
            }

            .fade-in.f3 {
                -webkit-animation-delay: 1.6s;
                -moz-animation-delay: 1.6s;
                animation-delay: 1.6s;
            }

            .fade-in.f4 {
                -webkit-animation-delay: 1.9s;
                -moz-animation-delay: 1.9s;
                animation-delay: 1.9s;
            }

        .InfoLabel {
            color: #ffffff;
            padding: 30px;
            display: block;
            font-weight: 900;
            font-size: 20px;
            text-shadow: 6px -9px 12px 8px black;
            text-shadow: 0 0 7px black;
        }

        .slick-prev:before, .slick-next:before {
            font-size: 65px !important;
            color: rgb(177, 177, 177) !important;
        }

        .slick-prev, .slick-next {
            height: 65px !important;
            width: 65px !important;
            z-index: 9 !important;
        }
    </style>
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="<%=SSClass.WebDomain %>/css/odometer-theme-default.css" />
    <link href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.7.1/slick/slick-theme.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="BodyContentPlaceHolder" runat="Server">
    <div class="container-fluid " style="background-image: url(<%=SSClass.WebDomain %>/images/ex/new-tehran-trainer.jpg); background-size: cover; background-position: center; background-repeat: no-repeat">
        <div class="container" style="margin-top: 100px; color: #fff; text-shadow: 1px 1px 4px black;">
            <div class="row ">
                <div class="col-sm-12 text-center">
                    <a href="<%=SSClass.WebDomain %>?ref=Header-Logo" style="display: inline-block; text-align: center; text-decoration: none" data-aos="zoom-in">
                        <img class="zoom" src="<%=SSClass.WebDomainFile %>/Images/Design/main-logo-white.png" title="<%=SSClass.FTitle+" | "+SSClass.ETitle %>" alt="<%=SSClass.FTitle+" | "+SSClass.ETitle %>" style="max-width: 150px!important; display: block" />
                    </a>
                    <div data-aos="fade-up">
                        <h1>نگارش مقاله</h1>
                        <h2 style="font-size: 25px">تهران آموز، همراه پیشرو</h2>
                        <strong style="display: block; margin-top: 30px">ارایه دهنده ی خدمات مشاوره ای آموزشی و پژوهشی با تیمی مجرب از دانشگاه های تهران، شریف و امیرکبیر</strong>
                    </div>
                </div>
            </div>
            <%--<div class="row fade-in f1" style="margin: 30px 15px" dir="ltr">
                <div class="col-md-2 col-sm-4 col-xs-6 home-service">
                    <div>
                        <span class="odometer odometer1 " style="font-size: 30px">000</span>
                        <hr />
                        <span dir="rtl">نگارش مقاله ISI</span>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 home-service">
                    <div>
                        <span class="odometer odometer2 " style="font-size: 30px">000</span>
                        <hr />
                        <span>اکسپت و چاپ</span>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 home-service">
                    <div>
                        <span class="odometer odometer3 " style="font-size: 30px">0</span>
                        <hr />
                        <span>پایان نامه</span>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 home-service">
                    <div>
                        <span class="odometer odometer4 " style="font-size: 30px">0</span>
                        <hr />
                        <span>تحلیل آماری</span>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 home-service">
                    <div>
                        <span class="odometer odometer5 " style="font-size: 30px">0</span>
                        <hr />
                        <span>طراحی پرسشنامه</span>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 home-service">
                    <div>
                        <span class="odometer odometer6 " style="font-size: 30px">000</span>
                        <hr />
                        <span>ترجمه تخصصی</span>
                    </div>
                </div>
            </div>--%>
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <div class="row " style="margin: 50px 15px; text-shadow: none" data-aos="zoom-out" data-toggle="validator" role="form">
                        <div class="col-sm-3 form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                                <asp:TextBox runat="server" placeholder="نام و نام خانوادگی" class="form-control input-lg" ID="QNameFamilyTextBox"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ValidationGroup="qr" ErrorMessage="نام و نام خانوادگی خود را وارد کنید" ControlToValidate="QNameFamilyTextBox" runat="server" />
                        </div>
                        <div class="col-sm-3 form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><span class="glyphicon glyphicon-book"></span></div>
                                <asp:TextBox runat="server" placeholder="رشته ی تحصیلی" class="form-control input-lg" ID="QFieldTextBox"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ValidationGroup="qr" ErrorMessage="رشته ی تحصیلی خود را وارد کنید" ControlToValidate="QFieldTextBox" runat="server" />
                        </div>
                        <div class="col-sm-3 form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <span class="fas fa-phone"></span>
                                </div>
                                <asp:TextBox dir="ltr" runat="server" placeholder="09..." class="form-control input-lg" ID="QMobTextBox"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ValidationGroup="qr" ErrorMessage="شماره تماس خود را وارد کنید" ControlToValidate="QMobTextBox" runat="server" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="QMobTextBox" Display="Dynamic" ErrorMessage="*" ForeColor="#ff9900" Font-Bold="true" Style="position: absolute; right: 2px; bottom: -20px" Text="شماره تلفن همراه یازده رقمی و با 09 آغاز می شود. نمونه صحیح: 09123456789" ValidationExpression="^09[0-9]{9}$" ValidationGroup="qr"></asp:RegularExpressionValidator>
                        </div>
                        <div class="col-sm-3 form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><span class="glyphicon glyphicon-star"></span></div>
                                <asp:TextBox runat="server" placeholder="کد تخفیف" class="form-control input-lg" ID="SCodeTextBox"></asp:TextBox>
                            </div>
                        </div>
                         <div class="col-sm-12 text-center form-group">
                            <asp:Label ID="InfoLabel" Text="" runat="server" CssClass="InfoLabel" Visible="false" />
                        </div>
                        <div class="col-sm-12 form-group">
                            <div class="col-sm-5 form-group">
                            </div>
                            <div class="col-sm-2 form-group">
                                <asp:Button ValidationGroup="qr" ID="QuickRegButton" Text="ثبت" runat="server" CssClass="btn btn-primary btn-block btn-lg" OnClick="QuickRegButton_Click" />
                            </div>
                            <div class="col-sm-5 form-group">
                            </div>
                        </div>
                       
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center" data-aos="fade-up">
                    <h2>دپارتمان پژوهش</h2>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <asp:Repeater ID="ServicesRepeater" runat="server" DataSourceID="ServicesSqlDataSource">
                            <ItemTemplate>
                                <div class="col-xs-<%#Eval("colxs") %> col-sm-<%#Eval("colsm") %> col-md-<%#Eval("colmd") %>" data-aos="zoom-out">
                                    <div class="trainer-services">
                                        <a href="<%#Eval("href") %>" class="link-image">
                                            <img class="img-center nice-shadow" src="<%#Eval("src") %>" alt="<%#Eval("Title") %>" title="<%#Eval("Title") %>" />
                                            <h2 style="height: 80px;"><%#Eval("Des") %></h2>
                                        </a>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                        <asp:SqlDataSource runat="server" ID="ServicesSqlDataSource" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT * FROM [sliderimages] where  sid=4 and situ=N'show' ORDER BY ordernum"></asp:SqlDataSource>
                    </div>
                </div>
                <div class="col-xs-12" style="margin-bottom: 20px">
                    <hr />
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-sm-12" data-aos="fade-in">
                    <h2>آخرین اخبار</h2>
                </div>
                <div class="col-sm-12">
                    <div class="row home-Content">
                        <asp:Repeater ID="LatestNewsRepeater" runat="server" DataSourceID="LatestNewsSqlDataSource">
                            <ItemTemplate>
                                <div class="col-sm-3 text-center" data-aos="zoom-in-up">
                                    <div>
                                        <div class="col-xs-12">
                                            <a  href="<%=SSClass.WebDomain %>/<%#MyFunc.SafeString(Eval("cat2title"))%>/<%#MyFunc.SafeString(Eval("header"))%>/<%#Eval("cid") %>">
                                                <img class="zoom" src='<%# Eval("Photo") %>' alt='<%#Eval("Header") %>' title='<%#Eval("Header") %>' style="max-width: 100%" />
                                            </a>
                                        </div>
                                        <div class="col-xs-12" style="margin-bottom: 15px">
                                            <a  href="<%=SSClass.WebDomain %>/<%#MyFunc.SafeString(Eval("cat2title"))%>/<%#MyFunc.SafeString(Eval("header"))%>/<%#Eval("cid") %>">
                                                <h3 class="NewsH"><%#Eval("Header") %></h3>
                                            </a>
                                            <p class="help-block text-justify small NewsP"><%#Eval("Summary")+"" %></p>
                                            <span class="label label-info small"><%# Eval("PageView  ")+" بازدید " %></span>
                                            <span style="color: #a7a7a7; font-weight: 100!important; margin-right: 5px" class="small"><%#MyFunc.DisplayDate(Eval("cdate")) %></span>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                        <asp:SqlDataSource runat="server" ID="LatestNewsSqlDataSource" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT top(20) Content.* , dbo.Tree.Name AS cat2title FROM dbo.[Content] INNER JOIN dbo.Tree ON dbo.[Content].cat2 = dbo.Tree.NodeID WHERE  cat1=N'Fa-News' and isdeleted=0 ORDER BY [Content].[CDate] DESC"></asp:SqlDataSource>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-sm-12" data-aos="fade-in">
                    <h2 style="display: inline-block">آخرین مقالات</h2>
                    <a class="pull-left" style="color: #3ba8c4; display: inline-block" href="<%=SSClass.WebDomain %>/blog">
                        <h2 style="font-size: 14px;">مقالات بیشتر...</h2>
                    </a>
                </div>
                <div class="col-sm-12">
                    <div class="row home-Content">
                        <asp:Repeater ID="LatestArticlesRepeater" runat="server" DataSourceID="LatestArticlesSqlDataSource">
                            <ItemTemplate>
                                <div class="col-sm-3 text-center" data-aos="zoom-in-up">
                                    <div>
                                        <div class="col-xs-12">
                                            <a href="<%=SSClass.WebDomain %>/<%#MyFunc.SafeString(Eval("cat2title"))%>/<%#MyFunc.SafeString(Eval("header"))%>/<%#Eval("cid") %>">
                                                <img class="zoom" src='<%# Eval("Photo") %>' alt='<%#Eval("Header") %>' title='<%#Eval("Header") %>' style="max-width: 100%" />
                                            </a>
                                        </div>
                                        <div class="col-xs-12" style="margin-bottom: 15px">
                                            <a  href="<%=SSClass.WebDomain %>/<%#MyFunc.SafeString(Eval("cat2title"))%>/<%#MyFunc.SafeString(Eval("header"))%>/<%#Eval("cid") %>">
                                                <h3 class="NewsH"><%#Eval("Header") %></h3>
                                            </a>
                                            <p class="help-block text-justify small NewsP"><%#Eval("Summary")+"" %></p>
                                            <span class="label label-info small"><%# Eval("PageView  ")+" بازدید " %></span>
                                            <span style="color: #a7a7a7; font-weight: 100!important; margin-right: 5px" class="small"><%#MyFunc.DisplayDate(Eval("cdate")) %></span>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                        <asp:SqlDataSource runat="server" ID="LatestArticlesSqlDataSource" ConnectionString='<%$ ConnectionStrings:DrB_DBConnectionString %>' SelectCommand="SELECT top(20) Content.* , dbo.Tree.Name AS cat2title FROM dbo.[Content] INNER JOIN dbo.Tree ON dbo.[Content].cat2 = dbo.Tree.NodeID WHERE  cat1=N'Fa-Blog' and isdeleted=0 ORDER BY [Content].[CDate] DESC"></asp:SqlDataSource>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="FooterContent" ContentPlaceHolderID="FooterContentPlaceHolder" runat="Server">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="https://github.hubspot.com/odometer/odometer.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

    <script type="text/javascript" src='<%= ResolveUrl(SSClass.WebDomainFile+"/js/jquery.sliderPro.min.js") %>'></script>
    <script type="text/javascript">
        $(document).ready(function ($) {
            setTimeout(function () {
                $('.odometer1').text('115');
            }, 1000);
            setTimeout(function () {
                $('.odometer2').text('620');
            }, 1500);
            setTimeout(function () {
                $('.odometer3').text('948');
            }, 2000);
            setTimeout(function () {
                $('.odometer4').text('383');
            }, 2500);
            setTimeout(function () {
                $('.odometer5').text('266');
            }, 3000);
            setTimeout(function () {
                $('.odometer6').text('1180');
            }, 3500);
            AOS.init();
            $('.home-Content').slick({
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 1,
                rtl: true,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 4,

                        }
                    },
                    {
                        breakpoint: 700,
                        settings: {
                            slidesToShow: 3,
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                        }
                    }]
            });
            $('#example2').sliderPro({
                keyboard: false,
                autoHeight: true,
                width: '100%',
                arrows: true,
                centerImage: true,
                fade: true,

            });

            $('#example5').sliderPro({
                keyboard: false,
                fullscreen: true,
                autoHeight: true,
                width: '770',
                //height:'100%',
                centerImage: true,
                orientation: 'vertical',
                loop: true,
                arrows: false,
                buttons: false,
                responsive: true,
                touchSwipe: true,
                thumbnailsPosition: 'right',
                thumbnailPointer: true,
                thumbnailWidth: 190,
                thumbnailHeight: 37,
                breakpoints: {
                    800: {
                        thumbnailsPosition: 'bottom',
                        thumbnailWidth: 170,
                        thumbnailHeight: 80
                    },
                    500: {
                        thumbnailsPosition: 'bottom',
                        thumbnailWidth: 120,
                        thumbnailHeight: 80
                    }

                }
            });

            $('.ColumnSliders').sliderPro({
                fullscreen: true,
                autoHeight: true,
                width: '100%',
                arrows: false,
                centerImage: true,
                buttons: false,
                orientation: 'vertical',
                autoplayDirection: 'backwards',
                keyboard: false,
                autoplayOnHover: 'none',

            });
            $('#example2').fadeIn();

        });
    </script>

</asp:Content>
