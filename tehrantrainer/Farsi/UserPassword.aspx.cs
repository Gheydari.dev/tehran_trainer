﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Farsi_UserPassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "تغییر رمز عبور | تهران آموز";

    }
    protected void SubmitButton_Click(object sender, EventArgs e)
    {
        if (CurPassTextBox.Text != "" && NewPassTextBox.Text != "" && RepeatNewPassTextBox.Text != "")
        {
            string CurPass = CurPassTextBox.Text;
            string NewPass = NewPassTextBox.Text;
            string pass = "";
            DataLayer dl = new DataLayer();
            string CS = dl.dataLayerConnectionString;
            SqlConnection con = new SqlConnection(CS);
            string FetchPassSqlSyntax = "select password from users where userid=@userid";
            SqlCommand FetchPassCmd = new SqlCommand(FetchPassSqlSyntax, con);

            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@userid", SqlDbType.Int);
            param[0].Value = Session["userid"].ToString();

            FetchPassCmd.Parameters.Add(param[0]);

            SqlDataReader dr;

            try
            {
                con.Open();
                dr = FetchPassCmd.ExecuteReader();
                if (dr.Read())
                {
                    pass = dr[0].ToString();
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                string msg = "FetchPassCmd Error:";
                msg += ex.Message;
                throw new Exception(msg);
            }
            finally
            {
                con.Close();
            }

            if (CurPass == pass)
            {
                string UpdatePassSqlSyntax = "update users set password=@password where userid=@userid";
                SqlParameter[] param2 = new SqlParameter[2];

                param2[0] = new SqlParameter("@password", SqlDbType.NVarChar);
                param2[1] = new SqlParameter("@userid", SqlDbType.BigInt);

                param2[0].Value = NewPass;
                param2[1].Value = Session["userid"].ToString();

                SqlCommand UpdatePassCmd = new SqlCommand(UpdatePassSqlSyntax, con);
                UpdatePassCmd.Parameters.Add(param2[0]);
                UpdatePassCmd.Parameters.Add(param2[1]);

                try
                {
                    con.Open();
                    UpdatePassCmd.ExecuteNonQuery();
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    string msg = "UpdatePassCmd Error:";
                    msg += ex.Message;
                    throw new Exception(msg);
                }
                finally
                {
                    con.Close();
                    InfoDiv.Visible = true;
                    InfoLiteral.Text = "رمز عبور شما با موفقیت تغییر یافت.";
                    InfoDiv.Attributes["class"] = " alert alert-success alert-dismissable text-right";
                    
                }




            }
            else
            {
                InfoDiv.Visible = true;
                InfoLiteral.Text = "رمز عبور قدیم شما نادرست است.";
                InfoDiv.Attributes["class"] = " alert alert-danger alert-dismissable text-right";
            }
        }
    }
}