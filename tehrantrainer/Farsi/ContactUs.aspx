﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ContactUs.aspx.cs" Inherits="Farsi_ContactUs" %>

<%@ Register Src="~/farsi/NumCaptcha.ascx" TagPrefix="uc1" TagName="NumCaptcha" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <asp:PlaceHolder runat="server">
        <link href="<%=SSClass.WebDomain%>/css/IranSans.css" rel="stylesheet" />
        <link href="<%=SSClass.WebDomain %>/css/AllInOne6.css" rel="stylesheet" />
        <link href="<%=SSClass.WebDomain %>/css/bootstrap.min.css" rel="stylesheet" />
    </asp:PlaceHolder>

</head>
<body>
    <form id="form1" runat="server">
        <div class="container-fluid BYekanGeneral" dir="rtl">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 ">
                        <div runat="server" id="MsgDiv" visible="false" class="form-group ">
                            <div class="alert alert-success alert-dismissable text-right">
                                <asp:Literal ID="InfoLiteral" runat="server" Text="پیام شما با موفقیت ارسال شد.<button onclick='$('.alert').alert()' type='button' class='close pull-left ' data-dismiss='alert' aria-hidden='true'>&times;</button>"></asp:Literal>
                            </div>
                        </div>
                        <%-- <div class="form-group">
                            <asp:Label ID="Label3" runat="server" Text="لطفا انتخاب کنید"></asp:Label>
                            <asp:DropDownList ID="SenderTypeDropDownList" runat="server">
                                <asp:ListItem>بازدید کنندگان عمومی</asp:ListItem>
                                <asp:ListItem>بیماران</asp:ListItem>
                                <asp:ListItem>پزشکان</asp:ListItem>
                                <asp:ListItem>کارمندان</asp:ListItem>
                            </asp:DropDownList>
                        </div>--%>
                        <div class="form-group">
                            <asp:Label ID="Label2" runat="server" Text="نام و نام خانوادگی"></asp:Label>
                            <asp:TextBox CssClass="form-control text-right input-sm" ID="NameFamilyTextBox" runat="server"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label4" runat="server" Text="تلفن تماس"></asp:Label>
                            <asp:TextBox CssClass="form-control text-right input-sm" ID="PhoneTextBox" runat="server"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label6" runat="server" Text="ایمیل"></asp:Label>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="پست الکترونیک نامعتبر" ControlToValidate="EmailTextBox" Display="Dynamic" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">پست الکترونیک نامعتبر!</asp:RegularExpressionValidator>
                            <asp:TextBox CssClass="form-control text-right input-sm" ID="EmailTextBox" runat="server"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label7" runat="server" Text="متن پیام"></asp:Label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="متن پیام الزامیست!" Text="*" ControlToValidate="MsgTextBox" ForeColor="Red" Display="Dynamic">متن پیام الزامیست!</asp:RequiredFieldValidator>
                            <asp:TextBox CssClass="form-control text-right input-sm" Style="min-height: 100px; max-height: 200px; max-width: 100%; min-width: 100%" ID="MsgTextBox" runat="server" TextMode="MultiLine" TabIndex="21"></asp:TextBox>
                        </div>
                        <div class="form-group ">
                            <uc1:NumCaptcha runat="server" ID="NumCaptcha1" />
                        </div>
                        <div class="form-group">
                            <asp:Button ID="SubmitButton" runat="server" Text="ارسال پیام" CssClass="btn btn-primary btn-block" OnClick="SubmitButton_Click" />
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </form>
</body>
</html>
