﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Farsi_Admin_FileManager : System.Web.UI.Page
{
    protected string DisplayDate(object Date)
    {
        PersianDateTime PersianDate = new PersianDateTime(Convert.ToDateTime(Date));


        return PersianDate.ToString(PersianDateTimeFormat.Date);
    }
    protected string GName(object Entry)
    {
        string fname = Entry.ToString();
        string[] Array = null;
        Array = fname.Split('/');

        return Array.Last().ToString();
    }
    protected string GLink(object Entry)
    {
        string FPath = Entry.ToString();
        //string[] Array = null;
        //Array = fname.Split('/');

        return FPath.Replace("~", SSClass.WebDomainFile);
    }
    protected string GLink2(object Entry)
    {
        string FPath = Entry.ToString();
        //string[] Array = null;
        //Array = fname.Split('/');

        return FPath.Replace("~", SSClass.WebDomainFile);
    }
    protected string GetImgLink(object Entry)
    {
        string FPath = Entry.ToString();
        //string[] Array = null;
        //Array = fname.Split('/');
        if (FPath.ToLower().Contains(".jpg") || FPath.ToLower().Contains(".png") || FPath.ToLower().Contains(".gif"))
        {
            return "<div class=\"text-center\"><img src=\"" + FPath.Replace("~", SSClass.WebDomainFile).ToString() + "\" alt=\"\" title=\"\" /></div>";

        }
        else
        {
            return "<a href=\"" + FPath.Replace("~", SSClass.WebDomainFile).ToString() + "\">نام_پیوند_را_اصلاح_کنید</a>";
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        System.Web.UI.HtmlControls.HtmlAnchor li = (System.Web.UI.HtmlControls.HtmlAnchor)this.Page.Master.Master.FindControl("BodyContentPlaceHolder").FindControl("FileManager");

        li.Attributes["class"] = "btn btn-primary";
    }
    protected void SubmitButton_Click(object sender, EventArgs e)
    {
        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);


        string DPath = "";

        if (DocumentsFileUpload.HasFile)
        {
            string SecCode = Guid.NewGuid().ToString("N").Substring(12);

            string fileExt =
            System.IO.Path.GetExtension(DocumentsFileUpload.FileName.ToLower());

            if (fileExt == ".zip" || fileExt == ".rar" || fileExt == ".pdf" || fileExt == ".jpg" || fileExt == ".doc" || fileExt == ".docx" || fileExt == ".jpeg" || fileExt == ".png" || fileExt == ".gif" || fileExt == ".xls" || fileExt == ".xlsx" || fileExt == ".swf" || fileExt == ".ppt" || fileExt == ".pptx")
            {
                if (DocumentsFileUpload.FileName.ToLower().Replace(fileExt, "").Length > 30)
                {
                    DPath = "~/Document/Download/" + DocumentsFileUpload.FileName.ToLower().Replace(fileExt, "").Substring(0, 29) + "_" + DateTime.Now.Millisecond.ToString() + SecCode + fileExt.Trim();
                }
                else
                {
                    DPath = "~/Document/Download/" + DocumentsFileUpload.FileName.ToLower().Replace(fileExt, "") + "_" + DateTime.Now.Millisecond.ToString() + SecCode + fileExt.Trim();
                }

                long size = DocumentsFileUpload.PostedFile.ContentLength;
                string unit = "KB";
                size = size / 1024; //to KB
                if (size < 1)
                {
                    size = 1;
                }
                else if (size >= 1024)
                {
                    size = size / 1024; //to MB
                    unit = "MB";
                }
                else if (size >= 1024)
                {
                    size = size / 1024; //to GB
                    unit = "GB";
                }
                unit = size.ToString("###.##") + unit;


                if (true)
                {

                    string RegSqlSyntax = @" INSERT INTO fileMng (fpath,fsize) VALUES (@fpath,@fsize) ";

                    SqlCommand RegCmd = new SqlCommand(RegSqlSyntax, con);
                    SqlParameter[] param = new SqlParameter[2];

                    param[00] = new SqlParameter("@fpath", SqlDbType.NVarChar);
                    param[01] = new SqlParameter("@fsize", SqlDbType.NVarChar);

                    param[00].Value = DPath;
                    param[01].Value = unit;


                    for (int i = 0; i < param.Length; i++)
                    {
                        RegCmd.Parameters.Add(param[i]);
                    }

                    try
                    {
                        DocumentsFileUpload.SaveAs(Server.MapPath(DPath));
                        con.Open();
                        RegCmd.ExecuteReader();
                        FileMngGridView.DataBind();
                        InfoDiv.Visible = true;
                        InfoLiteral.Text = "فایل مورد نظر شما با موفقیت بارگذاری شد.";
                        InfoDiv.Attributes["class"] = " alert alert-success alert-dismissable text-right";
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        string msg = "RegCmd Error:";
                        msg += ex.Message;
                        throw new Exception(msg);
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            else
            {
                InfoDiv.Visible = true;
                InfoLiteral.Text = "قالب این فایل غیر مجاز است و پشتیبانی نمی شود. در صورت لزوم آن را به صورت فشرده بارگذاری کنید.";
                InfoDiv.Attributes["class"] = " alert alert-danger alert-dismissable text-right";
            }
        }
    }

    protected void FileMngGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);
        string item = e.CommandArgument.ToString();
        string RegSqlSyntax = "delete from filemng where fpath=@fpath";

        SqlCommand RegCmd = new SqlCommand(RegSqlSyntax, con);
        SqlParameter[] param = new SqlParameter[1];

        param[00] = new SqlParameter("@fpath", SqlDbType.NVarChar);

        param[00].Value = item;


        for (int i = 0; i < param.Length; i++)
        {
            RegCmd.Parameters.Add(param[i]);
        }

        try
        {

            con.Open();
            if (item != null || item != String.Empty)
            {
                if (System.IO.File.Exists(HttpContext.Current.Server.MapPath(item)))
                {
                    System.IO.File.Delete(HttpContext.Current.Server.MapPath(item));
                }
            }
            RegCmd.ExecuteReader();

            FileMngGridView.DataBind();
            InfoDiv.Visible = true;
            InfoLiteral.Text = "فایل مورد نظر شما با موفقیت حذف شد.";
            InfoDiv.Attributes["class"] = " alert alert-success alert-dismissable text-right";
        }
        catch (System.Data.SqlClient.SqlException ex)
        {
            string msg = "RegCmd Error:";
            msg += ex.Message;
            throw new Exception(msg);
        }
        finally
        {
            con.Close();
        }
    }
}