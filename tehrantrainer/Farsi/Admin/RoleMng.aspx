﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Farsi/Admin/FaAdminMasterPage.master" AutoEventWireup="true" CodeFile="RoleMng.aspx.cs" Inherits="Farsi_Admin_RoleMng" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContentPlaceHolder" runat="Server">
    <style>
        .panel {
            background-color: transparent
        }

        label {
            display: block;
            width: 100%;
            cursor: pointer;
            font-weight: 100;
        }

        .sdiv {
            background-color: white;
            padding: 3px;
            margin: 0px;
            width: 100%;
            margin-bottom: 2px;
        }

            .sdiv:hover {
                background-color: #eeeeee
            }

        .panel-title {
            font-size: 12px;
        }

            .panel-title > a {
                text-decoration: none !important
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div dir="rtl" class="modal fade BYekan" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true" id="NewAdminModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close pull-left" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <span class="modal-title" id="gridSystemModalLabel2">تعریف کاربر جدید</span>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <asp:TextBox ID="NameTextBox" CssClass="form-control" placeholder="نام" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="NameTextBox" ForeColor="Red" Display="Dynamic" ValidationGroup="NewAdmin" >*</asp:RequiredFieldValidator>
                                </div>
                                <div class="form-group">
                                    <asp:TextBox ID="FamilyTextBox" CssClass="form-control" placeholder="نام خانوادگی" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="FamilyTextBox" ForeColor="Red" Display="Dynamic" ValidationGroup="NewAdmin" >*</asp:RequiredFieldValidator>
                                </div>
                                <div class="form-group">
                                    <asp:TextBox ID="MobPhoneTextBox" CssClass="form-control" placeholder="تلفن همراه" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="MobPhoneTextBox" ForeColor="Red" Display="Dynamic" ValidationGroup="NewAdmin" >*</asp:RequiredFieldValidator>
                                </div>
                                <div class="form-group">
                                    <asp:TextBox ID="PasswordTextBox" CssClass="form-control" placeholder="کلمه عبور" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="PasswordTextBox" ForeColor="Red" Display="Dynamic" ValidationGroup="NewAdmin" >*</asp:RequiredFieldValidator>
                                </div>
                                <div class="form-group">
                                    <asp:TextBox ID="ColorCodeTextBox" CssClass="form-control" placeholder="کد رنگ: #fff" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ColorCodeTextBox" ForeColor="Red" Display="Dynamic" ValidationGroup="NewAdmin" >*</asp:RequiredFieldValidator>
                                </div>
                                <div class="form-group">
                                    <asp:FileUpload runat="server" ID="AdminPhotoFileUpload" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer ">
                    <div class="text-center">
                        <asp:Button CssClass="btn btn-primary btn-sm btn-block" ID="AddNewAdminButton" runat="server" Text="ثبت کاربر" OnClick="AddNewAdminButton_Click" ValidationGroup="NewAdmin" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div dir="rtl" class="modal fade BYekan" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true" id="NewRoleModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close pull-left" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <span class="modal-title" id="gridSystemModalLabelwd2">تعریف نقش جدید</span>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <asp:TextBox ID="RoleNameTextBox" CssClass="form-control" placeholder="عنوان نقش" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="RoleNameTextBox" ForeColor="Red" Display="Dynamic" ValidationGroup="NewRole">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer ">
                    <div class="text-center">
                        <asp:Button CssClass="btn btn-primary btn-sm btn-block" ID="AddNewRoleButton" runat="server" Text="ثبت نقش" OnClick="AddNewRoleButton_Click" ValidationGroup="NewRole" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div runat="server" id="InfoDiv" class="alert alert-success alert-dismissable text-right" visible="false">
                <asp:Literal ID="InfoLiteral" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 text-center">
            <asp:Button CssClass="btn btn-default" ID="AddSelectedRolesButton" runat="server" Text="افزودن نقش ها به کاربران" OnClick="AddSelectedRolesButton_Click" />
            <asp:Button CssClass="btn btn-default" ID="AddSelectedPagesButton" runat="server" Text="افزودن صفحات و کنترل ها به نقش ها" OnClick="AddSelectedPagesButton_Click" />
            <hr />
        </div>
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <label style="padding-right: 15px; display: inline" class="">کاربران پنل</label>
                    <span style="margin: 3px" data-toggle="modal" data-target="#NewAdminModal" class="btn btn-xs btn-primary pull-left">تعریف کاربر جدید</span>
                </div>
                <div style="padding: 0px;" class="panel-body">
                    <asp:Repeater ID="AdminsRepeater" runat="server" DataSourceID="RM_AdminsSqlDataSource" OnItemCommand="AdminsRepeater_ItemCommand">
                        <ItemTemplate>
                            <div class="row sdiv">
                                <label>
                                    <div class="col-xs-1">
                                        <asp:CheckBox ID="AdminSelectCheckBox" runat="server" />
                                    </div>
                                    <div class="col-xs-8">
                                        <asp:Label Text='<%#Eval("adminid") %>' runat="server" ID="AdminIDLabel" />
                                        <span><%#". "+Eval("NameFamily") %></span>
                                    </div>
                                </label>
                                <div class="col-xs-2">
                                    <div class="btn-group  text-right">
                                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right text-right" role="menu">
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#UserRolesModal_<%#Container.ItemIndex %>"><span style="margin-left: 5px" class="glyphicon glyphicon-align-justify"></span>نقش ها</a>
                                            </li>
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#PassChangeModal_<%#Container.ItemIndex %>"><span style="margin-left: 5px" class="glyphicon glyphicon-lock"></span>تغییر کلمه عبور</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div dir="rtl" class="modal fade BYekan" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true" id="UserRolesModal_<%#Container.ItemIndex %>">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header text-center">
                                            <button type="button" class="close pull-left" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <span class="modal-title" id="gridSystemModalLabel2">نقش های <%#" "+Eval("NameFamily") %></span>
                                        </div>
                                        <div class="modal-body">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <asp:Repeater runat="server" ID="AdminRolesRepeater" DataSourceID="RM_AdminRolesSqlDataSource">
                                                            <ItemTemplate>
                                                                <asp:Label Text='<%#Eval("URID") %>' runat="server" ID="URIDLabel" Visible="false" />
                                                                <label>
                                                                    <div class="row sdiv">
                                                                        <div class="col-xs-1">
                                                                            <asp:CheckBox ID="AdminRoleSelectCheckBox" runat="server" />
                                                                        </div>
                                                                        <div class="col-xs-10">
                                                                            <asp:Label Text='<%#Eval("RoleID") %>' runat="server" ID="RoleIDLabel" />
                                                                            <span><%#". "+Eval("RoleName") %></span>
                                                                        </div>
                                                                    </div>
                                                                </label>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                        <asp:SqlDataSource ID="RM_AdminRolesSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DrB_DBConnectionString %>" SelectCommand="SELECT dbo.RM_Roles.RoleID, dbo.RM_Roles.RoleName, dbo.RM_AdminRoles.URID FROM dbo.RM_Roles INNER JOIN dbo.RM_AdminRoles ON dbo.RM_Roles.RoleID = dbo.RM_AdminRoles.RoleID WHERE (dbo.RM_AdminRoles.AdminID = @AdminID)">
                                                            <SelectParameters>
                                                                <asp:ControlParameter ControlID="AdminIDLabel" PropertyName="Text" Name="AdminID"></asp:ControlParameter>
                                                            </SelectParameters>
                                                        </asp:SqlDataSource>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer ">
                                            <div class="text-center">
                                                <asp:Button CssClass="btn btn-danger btn-sm btn-block" ID="DeleteAdminRolesButton" runat="server" Text="حذف نقش های انتخابی" CommandName="DeleteAdminRoles" CommandArgument='<%#Eval("adminid") %>' />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div dir="rtl" class="modal fade BYekan" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true" id="PassChangeModal_<%#Container.ItemIndex %>">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header text-center">
                                            <button type="button" class="close pull-left" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <span class="modal-title" id="gridSystemModalLabel">تغییر کلمه عبور</span>
                                        </div>
                                        <div class="modal-body">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="SubmitButton">
                                                            <div class="form-group">
                                                                <asp:TextBox ID="NewPassTextBox" CssClass="form-control input-sm" placeholder="کلمه عبور جدید" runat="server" TextMode="Password"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ValidationGroup='<%#"PassChange_"+Container.ItemIndex %>' ID="RequiredFieldValidator2" runat="server" ControlToValidate="NewPassTextBox" ForeColor="Red" Display="Dynamic">وارد کردن عبور جدید الزامیست</asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:TextBox ID="RepeatNewPassTextBox" CssClass="form-control input-sm" placeholder="تکرار کلمه عبور جدید" runat="server" TextMode="Password"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ValidationGroup='<%#"PassChange_"+Container.ItemIndex %>' ID="RequiredFieldValidator3" runat="server" ControlToValidate="RepeatNewPassTextBox" ForeColor="Red" Display="Dynamic">تکرار عبور جدید الزامیست</asp:RequiredFieldValidator>
                                                                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="RepeatNewPassTextBox" ControlToValidate="NewPassTextBox" Display="Dynamic" ForeColor="Red">گذرواژه به درستی تکرار نشده</asp:CompareValidator>
                                                            </div>
                                                            <asp:Button ValidationGroup='<%#"PassChange_"+Container.ItemIndex %>' CssClass="btn btn-primary btn-sm btn-block" ID="SubmitButton" runat="server" Text="تغییر گذرواژه" CommandName="ChangePassword" CommandArgument='<%#Eval("adminid") %>' />
                                                        </asp:Panel>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer ">
                                            <div class="text-center">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <asp:SqlDataSource ID="RM_AdminsSqlDataSource" runat="server" ProviderName="<%$ ConnectionStrings:DrB_DBConnectionString.ProviderName %>" ConnectionString="<%$ ConnectionStrings:DrB_DBConnectionString %>" SelectCommand="select *,name+N' '+family as namefamily from rm_admins"></asp:SqlDataSource>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <label class="BYekan" style="padding-right: 15px; display: inline">نقش ها</label>
                    <span data-toggle="modal" data-target="#NewRoleModal" class="btn btn-primary btn-xs pull-left">تعریف نقش جدید</span>
                </div>
                <div style="padding: 0px;" class="panel-body">
                    <asp:Repeater ID="RolesRepeater" runat="server" DataSourceID="RM_RolesSqlDataSource" OnItemCommand="RolesRepeater_ItemCommand">
                        <ItemTemplate>
                            <div class="row sdiv">
                                <label>
                                    <div class="col-xs-1">
                                        <asp:CheckBox ID="RoleSelectCheckBox" runat="server" />
                                    </div>
                                    <div class="col-xs-8">
                                        <asp:Label Text='<%#Eval("RoleID") %>' runat="server" ID="RoleIDLabel" />
                                        <span><%#". "+Eval("RoleName") %></span>
                                    </div>
                                </label>
                                <div class="col-xs-2">
                                    <div class="btn-group  text-right">
                                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right text-right" role="menu">
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#RoleAdmins_<%#Container.ItemIndex %>"><span style="margin-left: 5px" class="glyphicon glyphicon-align-justify"></span>کاربران این نقش</a>
                                            </li>
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#RoleAccessesModal_<%#Container.ItemIndex %>"><span style="margin-left: 5px" class="glyphicon glyphicon-lock"></span>دسترسی های این نقش</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div dir="rtl" class="modal fade BYekan" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true" id="RoleAdmins_<%#Container.ItemIndex %>">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header text-center">
                                            <button type="button" class="close pull-left" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <span class="modal-title" id="gridSystemModalLabel31">کاربران <%#" "+Eval("RoleName") %></span>
                                        </div>
                                        <div class="modal-body">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <asp:Repeater runat="server" ID="RoleAdminsRepeater" DataSourceID="RoleAdminsSqlDataSource">
                                                            <ItemTemplate>
                                                                <asp:Label Text='<%#Eval("URID") %>' runat="server" ID="URIDLabel" Visible="false" />
                                                                <div class="row sdiv">
                                                                    <label>
                                                                        <div class="col-xs-1">
                                                                            <asp:CheckBox ID="RoleSelectCheckBox" runat="server" />
                                                                        </div>
                                                                        <div class="col-xs-10">
                                                                            <asp:Label Text='<%#Eval("adminid") %>' runat="server" ID="AdminIDLabel" />
                                                                            <span><%#". "+Eval("NameFamily") %></span>
                                                                        </div>
                                                                    </label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                        <asp:SqlDataSource ID="RoleAdminsSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DrB_DBConnectionString %>" SelectCommand="SELECT dbo.RM_AdminRoles.URID, dbo.RM_Admins.Name+N' '+ dbo.RM_Admins.Family as NameFamily, dbo.RM_Admins.AdminID, dbo.RM_AdminRoles.RoleID FROM dbo.RM_AdminRoles inner JOIN dbo.RM_Admins on RM_AdminRoles.AdminID=RM_Admins.AdminID  WHERE (dbo.RM_AdminRoles.RoleID = @RoleID)">
                                                            <SelectParameters>
                                                                <asp:ControlParameter ControlID="RoleIDLabel" PropertyName="Text" Name="RoleID"></asp:ControlParameter>
                                                            </SelectParameters>
                                                        </asp:SqlDataSource>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer ">
                                            <div class="text-center">
                                                <asp:Button CssClass="btn btn-danger btn-sm btn-block" ID="DeleteAdminRolesButton" runat="server" Text="حذف کاربران انتخابی" CommandName="DeleteRoleAdmins" CommandArgument='<%#Eval("RoleID") %>' />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div dir="rtl" class="modal fade BYekan" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true" id="RoleAccessesModal_<%#Container.ItemIndex %>">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header text-center">
                                            <button type="button" class="close pull-left" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <span class="modal-title" id="gridSystemModalLabel4">دسترسی های <%#" "+Eval("RoleName") %></span>
                                        </div>
                                        <div class="modal-body">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <asp:Repeater ID="RolePagesRepeater" runat="server" DataSourceID="RolePagesSqlDataSource">
                                                        <HeaderTemplate>
                                                            <div class="panel-group" id="accordion1<%#((RepeaterItem)Container.Parent.Parent).ItemIndex %>" role="tablist" aria-multiselectable="true">
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingOne<%#Container.ItemIndex+1+"_"+((RepeaterItem)Container.Parent.Parent).ItemIndex %>">
                                                                    <h4 class="panel-title">
                                                                        <asp:CheckBox ID="PageSelectCheckBox" runat="server" />
                                                                        <a role="button" data-toggle="collapse" data-parent="#accordion1<%#((RepeaterItem)Container.Parent.Parent).ItemIndex %>" href="#collapse_collapse_<%#Container.ItemIndex+1+"_"+((RepeaterItem)Container.Parent.Parent).ItemIndex %>" aria-expanded="true" aria-controls="collapse_collapse_<%#Container.ItemIndex+1+"_"+((RepeaterItem)Container.Parent.Parent).ItemIndex %>"><%#Eval("PageID").ToString()+". "+Eval("PageTitle") %></a>
                                                                        <a class="pull-left" href="<%#Eval("PageName").ToString().Replace("ASP.Farsi_admin_","").Replace("_",".") %>" target="_blank">مشاهده</a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapse_collapse_<%#Container.ItemIndex+1+"_"+((RepeaterItem)Container.Parent.Parent).ItemIndex %>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne<%#Container.ItemIndex+1+"_"+((RepeaterItem)Container.Parent.Parent).ItemIndex %>">
                                                                    <div class="panel-body" style="padding: 0px; padding-right: 30px; background-color: #fff">
                                                                        <asp:Label ID="PageNameLabel" Text='<%#Eval("pagename") %>' Visible="false" runat="server" />
                                                                        <asp:Label ID="PageIDLabel" Text='<%#Eval("PageID") %>' Visible="false" runat="server" />

                                                                        <asp:Repeater ID="ControlsRepeater" runat="server" DataSourceID="RM_ControlsSqlDataSource">
                                                                            <ItemTemplate>
                                                                                <label>
                                                                                    <div class="row sdiv small">
                                                                                        <div class="col-xs-1">
                                                                                            <asp:CheckBox ID="ControlSelectCheckBox" runat="server" />
                                                                                        </div>
                                                                                        <div class="col-xs-10">
                                                                                            <asp:Label Text='<%#Eval("ControlID") %>' runat="server" ID="ControlIDLabel" />
                                                                                            <span><%#". "+Eval("ControlName") %></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </label>
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                        <asp:SqlDataSource ID="RM_ControlsSqlDataSource" runat="server" ProviderName="<%$ ConnectionStrings:DrB_DBConnectionString.ProviderName %>" ConnectionString="<%$ ConnectionStrings:DrB_DBConnectionString %>" SelectCommand="select * from RM_Controls where pagename=@pagename and controlid in (select controlid from RM_RoleControls where RoleID=@RoleID and click=N'True' and Visible=N'True') order by controlname">
                                                                            <SelectParameters>
                                                                                <asp:ControlParameter Name="pagename" PropertyName="Text" ControlID="PageNameLabel" />
                                                                                <asp:ControlParameter ControlID="RoleIDLabel" PropertyName="Text" Name="RoleID"></asp:ControlParameter>
                                                                            </SelectParameters>
                                                                        </asp:SqlDataSource>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </div>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                    <asp:SqlDataSource ID="RolePagesSqlDataSource" runat="server" ProviderName="<%$ ConnectionStrings:DrB_DBConnectionString.ProviderName %>" ConnectionString="<%$ ConnectionStrings:DrB_DBConnectionString %>" SelectCommand="select * from rm_pages where pageid in (select pageid from rm_rolepages where RoleID=@RoleID) order by pagetitle">
                                                        <SelectParameters>
                                                            <asp:ControlParameter ControlID="RoleIDLabel" PropertyName="Text" Name="RoleID"></asp:ControlParameter>
                                                        </SelectParameters>
                                                    </asp:SqlDataSource>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer ">
                                            <div class="text-center">
                                                <asp:Button CssClass="btn btn-danger btn-sm btn-block" ID="DeketeRoleAccessButton" runat="server" Text="حذف صفحات و کنترل های انتخابی" CommandName="DeleteRoleAccesses" CommandArgument='<%#Eval("RoleID") %>' />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <asp:SqlDataSource ID="RM_RolesSqlDataSource" runat="server" ProviderName="<%$ ConnectionStrings:DrB_DBConnectionString.ProviderName %>" ConnectionString="<%$ ConnectionStrings:DrB_DBConnectionString %>" SelectCommand="select * from rm_roles"></asp:SqlDataSource>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <label class="BYekan" style="padding-right: 15px; display: inline">صفحات و کنترل ها</label>
                    <asp:CheckBox  style="display:flex" CssClass="pull-left" AutoPostBack="true" Text="انتخاب همه" runat="server" ID="SelectAllAccessesCheckBox" OnCheckedChanged="SelectAllAccessesCheckBox_CheckedChanged" />
                </div>
                <div style="padding: 0px;" class="panel-body">
                    <asp:Repeater ID="PagesRepeater" runat="server" DataSourceID="RM_PagesSqlDataSource">
                        <HeaderTemplate>
                            <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne<%#Container.ItemIndex+1 %>">
                                    <h4 class="panel-title">
                                        <asp:CheckBox ID="PageSelectCheckBox" runat="server" CssClass="RolesCheckbox" />
                                        <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse_ss<%#Container.ItemIndex+1 %>" aria-expanded="true" aria-controls="collapse_ss<%#Container.ItemIndex+1 %>"><%#Eval("PageID").ToString()+". "+Eval("PageTitle") %></a>
                                        <a class="pull-left" href="<%#Eval("PageName").ToString().Replace("ASP.Farsi_admin_","").Replace("_",".") %>" target="_blank">مشاهده</a>
                                    </h4>
                                </div>
                                <div id="collapse_ss<%#Container.ItemIndex+1 %>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne<%#Container.ItemIndex+1 %>">
                                    <div class="panel-body" style="padding: 0px; padding-right: 30px; background-color: #fff">
                                        <asp:Label ID="PageNameLabel" Text='<%#Eval("pagename") %>' Visible="false" runat="server" />
                                        <asp:Label ID="PageIDLabel" Text='<%#Eval("PageID") %>' Visible="false" runat="server" />
                                        <asp:Repeater ID="ControlsRepeater" runat="server" DataSourceID="RM_ControlsSqlDataSource">
                                            <ItemTemplate>
                                                <label>
                                                    <div class="row sdiv small">
                                                        <div class="col-xs-1">
                                                            <asp:CheckBox ID="ControlSelectCheckBox" runat="server" CssClass="RolesCheckbox" />
                                                        </div>
                                                        <div class="col-xs-10">
                                                            <asp:Label Text='<%#Eval("ControlID") %>' runat="server" ID="ControlIDLabel" />
                                                            <span><%#". "+Eval("ControlName") %></span>
                                                        </div>
                                                    </div>
                                                </label>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <asp:SqlDataSource ID="RM_ControlsSqlDataSource" runat="server" ProviderName="<%$ ConnectionStrings:DrB_DBConnectionString.ProviderName %>" ConnectionString="<%$ ConnectionStrings:DrB_DBConnectionString %>" SelectCommand="select * from RM_Controls where pagename=@pagename order by controlname">
                                            <SelectParameters>
                                                <asp:ControlParameter Name="pagename" PropertyName="Text" ControlID="PageNameLabel" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                        <FooterTemplate>
                            </div>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:SqlDataSource ID="RM_PagesSqlDataSource" runat="server" ProviderName="<%$ ConnectionStrings:DrB_DBConnectionString.ProviderName %>" ConnectionString="<%$ ConnectionStrings:DrB_DBConnectionString %>" SelectCommand="select * from rm_pages order by pagetitle"></asp:SqlDataSource>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContentPlaceHolder" runat="Server">
</asp:Content>

