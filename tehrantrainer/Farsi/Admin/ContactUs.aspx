﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Farsi/Admin/FaAdminMasterPage.master" AutoEventWireup="true" CodeFile="ContactUs.aspx.cs" Inherits="Farsi_Admin_ContactUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row ">
        <div class="col-md-12">
            <div runat="server" id="InfoDiv" class="alert alert-info alert-dismissable text-right" visible="true">
                <asp:Literal Text="پیام ها، نظرات، پیشنهادات، انتقادات و بازخورد کاربران در این قسمت قابل مشاهده است." ID="InfoLiteral" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row container">
        <div class="col-md-12 text-right">
            <asp:GridView Font-Size="Small" CssClass="text-center table table-bordered table-condensed" ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="msgid" DataSourceID="PollViewSqlDataSource" ForeColor="Black" GridLines="Horizontal" Width="100%" AllowPaging="True" PageSize="40">
                <Columns>
                    <asp:TemplateField SortExpression="PollDate" HeaderText="" ShowHeader="false">
                        <ItemTemplate>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <p runat="server" id="BodyPTag"><%#Eval("msgbody") %></p>
                                </div>
                                <div class="col-md-12 text-right">
                                    <asp:Label Font-Size="Smaller" ID="Label3" runat="server" Text='<%#"نام و نام خانوادگی: "+Eval("msgsender") %>'></asp:Label>
                                    <asp:Label Font-Size="Smaller" Font-Bold="true" ForeColor="#3366ff" ID="Label4" runat="server" Text="  |  "></asp:Label>
                                    <asp:Label Font-Size="Smaller" ID="Label5" runat="server" Text='<%#"تلفن تماس: "+Eval("msgphone") %>'></asp:Label>
                                    <asp:Label Font-Size="Smaller" Font-Bold="true" ForeColor="#3366ff" ID="Label6" runat="server" Text="  |  "></asp:Label>
                                    <asp:Label Font-Size="Smaller" ID="Label7" runat="server" Text='<%#"نمابر: "+Eval("msgfax") %>'></asp:Label>
                                    <asp:Label Font-Size="Smaller" Font-Bold="true" ForeColor="#3366ff" ID="Label8" runat="server" Text="  |  "></asp:Label>
                                    <asp:Label Font-Size="Smaller" ID="Label9" runat="server" Text='<%#"ایمیل: "+Eval("msgemail") %>'></asp:Label>
                                    <%--<asp:Label Font-Bold="true" ForeColor="#3366ff" ID="Label10" runat="server" Text="  |  "></asp:Label>--%>
                                </div>
                                <div class="col-md-12 text-right">
                                    <asp:Label Font-Size="Smaller" ID="Label11" runat="server" Text='<%#"نام و نشانی شرکت: "+Eval("msgaddress") %>'></asp:Label>
                                </div>
                                <div class="col-md-12 text-left">
                                    <asp:Label ID="Label1" runat="server" Text='<%# DisplayDate(Eval("cdate")) %>' Font-Size="Smaller"></asp:Label>
                                </div>
                            </div>
                        </ItemTemplate>
                        <HeaderStyle CssClass="text-center" Font-Bold="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:TemplateField>

                </Columns>
                <EmptyDataTemplate>
                    <asp:Label CssClass="alert alert-warning" ID="Label2" runat="server" Text=".در حال حاضر اطلاعاتی جهت نمایش وجود ندارد"></asp:Label>
                </EmptyDataTemplate>
                <AlternatingRowStyle BackColor="#efefef" />
                <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                <HeaderStyle BackColor="#333333" Font-Bold="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" Height="0px" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#242121" />
            </asp:GridView>
            <asp:SqlDataSource ID="PollViewSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DrB_DBConnectionString %>" SelectCommand="SELECT * FROM contactus ORDER BY cdate DESC"></asp:SqlDataSource>
        </div>
    </div>
</asp:Content>

