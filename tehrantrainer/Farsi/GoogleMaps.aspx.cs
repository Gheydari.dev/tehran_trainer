﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Farsi_GoogleMaps : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        MapScriptLiteral.Text = @"function initMap() {
            var uluru = { lat: " + Request.QueryString["lat"] + @", lng: " + Request.QueryString["lng"] + @" };
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 16,
                center: uluru
            });
            var marker = new google.maps.Marker({
                position: uluru,
                map: map
            });
        }";
    }
}