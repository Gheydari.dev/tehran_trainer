﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Farsi_Default : System.Web.UI.Page
{


    protected void SubmitButton_Click(object sender, EventArgs e)
    {
        //string CS = DataLayer.connectionString;
        //SqlConnection con = new SqlConnection(CS);
        //string RegSqlSyntax = @"begin DECLARE @myInsertedTable table (insertedid  bigint NOT NULL)
        //                        INSERT INTO [AskForm] (                                                         
        //                                                 Userid
        //                                                ,Name
        //                                                ,Family
        //                                                ,EduGroup
        //                                                ,Field
        //                                                ,Department
        //                                                ,Service
        //                                                ,PhoneNo
        //                                                ,Email
        //                                                ,MoreDes
        //                                                ,Situ
        //                                                ,Isdeleted
        //                                                ,IntMethod
        //                                                ) OUTPUT inserted.AFID into @myInsertedTable   VALUES (
        //                                                 @Userid
        //                                                ,@Name
        //                                                ,@Family
        //                                                ,@EduGroup
        //                                                ,@Field
        //                                                ,@Department
        //                                                ,@Service
        //                                                ,@PhoneNo
        //                                                ,@Email
        //                                                ,@MoreDes
        //                                                ,@Situ
        //                                                ,@Isdeleted
        //                                                ,@IntMethod
        //                                                ) ";
        
        //RegSqlSyntax += " end ";
        //SqlCommand RegCmd = new SqlCommand(RegSqlSyntax, con);
        //SqlParameter[] param = new SqlParameter[14];

        //param[00] = new SqlParameter("@AFID", SqlDbType.NVarChar);
        //param[01] = new SqlParameter("@Userid", SqlDbType.NVarChar);
        //param[02] = new SqlParameter("@Name", SqlDbType.NVarChar);
        //param[03] = new SqlParameter("@Family", SqlDbType.NVarChar);
        //param[04] = new SqlParameter("@EduGroup", SqlDbType.NVarChar);
        //param[05] = new SqlParameter("@Field", SqlDbType.NVarChar);
        //param[06] = new SqlParameter("@Department", SqlDbType.NVarChar);
        //param[07] = new SqlParameter("@Service", SqlDbType.NVarChar);
        //param[08] = new SqlParameter("@PhoneNo", SqlDbType.NVarChar);
        //param[09] = new SqlParameter("@Email", SqlDbType.NVarChar);
        //param[10] = new SqlParameter("@MoreDes", SqlDbType.NVarChar);
        //param[11] = new SqlParameter("@Situ", SqlDbType.NVarChar);
        //param[12] = new SqlParameter("@Isdeleted", SqlDbType.NVarChar);
        //param[13] = new SqlParameter("@IntMethod", SqlDbType.NVarChar);


        //param[00].Value = "";
        //param[01].Value = PhoneNoTextBox.Text;
        //param[02].Value = NameTextBox.Text;
        //param[03].Value = FamilyTextBox.Text;
        //param[04].Value = EduGroupDropDownList.SelectedValue;
        //param[05].Value = FieldDropDownList.SelectedValue;
        //param[06].Value = DepartmentDropDownList.SelectedValue;
        //param[07].Value = ServiceDropDownList.SelectedValue;
        //param[08].Value = PhoneNoTextBox.Text;
        //param[09].Value = EmailTextBox.Text;
        //param[10].Value = MoreDesTextBox.Text;
        //param[11].Value = "جدید";
        //param[12].Value = 0;
        //param[13].Value = IntMethodDropDownList.SelectedValue;

       

        //for (int i = 0; i < param.Length; i++)
        //{
        //    RegCmd.Parameters.Add(param[i]);
        //}

        //try
        //{

        //    con.Open();
        //    RegCmd.ExecuteReader();


        //    PhoneNoTextBox.Text = "";
        //    NameTextBox.Text = "";
        //    FamilyTextBox.Text = "";
        //    EduGroupDropDownList.SelectedIndex = 0;
        //    FieldDropDownList.SelectedIndex = 0;
        //    DepartmentDropDownList.SelectedIndex = 0;
        //    ServiceDropDownList.SelectedIndex = 0;
        //    PhoneNoTextBox.Text = "";
        //    EmailTextBox.Text = "";
        //    MoreDesTextBox.Text = "";
        //    IntMethodDropDownList.SelectedIndex = 0;
        //    InfoDiv.Visible = true;
        //    InfoLabel.Text = "اطلاعات شما با موفقیت ثبت شد. با شما در تماس خواهیم بود.";
        //    InfoDiv.Attributes["class"] = " alert alert-success alert-dismissable";
        //}
        //catch (System.Data.SqlClient.SqlException ex)
        //{
        //    string msg = "RegCmd Error:";
        //    msg += ex.Message;
        //    throw new Exception(msg + RegSqlSyntax);
        //}
        //finally
        //{
        //    con.Close();
        //}
    }

    protected void QuickRegButton_Click(object sender, EventArgs e)
    {
        string CS = DataLayer.connectionString;
        SqlConnection con = new SqlConnection(CS);
        string RegSqlSyntax = @"begin DECLARE @myInsertedTable table (insertedid  bigint NOT NULL)
                                INSERT INTO [AskForm] (                                                         
                                                         Userid
                                                        ,Name
                                                        ,Family
                                                        ,EduGroup
                                                        ,Field
                                                        ,Department
                                                        ,Service
                                                        ,PhoneNo
                                                        ,Email
                                                        ,MoreDes
                                                        ,Situ
                                                        ,Isdeleted
                                                        ,IntMethod
                                                        ,SCode
                                                        ) OUTPUT inserted.AFID into @myInsertedTable   VALUES (
                                                         @Userid
                                                        ,@Name
                                                        ,@Family
                                                        ,@EduGroup
                                                        ,@Field
                                                        ,@Department
                                                        ,@Service
                                                        ,@PhoneNo
                                                        ,@Email
                                                        ,@MoreDes
                                                        ,@Situ
                                                        ,@Isdeleted
                                                        ,@IntMethod
                                                        ,@SCode
                                                        ) ";

        RegSqlSyntax += " end ";
        SqlCommand RegCmd = new SqlCommand(RegSqlSyntax, con);
        SqlParameter[] param = new SqlParameter[14];

        param[00] = new SqlParameter("@SCode", SqlDbType.NVarChar);
        param[01] = new SqlParameter("@Userid", SqlDbType.NVarChar);
        param[02] = new SqlParameter("@Name", SqlDbType.NVarChar);
        param[03] = new SqlParameter("@Family", SqlDbType.NVarChar);
        param[04] = new SqlParameter("@EduGroup", SqlDbType.NVarChar);
        param[05] = new SqlParameter("@Field", SqlDbType.NVarChar);
        param[06] = new SqlParameter("@Department", SqlDbType.NVarChar);
        param[07] = new SqlParameter("@Service", SqlDbType.NVarChar);
        param[08] = new SqlParameter("@PhoneNo", SqlDbType.NVarChar);
        param[09] = new SqlParameter("@Email", SqlDbType.NVarChar);
        param[10] = new SqlParameter("@MoreDes", SqlDbType.NVarChar);
        param[11] = new SqlParameter("@Situ", SqlDbType.NVarChar);
        param[12] = new SqlParameter("@Isdeleted", SqlDbType.NVarChar);
        param[13] = new SqlParameter("@IntMethod", SqlDbType.NVarChar);


        param[00].Value = SCodeTextBox.Text;
        param[01].Value = "";
        param[02].Value = QNameFamilyTextBox.Text;
        param[03].Value = "";
        param[04].Value = "";
        param[05].Value = QFieldTextBox.Text;
        param[06].Value = "";
        param[07].Value = "";
        param[08].Value = QMobTextBox.Text;
        param[09].Value = "";
        param[10].Value = "";
        param[11].Value = "جدید";
        param[12].Value = 0;
        param[13].Value = "";



        for (int i = 0; i < param.Length; i++)
        {
            RegCmd.Parameters.Add(param[i]);
        }

        try
        {

            con.Open();
            RegCmd.ExecuteReader();


            QNameFamilyTextBox.Text = "";
            QFieldTextBox.Text = "";
            QMobTextBox.Text = "";
            SCodeTextBox.Text = "";

            InfoLabel.Text = "اطلاعات شما با موفقیت ثبت شد. با شما در تماس خواهیم بود.";
            InfoLabel.Visible = true;
        }
        catch (System.Data.SqlClient.SqlException ex)
        {
            string msg = "RegCmd Error:";
            msg += ex.Message;
            throw new Exception(msg + RegSqlSyntax);
        }
        finally
        {
            con.Close();
        }
    }
}