﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Farsi_Admin_Logout : System.Web.UI.Page
{
    protected void Page_Init(object sender, EventArgs e)
    {
        Session.Clear();
        Response.Cookies["admin"].Value = "false";
        Response.Redirect(SSClass.WebDomain);
    }
}