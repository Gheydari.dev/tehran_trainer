﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Farsi_Blog_Page : System.Web.UI.Page
{
    protected string DisplayDate(object Date)
    {
        DateTime CDate = Convert.ToDateTime(Date);
        if ((DateTime.Now - CDate).TotalDays < 1)
        {
            return "امروز";
        }
        else if ((DateTime.Now - CDate).TotalDays < 2)
        {
            return "دیروز";
        }
        else if ((DateTime.Now - CDate).TotalDays < 3)
        {
            return "2 روز پیش";
        }
        else
        {
            PersianDateTime PersianDate = new PersianDateTime(Convert.ToDateTime(Date));
            return PersianDate.ToString("dddd d MMMM yyyy");
        }
    }
    protected string DisplayCommentDate(object Date)
    {
        DateTime CDate = Convert.ToDateTime(Date);
        if ((DateTime.Now - CDate).TotalDays < 1)
        {
            return "امروز ساعت "+CDate.ToShortTimeString();
        }
        else if ((DateTime.Now - CDate).TotalDays < 2)
        {
           return "دیروز ساعت " + CDate.ToShortTimeString();

        }
        else if ((DateTime.Now - CDate).TotalDays < 3)
        {
            return "2 روز پیش ساعت " + CDate.ToShortTimeString();

        }
        else
        {
            PersianDateTime PersianDate = new PersianDateTime(Convert.ToDateTime(Date));
            return PersianDate.ToString("dddd d MMMM yyyy");
        }
    }
    protected string SetSlider(object Input)
    {
        string PageSourceString = Input.ToString();
        string SubPageSourceString = PageSourceString;
        string TakenSIDsString = "";
        string TempForSubPageSourceString = "";


        for (int j = 0; j < 30; j++)
        {

            string TokenString = "";


            int ix = 0;

            if (SubPageSourceString.Contains("###"))
            {
                ix = SubPageSourceString.IndexOf("###");
                TempForSubPageSourceString = SubPageSourceString;


                if (ix != -1)
                {
                    SubPageSourceString = SubPageSourceString.Substring(ix + 10);

                    TokenString = TempForSubPageSourceString.Substring(ix, 10).Replace("#", ""); // take next 10 characters
                    string SIDCode = "";
                    for (int i = 0; i < 20; i++)
                    {
                        Int64 CharacterInt64;
                        bool CharacterResult = Int64.TryParse((TokenString.Substring(i, 1)), out CharacterInt64);

                        if (CharacterResult)
                        {
                            SIDCode += CharacterInt64.ToString();
                        }
                        else
                        {
                            break;
                        }


                    }
                    if (SIDCode.ToString().Trim() != "")
                    {
                        TakenSIDsString += SIDCode + ",";

                    }
                }
            }
        }

        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);

        string SQLQuerry = " SELECT sid,replace(replace(replace(slidercode,N'kamtar',N'<'),N'bishtar',N'>'),N'&quot;',N'\"') as slidercode FROM [slider] WHERE [sid] in (" + TakenSIDsString + "0) ";
        SqlCommand FetchUserCmd = new SqlCommand(SQLQuerry, con);

        try
        {
            con.Open();
            SqlDataReader FetchUserDr = FetchUserCmd.ExecuteReader();

            while (FetchUserDr.Read())
            {
                PageSourceString = PageSourceString.Replace("###" + FetchUserDr["sid"].ToString() + "###", FetchUserDr["slidercode"].ToString());
            }

        }
        catch (System.Data.SqlClient.SqlException ex)
        {
            return ("CatchError:" + ex.Message);
        }
        finally
        {
            con.Close();
        }

        return PageSourceString;
    }

    protected string SetCss(object ItemIndex)
    {
        if (Convert.ToInt32(ItemIndex) == 0)
        {

            return "item active";
        }
        else
        {
            return ("item");
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        string CID = "1";
        if (Page.RouteData.Values["cid"] != null)
        {
            CID = Page.RouteData.Values["cid"].ToString().Trim();
        }
        else if (Request.QueryString["nid"] != null)
        {
            CID = Request.QueryString["nid"].ToString().Trim();
        }
        CIDMainLabel.Text = CID;
        //Page.Title = Request.QueryString["Title"].Replace("-", " ") + " | " + "پاویش";
        if (!IsPostBack)
        {
            string ContentTag = SSClass.FTitle+",";
            //HtmlMeta keywords = new HtmlMeta { Name = "keywords", Content = "one,two,three" };
            //Header.Controls.Add(keywords);
            //Page.Header.DataBind();
            DataLayer dl = new DataLayer();
            string CS = dl.dataLayerConnectionString;
            SqlConnection con = new SqlConnection(CS);

            string FetchUserSqlSyntax = " select * from ContentKeyword where cid=@cid; select content.*,  dbo.Tree.Name AS cat2title FROM dbo.[Content] INNER JOIN dbo.Tree ON dbo.[Content].cat2 = dbo.Tree.NodeID WHERE cid=@cid ";
            SqlCommand FetchUserCmd = new SqlCommand(FetchUserSqlSyntax, con);
            SqlParameter[] FetchUserParam = new SqlParameter[1];

            FetchUserParam[0] = new SqlParameter("@cid", SqlDbType.NVarChar);

            FetchUserParam[0].Value = CID;

            for (int i = 0; i < FetchUserParam.Length; i++)
            {
                FetchUserCmd.Parameters.Add(FetchUserParam[i]);
            }
            SqlDataReader UserDr;
            try
            {
                con.Open();
                UserDr = FetchUserCmd.ExecuteReader();
                KeyWordsLiteral.Text = "<meta name=\"keywords\" content=\"";
                DescriptionLiteral.Text = "<meta name=\"description\" content=\"";
                while (UserDr.Read())
                {
                    ContentTag += UserDr["keyword"].ToString() + ", ";
                }
                ContentTag += SSClass.FTitle;
                KeyWordsLiteral.Text += ContentTag;
                KeyWordsLiteral.Text += "\" /> ";
                UserDr.NextResult();
                if (UserDr.Read())
                {
                    CanonicalLiteral.Text = "<link rel=\"canonical\" href=\"" + SSClass.WebDomain + "/"+ MyFunc.SafeString(UserDr["cat2title"]) + "/" + MyFunc.SafeString(UserDr["header"]) + "/" + UserDr["cid"] + "\"/>";
                    //Page.MetaDescription = SSClass.FTitle + " | " + UserDr["Summary"].ToString();
                    Page.Title = UserDr["header"].ToString() + " | "+SSClass.FTitle;
                    DescriptionLiteral.Text+= UserDr["summary"].ToString();
                    DescriptionLiteral.Text += "\" /> ";

                }

            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                string msg = "FetchTagsCmd Error:";
                msg += ex.Message;
                throw new Exception(msg);
            }

            finally
            {
                //con.Close();
            }
            //HtmlMeta keywords = new HtmlMeta();
            //keywords.HttpEquiv = "keywords";
            //keywords.Name = "keywords";
            //keywords.Content = "1,2,3,4";
            //this.Page.Header.Controls.Add(keywords);
            
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        string CID = "1";
        if (Page.RouteData.Values["cid"] != null)
        {
            CID = Page.RouteData.Values["cid"].ToString().Trim();
        }
        else if (Request.QueryString["nid"] != null)
        {
            CID = Request.QueryString["nid"].ToString().Trim();
        }
        //string cat5 = Request.QueryString["cat5"];
        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);

        string SQLQuerry = "update content set pageview=pageview+1 where cid=@cid";

        SqlCommand RegCmd = new SqlCommand(SQLQuerry, con);
        SqlParameter[] param = new SqlParameter[1];

        param[00] = new SqlParameter("@cid", SqlDbType.NVarChar);
        param[00].Value = CID;



        for (int r = 0; r < param.Length; r++)
        {
            RegCmd.Parameters.Add(param[r]);
        }

        try
        {
            con.Open();
            RegCmd.ExecuteScalar();

            //InfoDiv.Visible = true;
            //InfoLiteral.Text = "آیتم جدید با موفقیت افزوده شد.";
            //InfoDiv.Attributes["class"] = " alert alert-success alert-dismissable text-right";

            //Response.Redirect("Points.aspx?pid=" + pid + "&cat5=" + cat5);

            //FNameTextBox.Text =
            //ENameTextBox.Text =
            //PCodeTextBox.Text =
            //ModelTextBox.Text =
            //BrandTextBox.Text =
            //CountryTextBox.Text =
            //YearTextBox.Text =
            //AvailableTextBox.Text =
            //DiscountTextBox.Text =
            //ColorTextBox.Text =
            //PriceTextBox.Text =
            //WeighTextBox.Text =
            //DimensionTextBox.Text = "";
        }
        catch (System.Data.SqlClient.SqlException ex)
        {
            string msg = "RegCmd Error:";
            msg += ex.Message;
            throw new Exception(msg);
        }
        finally
        {
            con.Close();
        }
    }
    protected void IsReplyCheckBox_CheckedChanged(object sender, EventArgs e)
    {
        if (!IsReplyCheckBox.Checked)
        {
            IsReplyCheckBox.Visible = false;
            CIDReplyLabel.Text = "0";
            SubmitComment.Text = "ثبت نظر";
        }

    }
    protected void SubmitComment_Click(object sender, EventArgs e)
    {
        if (Session["ispublic"] == "false")
        {


            //string pid = Request.QueryString["pid"];
            string pid = Page.RouteData.Values["pid"].ToString().Trim();

            DataLayer dl = new DataLayer();
            string CS = dl.dataLayerConnectionString;
            SqlConnection con = new SqlConnection(CS);

            string SQLQuerry = "INSERT INTO comments (pid,commentbody,userid)values(@pid,@commentbody,@userid)";

            if (Convert.ToInt32(CIDReplyLabel.Text) == 0)
            {

            }
            else
            {
                SQLQuerry = "INSERT INTO commentsreply (cid,replybody,userid)values(@cid,@replybody,@userid)";
            }
            SqlCommand RegCmd = new SqlCommand(SQLQuerry, con);
            SqlParameter[] param = new SqlParameter[5];

            param[00] = new SqlParameter("@pid", SqlDbType.Int);
            param[01] = new SqlParameter("@commentbody", SqlDbType.NVarChar);
            param[02] = new SqlParameter("@userid", SqlDbType.NVarChar);

            param[03] = new SqlParameter("@cid", SqlDbType.NVarChar);
            param[04] = new SqlParameter("@replybody", SqlDbType.NVarChar);

            param[00].Value = pid;
            param[01].Value = CommentBodyTextBox.Text.Replace("\n", "<br />").Replace("\r\n", "<br />");
            param[02].Value = Session["userid"].ToString();
            param[03].Value = CIDReplyLabel.Text;
            param[04].Value = CommentBodyTextBox.Text.Replace("\n", "<br />").Replace("\r\n", "<br />");

            for (int r = 0; r < param.Length; r++)
            {
                RegCmd.Parameters.Add(param[r]);
            }

            try
            {
                con.Open();
                RegCmd.ExecuteScalar();
                CommentsRepeater.DataBind();
                CommentsDiv.Visible = true;

                if (Convert.ToInt32(CIDReplyLabel.Text) == 0)
                {
                    CommentsLiteral.Text = "نظر شما با موفقیت ثبت شد.";

                }
                else
                {
                    CommentsLiteral.Text = "پاسخ شما با موفقیت ثبت شد.";
                }


                CommentsDiv.Attributes["class"] = " alert alert-success alert-dismissable text-right";
                CommentBodyTextBox.Text = "";

                IsReplyCheckBox.Visible = false;
                CIDReplyLabel.Text = "0";
                SubmitComment.Text = "ثبت نظر";

            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                string msg = "RegCmd Error:";
                msg += ex.Message;
                throw new Exception(msg);
            }
            finally
            {
                con.Close();
            }
        }
        else
        {
            LoggedOutPanel.Visible = true;
            LoggedInPanel.Visible = false;

        }
    }

    protected void CommentsRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Reply")
        {
            CIDReplyLabel.Text = e.CommandArgument.ToString();
            IsReplyCheckBox.Visible = true;
            IsReplyCheckBox.Checked = true;
            SubmitComment.Text = "ثبت پاسخ";
            CommentBodyTextBox.Focus();


        }
    }
    protected void CommentsRepeater_PreRender(object sender, EventArgs e)
    {
        if (Session["name"] == null)
        {

            for (int i = 0; i < CommentsRepeater.Items.Count; i++)
            {
                Button ReplyButton = (Button)CommentsRepeater.Items[i].FindControl("ReplyButton");
                ReplyButton.Visible = false;
            }

        }
        else
        {
            for (int i = 0; i < CommentsRepeater.Items.Count; i++)
            {
                Button ReplyButton = (Button)CommentsRepeater.Items[i].FindControl("ReplyButton");
                ReplyButton.Visible = true;
            }

        }
    }
    protected string AdminComment(object UserID)
    {
        if (Convert.ToInt32(UserID.ToString()) == 0 || Convert.ToInt32(UserID.ToString()) == 10)
        {
            return "پاویش - واحد پاسخ گویی به سوالات: ";
        }
        else
        {
            return "";
        }

    }
    protected string CommentCss(object UserID)
    {
        if (Convert.ToInt32(UserID.ToString()) == 0 || Convert.ToInt32(UserID.ToString()) == 10)
        {
            return "CommentsReplyAdmin";
        }
        else
        {
            return "CommentsReply";
        }

    }
    protected void QuickRegButton_Click(object sender, EventArgs e)
    {
        string CS = DataLayer.connectionString;
        SqlConnection con = new SqlConnection(CS);
        string RegSqlSyntax = @"begin DECLARE @myInsertedTable table (insertedid  bigint NOT NULL)
                                INSERT INTO [AskForm] (                                                         
                                                         Userid
                                                        ,Name
                                                        ,Family
                                                        ,EduGroup
                                                        ,Field
                                                        ,Department
                                                        ,Service
                                                        ,PhoneNo
                                                        ,Email
                                                        ,MoreDes
                                                        ,Situ
                                                        ,Isdeleted
                                                        ,IntMethod
                                                        ,SCode
                                                        ) OUTPUT inserted.AFID into @myInsertedTable   VALUES (
                                                         @Userid
                                                        ,@Name
                                                        ,@Family
                                                        ,@EduGroup
                                                        ,@Field
                                                        ,@Department
                                                        ,@Service
                                                        ,@PhoneNo
                                                        ,@Email
                                                        ,@MoreDes
                                                        ,@Situ
                                                        ,@Isdeleted
                                                        ,@IntMethod
                                                        ,@SCode
                                                        ) ";

        RegSqlSyntax += " end ";
        SqlCommand RegCmd = new SqlCommand(RegSqlSyntax, con);
        SqlParameter[] param = new SqlParameter[14];

        param[00] = new SqlParameter("@SCode", SqlDbType.NVarChar);
        param[01] = new SqlParameter("@Userid", SqlDbType.NVarChar);
        param[02] = new SqlParameter("@Name", SqlDbType.NVarChar);
        param[03] = new SqlParameter("@Family", SqlDbType.NVarChar);
        param[04] = new SqlParameter("@EduGroup", SqlDbType.NVarChar);
        param[05] = new SqlParameter("@Field", SqlDbType.NVarChar);
        param[06] = new SqlParameter("@Department", SqlDbType.NVarChar);
        param[07] = new SqlParameter("@Service", SqlDbType.NVarChar);
        param[08] = new SqlParameter("@PhoneNo", SqlDbType.NVarChar);
        param[09] = new SqlParameter("@Email", SqlDbType.NVarChar);
        param[10] = new SqlParameter("@MoreDes", SqlDbType.NVarChar);
        param[11] = new SqlParameter("@Situ", SqlDbType.NVarChar);
        param[12] = new SqlParameter("@Isdeleted", SqlDbType.NVarChar);
        param[13] = new SqlParameter("@IntMethod", SqlDbType.NVarChar);


        param[00].Value = SCodeTextBox.Text;
        param[01].Value = "";
        param[02].Value = QNameFamilyTextBox.Text;
        param[03].Value = "";
        param[04].Value = "";
        param[05].Value = QFieldTextBox.Text;
        param[06].Value = "";
        param[07].Value = "";
        param[08].Value = QMobTextBox.Text;
        param[09].Value = "";
        param[10].Value = "";
        param[11].Value = "جدید";
        param[12].Value = 0;
        param[13].Value = "";



        for (int i = 0; i < param.Length; i++)
        {
            RegCmd.Parameters.Add(param[i]);
        }

        try
        {

            con.Open();
            RegCmd.ExecuteReader();


            QNameFamilyTextBox.Text = "";
            QFieldTextBox.Text = "";
            QMobTextBox.Text = "";
            SCodeTextBox.Text = "";

            InfoLabel.Text = "اطلاعات شما با موفقیت ثبت شد. با شما در تماس خواهیم بود.";
            InfoLabel.Visible = true;
        }
        catch (System.Data.SqlClient.SqlException ex)
        {
            string msg = "RegCmd Error:";
            msg += ex.Message;
            throw new Exception(msg + RegSqlSyntax);
        }
        finally
        {
            con.Close();
        }
    }
}