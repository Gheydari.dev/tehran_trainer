﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Farsi/Admin/FaAdminMasterPage.master" AutoEventWireup="true" CodeFile="GenderDefiner.aspx.cs" Inherits="Farsi_Admin_GenderDefiner" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">  
    <div class="row">
        <div class="col-md-12">
            <div runat="server" id="InfoDiv" class="alert alert-info alert-dismissable text-right">
                <asp:Literal ID="InfoLiteral" runat="server">از این قسمت می توانید جنسیت کاربران را تعیین کنید.</asp:Literal>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <asp:HiddenField ID="hdnUserId" runat="server" />
            <asp:GridView ID="GridView1" runat="server" CssClass="table-bordered table table-condensed text-right" CellPadding="4" AutoGenerateColumns="False"
                ForeColor="Black" GridLines="Horizontal" PageSize="40" OnRowDataBound="GridView1_RowDataBound" OnPageIndexChanging="GridView1_PageIndexChanging" AllowPaging="True" DataSourceID="SqlDataSource1" Width="100%" SortedDescendingHeaderStyle-VerticalAlign="NotSet" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
                <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" CssClass="text-center" />
                <PagerSettings NextPageText="بعدی" PreviousPageText="قبلی" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                <SelectedRowStyle BackColor="#CC3333" ForeColor="White" Font-Bold="True" />
                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#242121" />
                <Columns>
                    <asp:TemplateField HeaderText="کد کاربری">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblUserId" CssClass="fieldNamejq" Text='<%# Bind("userid") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="جنسیت">
                        <ItemTemplate>
                            <asp:RadioButtonList Font-Size="Large" ID="RadioButtonList1" runat="server" Enabled="true" RepeatDirection="Horizontal" CellSpacing="10" CellPadding="5" RepeatLayout="Table" Width="100%">
                                <asp:ListItem Value="0">مرد</asp:ListItem>
                                <asp:ListItem Value="1">زن</asp:ListItem>
                            </asp:RadioButtonList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="نام">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblFName" Text='<%# Bind("Name") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="نام خانوادگی">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblLName" Text='<%# Bind("Family") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="موبایل">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblMobPhone" Text='<%# Bind("MobPhone") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>

            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DrB_DBConnectionString %>"
                SelectCommand="SELECT [userid], [name], [family], [mobphone], [gender] FROM [users] order by cdate desc"
                UpdateCommand="UPDATE [users] SET [gender] = @gender WHERE [userid] = @userid">
                <UpdateParameters>
                    <asp:Parameter Name="userid" Type="Int32" />
                    <asp:Parameter Name="gender" Type="String" />
                </UpdateParameters>

            </asp:SqlDataSource>
            <asp:Button CssClass="btn btn-primary" ID="Button1" runat="server" Text="ثبت اطلاعات" OnClick="Button1_Click" />

        </div>
    </div>

</asp:Content>

