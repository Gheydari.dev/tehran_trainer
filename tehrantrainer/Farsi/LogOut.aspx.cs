﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Farsi_LogOut : System.Web.UI.Page
{
    protected void Page_Init(object sender, EventArgs e)
    {
        Page.Title = "خروج از حساب کاربری";

        Session.Clear();
        Response.Cookies.Clear();
        Response.Redirect(SSClass.WebDomain);
    }
}