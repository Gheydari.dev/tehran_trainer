﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for MultipleDelete
/// </summary>
public class MultipleOperations
{
    public void DeleteRegs(StringCollection sc)
    {
        DataLayer dl = new DataLayer();
        string CS=dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);
        StringBuilder sb = new StringBuilder(string.Empty);

        foreach (string item in sc)
        {
            const string sqlStatement = "DELETE FROM users WHERE userid";
            sb.AppendFormat("{0}='{1}'; ", sqlStatement, item);
            
        }
        try
        {
            con.Open();
            SqlCommand cmd = new SqlCommand(sb.ToString(), con);
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
        }
        catch (System.Data.SqlClient.SqlException ex)
        {
            string msg = "DELETE reg request Error:";
            msg += ex.Message;
            throw new Exception(msg);
        }
        finally
        {
            con.Close();
        }
    }
     
    public void Approve(StringCollection sc)
    {
        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);
        StringBuilder sb = new StringBuilder(string.Empty);

        foreach (string item in sc)
        {
            const string sqlStatement = "UPDATE users SET regsitu=N'true' WHERE userid";
            sb.AppendFormat("{0}='{1}'; ", sqlStatement, item);

        }
        try
        {
            con.Open();
            SqlCommand cmd = new SqlCommand(sb.ToString(), con);
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
        }
        catch (System.Data.SqlClient.SqlException ex)
        {
            string msg = "Approve reg request Error:";
            msg += ex.Message;
            throw new Exception(msg);
        }
        finally
        {
            con.Close();
        }
    }

    public void NewsTagsAndGroups(StringCollection sc)
    {
        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);
        StringBuilder sb = new StringBuilder(string.Empty);

        foreach (string item in sc)
        {
            const string sqlStatement = "UPDATE users SET regsitu=N'true' WHERE userid";
            sb.AppendFormat("{0}='{1}'; ", sqlStatement, item);

        }
        try
        {
            con.Open();
            SqlCommand cmd = new SqlCommand(sb.ToString(), con);
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
        }
        catch (System.Data.SqlClient.SqlException ex)
        {
            string msg = "Approve reg request Error:";
            msg += ex.Message;
            throw new Exception(msg);
        }
        finally
        {
            con.Close();
        }
    }

}