﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Farsi_Admin_SlidersMng : System.Web.UI.Page
{
    protected string InsertTitle(object Title, object Index)
    {
        if (Convert.ToInt32(Index.ToString()) == 0)
        {
            return Title.ToString();
        }
        else
        {
            return "";
        }
    }
    protected bool SetSituChecked(object Situ)
    {
        if (Situ.ToString() == "show")
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    protected bool SetCnterChecked(object Situ)
    {
        if (Situ.ToString() == "hide")
        {
            return false;


        }
        else
        {
            return true;
        }

    }
    protected string SetCnterTextBox(object Situ)
    {
        if (Situ.ToString() == "hide")
        {
            return "0";
        }
        else
        {
            if (Convert.ToInt64(Situ.ToString()) > 10)
            {
                return Situ.ToString();
            }
            else
            {
                return "0";
            }
        }

    }
    protected string SetCnterDropdown(object Situ)
    {
        if (Situ.ToString() == "hide")
        {
            return "0";
        }
        else
        {
            if (Convert.ToInt64(Situ.ToString()) < 10)
            {
                return Situ.ToString();
            }
            else
            {
                return "0";
            }
        }

    }

    protected void Page_Init(object sender, EventArgs e)
    {
        //if (SlidesRepeater.Items.Count == 0)
        //{
        //    UploadPhotosButton.Visible = false;
        //}
        //else
        //{
        //    UploadPhotosButton.Visible = true;

        //}

    }
    protected bool SetClockViz(object Cnter)
    {
        if (Cnter.ToString() == "hide")
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        //System.Web.UI.HtmlControls.HtmlAnchor li = (System.Web.UI.HtmlControls.HtmlAnchor)this.Page.Master.Master.FindControl("BodyContent").FindControl("SliderMng");

        //li.Attributes["class"] = "btn btn-primary";
    }

    protected void UploadPhotosButton_Click(object sender, EventArgs e)
    {
        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);

        for (int i = 0; i < SlidesRepeater.Items.Count; i++)
        {
            string flag = "go";
            FileUpload PhotoFileUpload = (FileUpload)SlidesRepeater.Items[i].FindControl("PhotoFileUpload");
            Label MsgLabel = (Label)SlidesRepeater.Items[i].FindControl("MsgLabel");
            Label SIIDLabel = (Label)SlidesRepeater.Items[i].FindControl("SIIDLabel");
            TextBox OrderNumTextBox = (TextBox)SlidesRepeater.Items[i].FindControl("OrderNumTextBox");
            TextBox TitleTextBox = (TextBox)SlidesRepeater.Items[i].FindControl("TitleTextBox");
            TextBox DesTextBox = (TextBox)SlidesRepeater.Items[i].FindControl("DesTextBox");
            Image SlideImage = (Image)SlidesRepeater.Items[i].FindControl("SlideImage");
            TextBox HrefTextBox = (TextBox)SlidesRepeater.Items[i].FindControl("HrefTextBox");
            CheckBox SituCheckBox = (CheckBox)SlidesRepeater.Items[i].FindControl("SituCheckBox");
            TextBox StartDateTextBox = (TextBox)SlidesRepeater.Items[i].FindControl("StartDateTextBox");
            CheckBox ShowCnterCheckBox = (CheckBox)SlidesRepeater.Items[i].FindControl("ShowCnterCheckBox");
            TextBox ColmdTextBox = (TextBox)SlidesRepeater.Items[i].FindControl("ColmdTextBox");
            TextBox ColsmTextBox = (TextBox)SlidesRepeater.Items[i].FindControl("ColsmTextBox");
            TextBox ColxsTextBox = (TextBox)SlidesRepeater.Items[i].FindControl("ColxsTextBox");


            Int64 number;
            bool result = Int64.TryParse(OrderNumTextBox.Text.Trim(), out number);
            if (!result)
            {
                flag = "stop";
            }

            //SlideImage.ImageUrl = Eval("imgsrc").ToString();
            System.Web.UI.HtmlControls.HtmlGenericControl PhotoDiv = (System.Web.UI.HtmlControls.HtmlGenericControl)SlidesRepeater.Items[i].FindControl("PhotoDiv"); ;

            if (PhotoFileUpload.HasFile)
            {
                if (flag == "go")
                {
                    string SecCode = Guid.NewGuid().ToString("N").Substring(12);
                    string fileExt = System.IO.Path.GetExtension(PhotoFileUpload.FileName.ToLower());
                    if (fileExt == ".gif" || fileExt == ".jpeg" || fileExt == ".jpg" || fileExt == ".png")
                    {
                        long size = PhotoFileUpload.PostedFile.ContentLength;
                        size = size / 1024; //to KB

                        if (size < 210)
                        {
                            PhotoFileUpload.SaveAs(Server.MapPath("~/images/slides/" + SecCode + fileExt));
                            //MsgLabel.Text = "تصویر با موفقیت ذخیره شد.";
                            //MsgLabel.ForeColor = System.Drawing.Color.Green;
                            //PhotoDiv.Attributes["Style"] = "background-color: #e6e6e6; border: solid; border-width: 2px; border-color: green; border-radius: 4px 5px; margin: 0px; padding: 5px";
                            Session[i.ToString()] = "background-color: #e6e6e6; border: solid; border-width: 2px; border-color: green; border-radius: 4px 5px; margin: 0px; padding: 5px";

                            //string RegSqlSyntax = "insert into slider (imgsrc,imgorder,imggroup) values (@imgsrc,@imgorder,@imggroup)";
                            string RegSqlSyntax = "update sliderimages set src=@src,ordernum=@ordernum,href=@href,title=@title,situ=@situ,des=@des,cnter=@cnter,colmd=@colmd,colsm=@colsm,colxs=@colxs where siid=@siid update slider set slidercode=@slidercode where sid=@sid ";
                            string item = SlideImage.ImageUrl.ToString().ToLower().Replace(SSClass.WebDomainFile + "/", "~/");
                            SqlCommand RegCmd = new SqlCommand(RegSqlSyntax, con);
                            SqlParameter[] param = new SqlParameter[13];

                            param[00] = new SqlParameter("@src", SqlDbType.NVarChar);
                            param[01] = new SqlParameter("@ordernum", SqlDbType.Int);
                            param[02] = new SqlParameter("@siid", SqlDbType.Int);
                            param[03] = new SqlParameter("@href", SqlDbType.NVarChar);
                            param[04] = new SqlParameter("@title", SqlDbType.NVarChar);
                            param[05] = new SqlParameter("@situ", SqlDbType.NVarChar);
                            param[06] = new SqlParameter("@des", SqlDbType.NVarChar);
                            param[07] = new SqlParameter("@cnter", SqlDbType.NVarChar);
                            param[08] = new SqlParameter("@colmd", SqlDbType.NVarChar);
                            param[09] = new SqlParameter("@colsm", SqlDbType.NVarChar);
                            param[10] = new SqlParameter("@colxs", SqlDbType.NVarChar);

                            param[00].Value = SSClass.WebDomainFile + "/Images/slides/" + SecCode + fileExt;
                            param[01].Value = OrderNumTextBox.Text;
                            param[02].Value = SIIDLabel.Text;
                            param[03].Value = HrefTextBox.Text;
                            param[04].Value = TitleTextBox.Text;




                            if (SituCheckBox.Checked)
                            {
                                param[05].Value = "show";
                            }
                            else
                            {
                                param[05].Value = "hide";

                            }
                            param[06].Value = DesTextBox.Text;
                            if (ShowCnterCheckBox.Checked)
                            {
                                param[07].Value = StartDateTextBox.Text;
                            }
                            else
                            {
                                param[07].Value = "hide";

                            }
                            param[08].Value = ColmdTextBox.Text;
                            param[09].Value = ColsmTextBox.Text;
                            param[10].Value = ColxsTextBox.Text;

                            param[11] = new SqlParameter("@sid", SqlDbType.Int);
                            param[12] = new SqlParameter("@slidercode", SqlDbType.NVarChar);

                            param[11].Value = SliderSelectorDropDownList.SelectedValue;
                            param[12].Value = SliderCodeTextArea.InnerHtml;


                            for (int r = 0; r < param.Length; r++)
                            {
                                RegCmd.Parameters.Add(param[r]);
                            }

                            try
                            {
                                con.Open();
                                if ((item != null || item != String.Empty) && item != "~/images/slides/newdefault.png")
                                {
                                    if (System.IO.File.Exists(HttpContext.Current.Server.MapPath((item))))
                                    {
                                        System.IO.File.Delete(HttpContext.Current.Server.MapPath((item)));
                                    }
                                }
                                RegCmd.ExecuteReader();
                            }
                            catch (System.Data.SqlClient.SqlException ex)
                            {
                                string msg = "RegCmd Error:";
                                msg += ex.Message;
                                throw new Exception(msg);
                            }
                            finally
                            {
                                con.Close();
                            }
                        }
                        else
                        {
                            //MsgLabel.Text = "حجم تصویر بیش از 100KB است.";
                            //MsgLabel.ForeColor = System.Drawing.Color.Red;
                            //PhotoDiv.Attributes["Style"] = "background-color: #e6e6e6; border: solid; border-width: 2px; border-color: orange; border-radius: 4px 5px; margin: 0px; padding: 5px";
                            //SlideImage.DataBind();
                            Session[i.ToString()] = "background-color: #e6e6e6; border: solid; border-width: 2px; border-color: orange; border-radius: 4px 5px; margin: 0px; padding: 5px";

                        }
                    }
                    else
                    {
                        //MsgLabel.Text = "قالب تصویر پشتیبانی نمی شود.";
                        //MsgLabel.ForeColor = System.Drawing.Color.Red;
                        //PhotoDiv.Attributes["Style"] = "background-color: #e6e6e6; border: solid; border-width: 2px; border-color: red; border-radius: 4px 5px; margin: 0px; padding: 5px";
                        Session[i.ToString()] = "background-color: #e6e6e6; border: solid; border-width: 2px; border-color: red; border-radius: 4px 5px; margin: 0px; padding: 5px";


                    }
                }
                else if (flag == "stop")
                {
                    //PhotoDiv.Attributes["Style"] = "background-color: #e6e6e6; border: solid; border-width: 2px; border-color: red; border-radius: 4px 5px; margin: 0px; padding: 5px";
                    Session[i.ToString()] = "background-color: #e6e6e6; border: solid; border-width: 2px; border-color: black; border-radius: 4px 5px; margin: 0px; padding: 5px";
                }

            }
            else
            {
                if (flag == "go")
                {
                    string RegSqlSyntax = "update sliderimages set ordernum=@ordernum,href=@href,title=@title,situ=@situ,des=@des,cnter=@cnter,colmd=@colmd,colsm=@colsm,colxs=@colxs  where siid=@siid update slider set slidercode=@slidercode where sid=@sid ";
                    string item = SlideImage.ImageUrl.ToString();
                    SqlCommand RegCmd = new SqlCommand(RegSqlSyntax, con);
                    SqlParameter[] param = new SqlParameter[12];

                    param[00] = new SqlParameter("@ordernum", SqlDbType.Int);
                    param[01] = new SqlParameter("@siid", SqlDbType.Int);
                    param[02] = new SqlParameter("@href", SqlDbType.NVarChar);
                    param[03] = new SqlParameter("@Title", SqlDbType.NVarChar);
                    param[04] = new SqlParameter("@situ", SqlDbType.NVarChar);
                    param[05] = new SqlParameter("@des", SqlDbType.NVarChar);
                    param[06] = new SqlParameter("@cnter", SqlDbType.NVarChar);
                    param[07] = new SqlParameter("@colmd", SqlDbType.NVarChar);
                    param[08] = new SqlParameter("@colsm", SqlDbType.NVarChar);
                    param[09] = new SqlParameter("@colxs", SqlDbType.NVarChar);

                    param[00].Value = OrderNumTextBox.Text;
                    param[01].Value = SIIDLabel.Text;
                    param[02].Value = HrefTextBox.Text;
                    param[03].Value = TitleTextBox.Text;


                    param[10] = new SqlParameter("@sid", SqlDbType.Int);
                    param[11] = new SqlParameter("@slidercode", SqlDbType.NVarChar);

                    param[10].Value = SliderSelectorDropDownList.SelectedValue;
                    param[11].Value = SliderCodeTextArea.InnerHtml;

                    if (SituCheckBox.Checked)
                    {
                        param[04].Value = "show";
                    }
                    else
                    {
                        param[04].Value = "hide";

                    }
                    param[05].Value = DesTextBox.Text;
                    if (ShowCnterCheckBox.Checked)
                    {
                        param[06].Value = StartDateTextBox.Text;
                    }
                    else
                    {
                        param[06].Value = "hide";

                    }
                    param[07].Value = ColmdTextBox.Text;
                    param[08].Value = ColsmTextBox.Text;
                    param[09].Value = ColxsTextBox.Text;


                    for (int r = 0; r < param.Length; r++)
                    {
                        RegCmd.Parameters.Add(param[r]);
                    }

                    try
                    {

                        con.Open();
                        RegCmd.ExecuteReader();
                        Session[i.ToString()] = "background-color: #e6e6e6; border: solid; border-width: 2px; border-color: green; border-radius: 4px 5px; margin: 0px; padding: 5px";



                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        string msg = "RegCmd Error:";
                        msg += ex.Message;
                        throw new Exception(msg);
                    }
                    finally
                    {
                        con.Close();
                    }
                }
                else if (flag == "stop")
                {
                    Session[i.ToString()] = "background-color: #e6e6e6; border: solid; border-width: 2px; border-color: black; border-radius: 4px 5px; margin: 0px; padding: 5px";
                }
            }


        }
        SlidesRepeater.DataBind();
        MainSliderRepeater.DataBind();
        for (int i = 0; i < SlidesRepeater.Items.Count; i++)
        {
            FileUpload PhotoFileUpload = (FileUpload)SlidesRepeater.Items[i].FindControl("PhotoFileUpload");
            if (true)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl PhotoDiv = (System.Web.UI.HtmlControls.HtmlGenericControl)SlidesRepeater.Items[i].FindControl("PhotoDiv");
                PhotoDiv.Attributes["style"] = Session[i.ToString()].ToString();
            }


        }
        InfoDiv.Visible = true;
        InfoLiteral.Text = "نتیجه بارگذاری با توجه به رنگ کادر هر تصویر و راهنمای موجود در زیر، قابل مشاهده است.";
        InfoDiv.Attributes["class"] = " alert alert-info alert-dismissable text-right";
        legendDiv.Visible = true;
    }
    protected void SlidesRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string[] Args = e.CommandArgument.ToString().Split('-');
        if (e.CommandName == "deletSlide")
        {
            DataLayer dl = new DataLayer();
            string CS = dl.dataLayerConnectionString;
            SqlConnection con = new SqlConnection(CS);
            string item = Args[1].ToLower().Replace(SSClass.WebDomainFile + "/", "~/");
            string RegSqlSyntax = "delete from sliderimages where siid=@siid";

            SqlCommand RegCmd = new SqlCommand(RegSqlSyntax, con);
            SqlParameter[] param = new SqlParameter[1];

            param[00] = new SqlParameter("@siid", SqlDbType.Int);

            param[00].Value = Args[0];


            for (int i = 0; i < param.Length; i++)
            {
                RegCmd.Parameters.Add(param[i]);
            }

            try
            {

                con.Open();
                if (item != null || item != String.Empty)
                {
                    if (System.IO.File.Exists(HttpContext.Current.Server.MapPath(item)) && item != "~/images/slides/newdefault.png")
                    {
                        System.IO.File.Delete(HttpContext.Current.Server.MapPath(item));
                    }
                }
                RegCmd.ExecuteReader();

                SlidesRepeater.DataBind();
                MainSliderRepeater.DataBind();

                InfoDiv.Visible = true;
                InfoLiteral.Text = "اسلاید مورد نظر با موفقیت حذف شد.";
                InfoDiv.Attributes["class"] = " alert alert-success alert-dismissable text-right";
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                string msg = "RegCmd Error:";
                msg += ex.Message;
                throw new Exception(msg);
            }
            finally
            {
                con.Close();
            }
        }
    }
    protected void AddNewItemLinkButton_Click(object sender, EventArgs e)
    {
        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);
        string RegSqlSyntax = "insert into sliderimages (sid,title,situ,href,src,ordernum) values (@sid,@title,N'hide',@href,@src,(select max(ordernum)+1 from sliderimages where sid=@sid))";
        SqlCommand RegCmd = new SqlCommand(RegSqlSyntax, con);
        SqlParameter[] param = new SqlParameter[4];

        param[00] = new SqlParameter("@sid", SqlDbType.NVarChar);
        param[01] = new SqlParameter("@title", SqlDbType.NVarChar);
        param[02] = new SqlParameter("@href", SqlDbType.NVarChar);
        param[03] = new SqlParameter("@src", SqlDbType.NVarChar);

        param[00].Value = SliderSelectorDropDownList.SelectedItem.Value;
        param[01].Value = "اسلاید جدید";
        param[02].Value = "#";
        param[03].Value = SSClass.WebDomainFile + "/images/slides/newdefault.png";


        for (int r = 0; r < param.Length; r++)
        {
            RegCmd.Parameters.Add(param[r]);
        }

        try
        {
            con.Open();
            RegCmd.ExecuteReader();
            SlidesRepeater.DataBind();
            MainSliderRepeater.DataBind();

            InfoDiv.Visible = true;
            InfoLiteral.Text = "اسلاید جدید با موفقیت اضافه شد.";
            InfoDiv.Attributes["class"] = " alert alert-success alert-dismissable text-right";
        }
        catch (System.Data.SqlClient.SqlException ex)
        {
            string msg = "RegCmd Error:";
            msg += ex.Message;
            throw new Exception(msg);
        }
        finally
        {
            con.Close();
        }
    }
    protected void SliderSelectorDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (SliderSelectorDropDownList.SelectedIndex == 0)
        {
            //UploadPhotosButton.Visible = true;
            //AddNewDiv.Visible = true;
            SlideShow.Visible = false;
        }
        else
        {
            //UploadPhotosButton.Visible = false;
            //AddNewDiv.Visible = false;
            SlideShow.Visible = true;
            MainSliderSqlDataSource.SelectCommand = "SELECT  * FROM [sliderimages] WHERE (([SID] = " + SliderSelectorDropDownList.SelectedValue + " ) and situ=N'show') ORDER BY ordernum asc; ";
            MainSliderRepeater.DataBind();
        }

    }

    protected void ShowAllCheckBox_CheckedChanged(object sender, EventArgs e)
    {
        if (ShowAllCheckBox.Checked)
        {
            SqlDataSource1.SelectCommand = "SELECT * FROM [sliderimages] WHERE [SID] = @SID  ORDER BY [ordernum] desc";
            SqlDataSource1.DataBind();
            SlidesRepeater.DataBind();
            MainSliderRepeater.DataBind();

        }
        else
        {
            SqlDataSource1.SelectCommand = "SELECT * FROM [sliderimages] WHERE [SID] = @SID and situ=N'show' or (src=N'~/images/slides/newdefault.png')  ORDER BY [ordernum] desc";
            SqlDataSource1.DataBind();
            SlidesRepeater.DataBind();
            MainSliderRepeater.DataBind();

        }
    }

    protected void SaveSliderCodeButton_Click(object sender, EventArgs e)
    {
        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);
        string RegSqlSyntax = "update slider set slidercode=@slidercode where sid=@sid";

        SqlCommand RegCmd = new SqlCommand(RegSqlSyntax, con);
        SqlParameter[] param = new SqlParameter[2];

        param[00] = new SqlParameter("@sid", SqlDbType.Int);
        param[01] = new SqlParameter("@slidercode", SqlDbType.NVarChar);

        param[00].Value = SliderSelectorDropDownList.SelectedValue;
        param[01].Value = SliderCodeTextArea.InnerHtml;


        for (int i = 0; i < param.Length; i++)
        {
            RegCmd.Parameters.Add(param[i]);
        }

        try
        {

            con.Open();
            RegCmd.ExecuteReader();
            InfoDiv.Visible = true;
            InfoLiteral.Text = "کد اسلایدر با موفقیت ذخیره شد.";
            InfoDiv.Attributes["class"] = " alert alert-success alert-dismissable text-right";
        }
        catch (System.Data.SqlClient.SqlException ex)
        {
            string msg = "RegCmd Error:";
            msg += ex.Message;
            throw new Exception(msg);
        }
        finally
        {
            con.Close();
        }
    }

    protected void AddNewSliderButton_Click(object sender, EventArgs e)
    {
        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);
        string RegSqlSyntax = "INSERT INTO [slider] (situ,title,slidercode) VALUES (N'ok',@title,N'')";

        SqlCommand RegCmd = new SqlCommand(RegSqlSyntax, con);
        SqlParameter[] param = new SqlParameter[1];

        param[00] = new SqlParameter("@title", SqlDbType.NVarChar);

        param[00].Value = NewSliderTextBox.Text;


        for (int i = 0; i < param.Length; i++)
        {
            RegCmd.Parameters.Add(param[i]);
        }

        try
        {

            con.Open();
            RegCmd.ExecuteReader();
            NewSliderTextBox.Text = "";
            ListOfCoursesSqlDataSource.DataBind();
            SliderSelectorDropDownList.DataBind();

            InfoDiv.Visible = true;
            InfoLiteral.Text = "اسلایدر جدید با موفقیت افزوده شد.";
            InfoDiv.Attributes["class"] = " alert alert-success alert-dismissable text-right";
        }
        catch (SqlException ex)
        {
            string msg = "RegCmd Error:";
            msg += ex.Message;
            throw new Exception(msg);
        }
        finally
        {
            con.Close();
        }
    }
    protected void SliderSelectorDropDownList_DataBound(object sender, EventArgs e)
    {
        SliderSelectorDropDownList.Items.Insert(0, new ListItem("اسلایدر مورد نظر خود را انتخاب کنید...", "0"));
    }

    protected void DeleteLinkButton_Click(object sender, EventArgs e)
    {
        DataLayer dl = new DataLayer();
        string CS = dl.dataLayerConnectionString;
        SqlConnection con = new SqlConnection(CS);
        string RegSqlSyntax = "Update [slider] set isdeleted=1 where SID=@SID";

        SqlCommand RegCmd = new SqlCommand(RegSqlSyntax, con);
        SqlParameter[] param = new SqlParameter[1];

        param[00] = new SqlParameter("@SID", SqlDbType.NVarChar);

        param[00].Value = SliderSelectorDropDownList.SelectedValue;


        for (int i = 0; i < param.Length; i++)
        {
            RegCmd.Parameters.Add(param[i]);
        }

        try
        {

            con.Open();
            RegCmd.ExecuteReader();
            SliderSelectorDropDownList.DataBind();
            InfoDiv.Visible = true;
            InfoLiteral.Text = "اسلایدر با موفقیت حذف شد.";
            InfoDiv.Attributes["class"] = " alert alert-success alert-dismissable text-right";
            SliderSelectorDropDownList.SelectedIndex = 0;
            SliderSelectorDropDownList_SelectedIndexChanged(null, null);
        }
        catch (SqlException ex)
        {
            string msg = "RegCmd Error:";
            msg += ex.Message;
            throw new Exception(msg);
        }
        finally
        {
            con.Close();
        }
    }
}