﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.SqlClient;

/// <summary>
/// Summary description for DataLayer
/// </summary>
public class DataLayer
{
    public string dataLayerConnectionString
    {
        get { return connectionString; }
        set { connectionString = value; }
    }

    public static string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DrB_DBConnectionString"].ConnectionString;
    //private string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TehrantrainerConnectionString"].ConnectionString;


    private SqlConnection connection;
    private SqlCommand command;
    private SqlDataAdapter dataAdapter;


    public DataLayer()
    {
        connection = new SqlConnection(connectionString);
        command = new SqlCommand("", connection);
        dataAdapter = new SqlDataAdapter(command);
    }



    public void Connect()
    {
        try
        {
            connection.Open();
        }
        catch (Exception exp)
        {
            throw exp;
        }
    }

    public void Disconnect()
    {
        try
        {
            connection.Close();
        }
        catch (Exception exp)
        {
            throw exp;
        }
    }
}